package com.pongo.health.Notifications;

import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.pongo.health.Session;
import com.pongo.health.ui.ActivityHome;
import com.pongo.health.ui.FeedbackActivity;
import com.pongo.health.ui.vet.ActivityChatWaiting;
import com.pongo.health.ui.vet.ActivityTechnician;
import com.pongo.health.ui.vet.ActivityVetDashboard;
import com.pongo.health.ui.vet.VideocallActivity;
import com.pongo.health.utils.Constants;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    private static final String TAGs = "MyFirebaseMessagingService";
    private Session mSession;
    private NotificationUtils notificationUtils;

    public void onMessageReceived(RemoteMessage remoteMessage) {

        this.mSession = new Session(this);
        if (remoteMessage != null) {
          /*  if (remoteMessage.getNotification() != null) {

                handleNotification(remoteMessage.getNotification().getBody());
            }*/
            if (remoteMessage.getData().size() > 0) {

                try {
//                    if (!this.mSession.getNotification()) {
//                        new JSONObject(remoteMessage.getData().toString())
//                        JSONObject jsonObject = new JSONObject(remoteMessage.getData());
//                    handleEnglishDataMessage(new JSONObject(remoteMessage.getData().toString()));
                    Map<String, String> data = remoteMessage.getData();
                    handleEnglishDataMessage(data);
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void onNewToken(String str) {
        this.mSession = new Session(this);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (task.isSuccessful()) {
                    if (VERSION.SDK_INT >= 19) {
                        Object result = task.getResult();
                        result.getClass();
                        String token = ((InstanceIdResult) result).getToken();
                        MyFirebaseMessagingService.this.storeRegIdInPref(token);
                        MyFirebaseMessagingService.this.sendRegistrationToServer(token);
                    }
                    return;
                }
                Log.w(MyFirebaseMessagingService.TAG, "getInstanceId failed", task.getException());
            }
        });
    }

    private void storeRegIdInPref(String str) {
        this.mSession.setToken(str);
    }

    private void sendRegistrationToServer(String str) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("sendRegistrationToServer: ");
        stringBuilder.append(str);
        Log.e(TAG, stringBuilder.toString());
        if (!TextUtils.isEmpty(str)) {
//            sendToServerApi(str);
        }
    }

    private void handleNotification(String str) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            Intent intent = new Intent(getApplicationContext(), ActivityHome.class);
            intent.putExtra("message", str);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            new NotificationUtils(getApplicationContext()).playNotificationSound();
        }
    }

    private void handleEnglishDataMessage(Map<String, String> jSONObject) {
        StringBuilder stringBuilder;

        try {
//            jSONObject = jSONObject.getJSONObject("data");
            if (jSONObject.containsKey("completemark")) {
                handleFeedbackCompleteChat(jSONObject);
            } else if (jSONObject.containsKey("chattype")) {
                handlefreeUserChat(jSONObject);
            } else if (jSONObject.containsKey("chatroom")) {
                handleVideoCall(jSONObject);
            } else if (jSONObject.containsKey("appointment")) {
                handleAppointmentNoti(jSONObject);
            } else {
                if (jSONObject.containsKey("userid")) {
                    if (jSONObject.containsKey("type")) {
                        String userId = jSONObject.get("userid");
                        if (userId != null && !userId.equalsIgnoreCase(Constants.chatingWith)) {
                            String title = jSONObject.get("name");
                            String message = jSONObject.get("message");
                            //                    String url = jSONObject.getString("url");
                            String image = jSONObject.get("image");
                            //                String image = "";
                            String userType = mSession.getuser_type();
                            if (!TextUtils.isEmpty(title)) {
                                Intent intent;
                                if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                                    if (TextUtils.isEmpty(userType)) {
                                        intent = new Intent(getApplicationContext(), ActivityHome.class);
                                    } else if (userType.equalsIgnoreCase("technician")) {
                                        intent = new Intent(getApplicationContext(), ActivityTechnician.class);

                                    } else {
                                        intent = new Intent(getApplicationContext(), ActivityVetDashboard.class);

                                    }
                                    intent.putExtra("title", title);
                                    intent.putExtra("message", message);
                                    if (TextUtils.isEmpty(image)) {
                                        showNotificationMessage(getApplicationContext(), message, title, intent, "");
                                        return;
                                    }
                                    showNotificationMessageWithBigImage(getApplicationContext(), message, title, intent, "", image);
                                    return;
                                }
                                if (TextUtils.isEmpty(userType)) {
                                    intent = new Intent(getApplicationContext(), ActivityHome.class);
                                } else if (userType.equalsIgnoreCase("technician")) {
                                    intent = new Intent(getApplicationContext(), ActivityTechnician.class);

                                } else {
                                    intent = new Intent(getApplicationContext(), ActivityVetDashboard.class);

                                }
                                intent.putExtra("title", title);
                                intent.putExtra("message", message);
                                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                                if (TextUtils.isEmpty(image)) {
                                    showNotificationMessage(getApplicationContext(), title, message, intent, "");
                                    return;
                                }
                                showNotificationMessageWithBigImage(getApplicationContext(), message, title, intent, "", image);
                            }
                        }
                    } else {
                        String title = jSONObject.get("title");
                        String userId = jSONObject.get("userid");
                        String message = jSONObject.get("message");
                        String order_id = jSONObject.get("order_id");
                        String image = "";
                        String signedUser = this.mSession.getuser_id();
                        if (!TextUtils.isEmpty(signedUser) && signedUser.equalsIgnoreCase(userId)) {
                            Intent intent;
                            if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                                intent = new Intent(getApplicationContext(), ActivityHome.class);
                                intent.putExtra("order_id", order_id);
                                intent.putExtra(message, message);
                                if (TextUtils.isEmpty(image)) {
                                    showNotificationMessage(getApplicationContext(), message, title, intent, userId);
                                    return;
                                }
                                showNotificationMessageWithBigImage(getApplicationContext(), message, title, intent, userId, image);
                                return;
                            }
                            intent = new Intent(getApplicationContext(), ActivityHome.class);
                            intent.putExtra("order_id", order_id);
                            intent.putExtra(message, message);
                            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                            if (TextUtils.isEmpty(image)) {
                                showNotificationMessage(getApplicationContext(), message, title, intent, userId);
                                return;
                            }
                            showNotificationMessageWithBigImage(getApplicationContext(), message, title, intent, userId, image);
                        }
                    }
                } else {
                    String title = jSONObject.get("title");
                    String message = jSONObject.get("message");
                    String url = jSONObject.get("url");
                    String image = jSONObject.get("logo");
//                String image = "";
                    if (!TextUtils.isEmpty(title)) {
                        Intent intent;
                        if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                            intent = new Intent(getApplicationContext(), ActivityHome.class);
                            intent.putExtra("title", title);
                            intent.putExtra("message", message);
                            if (TextUtils.isEmpty(image)) {
                                showNotificationMessage(getApplicationContext(), message, title, intent, "");
                                return;
                            }
                            showNotificationMessageWithBigImage(getApplicationContext(), message, title, intent, "", image);
                            return;
                        }
                        intent = new Intent(getApplicationContext(), ActivityHome.class);
                        intent.putExtra("title", title);
                        intent.putExtra("message", message);
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                        if (TextUtils.isEmpty(image)) {
                            showNotificationMessage(getApplicationContext(), title, message, intent, "");
                            return;
                        }
                        showNotificationMessageWithBigImage(getApplicationContext(), message, title, intent, "", image);
                    }
                }
            }
        } catch (Exception e) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Json Exception: ");
            stringBuilder.append(e.getMessage());
//            Log.e(str3, stringBuilder.toString());
        }
    }

    private void handlefreeUserChat(Map<String, String> jSONObject) {

        String loggedUser = mSession.getuser_type();
        String userType = jSONObject.get("usertype");
        if (userType != null && userType.equalsIgnoreCase(loggedUser)) {
            String title = jSONObject.get("title");
            String message = jSONObject.get("message");
            String url = jSONObject.get("url");
            String logo = jSONObject.get("logo");

            if (!TextUtils.isEmpty(title)) {
                Intent intent;
                if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {

                    intent = new Intent(getApplicationContext(), ActivityChatWaiting.class);

                    intent.putExtra("title", title);
                    intent.putExtra("message", message);
                    if (TextUtils.isEmpty(logo)) {
                        showNotificationMessage(getApplicationContext(), message, title, intent, "");
                        return;
                    }
                    showNotificationMessageWithBigImage(getApplicationContext(), message, title, intent, "", logo);
                    return;
                }

                intent = new Intent(getApplicationContext(), ActivityChatWaiting.class);
                intent.putExtra("title", title);
                intent.putExtra("message", message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                if (TextUtils.isEmpty(logo)) {
                    showNotificationMessage(getApplicationContext(), title, message, intent, "");
                    return;
                }
                showNotificationMessageWithBigImage(getApplicationContext(), message, title, intent, "", logo);

            }
        }
    }

    private void handleFeedbackCompleteChat(Map<String, String> jSONObject) {

        String userId = mSession.getuser_id();
        String userType = mSession.getuser_type();
        if (!TextUtils.isEmpty(userId) && TextUtils.isEmpty(userType)) {
            String title = jSONObject.get("title");
            String message = jSONObject.get("message");
            String url = jSONObject.get("url");
            String logo = jSONObject.get("logo");
            String petUserId = jSONObject.get("user_id");
            String techId = jSONObject.get("tech_id");
            String petName = jSONObject.get("pet_name");
            String chatid = jSONObject.get("chatid");

            if (!TextUtils.isEmpty(title)) {
                Intent intent;
                if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {

                    intent = new Intent(getApplicationContext(), FeedbackActivity.class);

                    intent.putExtra("title", title);
                    intent.putExtra("message", message);
                    intent.putExtra("logo", logo);
                    intent.putExtra("user_id", petUserId);
                    intent.putExtra("tech_id", techId);
                    intent.putExtra("pet_name", petName);
                    intent.putExtra("chatid", chatid);
                    if (TextUtils.isEmpty(logo)) {
                        showNotificationMessage(getApplicationContext(), message, title, intent, "");
                        return;
                    }
                    showNotificationMessageWithBigImage(getApplicationContext(), message, title, intent, "", logo);
                    return;
                }

                intent = new Intent(getApplicationContext(), FeedbackActivity.class);
                intent.putExtra("title", title);
                intent.putExtra("message", message);
                intent.putExtra("logo", logo);
                intent.putExtra("user_id", petUserId);
                intent.putExtra("tech_id", techId);
                intent.putExtra("pet_name", petName);
                intent.putExtra("chatid", chatid);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                if (TextUtils.isEmpty(logo)) {
                    showNotificationMessage(getApplicationContext(), title, message, intent, "");
                    return;
                }
                showNotificationMessageWithBigImage(getApplicationContext(), message, title, intent, "", logo);

            }
        }
    }

    private void handleVideoCall(Map<String, String> jSONObject) {
        String userId = mSession.getuser_id();
        String type = mSession.getuser_type();
        if (!TextUtils.isEmpty(userId) && type.equalsIgnoreCase("vet")) {
            String title = jSONObject.get("title");
            String message = jSONObject.get("message");
            String chatroom = jSONObject.get("chatroom");
            String url = jSONObject.get("url");
            String image = jSONObject.get("logo");
//                String image = "";
            if (!TextUtils.isEmpty(title)) {
                Intent intent;
                if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                    intent = new Intent(getApplicationContext(), ActivityVetDashboard.class);
                    intent.putExtra("title", title);
                    intent.putExtra("message", message);
                    intent.putExtra("chatroom", chatroom);
                    if (TextUtils.isEmpty(image)) {
                        showNotificationMessage(getApplicationContext(), message, title, intent, "");
                        return;
                    }
                    showNotificationMessageWithBigImage(getApplicationContext(), message, title, intent, "", image);
                    return;
                }
                intent = new Intent(getApplicationContext(), ActivityVetDashboard.class);
                intent.putExtra("title", title);
                intent.putExtra("message", message);
                intent.putExtra("chatroom", chatroom);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                if (TextUtils.isEmpty(image)) {
                    showNotificationMessage(getApplicationContext(), title, message, intent, "");
                    return;
                }
                showNotificationMessageWithBigImage(getApplicationContext(), message, title, intent, "", image);
            }
        }

    }

    private void handleAppointmentNoti(Map<String, String> jSONObject) {
        String userId = mSession.getuser_id();
        String type = mSession.getuser_type();
        if (!TextUtils.isEmpty(userId)) {
            Intent intent = null;
            if (type.equalsIgnoreCase("vet")) {
                intent = new Intent(getApplicationContext(), ActivityVetDashboard.class);
            } else {
                intent = new Intent(getApplicationContext(), ActivityHome.class);
            }

            String title = jSONObject.get("title");
            String message = jSONObject.get("message");
            String chatroom = jSONObject.get("chatroom");
            String url = jSONObject.get("url");
            String image = jSONObject.get("logo");
//                String image = "";
            if (!TextUtils.isEmpty(title)) {
                intent.putExtra("title", title);
                intent.putExtra("message", message);
                intent.putExtra("chatroom", chatroom);
                if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                    if (TextUtils.isEmpty(image)) {
                        showNotificationMessage(getApplicationContext(), message, title, intent, "");
                        return;
                    }
                    showNotificationMessageWithBigImage(getApplicationContext(), message, title, intent, "", image);
                    return;
                }
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                if (TextUtils.isEmpty(image)) {
                    showNotificationMessage(getApplicationContext(), title, message, intent, "");
                    return;
                }
                showNotificationMessageWithBigImage(getApplicationContext(), message, title, intent, "", image);
            }

        }
    }

    private void showNotificationMessage(Context context, String message, String title, Intent intent, String id) {
        this.notificationUtils = new NotificationUtils(context);
//        intent.setFlags(268468224);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        this.notificationUtils.showNotificationMessage(message, title, intent, id);
    }

    private void showNotificationMessageWithBigImage(Context context, String message, String title, Intent intent, String userId, String image) {
        this.notificationUtils = new NotificationUtils(context);
//        intent.setFlags(268468224);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        this.notificationUtils.showNotificationMessage(message, title, intent, userId, image);
    }

  /*  private void sendToServerApi(String token) {
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).sendToken(token).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("token");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    mSession.setPushTokenSent(true);
                                } else {
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                Log.e("cate", th.toString());
            }
        });
    }*/
}
