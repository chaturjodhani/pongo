package com.pongo.health.Notifications;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.Patterns;

import androidx.core.app.NotificationCompat.Builder;
import androidx.core.app.NotificationCompat.InboxStyle;


import com.pongo.health.R;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NotificationUtils {
    private static String TAG = "NotificationUtils";
    private Context mContext;

    public NotificationUtils(Context context) {
        this.mContext = context;
    }

    public void showNotificationMessage(String msg, String title, Intent intent) {
        showNotificationMessage(msg, title, intent, null);
    }

    public void showNotificationMessage(String msg, String title, Intent intent, String id) {
        if (!TextUtils.isEmpty(msg)) {
//            intent.setFlags(PendingIntent.FLAG_CANCEL_CURRENT);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            PendingIntent activity = PendingIntent.getActivity(this.mContext, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            Builder builder = new Builder(this.mContext, "pongo");
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("android.resource://");
            stringBuilder.append(this.mContext.getPackageName());
            stringBuilder.append("/raw/notification");
            Uri parse = Uri.parse(stringBuilder.toString());
            if (TextUtils.isEmpty(id)) {
//                showSmallNotification(builder, R.mipmap.logo, msg, title, activity, parse);
                showDiscountNotification(builder, R.mipmap.pongo_logo, msg, title, activity, parse);
            } else {
                showSmallNotification(builder, R.mipmap.pongo_logo, msg, title, activity, parse);
            }
        }
    }


    public void showNotificationMessage(String msg, String title, Intent intent, String id, String image) {
        if (!TextUtils.isEmpty(msg)) {
//            intent.setFlags(PendingIntent.FLAG_CANCEL_CURRENT);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            PendingIntent activity = PendingIntent.getActivity(this.mContext, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            Builder builder = new Builder(this.mContext, "pongo");
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("android.resource://");
            stringBuilder.append(this.mContext.getPackageName());
            stringBuilder.append("/raw/notification");
            Uri parse = Uri.parse(stringBuilder.toString());
            if (TextUtils.isEmpty(id)) {
//                showSmallNotification(builder, R.mipmap.logo, msg, title, activity, parse);
                if (image != null && image.length() > 4 && Patterns.WEB_URL.matcher(image).matches()) {
                    Bitmap bitmapFromURL = getBitmapFromURL(image);
                    if (bitmapFromURL != null) {
                        showBigNotification(bitmapFromURL, builder, R.mipmap.pongo_logo, msg, title, activity, parse);
                    } else {
                        showDiscountNotification(builder, R.mipmap.pongo_logo, msg, title, activity, parse);
                    }
                } else {
                    showDiscountNotification(builder, R.mipmap.pongo_logo, msg, title, activity, parse);

                }
            } else if (image != null && image.length() > 4 && Patterns.WEB_URL.matcher(image).matches()) {
                Bitmap bitmapFromURL = getBitmapFromURL(image);
                if (bitmapFromURL != null) {
                    showBigNotification(bitmapFromURL, builder, R.mipmap.pongo_logo, msg, title, activity, parse);
                } else {
                    showSmallNotification(builder, R.mipmap.pongo_logo, msg, title, activity, parse);
                }
            } else {
                showSmallNotification(builder, R.mipmap.pongo_logo, msg, title, activity, parse);

            }
        }
    }


    private void showDiscountNotification(Builder builder, int logo, String msg, String title, PendingIntent activity, Uri parse) {
        InboxStyle inboxStyle = new InboxStyle();
        inboxStyle.addLine(title);
        Notification build = builder.setAutoCancel(true).setContentIntent(activity).setSound(parse).setStyle(inboxStyle).setSmallIcon(getNotificationIcon(builder)).setContentTitle(title).setContentText(msg).build();
        NotificationManager notificationManager = (NotificationManager) this.mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (VERSION.SDK_INT >= 26) {
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(new NotificationChannel("pongo", "PongoHealth", NotificationManager.IMPORTANCE_HIGH));
            }
        }
        playNotificationSound();
        if (notificationManager != null) {
            notificationManager.notify((int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE), build);
        }
    }

    private void showSmallNotification(Builder builder, int i, String msg, String title, PendingIntent pendingIntent, Uri uri) {
        InboxStyle inboxStyle = new InboxStyle();
        inboxStyle.addLine(msg);
        Notification build = builder.setAutoCancel(true).setContentIntent(pendingIntent).setSound(uri).setStyle(inboxStyle).setSmallIcon(getNotificationIcon(builder)).setContentText(msg).build();
        NotificationManager notificationManager = (NotificationManager) this.mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (VERSION.SDK_INT >= 26) {
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(new NotificationChannel("pongo", "PongoHealth", NotificationManager.IMPORTANCE_HIGH));
            }
        }
        playNotificationSound();
        if (notificationManager != null) {
            notificationManager.notify((int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE), build);
        }
    }

    private void showBigNotification(Bitmap bitmap, Builder builder, int i, String msg, String title, PendingIntent pendingIntent, Uri uri) {
//        BigPictureStyle bigPictureStyle = new BigPictureStyle();
//        bigPictureStyle.setSummaryText(Html.fromHtml(str2).toString());
//        bigPictureStyle.bigPicture(bitmap);
        builder.setLargeIcon(bitmap);
        Notification build = builder.setAutoCancel(true).setContentIntent(pendingIntent).setSound(uri).setSmallIcon(getNotificationIcon(builder)).setContentTitle(title).setContentText(msg).build();
        NotificationManager notificationManager = (NotificationManager) this.mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (VERSION.SDK_INT >= 26) {
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(new NotificationChannel("pongo", "PongoHealth", NotificationManager.IMPORTANCE_HIGH));
            }
        }
        playNotificationSound();
        if (notificationManager != null) {
            notificationManager.notify((int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE), build);
        }
    }

    public Bitmap getBitmapFromURL(String str) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            return BitmapFactory.decodeStream(httpURLConnection.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void playNotificationSound() {
        try {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("android.resource://");
            stringBuilder.append(this.mContext.getPackageName());
            stringBuilder.append("/raw/notification");
            RingtoneManager.getRingtone(this.mContext, Uri.parse(stringBuilder.toString())).play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* public static boolean isAppIsInBackground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        boolean z = true;
        if (VERSION.SDK_INT > 20) {
            for (RunningAppProcessInfo runningAppProcessInfo : activityManager.getRunningAppProcesses()) {
                if (runningAppProcessInfo.importance == 100) {
                    boolean z2 = z;
                    for (String equals : runningAppProcessInfo.pkgList) {
                        if (equals.equals(context.getPackageName())) {
                            z2 = false;
                        }
                    }
                    z = z2;
                }
            }
            return z;
        } else if (((RunningTaskInfo) activityManager.getRunningTasks(1).get(0)).topActivity.getPackageName().equals(context.getPackageName())) {
            return false;
        } else {
            return true;
        }
    }*/

    /**
     * Method checks if the app is in background or not
     */
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = null;
            if (VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                componentInfo = taskInfo.get(0).topActivity;
            }
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    public static void clearNotifications(Context context) {
        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).cancelAll();
    }

    public static long getTimeMilliSec(String str) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private int getNotificationIcon(Builder builder) {
        if (VERSION.SDK_INT >= 21) {
        }
        return R.mipmap.pongo_logo;
    }
}
