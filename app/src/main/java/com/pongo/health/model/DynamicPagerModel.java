package com.pongo.health.model;

public class DynamicPagerModel {
    String header1;
    String header2;
    String images;

    public DynamicPagerModel(String header1, String header2, String images) {
        this.header1 = header1;
        this.header2 = header2;
        this.images = images;
    }

    public String getHeader1() {
        return header1;
    }

    public String getHeader2() {
        return header2;
    }

    public String getImages() {
        return images;
    }
}
