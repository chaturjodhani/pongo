package com.pongo.health.model;

import java.util.List;

public class OtpModel {

    /**
     * status : success
     * otp : 1962
     * statelist : [{"id":"5","state":"State3"},{"id":"4","state":"State2"}]
     */

    private String status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;
    private int otp;
    private List<StatelistBean> statelist;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getOtp() {
        return otp;
    }

    public void setOtp(int otp) {
        this.otp = otp;
    }

    public List<StatelistBean> getStatelist() {
        return statelist;
    }

    public void setStatelist(List<StatelistBean> statelist) {
        this.statelist = statelist;
    }

    public static class StatelistBean {
        /**
         * id : 5
         * state : State3
         */

        private String id;
        private String state;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }
}
