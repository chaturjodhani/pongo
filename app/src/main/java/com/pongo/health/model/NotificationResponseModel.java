package com.pongo.health.model;

import java.util.List;

public class NotificationResponseModel {

    /**
     * status : success
     * notify : [{"id":"Kkkkk","description":"Kkkk","image":"http://digittrix.com/staging/pongo/assets/images/push/pongo_logo.png"},{"id":"Boogie Woogie","description":"Let Me Type A Paragraph | Let Me Type A Paragraph |let Me Type A Paragraph | Let Me Type A Paragraph |let Me Type A Paragraph | Let Me Type A Paragraph | Let Me Type A Paragraph |let Me Type A Paragraph | Let Me Type A Paragraph |let Me Type A Paragraphle","image":"http://digittrix.com/staging/pongo/assets/images/push/athlete2.png"},{"id":"Ksksks","description":"Sksksksk S\r\nS\r\nD\r\nSd\r\nS\r\nDs\r\nD\r\nSd\r\nSd\r\nS\r\nDsds","image":"http://digittrix.com/staging/pongo/assets/images/push/employee.png"},{"id":"Roberts Discount","description":"Please Ensure You Contact Us At 33.21.34","image":"http://digittrix.com/staging/pongo/assets/images/push/athlete1.png"}]
     */

    private String status;
    private List<NotifyBean> notify;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<NotifyBean> getNotify() {
        return notify;
    }

    public void setNotify(List<NotifyBean> notify) {
        this.notify = notify;
    }

    public static class NotifyBean {
        /**
         * id : Kkkkk
         * description : Kkkk
         * image : http://digittrix.com/staging/pongo/assets/images/push/pongo_logo.png
         */

        private String id;
        private String description;
        private String image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
