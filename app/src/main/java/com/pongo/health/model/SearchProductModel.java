package com.pongo.health.model;

import java.util.List;

public class SearchProductModel {

    /**
     * status : success
     * productdata : [{"id":"3","name":"Dog Treats","description":"This Product \r\n+ Skdksds \r\n+ Dsds Sds \r\n+ Hello","orignalprice":"50.00","discountprice":"0.00","product_type":false,"product_image":"http://digittrix.com/staging/pongo/assets/products/dachshund-dog-wallpaper-background.jpg"}]
     */

    private String status;
    private List<ProductdataBean> productdata;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ProductdataBean> getProductdata() {
        return productdata;
    }

    public void setProductdata(List<ProductdataBean> productdata) {
        this.productdata = productdata;
    }

    public static class ProductdataBean {
        /**
         * id : 3
         * name : Dog Treats
         * description : This Product
         * + Skdksds
         * + Dsds Sds
         * + Hello
         * orignalprice : 50.00
         * discountprice : 0.00
         * product_type : false
         * product_image : http://digittrix.com/staging/pongo/assets/products/dachshund-dog-wallpaper-background.jpg
         */

        private String id;
        private String name;
        private String description;
        private String orignalprice;
        private String discountprice;
        private boolean product_type;
        private String product_image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getOrignalprice() {
            return orignalprice;
        }

        public void setOrignalprice(String orignalprice) {
            this.orignalprice = orignalprice;
        }

        public String getDiscountprice() {
            return discountprice;
        }

        public void setDiscountprice(String discountprice) {
            this.discountprice = discountprice;
        }

        public boolean isProduct_type() {
            return product_type;
        }

        public void setProduct_type(boolean product_type) {
            this.product_type = product_type;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }
    }
}
