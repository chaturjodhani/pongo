package com.pongo.health.model;

import java.util.List;

public class CheckoutResponseModel {

    /**
     * status : success
     * checkout : [{"cartid":"212","product_id":"15","product_name":"Original Jerky Treats - Beef Lung","product_type":false,"autorenew":"","quantity":"1","strength":"","isautorenew":"no","price":"13"},{"cartid":"204","product_id":"14","product_name":"Rawbble® Canned Wet Dog Food - Lamb Recipe","product_type":false,"autorenew":"","quantity":"1","strength":"","isautorenew":"no","price":"4"},{"cartid":"203","product_id":"21","product_name":"Heartgard Plus Chewable Tablets For Dogs, 51-100 Lbs (brown Box)","product_type":true,"autorenew":"yes","quantity":"2","strength":"6 Treatments","isautorenew":"yes","price":"99"}]
     * totalprice : 116.00
     * cardlist : [{"cardnumber":"xxxx-xxxx-xxxx-4242","exp_month":10,"exp_year":2022,"card_type":"Visa","customer":"cus_HetrkfKkS6uBM7","card_id":"card_1H7xITCCvFNIowRP1u7zHPzY"},{"cardnumber":"xxxx-xxxx-xxxx-1111","exp_month":10,"exp_year":2022,"card_type":"Visa","customer":"cus_HetrkfKkS6uBM7","card_id":"card_1H5a40CCvFNIowRP6BIILLDb"}]
     * shipping : 5.99
     * finalprice : 121.99
     * couponcode : health2020
     * discount : 1.90
     */

    private String status;
    private String totalprice;
    private String shipping;
    private String couponcode;
    private String discount;
    private double finalprice;
    private List<CheckoutBean> checkout;
    private List<CardlistBean> cardlist;

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getCouponcode() {
        return couponcode;
    }

    public void setCouponcode(String couponcode) {
        this.couponcode = couponcode;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(String totalprice) {
        this.totalprice = totalprice;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public double getFinalprice() {
        return finalprice;
    }

    public void setFinalprice(double finalprice) {
        this.finalprice = finalprice;
    }

    public List<CheckoutBean> getCheckout() {
        return checkout;
    }

    public void setCheckout(List<CheckoutBean> checkout) {
        this.checkout = checkout;
    }

    public List<CardlistBean> getCardlist() {
        return cardlist;
    }

    public void setCardlist(List<CardlistBean> cardlist) {
        this.cardlist = cardlist;
    }

    public static class CheckoutBean {
        /**
         * cartid : 212
         * product_id : 15
         * product_name : Original Jerky Treats - Beef Lung
         * product_type : false
         * autorenew :
         * quantity : 1
         * strength :
         * isautorenew : no
         * price : 13
         */

        private String cartid;
        private String product_id;
        private String product_name;
        private boolean product_type;
        private String autorenew;
        private String quantity;
        private String strength;
        private String isautorenew;
        private String price;

        public String getCartid() {
            return cartid;
        }

        public void setCartid(String cartid) {
            this.cartid = cartid;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public boolean isProduct_type() {
            return product_type;
        }

        public void setProduct_type(boolean product_type) {
            this.product_type = product_type;
        }

        public String getAutorenew() {
            return autorenew;
        }

        public void setAutorenew(String autorenew) {
            this.autorenew = autorenew;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getStrength() {
            return strength;
        }

        public void setStrength(String strength) {
            this.strength = strength;
        }

        public String getIsautorenew() {
            return isautorenew;
        }

        public void setIsautorenew(String isautorenew) {
            this.isautorenew = isautorenew;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }
    }

    public static class CardlistBean {
        /**
         * cardnumber : xxxx-xxxx-xxxx-4242
         * exp_month : 10
         * exp_year : 2022
         * card_type : Visa
         * customer : cus_HetrkfKkS6uBM7
         * card_id : card_1H7xITCCvFNIowRP1u7zHPzY
         */

        private String cardnumber;
        private int exp_month;
        private int exp_year;
        private String card_type;
        private String customer;
        private String card_id;

        public String getCardnumber() {
            return cardnumber;
        }

        public void setCardnumber(String cardnumber) {
            this.cardnumber = cardnumber;
        }

        public int getExp_month() {
            return exp_month;
        }

        public void setExp_month(int exp_month) {
            this.exp_month = exp_month;
        }

        public int getExp_year() {
            return exp_year;
        }

        public void setExp_year(int exp_year) {
            this.exp_year = exp_year;
        }

        public String getCard_type() {
            return card_type;
        }

        public void setCard_type(String card_type) {
            this.card_type = card_type;
        }

        public String getCustomer() {
            return customer;
        }

        public void setCustomer(String customer) {
            this.customer = customer;
        }

        public String getCard_id() {
            return card_id;
        }

        public void setCard_id(String card_id) {
            this.card_id = card_id;
        }
    }
}
