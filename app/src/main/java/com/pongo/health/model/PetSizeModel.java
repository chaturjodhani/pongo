package com.pongo.health.model;

public class PetSizeModel {

    private  int  image;
    private  String sizeType;
    private String Length;

    public PetSizeModel(int image, String sizeType, String length) {
        this.image = image;
        this.sizeType = sizeType;
        Length = length;
    }

    public int getImage() {
        return image;
    }

    public String getSizeType() {
        return sizeType;
    }

    public String getLength() {
        return Length;
    }
}
