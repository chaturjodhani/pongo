package com.pongo.health.model;

import java.util.List;

public class DoctorWaitingListModel {

    /**
     * status : success
     * wating : [{"chatid":"25","pastdoctor":[{"id":"135","name":"Vet Testing","userimg":"http://digittrix.com/staging/pongo/assets/images/avtar.png","problems":"Behavioral,Heartworm"}],"currentissue":["Behavioral"],"attachments":["http://digittrix.com/staging/pongo/assets/images/patient/Paper_281.pdf","http://digittrix.com/staging/pongo/assets/images/patient/IMG-20200701-WA0028.jpg"],"chatroomname":"chat_121","pet_id":"121","user_id":"144","pet_name":"rocky","pet_type":"dog","petimage":"http://digittrix.com/staging/pongo/assets/images/petimage/JPEG_20200720_105521_1915131527014351802.jpg","breed":"Pomeranian","status":"waiting","pet_age":"5/2/2020"}]
     */

    private String status;
    private List<WatingBean> wating;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<WatingBean> getWating() {
        return wating;
    }

    public void setWating(List<WatingBean> wating) {
        this.wating = wating;
    }

    public static class WatingBean {
        /**
         * chatid : 25
         * pastdoctor : [{"id":"135","name":"Vet Testing","userimg":"http://digittrix.com/staging/pongo/assets/images/avtar.png","problems":"Behavioral,Heartworm"}]
         * currentissue : ["Behavioral"]
         * attachments : ["http://digittrix.com/staging/pongo/assets/images/patient/Paper_281.pdf","http://digittrix.com/staging/pongo/assets/images/patient/IMG-20200701-WA0028.jpg"]
         * chatroomname : chat_121
         * pet_id : 121
         * user_id : 144
         * pet_name : rocky
         * pet_type : dog
         * petimage : http://digittrix.com/staging/pongo/assets/images/petimage/JPEG_20200720_105521_1915131527014351802.jpg
         * breed : Pomeranian
         * status : waiting
         * pet_age : 5/2/2020
         * medicalrecords :  ["http://digittrix.com/staging/pongo/assets/images/patient/Paper_281.pdf","http://digittrix.com/staging/pongo/assets/images/patient/IMG-20200701-WA0028.jpg"]
         */

        private String chatid;
        private String chatroomname;
        private String pet_id;
        private String user_id;
        private String pet_name;
        private String pet_type;
        private String petimage;
        private String breed;
        private String status;
        private String pet_age;
        private String booking_date;
        private String booking_time;
        private String currentproblem;
        private List<PastdoctorBean> pastdoctor;
        private List<String> currentissue;
        private List<String> attachments;
        private List<String> medicalrecords;

        public List<String> getMedicalrecords() {
            return medicalrecords;
        }

        public void setMedicalrecords(List<String> medicalrecords) {
            this.medicalrecords = medicalrecords;
        }


        public String getBooking_date() {
            return booking_date;
        }

        public void setBooking_date(String booking_date) {
            this.booking_date = booking_date;
        }

        public String getBooking_time() {
            return booking_time;
        }

        public void setBooking_time(String booking_time) {
            this.booking_time = booking_time;
        }

        public String getCurrentproblem() {
            return currentproblem;
        }

        public void setCurrentproblem(String currentproblem) {
            this.currentproblem = currentproblem;
        }


        public String getChatid() {
            return chatid;
        }

        public void setChatid(String chatid) {
            this.chatid = chatid;
        }

        public String getChatroomname() {
            return chatroomname;
        }

        public void setChatroomname(String chatroomname) {
            this.chatroomname = chatroomname;
        }

        public String getPet_id() {
            return pet_id;
        }

        public void setPet_id(String pet_id) {
            this.pet_id = pet_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getPet_name() {
            return pet_name;
        }

        public void setPet_name(String pet_name) {
            this.pet_name = pet_name;
        }

        public String getPet_type() {
            return pet_type;
        }

        public void setPet_type(String pet_type) {
            this.pet_type = pet_type;
        }

        public String getPetimage() {
            return petimage;
        }

        public void setPetimage(String petimage) {
            this.petimage = petimage;
        }

        public String getBreed() {
            return breed;
        }

        public void setBreed(String breed) {
            this.breed = breed;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPet_age() {
            return pet_age;
        }

        public void setPet_age(String pet_age) {
            this.pet_age = pet_age;
        }

        public List<PastdoctorBean> getPastdoctor() {
            return pastdoctor;
        }

        public void setPastdoctor(List<PastdoctorBean> pastdoctor) {
            this.pastdoctor = pastdoctor;
        }

        public List<String> getCurrentissue() {
            return currentissue;
        }

        public void setCurrentissue(List<String> currentissue) {
            this.currentissue = currentissue;
        }

        public List<String> getAttachments() {
            return attachments;
        }

        public void setAttachments(List<String> attachments) {
            this.attachments = attachments;
        }

        public static class PastdoctorBean {
            /**
             * id : 135
             * name : Vet Testing
             * userimg : http://digittrix.com/staging/pongo/assets/images/avtar.png
             * problems : Behavioral,Heartworm
             * doctor_note : test
             */

            private String id;
            private String name;
            private String userimg;
            private String problems;
            private String doctor_note;

            public String getDoctor_note() {
                return doctor_note;
            }

            public void setDoctor_note(String doctor_note) {
                this.doctor_note = doctor_note;
            }


            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getUserimg() {
                return userimg;
            }

            public void setUserimg(String userimg) {
                this.userimg = userimg;
            }

            public String getProblems() {
                return problems;
            }

            public void setProblems(String problems) {
                this.problems = problems;
            }
        }
    }
}
