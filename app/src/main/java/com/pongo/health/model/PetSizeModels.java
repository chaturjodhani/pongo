package com.pongo.health.model;

public class PetSizeModels {

    private  int  image;
    private  String sizeType;
    private String Length;
    private boolean isSelected = false;

    public PetSizeModels(int image, String sizeType, String length) {
        this.image = image;
        this.sizeType = sizeType;
        Length = length;
    }

    public int getImage() {
        return image;
    }

    public String getSizeType() {
        return sizeType;
    }

    public String getLength() {
        return Length;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
