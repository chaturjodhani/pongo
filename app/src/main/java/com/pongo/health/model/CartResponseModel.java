package com.pongo.health.model;

import java.util.List;

public class CartResponseModel {


    /**
     * status : success
     * cartlist : [{"cartid":"129","product_id":"20","quantity":"2","strength":"","price":"20","favorite":false,"isautorenew":"","name":"Terumo 3cc Luerlock Syringes With 22 Gauge Needles, 1 Inch, 100 Count","description":"Prescribed By Veterinarians To Administer Medication.","orignalprice":"15.00","discountprice":"","product_image":"http://digittrix.com/staging/pongo/assets/products/157665_PT2__AC_SL1500_V1554750797_.jpg","autorenew":"yes","product_type":true,"quantityarray":[{"quantity":"10"},{"quantity":"20"},{"quantity":"30"},{"quantity":"40"}]},{"cartid":"128","product_id":"14","quantity":"2","strength":"","price":"3.59","favorite":false,"isautorenew":"","name":"Rawbble® Canned Wet Dog Food - Lamb Recipe","description":"Rawbble Wet Food For Dogs Is Grain Free, Potato Free And Tapioca Free. The Lamb Recipe Is 93% Single-source Lamb Meat, Lamb Liver & Lamb Broth. With A Focus On Clean, Limited Ingredients, Rawbble® Wet Foods Do Not Include Meals, Carrageenan Or Other Gums ","orignalprice":"3.59","discountprice":"3.99","product_image":"http://digittrix.com/staging/pongo/assets/products/2020_RawbbleWet_Lamb_Front.png","autorenew":"","product_type":false,"quantityarray":[{"quantity":"1"}]},{"cartid":"125","product_id":"15","quantity":"2","strength":"","price":"13.49","favorite":false,"isautorenew":"","name":"Original Jerky Treats - Beef Lung","description":"Good Humans Give Great Treats\r\nWhether You Are Hiking, Playing Fetch Or Hanging At Home, Bixbi® Jerky Is The Perfect Treat For Your Perfect Pooch. Our Limited Ingredient Panel Delivers A Tasty Snack Without Artificial Preservatives Or Colors. So Be A Good","orignalprice":"13.49","discountprice":"14.99","product_image":"http://digittrix.com/staging/pongo/assets/products/Bixbi-Original-Jerky-Treats-Beef-Lung_10oz_Front.png","autorenew":"","product_type":false,"quantityarray":[{"quantity":"1"}]}]
     */

    private String status;
    private List<CartlistBean> cartlist;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<CartlistBean> getCartlist() {
        return cartlist;
    }

    public void setCartlist(List<CartlistBean> cartlist) {
        this.cartlist = cartlist;
    }

    public static class CartlistBean {
        /**
         * cartid : 129
         * product_id : 20
         * quantity : 2
         * strength :
         * price : 20
         * favorite : false
         * isautorenew :
         * name : Terumo 3cc Luerlock Syringes With 22 Gauge Needles, 1 Inch, 100 Count
         * description : Prescribed By Veterinarians To Administer Medication.
         * orignalprice : 15.00
         * discountprice :
         * product_image : http://digittrix.com/staging/pongo/assets/products/157665_PT2__AC_SL1500_V1554750797_.jpg
         * autorenew : yes
         * product_type : true
         * quantityarray : [{"quantity":"10"},{"quantity":"20"},{"quantity":"30"},{"quantity":"40"}]
         */

        private String cartid;
        private String product_id;
        private String quantity;
        private String strength;
        private String price;
        private boolean favorite;
        private String isautorenew;
        private String name;
        private String description;
        private String orignalprice;
        private String discountprice;
        private String product_image;
        private String autorenew;
        private boolean product_type;
        private List<QuantityarrayBean> quantityarray;

        public String getCartid() {
            return cartid;
        }

        public void setCartid(String cartid) {
            this.cartid = cartid;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getStrength() {
            return strength;
        }

        public void setStrength(String strength) {
            this.strength = strength;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public boolean isFavorite() {
            return favorite;
        }

        public void setFavorite(boolean favorite) {
            this.favorite = favorite;
        }

        public String getIsautorenew() {
            return isautorenew;
        }

        public void setIsautorenew(String isautorenew) {
            this.isautorenew = isautorenew;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getOrignalprice() {
            return orignalprice;
        }

        public void setOrignalprice(String orignalprice) {
            this.orignalprice = orignalprice;
        }

        public String getDiscountprice() {
            return discountprice;
        }

        public void setDiscountprice(String discountprice) {
            this.discountprice = discountprice;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getAutorenew() {
            return autorenew;
        }

        public void setAutorenew(String autorenew) {
            this.autorenew = autorenew;
        }

        public boolean isProduct_type() {
            return product_type;
        }

        public void setProduct_type(boolean product_type) {
            this.product_type = product_type;
        }

        public List<QuantityarrayBean> getQuantityarray() {
            return quantityarray;
        }

        public void setQuantityarray(List<QuantityarrayBean> quantityarray) {
            this.quantityarray = quantityarray;
        }

        public static class QuantityarrayBean {
            /**
             * quantity : 10
             */

            private String quantity;

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }
        }
    }
}
