package com.pongo.health.model;

import java.util.List;

public class NearestDoctorModel {

    /**
     * status : success
     * doctors : [{"id":"16","first_name":"Test","last_name":"Test2","email":"test66@gmail.com","vet_type":"vet","phone":"1234567890","expertise":"Dog","time":1.5,"starttime":"08:00:00","endtime":"09:30:00"}]
     * "message"=>'Doctors not found',"type"="0"
     */


    private String status;
    private String message;
    private String type;
    private List<DoctorsBean> doctors;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DoctorsBean> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<DoctorsBean> doctors) {
        this.doctors = doctors;
    }

    public static class DoctorsBean {
        /**
         * id : 16
         * first_name : Test
         * last_name : Test2
         * email : test66@gmail.com
         * vet_type : vet
         * phone : 1234567890
         * expertise : Dog
         * time : 1.5
         * starttime : 08:00:00
         * endtime : 09:30:00
         */

        private String id;
        private String first_name;
        private String last_name;
        private String email;
        private String vet_type;
        private String phone;
        private String expertise;
        private double time;
        private String starttime;
        private String endtime;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getVet_type() {
            return vet_type;
        }

        public void setVet_type(String vet_type) {
            this.vet_type = vet_type;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getExpertise() {
            return expertise;
        }

        public void setExpertise(String expertise) {
            this.expertise = expertise;
        }

        public double getTime() {
            return time;
        }

        public void setTime(double time) {
            this.time = time;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }
    }
}
