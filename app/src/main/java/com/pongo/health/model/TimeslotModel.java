package com.pongo.health.model;

import java.util.List;

public class TimeslotModel {

    /**
     * status : success
     * timeslots : [{"start":"10:00AM"},{"start":"11:00AM"},{"start":"12:00PM"},{"start":"01:00PM"},{"start":"02:00PM"},{"start":"03:00PM"},{"start":"04:00PM"},{"start":"05:00PM"}]
     * price : 15
     */

    private String status;
    private String price;
    private List<TimeslotsBean> timeslots;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public List<TimeslotsBean> getTimeslots() {
        return timeslots;
    }

    public void setTimeslots(List<TimeslotsBean> timeslots) {
        this.timeslots = timeslots;
    }

    public static class TimeslotsBean {
        /**
         * start : 10:00AM
         */

        private String start;

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }
    }
}
