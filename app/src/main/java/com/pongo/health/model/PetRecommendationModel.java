package com.pongo.health.model;

import java.util.List;

public class PetRecommendationModel {


    /**
     * status : success
     * productdata : [{"id":"17","name":"Rope Animal Figures","description":"Made Out Of Durable Woolly Cotton Rope And Crunchy Fiber Filling, This Fun Duck Dog Toy Features A Water Bottle That Creates An Entertaining Crinkle Sound. Your Dog Will Find This Duck Irresistible. 14 1/2 \"l X 3 3/4 \"w. Cotton Rope, Leather Trim, Plastic","product_image":"http://digittrix.com/staging/pongo/assets/products/HTB1asbfaZ_vK1Rjy0Foq6xIxVXaG.jpg","orignalprice":"5.00","discountprice":""},{"id":"16","name":"Digestive Support Powdered Mushroom Supplement","description":"Supports Healthy Gastrointestinal And Immune System Responses\r\nPoor Digestion Can Be The Result Of A Weakened Immune System. And That\u2019s Why Bixbi Digestion Is Made With Organic Mushrooms Containing Alpha And Beta Glucans, Polysaccharides And Terpenes That","product_image":"http://digittrix.com/staging/pongo/assets/products/Bixbi-Powdered-Mushroom-Supplement-Digestion-Support_Front.png","orignalprice":"25.19","discountprice":"27.99"},{"id":"13","name":"Rawbble® Dry Dog Food - Chicken Recipe","description":"Rawbble® Dry Food For Dogs Was Developed With Your Dog\u2019s Health In Mind. We Selected Ingredients For Gut, Renal And Colon Health While Maintaining The Awesome Flavor You And Your Pup Have Come To Know From Bixbi. Protein From Fresh Chicken, And Carbohydra","product_image":"http://digittrix.com/staging/pongo/assets/products/Bixbi-Rawbble-Dry-Dog-Food-Chicken_4LB_front1.png","orignalprice":"16.19","discountprice":"17.99"}]
     */

    private String status;
    private List<ProductdataBean> productdata;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ProductdataBean> getProductdata() {
        return productdata;
    }

    public void setProductdata(List<ProductdataBean> productdata) {
        this.productdata = productdata;
    }

    public static class ProductdataBean {
        /**
         * id : 17
         * name : Rope Animal Figures
         * description : Made Out Of Durable Woolly Cotton Rope And Crunchy Fiber Filling, This Fun Duck Dog Toy Features A Water Bottle That Creates An Entertaining Crinkle Sound. Your Dog Will Find This Duck Irresistible. 14 1/2 "l X 3 3/4 "w. Cotton Rope, Leather Trim, Plastic
         * product_image : http://digittrix.com/staging/pongo/assets/products/HTB1asbfaZ_vK1Rjy0Foq6xIxVXaG.jpg
         * orignalprice : 5.00
         * discountprice :
         * product_type : yes
         */

        private String id;
        private String name;
        private String description;
        private String product_image;
        private String orignalprice;
        private String discountprice;
        private String product_type;

        public String getProduct_type() {
            return product_type;
        }

        public void setProduct_type(String product_type) {
            this.product_type = product_type;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }

        public String getOrignalprice() {
            return orignalprice;
        }

        public void setOrignalprice(String orignalprice) {
            this.orignalprice = orignalprice;
        }

        public String getDiscountprice() {
            return discountprice;
        }

        public void setDiscountprice(String discountprice) {
            this.discountprice = discountprice;
        }
    }
}
