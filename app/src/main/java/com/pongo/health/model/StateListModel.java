package com.pongo.health.model;

import java.util.List;

public class StateListModel {

    /**
     * status : success
     * statelist : [{"id":"8","state":"Alabama"},{"id":"9","state":"Alaska"},{"id":"10","state":"Arizona"},{"id":"11","state":"Arkansas"},{"id":"12","state":"California"},{"id":"13","state":"Colorado"},{"id":"14","state":"Connecticut"},{"id":"15","state":"Delaware"},{"id":"16","state":"Florida"},{"id":"17","state":"Georgia"},{"id":"18","state":"Hawaii"},{"id":"19","state":"Idaho"},{"id":"20","state":"Illinois"},{"id":"23","state":"Indiana"},{"id":"24","state":"Iowa"},{"id":"25","state":"Kansas"},{"id":"26","state":"Kentucky"},{"id":"27","state":"Louisiana"},{"id":"28","state":"Maine"},{"id":"29","state":"Maryland"},{"id":"30","state":"Massachusetts"},{"id":"31","state":"Michigan"},{"id":"32","state":"Minnesota"},{"id":"33","state":"Mississipi"},{"id":"34","state":"Missouri"},{"id":"35","state":"Montana"},{"id":"36","state":"Nebrasaka"},{"id":"37","state":"Nevada"},{"id":"38","state":"New Hampshire"},{"id":"39","state":"New Jersey"},{"id":"40","state":"New Mexico"},{"id":"41","state":"New York"},{"id":"42","state":"North Carolina"},{"id":"43","state":"North Dakota"},{"id":"44","state":"Ohio"},{"id":"45","state":"Oklahoma"},{"id":"46","state":"Oregon"},{"id":"47","state":"Pennsylvania"},{"id":"48","state":"Rhode Island"},{"id":"49","state":"South Carolina"},{"id":"50","state":"South Dakota"},{"id":"51","state":"Tennessee"},{"id":"52","state":"Texas"},{"id":"53","state":"Utah"},{"id":"54","state":"Vermont"},{"id":"55","state":"Virginia"},{"id":"56","state":"Washington"},{"id":"60","state":"Washington D.c."},{"id":"57","state":"West Virginia"},{"id":"58","state":"Wisconsin"},{"id":"59","state":"Wyoming"}]
     */

    private String status;
    private List<StatelistBean> statelist;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<StatelistBean> getStatelist() {
        return statelist;
    }

    public void setStatelist(List<StatelistBean> statelist) {
        this.statelist = statelist;
    }

    public static class StatelistBean {
        /**
         * id : 8
         * state : Alabama
         */

        private String id;
        private String state;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }
}
