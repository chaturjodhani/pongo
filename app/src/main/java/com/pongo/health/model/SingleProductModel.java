package com.pongo.health.model;

import java.util.List;

public class SingleProductModel {


    /**
     * status : success
     * singleproduct : [{"id":"20","name":"Terumo 3cc Luerlock Syringes With 22 Gauge Needles, 1 Inch, 100 Count","description":"Prescribed By Veterinarians To Administer Medication.","orignalprice":"15.00","discountprice":"","product_type":true,"prescription_item":"No","product_image":[{"image":"http://digittrix.com/staging/pongo/assets/products/157665_PT2__AC_SL1500_V1554750797_.jpg"},{"image":"http://digittrix.com/staging/pongo/assets/products/157665_PT1__AC_SL1500_V1554750791_.jpg"}],"special_shipping":"","precautions":"Syringes Should Be Kept Out Of The Reach Of Children And Pets. Used Sharps Should Be Immediately Placed In A Sharps Disposal Container. Fda-cleared Sharps Containers Are Generally Available Through Pharmacies, Medical Supply Companies, Health Care Provide","keybenifits":"","drug_type":"Devices & Supplies","prescribing_info":"http://digittrix.com/staging/pongo/assets/products/146155_PackageInsert__V1542385313_2.pdf","patient_info_sheet":"http://digittrix.com/staging/pongo/assets/products/146155_PackageInsert__V1542385313_2.pdf","category":"Pet Meds","subcategory":"Flea & Tick","ingredients":[{"description":"n/a"}],"side_effect":[{"description":"n/a"}],"administartion_form":"","product_form":"","generic_name":"Canine Syringes","faq":[{"answer":"Recommended Dosage\r\nUse according to your veterinarian's instructions.\r\n\r\nStorage Instructions\r\nUsed sharps should be immediately placed in a sharps disposal container. FDA-cleared sharps containers are generally available through pharmacies, medical supply c"}],"reviews":[{"id":"10","userid":"45","name":"when the sun goes down the moon comes up and we go ","rating":"4","description":"hello this is my rescues hello this is my rescues hello this is my rescues jejdjdjdjdjdj hello hi how are you what are you doing let\u2019s go out to play tomorrow in the su. hello how\u2019s it going is the car in the driveway","productid":"1","reviewimage":"http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg"},{"id":"7","userid":"67","name":"hi ","rating":"5","description":"this is a great product ","productid":"1","reviewimage":"http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg"},{"id":"6","userid":"67","name":"hi ","rating":"5","description":"this is a great product ","productid":"1","reviewimage":"http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg"},{"id":"5","userid":"41","name":"dummy","rating":"4","description":"bsbsbs","productid":"1","reviewimage":"http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg"},{"id":"4","userid":"69","name":"testing","rating":"5","description":"testingggggg","productid":"1","reviewimage":"http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg"},{"id":"3","userid":"20","name":"test","rating":"4","description":"test review","productid":"1","reviewimage":"http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg"},{"id":"2","userid":"20","name":"test review2","rating":"3","description":"testing by nk again","productid":"1","reviewimage":"http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg"},{"id":"1","userid":"20","name":"test review","rating":"4","description":"testing by nk","productid":"1","reviewimage":"http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg"}],"prescription":[{"id":"10","userid":"45","title":"when the sun goes down the moon comes up and we go ","rating":"4","description":"hello this is my rescues hello this is my rescues hello this is my rescues jejdjdjdjdjdj hello hi how are you what are you doing let\u2019s go out to play tomorrow in the su. hello how\u2019s it going is the car in the driveway","productid":"1","by":"JS JS disk","date":"June 07,2020"},{"id":"7","userid":"67","title":"hi ","rating":"5","description":"this is a great product ","productid":"1","by":"Pankaj Udas","date":"June 06,2020"},{"id":"6","userid":"67","title":"hi ","rating":"5","description":"this is a great product ","productid":"1","by":"Pankaj Udas","date":"June 06,2020"},{"id":"5","userid":"41","title":"dummy","rating":"4","description":"bsbsbs","productid":"1","by":"test 1","date":"June 05,2020"},{"id":"4","userid":"69","title":"testing","rating":"5","description":"testingggggg","productid":"1","by":"Daman Preet","date":"June 05,2020"},{"id":"3","userid":"20","title":"test","rating":"4","description":"test review","productid":"1","by":"guri bhullar","date":"June 04,2020"},{"id":"2","userid":"20","title":"test review2","rating":"3","description":"testing by nk again","productid":"1","by":"guri bhullar","date":"June 04,2020"},{"id":"1","userid":"20","title":"test review","rating":"4","description":"testing by nk","productid":"1","by":"guri bhullar","date":"June 04,2020"}],"favorite":false,"brand":"Terumo","review_count":0,"quantity":"5","quantityarray":[{"quantity":"10"},{"quantity":"20"},{"quantity":"30"},{"quantity":"40"}],"stock":"10, 20, 30, 40","strength":[{"id":0,"name":"100count","price":"15.00","doseage":"1 tablet"}],"review_rating":0,"color":"Brown","item_number":"157665","for_use_with":"Dogs","common_brand_name":"N/a","healthcondition":"Diabetes","reviewpercentage":0,"breed_size":[{"name":"small"},{"name":"medium"},{"name":"large"},{"name":"extra large"}],"company":"company","other":"Lorem"}]
     */

    private String status;
    private List<SingleproductBean> singleproduct;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<SingleproductBean> getSingleproduct() {
        return singleproduct;
    }

    public void setSingleproduct(List<SingleproductBean> singleproduct) {
        this.singleproduct = singleproduct;
    }

    public static class SingleproductBean {
        /**
         * id : 20
         * name : Terumo 3cc Luerlock Syringes With 22 Gauge Needles, 1 Inch, 100 Count
         * description : Prescribed By Veterinarians To Administer Medication.
         * orignalprice : 15.00
         * discountprice :
         * product_type : true
         * prescription_item : No
         * product_image : [{"image":"http://digittrix.com/staging/pongo/assets/products/157665_PT2__AC_SL1500_V1554750797_.jpg"},{"image":"http://digittrix.com/staging/pongo/assets/products/157665_PT1__AC_SL1500_V1554750791_.jpg"}]
         * special_shipping :
         * precautions : Syringes Should Be Kept Out Of The Reach Of Children And Pets. Used Sharps Should Be Immediately Placed In A Sharps Disposal Container. Fda-cleared Sharps Containers Are Generally Available Through Pharmacies, Medical Supply Companies, Health Care Provide
         * keybenifits :
         * drug_type : Devices & Supplies
         * prescribing_info : http://digittrix.com/staging/pongo/assets/products/146155_PackageInsert__V1542385313_2.pdf
         * patient_info_sheet : http://digittrix.com/staging/pongo/assets/products/146155_PackageInsert__V1542385313_2.pdf
         * category : Pet Meds
         * subcategory : Flea & Tick
         * ingredients : [{"description":"n/a"}]
         * side_effect : [{"description":"n/a"}]
         * administartion_form :
         * product_form :
         * generic_name : Canine Syringes
         * faq : [{"answer":"Recommended Dosage\r\nUse according to your veterinarian's instructions.\r\n\r\nStorage Instructions\r\nUsed sharps should be immediately placed in a sharps disposal container. FDA-cleared sharps containers are generally available through pharmacies, medical supply c"}]
         * reviews : [{"id":"10","userid":"45","name":"when the sun goes down the moon comes up and we go ","rating":"4","description":"hello this is my rescues hello this is my rescues hello this is my rescues jejdjdjdjdjdj hello hi how are you what are you doing let\u2019s go out to play tomorrow in the su. hello how\u2019s it going is the car in the driveway","productid":"1","reviewimage":"http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg"},{"id":"7","userid":"67","name":"hi ","rating":"5","description":"this is a great product ","productid":"1","reviewimage":"http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg"},{"id":"6","userid":"67","name":"hi ","rating":"5","description":"this is a great product ","productid":"1","reviewimage":"http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg"},{"id":"5","userid":"41","name":"dummy","rating":"4","description":"bsbsbs","productid":"1","reviewimage":"http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg"},{"id":"4","userid":"69","name":"testing","rating":"5","description":"testingggggg","productid":"1","reviewimage":"http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg"},{"id":"3","userid":"20","name":"test","rating":"4","description":"test review","productid":"1","reviewimage":"http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg"},{"id":"2","userid":"20","name":"test review2","rating":"3","description":"testing by nk again","productid":"1","reviewimage":"http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg"},{"id":"1","userid":"20","name":"test review","rating":"4","description":"testing by nk","productid":"1","reviewimage":"http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg"}]
         * prescription : [{"id":"10","userid":"45","title":"when the sun goes down the moon comes up and we go ","rating":"4","description":"hello this is my rescues hello this is my rescues hello this is my rescues jejdjdjdjdjdj hello hi how are you what are you doing let\u2019s go out to play tomorrow in the su. hello how\u2019s it going is the car in the driveway","productid":"1","by":"JS JS disk","date":"June 07,2020"},{"id":"7","userid":"67","title":"hi ","rating":"5","description":"this is a great product ","productid":"1","by":"Pankaj Udas","date":"June 06,2020"},{"id":"6","userid":"67","title":"hi ","rating":"5","description":"this is a great product ","productid":"1","by":"Pankaj Udas","date":"June 06,2020"},{"id":"5","userid":"41","title":"dummy","rating":"4","description":"bsbsbs","productid":"1","by":"test 1","date":"June 05,2020"},{"id":"4","userid":"69","title":"testing","rating":"5","description":"testingggggg","productid":"1","by":"Daman Preet","date":"June 05,2020"},{"id":"3","userid":"20","title":"test","rating":"4","description":"test review","productid":"1","by":"guri bhullar","date":"June 04,2020"},{"id":"2","userid":"20","title":"test review2","rating":"3","description":"testing by nk again","productid":"1","by":"guri bhullar","date":"June 04,2020"},{"id":"1","userid":"20","title":"test review","rating":"4","description":"testing by nk","productid":"1","by":"guri bhullar","date":"June 04,2020"}]
         * favorite : false
         * prescribing_show : false
         * brand : Terumo
         * review_count : 0
         * quantity : 5
         * quantityarray : [{"quantity":"10"},{"quantity":"20"},{"quantity":"30"},{"quantity":"40"}]
         * stock : 10, 20, 30, 40
         * strength : [{"id":0,"name":"100count","price":"15.00","doseage":"1 tablet"}]
         * review_rating : 0
         * color : Brown
         * item_number : 157665
         * for_use_with : Dogs
         * common_brand_name : N/a
         * healthcondition : Diabetes
         * reviewpercentage : 0
         * breed_size : [{"name":"small"},{"name":"medium"},{"name":"large"},{"name":"extra large"}]
         * company : company
         * other : Lorem
         * 1star: 0
         * 2star: 25
         * 3star: 0
         * 4star: 25
         * 5star: 50
         */

        private String id;
        private String name;
        private String description;
        private String orignalprice;
        private String discountprice;
        private boolean product_type;
        private boolean prescribing_show;
        private String prescription_item;
        private String special_shipping;
        private String precautions;
        private String keybenifits;
        private String drug_type;
        private String prescribing_info;
        private String patient_info_sheet;
        private String category;
        private String subcategory;
        private String administartion_form;
        private String product_form;
        private String generic_name;
        private boolean favorite;
        private String brand;
        private int review_count;
        private String quantity;
        private String stock;
        private int review_rating;
        private String color;
        private String item_number;
        private String for_use_with;
        private String common_brand_name;
        private String healthcondition;
        private int reviewpercentage;
        private String company;
        private String other;
        private List<ProductImageBean> product_image;
        private List<IngredientsBean> ingredients;
        private List<SideEffectBean> side_effect;
        private List<FaqBean> faq;
        private List<ReviewsBean> reviews;
        private List<PrescriptionBean> prescription;
        private List<QuantityarrayBean> quantityarray;
        private List<StrengthBean> strength;
        private List<BreedSizeBean> breed_size;
        private int star1;
        private int star2;
        private int star3;
        private int star4;
        private int star5;

        public boolean isPrescribing_show() {
            return prescribing_show;
        }

        public void setPrescribing_show(boolean prescribing_show) {
            this.prescribing_show = prescribing_show;
        }

        public int getStar1() {
            return star1;
        }

        public void setStar1(int star1) {
            this.star1 = star1;
        }

        public int getStar2() {
            return star2;
        }

        public void setStar2(int star2) {
            this.star2 = star2;
        }

        public int getStar3() {
            return star3;
        }

        public void setStar3(int star3) {
            this.star3 = star3;
        }

        public int getStar4() {
            return star4;
        }

        public void setStar4(int star4) {
            this.star4 = star4;
        }

        public int getStar5() {
            return star5;
        }

        public void setStar5(int star5) {
            this.star5 = star5;
        }


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getOrignalprice() {
            return orignalprice;
        }

        public void setOrignalprice(String orignalprice) {
            this.orignalprice = orignalprice;
        }

        public String getDiscountprice() {
            return discountprice;
        }

        public void setDiscountprice(String discountprice) {
            this.discountprice = discountprice;
        }

        public boolean isProduct_type() {
            return product_type;
        }

        public void setProduct_type(boolean product_type) {
            this.product_type = product_type;
        }

        public String getPrescription_item() {
            return prescription_item;
        }

        public void setPrescription_item(String prescription_item) {
            this.prescription_item = prescription_item;
        }

        public String getSpecial_shipping() {
            return special_shipping;
        }

        public void setSpecial_shipping(String special_shipping) {
            this.special_shipping = special_shipping;
        }

        public String getPrecautions() {
            return precautions;
        }

        public void setPrecautions(String precautions) {
            this.precautions = precautions;
        }

        public String getKeybenifits() {
            return keybenifits;
        }

        public void setKeybenifits(String keybenifits) {
            this.keybenifits = keybenifits;
        }

        public String getDrug_type() {
            return drug_type;
        }

        public void setDrug_type(String drug_type) {
            this.drug_type = drug_type;
        }

        public String getPrescribing_info() {
            return prescribing_info;
        }

        public void setPrescribing_info(String prescribing_info) {
            this.prescribing_info = prescribing_info;
        }

        public String getPatient_info_sheet() {
            return patient_info_sheet;
        }

        public void setPatient_info_sheet(String patient_info_sheet) {
            this.patient_info_sheet = patient_info_sheet;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getSubcategory() {
            return subcategory;
        }

        public void setSubcategory(String subcategory) {
            this.subcategory = subcategory;
        }

        public String getAdministartion_form() {
            return administartion_form;
        }

        public void setAdministartion_form(String administartion_form) {
            this.administartion_form = administartion_form;
        }

        public String getProduct_form() {
            return product_form;
        }

        public void setProduct_form(String product_form) {
            this.product_form = product_form;
        }

        public String getGeneric_name() {
            return generic_name;
        }

        public void setGeneric_name(String generic_name) {
            this.generic_name = generic_name;
        }

        public boolean isFavorite() {
            return favorite;
        }

        public void setFavorite(boolean favorite) {
            this.favorite = favorite;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public int getReview_count() {
            return review_count;
        }

        public void setReview_count(int review_count) {
            this.review_count = review_count;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getStock() {
            return stock;
        }

        public void setStock(String stock) {
            this.stock = stock;
        }

        public int getReview_rating() {
            return review_rating;
        }

        public void setReview_rating(int review_rating) {
            this.review_rating = review_rating;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getItem_number() {
            return item_number;
        }

        public void setItem_number(String item_number) {
            this.item_number = item_number;
        }

        public String getFor_use_with() {
            return for_use_with;
        }

        public void setFor_use_with(String for_use_with) {
            this.for_use_with = for_use_with;
        }

        public String getCommon_brand_name() {
            return common_brand_name;
        }

        public void setCommon_brand_name(String common_brand_name) {
            this.common_brand_name = common_brand_name;
        }

        public String getHealthcondition() {
            return healthcondition;
        }

        public void setHealthcondition(String healthcondition) {
            this.healthcondition = healthcondition;
        }

        public int getReviewpercentage() {
            return reviewpercentage;
        }

        public void setReviewpercentage(int reviewpercentage) {
            this.reviewpercentage = reviewpercentage;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getOther() {
            return other;
        }

        public void setOther(String other) {
            this.other = other;
        }

        public List<ProductImageBean> getProduct_image() {
            return product_image;
        }

        public void setProduct_image(List<ProductImageBean> product_image) {
            this.product_image = product_image;
        }

        public List<IngredientsBean> getIngredients() {
            return ingredients;
        }

        public void setIngredients(List<IngredientsBean> ingredients) {
            this.ingredients = ingredients;
        }

        public List<SideEffectBean> getSide_effect() {
            return side_effect;
        }

        public void setSide_effect(List<SideEffectBean> side_effect) {
            this.side_effect = side_effect;
        }

        public List<FaqBean> getFaq() {
            return faq;
        }

        public void setFaq(List<FaqBean> faq) {
            this.faq = faq;
        }

        public List<ReviewsBean> getReviews() {
            return reviews;
        }

        public void setReviews(List<ReviewsBean> reviews) {
            this.reviews = reviews;
        }

        public List<PrescriptionBean> getPrescription() {
            return prescription;
        }

        public void setPrescription(List<PrescriptionBean> prescription) {
            this.prescription = prescription;
        }

        public List<QuantityarrayBean> getQuantityarray() {
            return quantityarray;
        }

        public void setQuantityarray(List<QuantityarrayBean> quantityarray) {
            this.quantityarray = quantityarray;
        }

        public List<StrengthBean> getStrength() {
            return strength;
        }

        public void setStrength(List<StrengthBean> strength) {
            this.strength = strength;
        }

        public List<BreedSizeBean> getBreed_size() {
            return breed_size;
        }

        public void setBreed_size(List<BreedSizeBean> breed_size) {
            this.breed_size = breed_size;
        }

        public static class ProductImageBean {
            /**
             * image : http://digittrix.com/staging/pongo/assets/products/157665_PT2__AC_SL1500_V1554750797_.jpg
             */

            private String image;

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }
        }

        public static class IngredientsBean {
            /**
             * description : n/a
             */

            private String description;

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }
        }

        public static class SideEffectBean {
            /**
             * description : n/a
             */

            private String description;

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }
        }

        public static class FaqBean {
            /**
             * answer : Recommended Dosage
             * Use according to your veterinarian's instructions.
             * <p>
             * Storage Instructions
             * Used sharps should be immediately placed in a sharps disposal container. FDA-cleared sharps containers are generally available through pharmacies, medical supply c
             */

            private String answer;

            public String getAnswer() {
                return answer;
            }

            public void setAnswer(String answer) {
                this.answer = answer;
            }
        }

        public static class ReviewsBean {
            /**
             * id : 10
             * userid : 45
             * name : when the sun goes down the moon comes up and we go
             * rating : 4
             * description : hello this is my rescues hello this is my rescues hello this is my rescues jejdjdjdjdjdj hello hi how are you what are you doing let’s go out to play tomorrow in the su. hello how’s it going is the car in the driveway
             * productid : 1
             * reviewimage : http://digittrix.com/staging/pongo/assets/images/petimage/IMG-20200525-WA0008.jpg
             */

            private String id;
            private String userid;
            private String name;
            private String rating;
            private String description;
            private String productid;
            private String reviewimage;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUserid() {
                return userid;
            }

            public void setUserid(String userid) {
                this.userid = userid;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getRating() {
                return rating;
            }

            public void setRating(String rating) {
                this.rating = rating;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getProductid() {
                return productid;
            }

            public void setProductid(String productid) {
                this.productid = productid;
            }

            public String getReviewimage() {
                return reviewimage;
            }

            public void setReviewimage(String reviewimage) {
                this.reviewimage = reviewimage;
            }
        }

        public static class PrescriptionBean {
            /**
             * id : 10
             * userid : 45
             * title : when the sun goes down the moon comes up and we go
             * rating : 4
             * description : hello this is my rescues hello this is my rescues hello this is my rescues jejdjdjdjdjdj hello hi how are you what are you doing let’s go out to play tomorrow in the su. hello how’s it going is the car in the driveway
             * productid : 1
             * by : JS JS disk
             * date : June 07,2020
             */

            private String id;
            private String userid;
            private String title;
            private String rating;
            private String description;
            private String productid;
            private String by;
            private String date;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUserid() {
                return userid;
            }

            public void setUserid(String userid) {
                this.userid = userid;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getRating() {
                return rating;
            }

            public void setRating(String rating) {
                this.rating = rating;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getProductid() {
                return productid;
            }

            public void setProductid(String productid) {
                this.productid = productid;
            }

            public String getBy() {
                return by;
            }

            public void setBy(String by) {
                this.by = by;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }
        }

        public static class QuantityarrayBean {
            /**
             * quantity : 10
             */

            private String quantity;

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }
        }

        public static class StrengthBean {
            /**
             * id : 0
             * name : 100count
             * price : 15.00
             * doseage : 1 tablet
             */

            private int id;
            private String name;
            private String price;
            private String doseage;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getDoseage() {
                return doseage;
            }

            public void setDoseage(String doseage) {
                this.doseage = doseage;
            }
        }

        public static class BreedSizeBean {
            /**
             * name : small
             */

            private String name;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
