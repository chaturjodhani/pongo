package com.pongo.health.model;

public class CartModel {

    private int itemImage;
    private String ItemName;
    private String ItemDescription;
    private String ItemQuantity;

    public CartModel(int itemImage, String itemName, String itemDescription, String itemQuantity) {
        this.itemImage = itemImage;
        ItemName = itemName;
        ItemDescription = itemDescription;
        ItemQuantity = itemQuantity;
    }

    public int getItemImage() {
        return itemImage;
    }

    public String getItemName() {
        return ItemName;
    }

    public String getItemDescription() {
        return ItemDescription;
    }

    public String getItemQuantity() {
        return ItemQuantity;
    }
}
