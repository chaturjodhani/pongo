package com.pongo.health.model;

public class BreedDynamicModel {
    private String id;
    private String type;
    private String breed;

    public BreedDynamicModel(String type, String id, String breed) {
        this.id = id;
        this.type = type;
        this.breed = breed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

}
