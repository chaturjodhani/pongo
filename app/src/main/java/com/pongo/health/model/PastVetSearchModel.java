package com.pongo.health.model;

import java.util.List;

public class PastVetSearchModel {

    /**
     * status : success
     * recent : [{"id":"4","userId":"144","search":"ve"},{"id":"3","userId":"144","search":"a"},{"id":"2","userId":"144","search":"ab"}]
     * pastdoctor : [{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"},{"userId":"135","name":"Vet Testing","expertise":"Horses (equine),dogs (cannies)","date":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png"}]
     */

    private String status;
    private List<RecentBean> recent;
    private List<PastdoctorBean> pastdoctor;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<RecentBean> getRecent() {
        return recent;
    }

    public void setRecent(List<RecentBean> recent) {
        this.recent = recent;
    }

    public List<PastdoctorBean> getPastdoctor() {
        return pastdoctor;
    }

    public void setPastdoctor(List<PastdoctorBean> pastdoctor) {
        this.pastdoctor = pastdoctor;
    }

    public static class RecentBean {
        /**
         * id : 4
         * userId : 144
         * search : ve
         */

        private String id;
        private String userId;
        private String search;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getSearch() {
            return search;
        }

        public void setSearch(String search) {
            this.search = search;
        }
    }

    public static class PastdoctorBean {
        /**
         * userId : 135
         * name : Vet Testing
         * expertise : Horses (equine),dogs (cannies)
         * date : July 2020
         * userimage : http://digittrix.com/staging/pongo/assets/images/avtar.png
         * "callprice": "30",
         */

        private String userId;
        private String name;
        private String expertise;
        private String date;
        private String userimage;
        private String callprice;

        public String getCallprice() {
            return callprice;
        }

        public void setCallprice(String callprice) {
            this.callprice = callprice;
        }


        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getExpertise() {
            return expertise;
        }

        public void setExpertise(String expertise) {
            this.expertise = expertise;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getUserimage() {
            return userimage;
        }

        public void setUserimage(String userimage) {
            this.userimage = userimage;
        }
    }
}
