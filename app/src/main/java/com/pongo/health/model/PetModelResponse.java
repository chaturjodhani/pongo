package com.pongo.health.model;

import java.util.List;

public class PetModelResponse {

    /**
     * status : success
     * data : [{"id":"114","user_id":"75","pet_name":"tommy","pet_type":"dog","pet_size":"large","pet_old":"senior","gender":"","birthday":"","neutered":"","microchipnumber":"","helath_goal":"protect skin & coat","recommended_iem_id":"","pet_note":"","petimage":"http://digittrix.com/staging/pongo/assets/images/petimage/JPEG_20200711_115026_-778358140.jpg","breed":"Golden Doodle","pet_age":"+ 8 years"},{"id":"113","user_id":"75","pet_name":"jimmy test","pet_type":"cat","pet_size":"small","pet_old":"puppy","gender":"Female","birthday":"","neutered":"Yes","microchipnumber":"","helath_goal":"protect skin & coat","recommended_iem_id":"","pet_note":"","petimage":"http://digittrix.com/staging/pongo/assets/images/petimage/JPEG_20200711_111158_-778358140.jpg","breed":"Bengal Cat","pet_age":"< 2 years"},{"id":"112","user_id":"75","pet_name":"tiger","pet_type":"dog","pet_size":"medium","pet_old":"adult","gender":"","birthday":"","neutered":"","microchipnumber":"","helath_goal":"behavioural improvement","recommended_iem_id":"","pet_note":"","petimage":"http://digittrix.com/staging/pongo/assets/images/petimage/JPEG_20200711_110626_-778358140.jpg","breed":"Golden Doodle","pet_age":"2-8 years"}]
     */

    private String status;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 114
         * user_id : 75
         * pet_name : tommy
         * pet_type : dog
         * pet_size : large
         * pet_old : senior
         * gender :
         * birthday :
         * neutered :
         * microchipnumber :
         * helath_goal : protect skin & coat
         * recommended_iem_id :
         * pet_note :
         * petimage : http://digittrix.com/staging/pongo/assets/images/petimage/JPEG_20200711_115026_-778358140.jpg
         * breed : Golden Doodle
         * pet_age : + 8 years
         */

        private String id;
        private String user_id;
        private String pet_name;
        private String pet_type;
        private String pet_size;
        private String pet_old;
        private String gender;
        private String birthday;
        private String neutered;
        private String microchipnumber;
        private String helath_goal;
        private String recommended_iem_id;
        private String pet_note;
        private String petimage;
        private String breed;
        private String pet_age;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getPet_name() {
            return pet_name;
        }

        public void setPet_name(String pet_name) {
            this.pet_name = pet_name;
        }

        public String getPet_type() {
            return pet_type;
        }

        public void setPet_type(String pet_type) {
            this.pet_type = pet_type;
        }

        public String getPet_size() {
            return pet_size;
        }

        public void setPet_size(String pet_size) {
            this.pet_size = pet_size;
        }

        public String getPet_old() {
            return pet_old;
        }

        public void setPet_old(String pet_old) {
            this.pet_old = pet_old;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getBirthday() {
            return birthday;
        }

        public void setBirthday(String birthday) {
            this.birthday = birthday;
        }

        public String getNeutered() {
            return neutered;
        }

        public void setNeutered(String neutered) {
            this.neutered = neutered;
        }

        public String getMicrochipnumber() {
            return microchipnumber;
        }

        public void setMicrochipnumber(String microchipnumber) {
            this.microchipnumber = microchipnumber;
        }

        public String getHelath_goal() {
            return helath_goal;
        }

        public void setHelath_goal(String helath_goal) {
            this.helath_goal = helath_goal;
        }

        public String getRecommended_iem_id() {
            return recommended_iem_id;
        }

        public void setRecommended_iem_id(String recommended_iem_id) {
            this.recommended_iem_id = recommended_iem_id;
        }

        public String getPet_note() {
            return pet_note;
        }

        public void setPet_note(String pet_note) {
            this.pet_note = pet_note;
        }

        public String getPetimage() {
            return petimage;
        }

        public void setPetimage(String petimage) {
            this.petimage = petimage;
        }

        public String getBreed() {
            return breed;
        }

        public void setBreed(String breed) {
            this.breed = breed;
        }

        public String getPet_age() {
            return pet_age;
        }

        public void setPet_age(String pet_age) {
            this.pet_age = pet_age;
        }
    }
}
