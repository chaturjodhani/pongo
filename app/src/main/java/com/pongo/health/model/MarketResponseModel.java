package com.pongo.health.model;

import java.util.List;

public class MarketResponseModel {

    /**
     * status : success
     * categorylist : [{"cat_id":"16","cat_name":"Rx","cat_image":"http://digittrix.com/staging/pongo/assets/caticon/1.png"},{"cat_id":"15","cat_name":"Food","cat_image":"http://digittrix.com/staging/pongo/assets/caticon/2.png"},{"cat_id":"14","cat_name":"Supplies","cat_image":"http://digittrix.com/staging/pongo/assets/caticon/3.png"},{"cat_id":"13","cat_name":"Insurance","cat_image":"http://digittrix.com/staging/pongo/assets/caticon/4.png"}]
     * productlist : [{"id":"1","name":"Pro11","description":"Testing","orignalprice":"122","discountprice":"","product_type":true,"product_image":"http://digittrix.com/staging/pongo/assets/products/1.png"},{"id":"2","name":"Nonpres","description":"Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test 22","orignalprice":"36","discountprice":"40","product_type":false,"product_image":"http://digittrix.com/staging/pongo/assets/products/33.jpeg"}]
     */

    private String status;
    private List<CategorylistBean> categorylist;
    private List<ProductlistBean> productlist;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<CategorylistBean> getCategorylist() {
        return categorylist;
    }

    public void setCategorylist(List<CategorylistBean> categorylist) {
        this.categorylist = categorylist;
    }

    public List<ProductlistBean> getProductlist() {
        return productlist;
    }

    public void setProductlist(List<ProductlistBean> productlist) {
        this.productlist = productlist;
    }

    public static class CategorylistBean {
        /**
         * cat_id : 16
         * cat_name : Rx
         * cat_image : http://digittrix.com/staging/pongo/assets/caticon/1.png
         */

        private String cat_id;
        private String cat_name;
        private String cat_image;

        public String getCat_id() {
            return cat_id;
        }

        public void setCat_id(String cat_id) {
            this.cat_id = cat_id;
        }

        public String getCat_name() {
            return cat_name;
        }

        public void setCat_name(String cat_name) {
            this.cat_name = cat_name;
        }

        public String getCat_image() {
            return cat_image;
        }

        public void setCat_image(String cat_image) {
            this.cat_image = cat_image;
        }
    }

    public static class ProductlistBean {
        /**
         * id : 1
         * name : Pro11
         * description : Testing
         * orignalprice : 122
         * discountprice :
         * product_type : true
         * category: Supplies,
         * subcategory: Crates,
         * product_image : http://digittrix.com/staging/pongo/assets/products/1.png
         */

        private String id;
        private String name;
        private String description;
        private String orignalprice;
        private String discountprice;
        private String category;
        private String subcategory;
        private boolean product_type;
        private String product_image;

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getSubcategory() {
            return subcategory;
        }

        public void setSubcategory(String subcategory) {
            this.subcategory = subcategory;
        }


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getOrignalprice() {
            return orignalprice;
        }

        public void setOrignalprice(String orignalprice) {
            this.orignalprice = orignalprice;
        }

        public String getDiscountprice() {
            return discountprice;
        }

        public void setDiscountprice(String discountprice) {
            this.discountprice = discountprice;
        }

        public boolean isProduct_type() {
            return product_type;
        }

        public void setProduct_type(boolean product_type) {
            this.product_type = product_type;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }
    }
}
