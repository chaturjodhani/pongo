package com.pongo.health.model;

import java.util.List;

public class VetTimeSlotModel {

    /**
     * status : success
     * timeslots : [{"start":"10:00AM","status":false},{"start":"11:00AM","status":false},{"start":"12:00PM","status":false},{"start":"01:00PM","status":false},{"start":"02:00PM","status":false},{"start":"03:00PM","status":false},{"start":"04:00PM","status":false},{"start":"05:00PM","status":false}]
     */

    private String status;
    private List<TimeslotsBean> timeslots;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<TimeslotsBean> getTimeslots() {
        return timeslots;
    }

    public void setTimeslots(List<TimeslotsBean> timeslots) {
        this.timeslots = timeslots;
    }

    public static class TimeslotsBean {
        /**
         * start : 10:00AM
         * status : false
         */

        private String start;
        private boolean status;

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }
    }
}
