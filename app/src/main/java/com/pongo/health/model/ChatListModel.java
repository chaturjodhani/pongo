package com.pongo.health.model;

import java.util.List;

public class ChatListModel {

    /**
     * id : 82
     * user_id : 90
     * pet_name : Bingo
     * pet_type : cat
     * petimage : http://digittrix.com/staging/pongo/assets/images/petimage/1592794926_6127272.jpeg
     * breed : Bengal Cat
     * status : complete
     * pet_age : 2-8 years
     */

    private String id;
    private String user_id;
    private String pet_name;
    private String pet_type;
    private String petimage;
    private String breed;
    private String status;
    private String pet_age;

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getBooking_time() {
        return booking_time;
    }

    public void setBooking_time(String booking_time) {
        this.booking_time = booking_time;
    }

    public String getCurrentproblem() {
        return currentproblem;
    }

    public void setCurrentproblem(String currentproblem) {
        this.currentproblem = currentproblem;
    }

    private String booking_date;
    private String booking_time;
    private String currentproblem;

    public String getChatid() {
        return chatid;
    }

    public void setChatid(String chatid) {
        this.chatid = chatid;
    }

    private String chatid;
    private List<String> token;

    public List<String> getToken() {
        return token;
    }

    public void setToken(List<String> token) {
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPet_name() {
        return pet_name;
    }

    public void setPet_name(String pet_name) {
        this.pet_name = pet_name;
    }

    public String getPet_type() {
        return pet_type;
    }

    public void setPet_type(String pet_type) {
        this.pet_type = pet_type;
    }

    public String getPetimage() {
        return petimage;
    }

    public void setPetimage(String petimage) {
        this.petimage = petimage;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPet_age() {
        return pet_age;
    }

    public void setPet_age(String pet_age) {
        this.pet_age = pet_age;
    }
}
