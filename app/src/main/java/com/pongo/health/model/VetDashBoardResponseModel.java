package com.pongo.health.model;

public class VetDashBoardResponseModel {


    /**
     * status : success
     * doctors : {"id":"135","first_name":"Vet","last_name":"Testing","vet_type":"Horses (equine),dogs (cannies)","expertise":"July 2020","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png","recieve_patient":"1","adminstatus":"1"}
     */

    private String status;
    private DoctorsBean doctors;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DoctorsBean getDoctors() {
        return doctors;
    }

    public void setDoctors(DoctorsBean doctors) {
        this.doctors = doctors;
    }

    public static class DoctorsBean {
        /**
         * id : 135
         * first_name : Vet
         * last_name : Testing
         * vet_type : Horses (equine),dogs (cannies)
         * expertise : July 2020
         * userimage : http://digittrix.com/staging/pongo/assets/images/avtar.png
         * recieve_patient : 1
         * adminstatus : 1
         * "walking": 8
         * "requestor": 8
         */

        private String id;
        private String first_name;
        private String last_name;
        private String vet_type;
        private String expertise;
        private String userimage;
        private String recieve_patient;
        private String adminstatus;
        private String walking;
        private String requestor;

        public String getRequestor() {
            return requestor;
        }

        public void setRequestor(String requestor) {
            this.requestor = requestor;
        }


        public String getWalking() {
            return walking;
        }

        public void setWalking(String walking) {
            this.walking = walking;
        }


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getVet_type() {
            return vet_type;
        }

        public void setVet_type(String vet_type) {
            this.vet_type = vet_type;
        }

        public String getExpertise() {
            return expertise;
        }

        public void setExpertise(String expertise) {
            this.expertise = expertise;
        }

        public String getUserimage() {
            return userimage;
        }

        public void setUserimage(String userimage) {
            this.userimage = userimage;
        }

        public String getRecieve_patient() {
            return recieve_patient;
        }

        public void setRecieve_patient(String recieve_patient) {
            this.recieve_patient = recieve_patient;
        }

        public String getAdminstatus() {
            return adminstatus;
        }

        public void setAdminstatus(String adminstatus) {
            this.adminstatus = adminstatus;
        }
    }
}
