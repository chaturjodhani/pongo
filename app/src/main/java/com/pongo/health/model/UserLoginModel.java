package com.pongo.health.model;

import java.util.List;

public class UserLoginModel {


    /**
     * status : success
     * otp :
     * userinfo : [{"id":"33","first_name":"daman","last_name":"preet","email":"daman@gmail.com","vet_type":"","phone":"4444444"}]
     * userdetail : {"id":"33","first_name":"daman","last_name":"preet","email":"daman@gmail.com","vet_type":"","phone":"4444444"}
     * message : login successfully
     */

    private String status;
    private String otp;
    private UserdetailBean userdetail;
    private String message;
    private List<UserinfoBean> userinfo;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public UserdetailBean getUserdetail() {
        return userdetail;
    }

    public void setUserdetail(UserdetailBean userdetail) {
        this.userdetail = userdetail;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserinfoBean> getUserinfo() {
        return userinfo;
    }

    public void setUserinfo(List<UserinfoBean> userinfo) {
        this.userinfo = userinfo;
    }

    public static class UserdetailBean {
        /**
         * id : 33
         * first_name : daman
         * last_name : preet
         * email : daman@gmail.com
         * vet_type :
         * phone : 4444444
         */

        private String id;
        private String first_name;
        private String last_name;
        private String email;
        private String vet_type;
        private String phone;
        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }

        private String usertype;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getVet_type() {
            return vet_type;
        }

        public void setVet_type(String vet_type) {
            this.vet_type = vet_type;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }

    public static class UserinfoBean {
        /**
         * id : 33
         * first_name : daman
         * last_name : preet
         * email : daman@gmail.com
         * vet_type :
         * phone : 4444444
         */

        private String id;
        private String first_name;
        private String last_name;
        private String email;
        private String vet_type;
        private String phone;

        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }

        private String usertype;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getVet_type() {
            return vet_type;
        }

        public void setVet_type(String vet_type) {
            this.vet_type = vet_type;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }
}
