package com.pongo.health.model;

import java.util.List;

public class ProductResponseModel {

    /**
     * status : success
     * "totalpages": 41,
     * filter : [{"id":"1","name":"Shop By Brands","icon":""},{"id":"2","name":"Food","icon":""},{"id":"3","name":"Treats","icon":""},{"id":"4","name":"Toys","icon":""},{"id":"5","name":"HealthCare","icon":""},{"id":"6","name":"DentalCare","icon":""}]
     * productlist : [{"id":"1","name":"Pro11","description":"Testing","orignalprice":"122","discountprice":"","product_type":true,"product_image":"http://digittrix.com/staging/pongo/assets/products/1.png"}]
     */

    private String status;
    private int totalpages;
    private List<FilterBean> filter;
    private List<ProductlistBean> productlist;

    public int getTotalpages() {
        return totalpages;
    }

    public void setTotalpages(int totalpages) {
        this.totalpages = totalpages;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<FilterBean> getFilter() {
        return filter;
    }

    public void setFilter(List<FilterBean> filter) {
        this.filter = filter;
    }

    public List<ProductlistBean> getProductlist() {
        return productlist;
    }

    public void setProductlist(List<ProductlistBean> productlist) {
        this.productlist = productlist;
    }

    public static class FilterBean {
        /**
         * id : 1
         * name : Shop By Brands
         * icon :
         */

        private String id;
        private String name;
        private String icon;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }
    }

    public static class ProductlistBean {
        /**
         * id : 1
         * name : Pro11
         * description : Testing
         * orignalprice : 122
         * discountprice :
         * product_type : true
         * product_image : http://digittrix.com/staging/pongo/assets/products/1.png
         */

        private String id;
        private String name;
        private String description;
        private String orignalprice;
        private String discountprice;
        private boolean product_type;
        private String product_image;
        private String category;
        private String subcategory;

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getSubcategory() {
            return subcategory;
        }

        public void setSubcategory(String subcategory) {
            this.subcategory = subcategory;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getOrignalprice() {
            return orignalprice;
        }

        public void setOrignalprice(String orignalprice) {
            this.orignalprice = orignalprice;
        }

        public String getDiscountprice() {
            return discountprice;
        }

        public void setDiscountprice(String discountprice) {
            this.discountprice = discountprice;
        }

        public boolean isProduct_type() {
            return product_type;
        }

        public void setProduct_type(boolean product_type) {
            this.product_type = product_type;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }
    }
}
