package com.pongo.health.model;

import java.util.List;

public class EarningModel {

    /**
     * status : success
     * patient_seen : 6
     * earning : 2.00
     * pethistory : [{"pet_id":"141","user_id":"144","pet_name":"staila","pet_type":"cat","petimage":"http://digittrix.com/staging/pongo/assets/images/petimage/1595653221229.jpg","breed":"Somali","pet_age":"2-8 years","date":"06-08-2020","price":"","pastdoctor":[{"id":"135","name":"Vet Testing","userimg":"http://digittrix.com/staging/pongo/assets/images/profile/1596797470_572013.jpeg","problems":"General Checkup"},{"id":"135","name":"Vet Testing","userimg":"http://digittrix.com/staging/pongo/assets/images/profile/1596797470_572013.jpeg","problems":"Behavioral,Diarrhea"},{"id":"135","name":"Vet Testing","userimg":"http://digittrix.com/staging/pongo/assets/images/profile/1596797470_572013.jpeg","problems":"Behavioral"}],"attachments":["http://digittrix.com/staging/pongo/assets/images/patient/Paper_281.pdf","http://digittrix.com/staging/pongo/assets/images/patient/IMG-20200701-WA0028.jpg"],"problemsissue":"General Checkup","currentissue":["General Checkup"]}]
     */

    private String status;
    private int patient_seen;
    private String earning;
    private List<PethistoryBean> pethistory;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPatient_seen() {
        return patient_seen;
    }

    public void setPatient_seen(int patient_seen) {
        this.patient_seen = patient_seen;
    }

    public String getEarning() {
        return earning;
    }

    public void setEarning(String earning) {
        this.earning = earning;
    }

    public List<PethistoryBean> getPethistory() {
        return pethistory;
    }

    public void setPethistory(List<PethistoryBean> pethistory) {
        this.pethistory = pethistory;
    }

    public static class PethistoryBean {
        /**
         * pet_id : 141
         * user_id : 144
         * pet_name : staila
         * pet_type : cat
         * petimage : http://digittrix.com/staging/pongo/assets/images/petimage/1595653221229.jpg
         * breed : Somali
         * pet_age : 2-8 years
         * date : 06-08-2020
         * price :
         * pastdoctor : [{"id":"135","name":"Vet Testing","userimg":"http://digittrix.com/staging/pongo/assets/images/profile/1596797470_572013.jpeg","problems":"General Checkup"},{"id":"135","name":"Vet Testing","userimg":"http://digittrix.com/staging/pongo/assets/images/profile/1596797470_572013.jpeg","problems":"Behavioral,Diarrhea"},{"id":"135","name":"Vet Testing","userimg":"http://digittrix.com/staging/pongo/assets/images/profile/1596797470_572013.jpeg","problems":"Behavioral"}]
         * attachments : ["http://digittrix.com/staging/pongo/assets/images/patient/Paper_281.pdf","http://digittrix.com/staging/pongo/assets/images/patient/IMG-20200701-WA0028.jpg"]
         * problemsissue : General Checkup
         * currentissue : ["General Checkup"]
         */

        private String pet_id;
        private String user_id;
        private String pet_name;
        private String pet_type;
        private String petimage;
        private String breed;
        private String pet_age;
        private String date;
        private String price;
        private String problemsissue;
        private List<PastdoctorBean> pastdoctor;
        private List<String> attachments;
        private List<String> currentissue;

        public String getPet_id() {
            return pet_id;
        }

        public void setPet_id(String pet_id) {
            this.pet_id = pet_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getPet_name() {
            return pet_name;
        }

        public void setPet_name(String pet_name) {
            this.pet_name = pet_name;
        }

        public String getPet_type() {
            return pet_type;
        }

        public void setPet_type(String pet_type) {
            this.pet_type = pet_type;
        }

        public String getPetimage() {
            return petimage;
        }

        public void setPetimage(String petimage) {
            this.petimage = petimage;
        }

        public String getBreed() {
            return breed;
        }

        public void setBreed(String breed) {
            this.breed = breed;
        }

        public String getPet_age() {
            return pet_age;
        }

        public void setPet_age(String pet_age) {
            this.pet_age = pet_age;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getProblemsissue() {
            return problemsissue;
        }

        public void setProblemsissue(String problemsissue) {
            this.problemsissue = problemsissue;
        }

        public List<PastdoctorBean> getPastdoctor() {
            return pastdoctor;
        }

        public void setPastdoctor(List<PastdoctorBean> pastdoctor) {
            this.pastdoctor = pastdoctor;
        }

        public List<String> getAttachments() {
            return attachments;
        }

        public void setAttachments(List<String> attachments) {
            this.attachments = attachments;
        }

        public List<String> getCurrentissue() {
            return currentissue;
        }

        public void setCurrentissue(List<String> currentissue) {
            this.currentissue = currentissue;
        }

        public static class PastdoctorBean {
            /**
             * id : 135
             * name : Vet Testing
             * userimg : http://digittrix.com/staging/pongo/assets/images/profile/1596797470_572013.jpeg
             * problems : General Checkup
             */

            private String id;
            private String name;
            private String userimg;
            private String problems;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getUserimg() {
                return userimg;
            }

            public void setUserimg(String userimg) {
                this.userimg = userimg;
            }

            public String getProblems() {
                return problems;
            }

            public void setProblems(String problems) {
                this.problems = problems;
            }
        }
    }
}
