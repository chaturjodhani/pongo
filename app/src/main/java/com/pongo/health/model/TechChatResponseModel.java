package com.pongo.health.model;

import java.util.List;

public class TechChatResponseModel {


    /**
     * status : success
     * wating : [{"id":"75","user_id":"75","pet_name":"dummy","pet_type":"Dog","petimage":"http://digittrix.com/staging/pongo/assets/images/petimage/1592555021_985806.jpeg","breed":"Pomeranian","status":"processing","pet_age":"19/06/2018","token":["f0JvxLVSTMuDLThZXX2Yqo:APA91bEVROd4J5F-GbLzZOvTgjGxUjFDqmx11Wqf3JJS3rV5yDlw8ahMp9q9NRrt51StqLidAYUp7QheTCvVRXLDh7oL4MeNDezqhnaECLc1UN5rqn103rpX61RxUMWFgzroLHyj-QRU","cMD61i15RUzbmbnZmK4gPd:APA91bGBLinvD3L-ZWBjmhNqK0NcByAfKVrrECsSsCQrGTF8Je5mcgftJp5iqLL8xc6wzPmjmhQmH4tZ8HvWmk17r3Rw40HwzJk1CAX2xdCaJDSVTtknMsjs_I9pjF9szYVBZM-52H2a","dGIyPjsmQk9dmcx5dW-_yz:APA91bEOMn-Mk2N7uaEb5LTvyEzv7UYkcXLWHKS60R4qNeAs6mWaM88vGe_locZXb16aCuKgoiv0niHPKD4fhdZ21w2VCd8feMiLWBMXa-639zz7tmBRYeo7a5n11RHZ1LB-PFjRtYXW","ev0cf1cQtEtnkFUajnIQ8g:APA91bHQDgvDlFgXB4sP3m57BPIdszoQK0A84Zo03gzLdbX-HYib8G7YJ9mxDKwPenKwtLfNK-4I50s-b86y10mle--eIGxB3XWXXEn8R7HWfBXMoEKvOnpV1oI5AKrJXMs1osvnh4Cc","f47NJLN7wkzUg-O0-JizIH:APA91bF3j9jhAcFLbdmQNIiCUiLYoj7tZ07bI1EFraQWK5QFdAC6vx2DrgvOmwUk-NOY80tZVFWs9rDWScRlPsIGhQNMTGadSJuuwmj2wbpab6FfHqHhYBSeC558n9CRrTUt31JQjkjA","fz0Ui7G7RxuO_6s-TnyZUy:APA91bEYHL96rI771uRmbFfPut0JhRgrFRaspR2WBVqIaU45mofuwzcL-ws2yWATQTqwmrfs7tI_4iIIrgq7tR0jLEB1DPgD-T-2jVc7rjiRD0--xFb7T9mzftIHEtq-diZC4UclEKPw","eYwt5womlkOImY5u7D6jDV:APA91bG8XKSEZNS3WF1AYRC6XQRrG85BylxlEibez7EZ1PgD6iH01oh2AhfIMKf2iZzG6qNKleC_oMnAFms3Jzx4uZfWEMNL1_4fv-zZZ8WF4_MX8QMQCdKhwlk6A2xZ11HEsHMwKIj-","c0hY-hiYQaKx7BdUddu5I7:APA91bGUNb8shqhCg0waqi6RJB0-Xv2_BLsatvTSviId_pRnGWAReuYf-9jKPIOp92_j2eUxRrVkLrF_KZvYOrwZoP8VqNI9wPKKxpfBAeZ4FA17z9nO9JZHzRz1g2pi42wJLqWfVOM4","cFdQcwGXRo6LQVUji5mO31:APA91bH7mXqiOodrAa_ShUzX8MK3fN3bi9a1TwFiHZyfcqB8tP6PGswlDlmgTdpsHFcgxDB9rZ4FbDc_wkwPv48QUnddYA8dcrBs6nZcfkBYBLf7fSe1ejkai_1rWihelMMIjl9yq4uP"]}]
     * processing : [{"id":"75","user_id":"75","pet_name":"dummy","pet_type":"Dog","petimage":"http://digittrix.com/staging/pongo/assets/images/petimage/1592555021_985806.jpeg","breed":"Pomeranian","status":"processing","pet_age":"19/06/2018","token":["f0JvxLVSTMuDLThZXX2Yqo:APA91bEVROd4J5F-GbLzZOvTgjGxUjFDqmx11Wqf3JJS3rV5yDlw8ahMp9q9NRrt51StqLidAYUp7QheTCvVRXLDh7oL4MeNDezqhnaECLc1UN5rqn103rpX61RxUMWFgzroLHyj-QRU","cMD61i15RUzbmbnZmK4gPd:APA91bGBLinvD3L-ZWBjmhNqK0NcByAfKVrrECsSsCQrGTF8Je5mcgftJp5iqLL8xc6wzPmjmhQmH4tZ8HvWmk17r3Rw40HwzJk1CAX2xdCaJDSVTtknMsjs_I9pjF9szYVBZM-52H2a","dGIyPjsmQk9dmcx5dW-_yz:APA91bEOMn-Mk2N7uaEb5LTvyEzv7UYkcXLWHKS60R4qNeAs6mWaM88vGe_locZXb16aCuKgoiv0niHPKD4fhdZ21w2VCd8feMiLWBMXa-639zz7tmBRYeo7a5n11RHZ1LB-PFjRtYXW","ev0cf1cQtEtnkFUajnIQ8g:APA91bHQDgvDlFgXB4sP3m57BPIdszoQK0A84Zo03gzLdbX-HYib8G7YJ9mxDKwPenKwtLfNK-4I50s-b86y10mle--eIGxB3XWXXEn8R7HWfBXMoEKvOnpV1oI5AKrJXMs1osvnh4Cc","f47NJLN7wkzUg-O0-JizIH:APA91bF3j9jhAcFLbdmQNIiCUiLYoj7tZ07bI1EFraQWK5QFdAC6vx2DrgvOmwUk-NOY80tZVFWs9rDWScRlPsIGhQNMTGadSJuuwmj2wbpab6FfHqHhYBSeC558n9CRrTUt31JQjkjA","fz0Ui7G7RxuO_6s-TnyZUy:APA91bEYHL96rI771uRmbFfPut0JhRgrFRaspR2WBVqIaU45mofuwzcL-ws2yWATQTqwmrfs7tI_4iIIrgq7tR0jLEB1DPgD-T-2jVc7rjiRD0--xFb7T9mzftIHEtq-diZC4UclEKPw","eYwt5womlkOImY5u7D6jDV:APA91bG8XKSEZNS3WF1AYRC6XQRrG85BylxlEibez7EZ1PgD6iH01oh2AhfIMKf2iZzG6qNKleC_oMnAFms3Jzx4uZfWEMNL1_4fv-zZZ8WF4_MX8QMQCdKhwlk6A2xZ11HEsHMwKIj-","c0hY-hiYQaKx7BdUddu5I7:APA91bGUNb8shqhCg0waqi6RJB0-Xv2_BLsatvTSviId_pRnGWAReuYf-9jKPIOp92_j2eUxRrVkLrF_KZvYOrwZoP8VqNI9wPKKxpfBAeZ4FA17z9nO9JZHzRz1g2pi42wJLqWfVOM4","cFdQcwGXRo6LQVUji5mO31:APA91bH7mXqiOodrAa_ShUzX8MK3fN3bi9a1TwFiHZyfcqB8tP6PGswlDlmgTdpsHFcgxDB9rZ4FbDc_wkwPv48QUnddYA8dcrBs6nZcfkBYBLf7fSe1ejkai_1rWihelMMIjl9yq4uP"]}]
     * complete : [{"id":"75","user_id":"75","pet_name":"dummy","pet_type":"Dog","petimage":"http://digittrix.com/staging/pongo/assets/images/petimage/1592555021_985806.jpeg","breed":"Pomeranian","status":"processing","pet_age":"19/06/2018","token":["f0JvxLVSTMuDLThZXX2Yqo:APA91bEVROd4J5F-GbLzZOvTgjGxUjFDqmx11Wqf3JJS3rV5yDlw8ahMp9q9NRrt51StqLidAYUp7QheTCvVRXLDh7oL4MeNDezqhnaECLc1UN5rqn103rpX61RxUMWFgzroLHyj-QRU","cMD61i15RUzbmbnZmK4gPd:APA91bGBLinvD3L-ZWBjmhNqK0NcByAfKVrrECsSsCQrGTF8Je5mcgftJp5iqLL8xc6wzPmjmhQmH4tZ8HvWmk17r3Rw40HwzJk1CAX2xdCaJDSVTtknMsjs_I9pjF9szYVBZM-52H2a","dGIyPjsmQk9dmcx5dW-_yz:APA91bEOMn-Mk2N7uaEb5LTvyEzv7UYkcXLWHKS60R4qNeAs6mWaM88vGe_locZXb16aCuKgoiv0niHPKD4fhdZ21w2VCd8feMiLWBMXa-639zz7tmBRYeo7a5n11RHZ1LB-PFjRtYXW","ev0cf1cQtEtnkFUajnIQ8g:APA91bHQDgvDlFgXB4sP3m57BPIdszoQK0A84Zo03gzLdbX-HYib8G7YJ9mxDKwPenKwtLfNK-4I50s-b86y10mle--eIGxB3XWXXEn8R7HWfBXMoEKvOnpV1oI5AKrJXMs1osvnh4Cc","f47NJLN7wkzUg-O0-JizIH:APA91bF3j9jhAcFLbdmQNIiCUiLYoj7tZ07bI1EFraQWK5QFdAC6vx2DrgvOmwUk-NOY80tZVFWs9rDWScRlPsIGhQNMTGadSJuuwmj2wbpab6FfHqHhYBSeC558n9CRrTUt31JQjkjA","fz0Ui7G7RxuO_6s-TnyZUy:APA91bEYHL96rI771uRmbFfPut0JhRgrFRaspR2WBVqIaU45mofuwzcL-ws2yWATQTqwmrfs7tI_4iIIrgq7tR0jLEB1DPgD-T-2jVc7rjiRD0--xFb7T9mzftIHEtq-diZC4UclEKPw","eYwt5womlkOImY5u7D6jDV:APA91bG8XKSEZNS3WF1AYRC6XQRrG85BylxlEibez7EZ1PgD6iH01oh2AhfIMKf2iZzG6qNKleC_oMnAFms3Jzx4uZfWEMNL1_4fv-zZZ8WF4_MX8QMQCdKhwlk6A2xZ11HEsHMwKIj-","c0hY-hiYQaKx7BdUddu5I7:APA91bGUNb8shqhCg0waqi6RJB0-Xv2_BLsatvTSviId_pRnGWAReuYf-9jKPIOp92_j2eUxRrVkLrF_KZvYOrwZoP8VqNI9wPKKxpfBAeZ4FA17z9nO9JZHzRz1g2pi42wJLqWfVOM4","cFdQcwGXRo6LQVUji5mO31:APA91bH7mXqiOodrAa_ShUzX8MK3fN3bi9a1TwFiHZyfcqB8tP6PGswlDlmgTdpsHFcgxDB9rZ4FbDc_wkwPv48QUnddYA8dcrBs6nZcfkBYBLf7fSe1ejkai_1rWihelMMIjl9yq4uP"]}]
     */

    private String status;
    private List<WatingBean> wating;
    private List<ProcessingBean> processing;
    private List<CompleteBean> complete;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<WatingBean> getWating() {
        return wating;
    }

    public void setWating(List<WatingBean> wating) {
        this.wating = wating;
    }

    public List<ProcessingBean> getProcessing() {
        return processing;
    }

    public void setProcessing(List<ProcessingBean> processing) {
        this.processing = processing;
    }

    public List<CompleteBean> getComplete() {
        return complete;
    }

    public void setComplete(List<CompleteBean> complete) {
        this.complete = complete;
    }

    public static class WatingBean {
        /**
         * id : 75
         * user_id : 75
         * pet_name : dummy
         * pet_type : Dog
         * petimage : http://digittrix.com/staging/pongo/assets/images/petimage/1592555021_985806.jpeg
         * breed : Pomeranian
         * status : processing
         * pet_age : 19/06/2018
         * token : ["f0JvxLVSTMuDLThZXX2Yqo:APA91bEVROd4J5F-GbLzZOvTgjGxUjFDqmx11Wqf3JJS3rV5yDlw8ahMp9q9NRrt51StqLidAYUp7QheTCvVRXLDh7oL4MeNDezqhnaECLc1UN5rqn103rpX61RxUMWFgzroLHyj-QRU","cMD61i15RUzbmbnZmK4gPd:APA91bGBLinvD3L-ZWBjmhNqK0NcByAfKVrrECsSsCQrGTF8Je5mcgftJp5iqLL8xc6wzPmjmhQmH4tZ8HvWmk17r3Rw40HwzJk1CAX2xdCaJDSVTtknMsjs_I9pjF9szYVBZM-52H2a","dGIyPjsmQk9dmcx5dW-_yz:APA91bEOMn-Mk2N7uaEb5LTvyEzv7UYkcXLWHKS60R4qNeAs6mWaM88vGe_locZXb16aCuKgoiv0niHPKD4fhdZ21w2VCd8feMiLWBMXa-639zz7tmBRYeo7a5n11RHZ1LB-PFjRtYXW","ev0cf1cQtEtnkFUajnIQ8g:APA91bHQDgvDlFgXB4sP3m57BPIdszoQK0A84Zo03gzLdbX-HYib8G7YJ9mxDKwPenKwtLfNK-4I50s-b86y10mle--eIGxB3XWXXEn8R7HWfBXMoEKvOnpV1oI5AKrJXMs1osvnh4Cc","f47NJLN7wkzUg-O0-JizIH:APA91bF3j9jhAcFLbdmQNIiCUiLYoj7tZ07bI1EFraQWK5QFdAC6vx2DrgvOmwUk-NOY80tZVFWs9rDWScRlPsIGhQNMTGadSJuuwmj2wbpab6FfHqHhYBSeC558n9CRrTUt31JQjkjA","fz0Ui7G7RxuO_6s-TnyZUy:APA91bEYHL96rI771uRmbFfPut0JhRgrFRaspR2WBVqIaU45mofuwzcL-ws2yWATQTqwmrfs7tI_4iIIrgq7tR0jLEB1DPgD-T-2jVc7rjiRD0--xFb7T9mzftIHEtq-diZC4UclEKPw","eYwt5womlkOImY5u7D6jDV:APA91bG8XKSEZNS3WF1AYRC6XQRrG85BylxlEibez7EZ1PgD6iH01oh2AhfIMKf2iZzG6qNKleC_oMnAFms3Jzx4uZfWEMNL1_4fv-zZZ8WF4_MX8QMQCdKhwlk6A2xZ11HEsHMwKIj-","c0hY-hiYQaKx7BdUddu5I7:APA91bGUNb8shqhCg0waqi6RJB0-Xv2_BLsatvTSviId_pRnGWAReuYf-9jKPIOp92_j2eUxRrVkLrF_KZvYOrwZoP8VqNI9wPKKxpfBAeZ4FA17z9nO9JZHzRz1g2pi42wJLqWfVOM4","cFdQcwGXRo6LQVUji5mO31:APA91bH7mXqiOodrAa_ShUzX8MK3fN3bi9a1TwFiHZyfcqB8tP6PGswlDlmgTdpsHFcgxDB9rZ4FbDc_wkwPv48QUnddYA8dcrBs6nZcfkBYBLf7fSe1ejkai_1rWihelMMIjl9yq4uP"]
         */

        private String id;
        private String user_id;
        private String pet_name;
        private String pet_type;
        private String petimage;
        private String breed;
        private String status;
        private String pet_age;

        public String getChatid() {
            return chatid;
        }

        public void setChatid(String chatid) {
            this.chatid = chatid;
        }

        private String chatid;
        private List<String> token;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getPet_name() {
            return pet_name;
        }

        public void setPet_name(String pet_name) {
            this.pet_name = pet_name;
        }

        public String getPet_type() {
            return pet_type;
        }

        public void setPet_type(String pet_type) {
            this.pet_type = pet_type;
        }

        public String getPetimage() {
            return petimage;
        }

        public void setPetimage(String petimage) {
            this.petimage = petimage;
        }

        public String getBreed() {
            return breed;
        }

        public void setBreed(String breed) {
            this.breed = breed;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPet_age() {
            return pet_age;
        }

        public void setPet_age(String pet_age) {
            this.pet_age = pet_age;
        }

        public List<String> getToken() {
            return token;
        }

        public void setToken(List<String> token) {
            this.token = token;
        }
    }

    public static class ProcessingBean {
        /**
         * id : 75
         * user_id : 75
         * pet_name : dummy
         * pet_type : Dog
         * petimage : http://digittrix.com/staging/pongo/assets/images/petimage/1592555021_985806.jpeg
         * breed : Pomeranian
         * status : processing
         * pet_age : 19/06/2018
         * token : ["f0JvxLVSTMuDLThZXX2Yqo:APA91bEVROd4J5F-GbLzZOvTgjGxUjFDqmx11Wqf3JJS3rV5yDlw8ahMp9q9NRrt51StqLidAYUp7QheTCvVRXLDh7oL4MeNDezqhnaECLc1UN5rqn103rpX61RxUMWFgzroLHyj-QRU","cMD61i15RUzbmbnZmK4gPd:APA91bGBLinvD3L-ZWBjmhNqK0NcByAfKVrrECsSsCQrGTF8Je5mcgftJp5iqLL8xc6wzPmjmhQmH4tZ8HvWmk17r3Rw40HwzJk1CAX2xdCaJDSVTtknMsjs_I9pjF9szYVBZM-52H2a","dGIyPjsmQk9dmcx5dW-_yz:APA91bEOMn-Mk2N7uaEb5LTvyEzv7UYkcXLWHKS60R4qNeAs6mWaM88vGe_locZXb16aCuKgoiv0niHPKD4fhdZ21w2VCd8feMiLWBMXa-639zz7tmBRYeo7a5n11RHZ1LB-PFjRtYXW","ev0cf1cQtEtnkFUajnIQ8g:APA91bHQDgvDlFgXB4sP3m57BPIdszoQK0A84Zo03gzLdbX-HYib8G7YJ9mxDKwPenKwtLfNK-4I50s-b86y10mle--eIGxB3XWXXEn8R7HWfBXMoEKvOnpV1oI5AKrJXMs1osvnh4Cc","f47NJLN7wkzUg-O0-JizIH:APA91bF3j9jhAcFLbdmQNIiCUiLYoj7tZ07bI1EFraQWK5QFdAC6vx2DrgvOmwUk-NOY80tZVFWs9rDWScRlPsIGhQNMTGadSJuuwmj2wbpab6FfHqHhYBSeC558n9CRrTUt31JQjkjA","fz0Ui7G7RxuO_6s-TnyZUy:APA91bEYHL96rI771uRmbFfPut0JhRgrFRaspR2WBVqIaU45mofuwzcL-ws2yWATQTqwmrfs7tI_4iIIrgq7tR0jLEB1DPgD-T-2jVc7rjiRD0--xFb7T9mzftIHEtq-diZC4UclEKPw","eYwt5womlkOImY5u7D6jDV:APA91bG8XKSEZNS3WF1AYRC6XQRrG85BylxlEibez7EZ1PgD6iH01oh2AhfIMKf2iZzG6qNKleC_oMnAFms3Jzx4uZfWEMNL1_4fv-zZZ8WF4_MX8QMQCdKhwlk6A2xZ11HEsHMwKIj-","c0hY-hiYQaKx7BdUddu5I7:APA91bGUNb8shqhCg0waqi6RJB0-Xv2_BLsatvTSviId_pRnGWAReuYf-9jKPIOp92_j2eUxRrVkLrF_KZvYOrwZoP8VqNI9wPKKxpfBAeZ4FA17z9nO9JZHzRz1g2pi42wJLqWfVOM4","cFdQcwGXRo6LQVUji5mO31:APA91bH7mXqiOodrAa_ShUzX8MK3fN3bi9a1TwFiHZyfcqB8tP6PGswlDlmgTdpsHFcgxDB9rZ4FbDc_wkwPv48QUnddYA8dcrBs6nZcfkBYBLf7fSe1ejkai_1rWihelMMIjl9yq4uP"]
         */

        private String id;
        private String user_id;
        private String pet_name;
        private String pet_type;
        private String petimage;
        private String breed;
        private String status;
        private String pet_age;

        public String getChatid() {
            return chatid;
        }

        public void setChatid(String chatid) {
            this.chatid = chatid;
        }

        private String chatid;
        private List<String> token;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getPet_name() {
            return pet_name;
        }

        public void setPet_name(String pet_name) {
            this.pet_name = pet_name;
        }

        public String getPet_type() {
            return pet_type;
        }

        public void setPet_type(String pet_type) {
            this.pet_type = pet_type;
        }

        public String getPetimage() {
            return petimage;
        }

        public void setPetimage(String petimage) {
            this.petimage = petimage;
        }

        public String getBreed() {
            return breed;
        }

        public void setBreed(String breed) {
            this.breed = breed;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPet_age() {
            return pet_age;
        }

        public void setPet_age(String pet_age) {
            this.pet_age = pet_age;
        }

        public List<String> getToken() {
            return token;
        }

        public void setToken(List<String> token) {
            this.token = token;
        }
    }

    public static class CompleteBean {
        /**
         * id : 75
         * user_id : 75
         * pet_name : dummy
         * pet_type : Dog
         * petimage : http://digittrix.com/staging/pongo/assets/images/petimage/1592555021_985806.jpeg
         * breed : Pomeranian
         * status : processing
         * pet_age : 19/06/2018
         * token : ["f0JvxLVSTMuDLThZXX2Yqo:APA91bEVROd4J5F-GbLzZOvTgjGxUjFDqmx11Wqf3JJS3rV5yDlw8ahMp9q9NRrt51StqLidAYUp7QheTCvVRXLDh7oL4MeNDezqhnaECLc1UN5rqn103rpX61RxUMWFgzroLHyj-QRU","cMD61i15RUzbmbnZmK4gPd:APA91bGBLinvD3L-ZWBjmhNqK0NcByAfKVrrECsSsCQrGTF8Je5mcgftJp5iqLL8xc6wzPmjmhQmH4tZ8HvWmk17r3Rw40HwzJk1CAX2xdCaJDSVTtknMsjs_I9pjF9szYVBZM-52H2a","dGIyPjsmQk9dmcx5dW-_yz:APA91bEOMn-Mk2N7uaEb5LTvyEzv7UYkcXLWHKS60R4qNeAs6mWaM88vGe_locZXb16aCuKgoiv0niHPKD4fhdZ21w2VCd8feMiLWBMXa-639zz7tmBRYeo7a5n11RHZ1LB-PFjRtYXW","ev0cf1cQtEtnkFUajnIQ8g:APA91bHQDgvDlFgXB4sP3m57BPIdszoQK0A84Zo03gzLdbX-HYib8G7YJ9mxDKwPenKwtLfNK-4I50s-b86y10mle--eIGxB3XWXXEn8R7HWfBXMoEKvOnpV1oI5AKrJXMs1osvnh4Cc","f47NJLN7wkzUg-O0-JizIH:APA91bF3j9jhAcFLbdmQNIiCUiLYoj7tZ07bI1EFraQWK5QFdAC6vx2DrgvOmwUk-NOY80tZVFWs9rDWScRlPsIGhQNMTGadSJuuwmj2wbpab6FfHqHhYBSeC558n9CRrTUt31JQjkjA","fz0Ui7G7RxuO_6s-TnyZUy:APA91bEYHL96rI771uRmbFfPut0JhRgrFRaspR2WBVqIaU45mofuwzcL-ws2yWATQTqwmrfs7tI_4iIIrgq7tR0jLEB1DPgD-T-2jVc7rjiRD0--xFb7T9mzftIHEtq-diZC4UclEKPw","eYwt5womlkOImY5u7D6jDV:APA91bG8XKSEZNS3WF1AYRC6XQRrG85BylxlEibez7EZ1PgD6iH01oh2AhfIMKf2iZzG6qNKleC_oMnAFms3Jzx4uZfWEMNL1_4fv-zZZ8WF4_MX8QMQCdKhwlk6A2xZ11HEsHMwKIj-","c0hY-hiYQaKx7BdUddu5I7:APA91bGUNb8shqhCg0waqi6RJB0-Xv2_BLsatvTSviId_pRnGWAReuYf-9jKPIOp92_j2eUxRrVkLrF_KZvYOrwZoP8VqNI9wPKKxpfBAeZ4FA17z9nO9JZHzRz1g2pi42wJLqWfVOM4","cFdQcwGXRo6LQVUji5mO31:APA91bH7mXqiOodrAa_ShUzX8MK3fN3bi9a1TwFiHZyfcqB8tP6PGswlDlmgTdpsHFcgxDB9rZ4FbDc_wkwPv48QUnddYA8dcrBs6nZcfkBYBLf7fSe1ejkai_1rWihelMMIjl9yq4uP"]
         */

        private String id;
        private String user_id;
        private String pet_name;
        private String pet_type;
        private String petimage;
        private String breed;
        private String status;
        private String pet_age;

        public String getChatid() {
            return chatid;
        }

        public void setChatid(String chatid) {
            this.chatid = chatid;
        }

        private String chatid;
        private List<String> token;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getPet_name() {
            return pet_name;
        }

        public void setPet_name(String pet_name) {
            this.pet_name = pet_name;
        }

        public String getPet_type() {
            return pet_type;
        }

        public void setPet_type(String pet_type) {
            this.pet_type = pet_type;
        }

        public String getPetimage() {
            return petimage;
        }

        public void setPetimage(String petimage) {
            this.petimage = petimage;
        }

        public String getBreed() {
            return breed;
        }

        public void setBreed(String breed) {
            this.breed = breed;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPet_age() {
            return pet_age;
        }

        public void setPet_age(String pet_age) {
            this.pet_age = pet_age;
        }

        public List<String> getToken() {
            return token;
        }

        public void setToken(List<String> token) {
            this.token = token;
        }
    }
}
