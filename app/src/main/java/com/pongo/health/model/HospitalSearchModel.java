package com.pongo.health.model;

import java.util.List;

public class HospitalSearchModel {


    /**
     * html_attributions : []
     * results : [{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.720576,"lng":76.7047106},"viewport":{"northeast":{"lat":30.72190052989272,"lng":76.70602017989273},"southwest":{"lat":30.71920087010728,"lng":76.70332052010728}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"4cf7720fb5c39abae5d3a46202b2bdb2bdd39671","name":"AM Hospital Mohali","opening_hours":{"open_now":true},"photos":[{"height":360,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/115057308097687163521\">Atul Mahajan Hospital<\/a>"],"photo_reference":"CmRaAAAALutvN04RwkEKsGRu39GCM7Xawck9CGpqGFGSW4qbgswBLzWn_1R9SA3jRKSkxRN5ouoHrgrUO4JO-Kidc83Mg373Dw3Cp9rGt6DtbKw43jspsg2ymIH8gMFvUJho86TbEhAXXGhXnm7iFjr8Z05NufYqGhTfwOotfUPHJqGSidI1quK2yVvr6w","width":640}],"place_id":"ChIJjx01gV7uDzkRtu8Df04gIII","plus_code":{"compound_code":"PPC3+6V Sector 58, Sahibzada Ajit Singh Nagar, Punjab","global_code":"8J2RPPC3+6V"},"rating":3.9,"reference":"ChIJjx01gV7uDzkRtu8Df04gIII","scope":"GOOGLE","types":["hospital","health","point_of_interest","establishment"],"user_ratings_total":41,"vicinity":"MOHALI BYEPASS, ROAD, Sector 58, Sahibzada Ajit Singh Nagar"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.7649646,"lng":76.7750066},"viewport":{"northeast":{"lat":30.76959569999999,"lng":76.78541605000001},"southwest":{"lat":30.75853410000001,"lng":76.76493144999999}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/school-71.png","id":"599fc5609990fa4cf748f526152620fee6ed938d","name":"Post Graduate Institute of Medical Education & Research, Chandigarh","opening_hours":{"open_now":true},"photos":[{"height":3120,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/111681141054012704642\">Varunn MD<\/a>"],"photo_reference":"CmRaAAAAXTolFS7EUvRiyitus9O3p58l9ArdlAEHsU1GupJDO9Zfw9Ecgq3WLOH5XH_iZxTe-4x_2TKi5gxALgpgYRI0Y-qbfTDjtY1K5trv3TstiYc5NRyBpBy65xFwvUtPwJNfEhBjh-KNssEvm5JnF64VQ1qQGhT1fzzbh8F971HmnUjyZLRhK6E84w","width":4160}],"place_id":"ChIJ-xpmZ3_yDzkRg5rh7uCJQ60","plus_code":{"compound_code":"QQ7G+X2 Sector 12, Chandigarh","global_code":"8J2RQQ7G+X2"},"rating":4.2,"reference":"ChIJ-xpmZ3_yDzkRg5rh7uCJQ60","scope":"GOOGLE","types":["hospital","university","health","point_of_interest","establishment"],"user_ratings_total":1245,"vicinity":"Madhya Marg, Sector 12, Chandigarh"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.708827,"lng":76.7804878},"viewport":{"northeast":{"lat":30.70980122989272,"lng":76.78196282989272},"southwest":{"lat":30.70710157010728,"lng":76.77926317010728}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"611d0d2b2a766089bfa6b91af5b609224aef322c","name":"Government Medical College & Hospital, Chandigarh","opening_hours":{"open_now":true},"photos":[{"height":3000,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/117516022799575723667\">Ayush Malaan<\/a>"],"photo_reference":"CmRZAAAADm4thL9DjfKxQO_KWtNl3G1YFovm-mEnl4VtJmXwdH-o8H_yX4Sp8wMRrKco4vuaxnuAPlwcdnLRwiozOVwmM2A07FVuKByVuT_0MdTh74lj5twMSZZgCqrP_pi77-2ZEhCQY1DcCFy62lPHmp0DnshWGhRqDSLH4d6FskoKkcembGVZeVumlw","width":4000}],"place_id":"ChIJaTv3kFfsDzkRqVrW32d7pcU","plus_code":{"compound_code":"PQ5J+G5 Chandigarh","global_code":"8J2RPQ5J+G5"},"rating":4,"reference":"ChIJaTv3kFfsDzkRqVrW32d7pcU","scope":"GOOGLE","types":["hospital","health","point_of_interest","establishment"],"user_ratings_total":692,"vicinity":"Chandi Path, Sector 32B, 32B, Sector 32, Chandigarh"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.752117,"lng":76.633783},"viewport":{"northeast":{"lat":30.75343417989273,"lng":76.63511902989272},"southwest":{"lat":30.75073452010728,"lng":76.63241937010727}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"c2e767186d5410dd6dcc63a96cfb3f4c8da91e29","name":"Max Healthcare - Ambulance Service in Mohali","opening_hours":{},"place_id":"ChIJsZq486z6DzkRT0ILqpUUa4Q","plus_code":{"compound_code":"QJ2M+RG Kharar, Punjab","global_code":"8J2RQJ2M+RG"},"rating":4,"reference":"ChIJsZq486z6DzkRT0ILqpUUa4Q","scope":"GOOGLE","types":["hospital","health","point_of_interest","establishment"],"user_ratings_total":4,"vicinity":"Civil Hospital Rd, Ward No. 6, Garden Colony, Kharar"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.7089058,"lng":76.7225259},"viewport":{"northeast":{"lat":30.71018902989272,"lng":76.72377372989271},"southwest":{"lat":30.70748937010728,"lng":76.72107407010728}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"ec8d74ecbeb83c941174d0dd1c8def0669a2c360","name":"Indus Hospital","opening_hours":{"open_now":true},"photos":[{"height":520,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/117247535686101419543\">Indus Hospital, Unit Of Indus Healthcare Services Pvt Ltd<\/a>"],"photo_reference":"CmRaAAAA1snoL26Scqek_RFtfnFfCQmHTyq5OZvEcxHi-e--9TCcVTPRE7GlawhrdR8HyPf4wAJLZX1nVD8CDV2nVAOd7udHi41wsxPNi0V_0FnQaPBYhZrteQG6uxZzx4R4YNpuEhDdEWFcWo3esTr7pskeXPTbGhRx7yo2NCX1f1oMMyh_CFCTXkOyqQ","width":500}],"place_id":"ChIJXZ1oBYbuDzkRCYr1GjtLm9U","plus_code":{"compound_code":"PP5F+H2 Sector 60, Sahibzada Ajit Singh Nagar, Punjab","global_code":"8J2RPP5F+H2"},"rating":3.6,"reference":"ChIJXZ1oBYbuDzkRCYr1GjtLm9U","scope":"GOOGLE","types":["hospital","health","point_of_interest","establishment"],"user_ratings_total":142,"vicinity":"SCF 98-100, Near Chawla Chowk, Phase 3B-2, Sector 60, Sahibzada Ajit Singh Nagar"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.7292742,"lng":76.72198709999999},"viewport":{"northeast":{"lat":30.73055647989273,"lng":76.72322822989271},"southwest":{"lat":30.72785682010728,"lng":76.72052857010726}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"17e850bab28242fbcbffb2df9bb26dd83d7121dd","name":"Indus Super Speciality Hospital","opening_hours":{"open_now":true},"photos":[{"height":400,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/112207041590584840037\">Indus Superspecialty Hospital Mohali<\/a>"],"photo_reference":"CmRaAAAAMM-TCuMngpfdGBgbUxhdNRmKoiGlUPe0vQ5HJ37mmkgh1zGXjNErI4NXTXDHpQ9DxAxP9UpVAFlpuC88QoUjaKMGy9-4TWk0SflMhq8u42iBtt-P62oSes9p9gSlDg8MEhBfeMW3eKsDC-IReiqnWaKzGhQrD54H1tMSCO16tB4ThP6GHi1fTA","width":1600}],"place_id":"ChIJX-zShaftDzkR7eBpRltJAWM","plus_code":{"compound_code":"PPHC+PQ Sector 55, Chandigarh","global_code":"8J2RPPHC+PQ"},"rating":3.5,"reference":"ChIJX-zShaftDzkR7eBpRltJAWM","scope":"GOOGLE","types":["hospital","health","point_of_interest","establishment"],"user_ratings_total":137,"vicinity":"opp. Old DC Office, Sector 55, Phase 1, Sector 55, Sahibzada Ajit Singh Nagar"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.6472717,"lng":76.8047668},"viewport":{"northeast":{"lat":30.64862082989271,"lng":76.80614502989273},"southwest":{"lat":30.64592117010727,"lng":76.80344537010728}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"f28278541d974b6f485860681444cb1657f2db39","name":"Trinity Hospital & Medical Research Institute","opening_hours":{"open_now":true},"photos":[{"height":3120,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/114775522694848650519\">akshay semwal<\/a>"],"photo_reference":"CmRaAAAA1dv0VNPSN2Z6T4AhAu43nf9zDgZDO0cw9ySGJMKYpMMkC4qFYJdUcKltktm1BNU3cNTdNhnlO-ekOfjtYpTRUE2fbE_MG_DonCNTrFgDoTmjMLTTWr-KHooZ8T6nq4oEEhDfsY4fo5e6GqaMVcEK-dWYGhTdiqpHOXNXpPKM-9uIiu3eXB5qHg","width":4160}],"place_id":"ChIJuadAuT3rDzkRnkF7X2lKsp4","plus_code":{"compound_code":"JRW3+WW Zirakpur, Punjab","global_code":"8J2RJRW3+WW"},"rating":4.3,"reference":"ChIJuadAuT3rDzkRnkF7X2lKsp4","scope":"GOOGLE","types":["hospital","health","point_of_interest","establishment"],"user_ratings_total":107,"vicinity":"SCO 352-355, Patiala Rd, Swastik Vihar, Zirakpur"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.6963145,"lng":76.7165546},"viewport":{"northeast":{"lat":30.69774867989273,"lng":76.71783477989271},"southwest":{"lat":30.69504902010728,"lng":76.71513512010728}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"3689ea43c7d5a21789134483d559a417df8215dc","name":"Amar Hospital","opening_hours":{"open_now":true},"photos":[{"height":2340,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/105424548938073654128\">Talent Lover<\/a>"],"photo_reference":"CmRaAAAAnJOyPkeOUpkeoQGWTGCMzbTt-NWp7bc1B9yARJA7DjnPBGf-LZtoQr_Wrq7Nhq25AULYo2g_u_fVm_ecK0wmNlgISUrL2cM-Ztvf7pTWiWlCNX9KpPmCUD1uGLuO-llOEhDG8Q8BIygDQZY3OJtDQ0nFGhSjluv8KlbqeaGiyq0e-GL39A1sRw","width":4160}],"place_id":"ChIJ_f__P5HuDzkR8DW98aX2AwI","plus_code":{"compound_code":"MPW8+GJ Sector 70, Sahibzada Ajit Singh Nagar, Punjab","global_code":"8J2RMPW8+GJ"},"rating":3.3,"reference":"ChIJ_f__P5HuDzkR8DW98aX2AwI","scope":"GOOGLE","types":["hospital","health","point_of_interest","establishment"],"user_ratings_total":53,"vicinity":"Sector 70, Sahibzada Ajit Singh Nagar"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.6613145,"lng":76.8183787},"viewport":{"northeast":{"lat":30.66272697989272,"lng":76.81985472989273},"southwest":{"lat":30.66002732010728,"lng":76.81715507010728}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"fa897a3f3d744e739eb4a5235b8beb8b24fd771e","name":"J.P Hospital","opening_hours":{"open_now":true},"photos":[{"height":312,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/111076288221829084076\">A Google User<\/a>"],"photo_reference":"CmRaAAAAi5ejG3t2316WZ0_QKXMp80fjSeg-qYNjAE8d3tUau8ZGaPFxSBCp1NSRyn_RfdboXpk07wU7-1IeDa1apa6zW8QEb1HEzLdrfOUXcj-S600OJNOzx5lyfo-IrOQWh1uBEhAitMnB7wQaAq84TQMxiLNDGhQYkKRzk6zcByRTKbQi4oRxK1YqUg","width":556}],"place_id":"ChIJC241JkrrDzkRalCgFHKjqzU","plus_code":{"compound_code":"MR69+G9 Zirakpur, Punjab","global_code":"8J2RMR69+G9"},"rating":4.6,"reference":"ChIJC241JkrrDzkRalCgFHKjqzU","scope":"GOOGLE","types":["hospital","health","point_of_interest","establishment"],"user_ratings_total":1100,"vicinity":"Near, NH 21, Zirakpur - Chandigarh Highway, Under Flyover, Panchkula, Zirakpur"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.7200019,"lng":76.8532211},"viewport":{"northeast":{"lat":30.72137922989272,"lng":76.85454477989272},"southwest":{"lat":30.71867957010727,"lng":76.85184512010728}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"dc7b3138f135f9480958791de5b023be6bda9147","name":"IVY Hospital Panchkula","opening_hours":{"open_now":true},"photos":[{"height":2592,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/112485025871139037761\">Saini<\/a>"],"photo_reference":"CmRaAAAAus8t5BF1sRlJVadrfq9bxVs4jyiBwlNeu7_6ND9hSa381hQ_6Y_wp7mkehILBjS16gQ1NxTBp8zOzNuu8eT7k-C5OZRn1tCQF1wRtlyZ-42xSfd6ZBmYMsRWwGFSIgkYEhBRq_YYlb45xM77c9-hNVeXGhRzsNM7ZVQ6jtNUeRzQmr9Kzws9gQ","width":4608}],"place_id":"ChIJR5H1FBmTDzkR2qT1TDEr-hE","plus_code":{"compound_code":"PVC3+27 Panchkula, Haryana","global_code":"8J2RPVC3+27"},"rating":3.9,"reference":"ChIJR5H1FBmTDzkR2qT1TDEr-hE","scope":"GOOGLE","types":["hospital","health","point_of_interest","establishment"],"user_ratings_total":100,"vicinity":"MDC, Sector 5C, Panchkula"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.7336092,"lng":76.6470873},"viewport":{"northeast":{"lat":30.73502282989272,"lng":76.64865147989272},"southwest":{"lat":30.73232317010728,"lng":76.64595182010729}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"861c90711dde9a468096a9475cea70e233e9be7c","name":"NuLife Hospital","opening_hours":{"open_now":true},"photos":[{"height":3024,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/114559856922080234341\">A Google User<\/a>"],"photo_reference":"CmRaAAAADOyoD1apS2HIGNBE43LIqnuViQLBWII-wozqhQt_JmsPM-Bo6Yw-A_uwnR_Ru1em-MZvW4MQmT-h2_yRbDOLPQFMohqxuYOT_eZq4gLILKTfAvAWKXh3hz_QZA0Qab70EhCGDlTpkuub9U5kS8Vz_o2IGhQOm7EafMW7d7_mKUxUPXW4K59-8Q","width":4032}],"place_id":"ChIJj07frPbvDzkRYbDZGmqyXQo","plus_code":{"compound_code":"PJMW+CR Sector 115, Sahibzada Ajit Singh Nagar, Punjab","global_code":"8J2RPJMW+CR"},"rating":4.8,"reference":"ChIJj07frPbvDzkRYbDZGmqyXQo","scope":"GOOGLE","types":["hospital","doctor","health","point_of_interest","establishment"],"user_ratings_total":93,"vicinity":"Sector 115, Kharar - Landran Rd, adj. Gillco Valley, Greater Mohali"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.7640182,"lng":76.7798088},"viewport":{"northeast":{"lat":30.76523882989272,"lng":76.78112347989273},"southwest":{"lat":30.76253917010727,"lng":76.77842382010728}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"973764ea018f964dcfe4c0f48f7b176a784be55f","name":"Advanced Eye Centre, PGIMER, Chandigarh","opening_hours":{"open_now":true},"photos":[{"height":1080,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/107792389067553044366\">sahil jaat<\/a>"],"photo_reference":"CmRaAAAAA4X6H0YS9SV6KY4RsV3gboweHFEexWyaNdrnfxrxQ5NRhVyPD5Kv5Wfc88q4fUx0G6yoLycZStRHLPAabOnLhyu-Z-HevSrKMxF87nPu6909y-WfMbtpdJbhXLzANzKbEhDEIsvzjmCgv-6DMxGMy8LIGhS5d4HP8yiDe3Kdiq0Rd-oZg_lkww","width":1920}],"place_id":"ChIJezgH93_tDzkR-fRnby-7T4Y","plus_code":{"compound_code":"QQ7H+JW Sector 12, Chandigarh","global_code":"8J2RQQ7H+JW"},"rating":4.5,"reference":"ChIJezgH93_tDzkR-fRnby-7T4Y","scope":"GOOGLE","types":["hospital","health","point_of_interest","establishment"],"user_ratings_total":139,"vicinity":"Sector 12, Chandigarh"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.6932765,"lng":76.88334909999999},"viewport":{"northeast":{"lat":30.69465412989272,"lng":76.88482237989271},"southwest":{"lat":30.69195447010728,"lng":76.88212272010726}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"cee8c40cdbbe9783ad05a287e4213ea9382d788d","name":"Paras Hospitals, Panchkula","opening_hours":{"open_now":true},"photos":[{"height":3120,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/102887372270940524721\">Ranjit Singh<\/a>"],"photo_reference":"CmRaAAAAZq9cE2hBLeebAVwWdOf-uzZzdIyuOY_oEOHEfSYyZqL3PC3K0ZHmnXFg4w20zr9xUOfmUMVFX3A99LwCAKlAXWf1wu7jRRGsMR5cWasI7rPNYMCEfxDByVXzxuthLD_FEhBLw10VaaTtofnrb3_SmDdNGhQWlVgdGDz8Lf1QuiH9MRWCX4rPPQ","width":4160}],"place_id":"ChIJcXnKhoWTDzkR-pjO8FW91KU","plus_code":{"compound_code":"MVVM+88 Panchkula, Haryana","global_code":"8J2RMVVM+88"},"rating":4.7,"reference":"ChIJcXnKhoWTDzkR-pjO8FW91KU","scope":"GOOGLE","types":["hospital","health","point_of_interest","establishment"],"user_ratings_total":443,"vicinity":"Gurudwara, Paras Hospital, near Nada Sahib, Panchkula"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.69151829999999,"lng":76.7163021},"viewport":{"northeast":{"lat":30.69281127989272,"lng":76.71756007989272},"southwest":{"lat":30.69011162010727,"lng":76.71486042010727}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"4a451e407f5efc1691d4e796177ac118072e6332","name":"Mayo Healthcare Super Specialty Hospital","opening_hours":{"open_now":true},"photos":[{"height":3264,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/112075886105231028929\">frode &amp; cheeter Speciality Hospital<\/a>"],"photo_reference":"CmRaAAAAunBJPl-uTeDmkBNRIeOcbg-YqApWqhr7p8TmA3Wqyz0OuLXTMKfZ9ZT-Yr_mfRHDcdwVWgpwKJ_qiz5NiP8nm3rxGwDPE02fq9WSN7-kmgvI-ibPGktmmqoEkLuKpRmoEhDovJYJtm_9onUX1djhvaipGhR2CVVWa26tYaPvUyOYVneTXbk5_g","width":4928}],"place_id":"ChIJv54F4r3uDzkRHUV2rO5IKkQ","plus_code":{"compound_code":"MPR8+JG Sahibzada Ajit Singh Nagar, Punjab","global_code":"8J2RMPR8+JG"},"rating":4.1,"reference":"ChIJv54F4r3uDzkRHUV2rO5IKkQ","scope":"GOOGLE","types":["hospital","health","point_of_interest","establishment"],"user_ratings_total":272,"vicinity":"Site 1 & 2, near Gurudwara Singh Shaheedan, Phase 9, Sahibzada Ajit Singh Nagar"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.7611361,"lng":76.7749661},"viewport":{"northeast":{"lat":30.76252847989272,"lng":76.77639682989272},"southwest":{"lat":30.75982882010727,"lng":76.77369717010728}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"30853dffd5ed93668a9a02b17dadecf3e47bc589","name":"Advanced Trauma Centre, PGI","opening_hours":{"open_now":true},"photos":[{"height":2448,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/108154157708208388561\">Haraman singh<\/a>"],"photo_reference":"CmRaAAAATropl5VPzwkiRKrDe2sQMwykHp7-s5SHznE309AvvYMfGHSMn2Anc-tfvn_xoecBbmQay81zyw_EXN7u-8IyeWGJq-A2KvWXx1z8Pd8U4d1Q1tu8bT7PFa76lBZoWbZaEhCTxpuI9KkwDgtNpZesvzCmGhTgHb5FNSzYdYD3pdIFphCft2MRAw","width":3264}],"place_id":"ChIJLzZMJIHtDzkRsBElC2MRYMU","plus_code":{"compound_code":"QQ6F+FX Chandigarh","global_code":"8J2RQQ6F+FX"},"rating":4.2,"reference":"ChIJLzZMJIHtDzkRsBElC2MRYMU","scope":"GOOGLE","types":["hospital","health","point_of_interest","establishment"],"user_ratings_total":29,"vicinity":"Sector 12, Chandigarh"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.661016,"lng":76.88247710000002},"viewport":{"northeast":{"lat":30.66244287989272,"lng":76.88350487989273},"southwest":{"lat":30.65974322010727,"lng":76.88080522010729}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"695efa8e8290924e0c7d59ea3d00903fcf3be44d","name":"Ojas Hospital - Super Specialty Hospital Offering Quality Healthcare","opening_hours":{"open_now":true},"photos":[{"height":3120,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/103206052932749892017\">Dinesh Puri<\/a>"],"photo_reference":"CmRaAAAAhQCyY12YmiQ5cR4fDKUZN93v7DK1Y_bi3NatHt4e2W1CZXdw5auOMUfdFLQtdaqsuGnLZ0H0D-K6M1o7bwtrcxHd91GYeYARO4jB22ZfoDOfU1lvpWfJ8CFycDRuo8gnEhAujizbEU9dY6GeAe6o8a31GhSedUrUwVdS6iHJF42g5OS73m5HpQ","width":4160}],"place_id":"ChIJjZ3xFF2UDzkRaG6beG9by-I","plus_code":{"compound_code":"MV6J+CX Panchkula Extension, Panchkula, Haryana","global_code":"8J2RMV6J+CX"},"rating":4.4,"reference":"ChIJjZ3xFF2UDzkRaG6beG9by-I","scope":"GOOGLE","types":["hospital","doctor","health","point_of_interest","establishment"],"user_ratings_total":260,"vicinity":"H1, Sector 26, Panchkula Extension, Panchkula"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.7608744,"lng":76.7608968},"viewport":{"northeast":{"lat":30.76221787989272,"lng":76.76223482989273},"southwest":{"lat":30.75951822010727,"lng":76.75953517010728}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"3c0d400c62cc3512670fb7ed20e0966082b9b53f","name":"Drug Information Unit, PGIMER","opening_hours":{"open_now":true},"place_id":"ChIJ10CIhWHtDzkRTyMJJIoGESU","plus_code":{"compound_code":"QQ66+89 Sector 14, Panjab University, Chandigarh","global_code":"8J2RQQ66+89"},"rating":0,"reference":"ChIJ10CIhWHtDzkRTyMJJIoGESU","scope":"GOOGLE","types":["hospital","health","point_of_interest","establishment"],"user_ratings_total":0,"vicinity":"Pharmacology Department, PGI Rd, Sector: 14, Sector 14, Chandigarh"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.7416715,"lng":76.7943035},"viewport":{"northeast":{"lat":30.74299132989273,"lng":76.79567957989272},"southwest":{"lat":30.74029167010728,"lng":76.79297992010727}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"55a934a498b93d046c054f03378d1f871135c90d","name":"Chd City Hospital","opening_hours":{"open_now":true},"photos":[{"height":780,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/112727707040763465438\">A Google User<\/a>"],"photo_reference":"CmRaAAAADmntl1CEE3YW1KVc76034wqt3tCjxd9e8FLo6rw4OqwAXjfgXqDM6eC1XgTWOXFMHZl-JLdAOqYqeo5_uKP6kib70fCB8crgnmNSg5lgwOj7j3l1gl2GZHvflUKTr6V4EhBKLtiPsM866mRVQeF2DHX9GhTQf9xJHK2qkbBt3t089t5kE46LUQ","width":1040}],"place_id":"ChIJqcKhnpXtDzkRJ87ygJeP_Jw","plus_code":{"compound_code":"PQRV+MP Sector 9, Chandigarh","global_code":"8J2RPQRV+MP"},"rating":5,"reference":"ChIJqcKhnpXtDzkRJ87ygJeP_Jw","scope":"GOOGLE","types":["hospital","health","point_of_interest","establishment"],"user_ratings_total":32,"vicinity":"SCO 10, 11, Madhya Marg, Sector 8C, Sector 9, Chandigarh"},{"business_status":"OPERATIONAL","geometry":{"location":{"lat":30.768528,"lng":76.772629},"viewport":{"northeast":{"lat":30.76989102989273,"lng":76.77396897989271},"southwest":{"lat":30.76719137010728,"lng":76.77126932010727}}},"icon":"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png","id":"ab4e989c96ce38ff6254468040cee467ef343d2a","name":"PGI Campus, PGIMER, Chandigarh","opening_hours":{"open_now":true},"photos":[{"height":4000,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/110631437976944790278\">A Google User<\/a>"],"photo_reference":"CmRaAAAAMs8KDAHOBNqZUnX0KFYLlNYtCn-rJ5kBQoqXykIrRY9nS5iOzIOBKg7asKbzrE6QWw_RBfmMCUr7XzA_YPtGlkfdzRjwddJX1jvrrBsxjoKDq3MJnk1Q8dc96wH5hxoJEhBUMIufo7xNJlJM_Ei5GBfvGhRDaXBkYGnkCGXyIuMM1XYAuYZuCQ","width":1844}],"place_id":"ChIJifsjQHnyDzkRCJTQApr_35I","plus_code":{"compound_code":"QQ9F+C3 Sector 12, Chandigarh","global_code":"8J2RQQ9F+C3"},"rating":3.8,"reference":"ChIJifsjQHnyDzkRCJTQApr_35I","scope":"GOOGLE","types":["hospital","health","point_of_interest","establishment"],"user_ratings_total":8,"vicinity":"223, Vidya Path, Sector 12, Chandigarh"}]
     * status : OK
     */

    private String status;
    private List<?> html_attributions;
    private List<ResultsBean> results;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<?> getHtml_attributions() {
        return html_attributions;
    }

    public void setHtml_attributions(List<?> html_attributions) {
        this.html_attributions = html_attributions;
    }

    public List<ResultsBean> getResults() {
        return results;
    }

    public void setResults(List<ResultsBean> results) {
        this.results = results;
    }

    public static class ResultsBean {
        /**
         * business_status : OPERATIONAL
         * geometry : {"location":{"lat":30.720576,"lng":76.7047106},"viewport":{"northeast":{"lat":30.72190052989272,"lng":76.70602017989273},"southwest":{"lat":30.71920087010728,"lng":76.70332052010728}}}
         * icon : https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png
         * id : 4cf7720fb5c39abae5d3a46202b2bdb2bdd39671
         * name : AM Hospital Mohali
         * opening_hours : {"open_now":true}
         * photos : [{"height":360,"html_attributions":["<a href=\"https://maps.google.com/maps/contrib/115057308097687163521\">Atul Mahajan Hospital<\/a>"],"photo_reference":"CmRaAAAALutvN04RwkEKsGRu39GCM7Xawck9CGpqGFGSW4qbgswBLzWn_1R9SA3jRKSkxRN5ouoHrgrUO4JO-Kidc83Mg373Dw3Cp9rGt6DtbKw43jspsg2ymIH8gMFvUJho86TbEhAXXGhXnm7iFjr8Z05NufYqGhTfwOotfUPHJqGSidI1quK2yVvr6w","width":640}]
         * place_id : ChIJjx01gV7uDzkRtu8Df04gIII
         * plus_code : {"compound_code":"PPC3+6V Sector 58, Sahibzada Ajit Singh Nagar, Punjab","global_code":"8J2RPPC3+6V"}
         * rating : 3.9
         * reference : ChIJjx01gV7uDzkRtu8Df04gIII
         * scope : GOOGLE
         * types : ["hospital","health","point_of_interest","establishment"]
         * user_ratings_total : 41
         * vicinity : MOHALI BYEPASS, ROAD, Sector 58, Sahibzada Ajit Singh Nagar
         */

        private String business_status;
        private GeometryBean geometry;
        private String icon;
        private String id;
        private String name;
        private OpeningHoursBean opening_hours;
        private String place_id;
        private PlusCodeBean plus_code;
        private double rating;
        private String reference;
        private String scope;
        private int user_ratings_total;
        private String vicinity;
        private List<PhotosBean> photos;
        private List<String> types;

        public String getBusiness_status() {
            return business_status;
        }

        public void setBusiness_status(String business_status) {
            this.business_status = business_status;
        }

        public GeometryBean getGeometry() {
            return geometry;
        }

        public void setGeometry(GeometryBean geometry) {
            this.geometry = geometry;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public OpeningHoursBean getOpening_hours() {
            return opening_hours;
        }

        public void setOpening_hours(OpeningHoursBean opening_hours) {
            this.opening_hours = opening_hours;
        }

        public String getPlace_id() {
            return place_id;
        }

        public void setPlace_id(String place_id) {
            this.place_id = place_id;
        }

        public PlusCodeBean getPlus_code() {
            return plus_code;
        }

        public void setPlus_code(PlusCodeBean plus_code) {
            this.plus_code = plus_code;
        }

        public double getRating() {
            return rating;
        }

        public void setRating(double rating) {
            this.rating = rating;
        }

        public String getReference() {
            return reference;
        }

        public void setReference(String reference) {
            this.reference = reference;
        }

        public String getScope() {
            return scope;
        }

        public void setScope(String scope) {
            this.scope = scope;
        }

        public int getUser_ratings_total() {
            return user_ratings_total;
        }

        public void setUser_ratings_total(int user_ratings_total) {
            this.user_ratings_total = user_ratings_total;
        }

        public String getVicinity() {
            return vicinity;
        }

        public void setVicinity(String vicinity) {
            this.vicinity = vicinity;
        }

        public List<PhotosBean> getPhotos() {
            return photos;
        }

        public void setPhotos(List<PhotosBean> photos) {
            this.photos = photos;
        }

        public List<String> getTypes() {
            return types;
        }

        public void setTypes(List<String> types) {
            this.types = types;
        }

        public static class GeometryBean {
            /**
             * location : {"lat":30.720576,"lng":76.7047106}
             * viewport : {"northeast":{"lat":30.72190052989272,"lng":76.70602017989273},"southwest":{"lat":30.71920087010728,"lng":76.70332052010728}}
             */

            private LocationBean location;
            private ViewportBean viewport;

            public LocationBean getLocation() {
                return location;
            }

            public void setLocation(LocationBean location) {
                this.location = location;
            }

            public ViewportBean getViewport() {
                return viewport;
            }

            public void setViewport(ViewportBean viewport) {
                this.viewport = viewport;
            }

            public static class LocationBean {
                /**
                 * lat : 30.720576
                 * lng : 76.7047106
                 */

                private double lat;
                private double lng;

                public double getLat() {
                    return lat;
                }

                public void setLat(double lat) {
                    this.lat = lat;
                }

                public double getLng() {
                    return lng;
                }

                public void setLng(double lng) {
                    this.lng = lng;
                }
            }

            public static class ViewportBean {
                /**
                 * northeast : {"lat":30.72190052989272,"lng":76.70602017989273}
                 * southwest : {"lat":30.71920087010728,"lng":76.70332052010728}
                 */

                private NortheastBean northeast;
                private SouthwestBean southwest;

                public NortheastBean getNortheast() {
                    return northeast;
                }

                public void setNortheast(NortheastBean northeast) {
                    this.northeast = northeast;
                }

                public SouthwestBean getSouthwest() {
                    return southwest;
                }

                public void setSouthwest(SouthwestBean southwest) {
                    this.southwest = southwest;
                }

                public static class NortheastBean {
                    /**
                     * lat : 30.72190052989272
                     * lng : 76.70602017989273
                     */

                    private double lat;
                    private double lng;

                    public double getLat() {
                        return lat;
                    }

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public double getLng() {
                        return lng;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }
                }

                public static class SouthwestBean {
                    /**
                     * lat : 30.71920087010728
                     * lng : 76.70332052010728
                     */

                    private double lat;
                    private double lng;

                    public double getLat() {
                        return lat;
                    }

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public double getLng() {
                        return lng;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }
                }
            }
        }

        public static class OpeningHoursBean {
            /**
             * open_now : true
             */

            private boolean open_now;

            public boolean isOpen_now() {
                return open_now;
            }

            public void setOpen_now(boolean open_now) {
                this.open_now = open_now;
            }
        }

        public static class PlusCodeBean {
            /**
             * compound_code : PPC3+6V Sector 58, Sahibzada Ajit Singh Nagar, Punjab
             * global_code : 8J2RPPC3+6V
             */

            private String compound_code;
            private String global_code;

            public String getCompound_code() {
                return compound_code;
            }

            public void setCompound_code(String compound_code) {
                this.compound_code = compound_code;
            }

            public String getGlobal_code() {
                return global_code;
            }

            public void setGlobal_code(String global_code) {
                this.global_code = global_code;
            }
        }

        public static class PhotosBean {
            /**
             * height : 360
             * html_attributions : ["<a href=\"https://maps.google.com/maps/contrib/115057308097687163521\">Atul Mahajan Hospital<\/a>"]
             * photo_reference : CmRaAAAALutvN04RwkEKsGRu39GCM7Xawck9CGpqGFGSW4qbgswBLzWn_1R9SA3jRKSkxRN5ouoHrgrUO4JO-Kidc83Mg373Dw3Cp9rGt6DtbKw43jspsg2ymIH8gMFvUJho86TbEhAXXGhXnm7iFjr8Z05NufYqGhTfwOotfUPHJqGSidI1quK2yVvr6w
             * width : 640
             */

            private int height;
            private String photo_reference;
            private int width;
            private List<String> html_attributions;

            public int getHeight() {
                return height;
            }

            public void setHeight(int height) {
                this.height = height;
            }

            public String getPhoto_reference() {
                return photo_reference;
            }

            public void setPhoto_reference(String photo_reference) {
                this.photo_reference = photo_reference;
            }

            public int getWidth() {
                return width;
            }

            public void setWidth(int width) {
                this.width = width;
            }

            public List<String> getHtml_attributions() {
                return html_attributions;
            }

            public void setHtml_attributions(List<String> html_attributions) {
                this.html_attributions = html_attributions;
            }
        }
    }
}
