package com.pongo.health.model;

import java.util.List;

public class AddressModel {

    /**
     * status : success
     * homeaddress : [{"id":"6","street_address":"test","apartment_detail":"467","city":"chd","state":"chd","zip_code":"160017"}]
     * shipaddress : [{"id":"1","street_address":"test","apartment_detail":"467","city":"chd","state":"chd","zip_code":"160017"}]
     */

    private String status;
    private List<HomeaddressBean> homeaddress;
    private List<ShipaddressBean> shipaddress;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<HomeaddressBean> getHomeaddress() {
        return homeaddress;
    }

    public void setHomeaddress(List<HomeaddressBean> homeaddress) {
        this.homeaddress = homeaddress;
    }

    public List<ShipaddressBean> getShipaddress() {
        return shipaddress;
    }

    public void setShipaddress(List<ShipaddressBean> shipaddress) {
        this.shipaddress = shipaddress;
    }

    public static class HomeaddressBean {
        /**
         * id : 6
         * street_address : test
         * apartment_detail : 467
         * city : chd
         * state : chd
         * zip_code : 160017
         */

        private String id;
        private String street_address;
        private String apartment_detail;
        private String city;
        private String state;
        private String zip_code;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStreet_address() {
            return street_address;
        }

        public void setStreet_address(String street_address) {
            this.street_address = street_address;
        }

        public String getApartment_detail() {
            return apartment_detail;
        }

        public void setApartment_detail(String apartment_detail) {
            this.apartment_detail = apartment_detail;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getZip_code() {
            return zip_code;
        }

        public void setZip_code(String zip_code) {
            this.zip_code = zip_code;
        }
    }

    public static class ShipaddressBean {
        /**
         * id : 1
         * street_address : test
         * apartment_detail : 467
         * city : chd
         * state : chd
         * zip_code : 160017
         */

        private String id;
        private String street_address;
        private String apartment_detail;
        private String city;
        private String state;
        private String zip_code;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStreet_address() {
            return street_address;
        }

        public void setStreet_address(String street_address) {
            this.street_address = street_address;
        }

        public String getApartment_detail() {
            return apartment_detail;
        }

        public void setApartment_detail(String apartment_detail) {
            this.apartment_detail = apartment_detail;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getZip_code() {
            return zip_code;
        }

        public void setZip_code(String zip_code) {
            this.zip_code = zip_code;
        }
    }
}
