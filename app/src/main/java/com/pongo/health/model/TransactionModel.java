package com.pongo.health.model;

import java.util.List;

public class TransactionModel {

    /**
     * status : success
     * yourtransaction : [{"orderid":"356","orderdate":"23-07-2020","total":"103.00","products":[{"product_id":"21","product_name":"Cat Anxiety And Oral","quantity":"2","price":"99","image":"http://digittrix.com/staging/pongo/assets/products/146391_MAIN__AC_SL1500_V1525380453_8.jpg"}],"image":"http://digittrix.com/staging/pongo/assets/products/146391_MAIN__AC_SL1500_V1525380453_8.jpg"},{"orderid":"355","orderdate":"20-07-2020","total":"98.98","products":[{"product_id":"21","product_name":"Cat Anxiety And Oral","quantity":"2","price":"99","image":"http://digittrix.com/staging/pongo/assets/products/146391_MAIN__AC_SL1500_V1525380453_8.jpg"}],"image":"http://digittrix.com/staging/pongo/assets/products/146391_MAIN__AC_SL1500_V1525380453_8.jpg"}]
     * subscribeproduct : [{"orderid":"current plan","orderdate":"2020-09-04","total":"9.99","products":[{"product_id":"221","product_name":"Only Dog Hello My Name Is Barbara What Is Your Name I Would Like To Talk To You Would You Like To Dance In The Club?","quantity":"1","price":"70","subscribeid":"","image":"http://digittrix.com/staging/pongo/assets/products/2-1595800005img1.jpg"}],"image":"http://digittrix.com/staging/pongo/assets/logo/pongologo.png","type":"plan","subscribe_id":"sub_HmKH10sxz1XUIN","status":"active"},{"orderid":"356","orderdate":"23-07-2020","total":"103.00","products":[{"product_id":"21","product_name":"Cat Anxiety And Oral","quantity":"2","price":"99","subscribeid":"","image":"http://digittrix.com/staging/pongo/assets/products/146391_MAIN__AC_SL1500_V1525380453_8.jpg"}],"image":"http://digittrix.com/staging/pongo/assets/products/146391_MAIN__AC_SL1500_V1525380453_8.jpg","type":"product","subscribe_id":"","status":""},{"orderid":"355","orderdate":"20-07-2020","total":"98.98","products":[{"product_id":"21","product_name":"Cat Anxiety And Oral","quantity":"2","price":"99","subscribeid":"","image":"http://digittrix.com/staging/pongo/assets/products/146391_MAIN__AC_SL1500_V1525380453_8.jpg"}],"image":"http://digittrix.com/staging/pongo/assets/products/146391_MAIN__AC_SL1500_V1525380453_8.jpg","type":"product","subscribe_id":"","status":""},{"orderid":"367","orderdate":"05-08-2020","total":"87.98","products":[{"product_id":"220","product_name":"For Cat Anxiety","quantity":"1","price":"18","subscribeid":"","image":"http://digittrix.com/staging/pongo/assets/products/1-1595800005img1.jpg"}],"image":"http://digittrix.com/staging/pongo/assets/products/1-1595800005img1.jpg","type":"product","subscribe_id":"","status":""},{"orderid":"368","orderdate":"05-08-2020","total":"69.99","products":[{"product_id":"221","product_name":"Only Dog Hello My Name Is Barbara What Is Your Name I Would Like To Talk To You Would You Like To Dance In The Club?","quantity":"1","price":"70","subscribeid":"","image":"http://digittrix.com/staging/pongo/assets/products/2-1595800005img1.jpg"}],"image":"http://digittrix.com/staging/pongo/assets/products/2-1595800005img1.jpg","type":"product","subscribe_id":"","status":""},{"orderid":"369","orderdate":"05-08-2020","total":"150.00","products":[{"product_id":"20","product_name":"Cat  Oral Skin Behavior Anxiety","quantity":"10","price":"150","subscribeid":"","image":"http://digittrix.com/staging/pongo/assets/products/157665_PT2__AC_SL1500_V1554750797_.jpg"}],"image":"http://digittrix.com/staging/pongo/assets/products/157665_PT2__AC_SL1500_V1554750797_.jpg","type":"product","subscribe_id":"","status":""}]
     */

    private String status;
    private List<YourtransactionBean> yourtransaction;
    private List<SubscribeproductBean> subscribeproduct;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<YourtransactionBean> getYourtransaction() {
        return yourtransaction;
    }

    public void setYourtransaction(List<YourtransactionBean> yourtransaction) {
        this.yourtransaction = yourtransaction;
    }

    public List<SubscribeproductBean> getSubscribeproduct() {
        return subscribeproduct;
    }

    public void setSubscribeproduct(List<SubscribeproductBean> subscribeproduct) {
        this.subscribeproduct = subscribeproduct;
    }

    public static class YourtransactionBean {
        /**
         * orderid : 356
         * orderdate : 23-07-2020
         * total : 103.00
         * products : [{"product_id":"21","product_name":"Cat Anxiety And Oral","quantity":"2","price":"99","image":"http://digittrix.com/staging/pongo/assets/products/146391_MAIN__AC_SL1500_V1525380453_8.jpg"}]
         * image : http://digittrix.com/staging/pongo/assets/products/146391_MAIN__AC_SL1500_V1525380453_8.jpg
         */

        private String orderid;
        private String orderdate;
        private String total;
        private String image;
        private List<ProductsBean> products;

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public String getOrderdate() {
            return orderdate;
        }

        public void setOrderdate(String orderdate) {
            this.orderdate = orderdate;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public List<ProductsBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBean> products) {
            this.products = products;
        }

        public static class ProductsBean {
            /**
             * product_id : 21
             * product_name : Cat Anxiety And Oral
             * quantity : 2
             * price : 99
             * image : http://digittrix.com/staging/pongo/assets/products/146391_MAIN__AC_SL1500_V1525380453_8.jpg
             */

            private String product_id;
            private String product_name;
            private String quantity;
            private String price;
            private String image;

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }
        }
    }

    public static class SubscribeproductBean {
        /**
         * orderid : current plan
         * orderdate : 2020-09-04
         * total : 9.99
         * products : [{"product_id":"221","product_name":"Only Dog Hello My Name Is Barbara What Is Your Name I Would Like To Talk To You Would You Like To Dance In The Club?","quantity":"1","price":"70","subscribeid":"","image":"http://digittrix.com/staging/pongo/assets/products/2-1595800005img1.jpg"}]
         * image : http://digittrix.com/staging/pongo/assets/logo/pongologo.png
         * type : plan
         * subscribe_id : sub_HmKH10sxz1XUIN
         * status : active
         */

        private String orderid;
        private String orderdate;
        private String total;
        private String image;
        private String type;
        private String subscribe_id;
        private String status;
        private List<ProductsBeanX> products;

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public String getOrderdate() {
            return orderdate;
        }

        public void setOrderdate(String orderdate) {
            this.orderdate = orderdate;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getSubscribe_id() {
            return subscribe_id;
        }

        public void setSubscribe_id(String subscribe_id) {
            this.subscribe_id = subscribe_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<ProductsBeanX> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBeanX> products) {
            this.products = products;
        }

        public static class ProductsBeanX {
            /**
             * product_id : 221
             * product_name : Only Dog Hello My Name Is Barbara What Is Your Name I Would Like To Talk To You Would You Like To Dance In The Club?
             * quantity : 1
             * price : 70
             * subscribeid :
             * image : http://digittrix.com/staging/pongo/assets/products/2-1595800005img1.jpg
             */

            private String product_id;
            private String product_name;
            private String quantity;
            private String price;
            private String subscribeid;
            private String image;

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getSubscribeid() {
                return subscribeid;
            }

            public void setSubscribeid(String subscribeid) {
                this.subscribeid = subscribeid;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }
        }
    }
}
