package com.pongo.health.model;

import java.util.List;

public class CategoriesModel {


    /**
     * status : success
     * problemlist : [{"id":"1","name":"General Checkup","image":"http://digittrix.com/staging/pongo/assets/images/problems/general_check_img.png"},{"id":"2","name":"Skin rash/allery","image":"http://digittrix.com/staging/pongo/assets/images/problems/skin_rash_img.png"},{"id":"3","name":"Ear Infaction","image":"http://digittrix.com/staging/pongo/assets/images/problems/ear_infection_img.png"},{"id":"4","name":"Limps or tenderness","image":"http://digittrix.com/staging/pongo/assets/images/problems/tenderness_img.png"},{"id":"5","name":"Eating Problems","image":"http://digittrix.com/staging/pongo/assets/images/problems/dog_eat_img.png"},{"id":"6","name":"Diarrhea","image":"http://digittrix.com/staging/pongo/assets/images/problems/diarreha.png"},{"id":"7","name":"Throwing Up","image":"http://digittrix.com/staging/pongo/assets/images/problems/throwing_up_img.png"},{"id":"8","name":"Behavioral","image":"http://digittrix.com/staging/pongo/assets/images/problems/pet_medium_size_img.png"},{"id":"9","name":"other","image":"http://digittrix.com/staging/pongo/assets/images/problems/other_img.png"}]
     * payment : 30.00
     */

    private String status;
    private String payment;
    private List<ProblemlistBean> problemlist;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public List<ProblemlistBean> getProblemlist() {
        return problemlist;
    }

    public void setProblemlist(List<ProblemlistBean> problemlist) {
        this.problemlist = problemlist;
    }

    public static class ProblemlistBean {
        /**
         * id : 1
         * name : General Checkup
         * image : http://digittrix.com/staging/pongo/assets/images/problems/general_check_img.png
         */

        private String id;
        private String name;
        private String image;

        public boolean isCheck() {
            return isCheck;
        }

        public void setCheck(boolean check) {
            isCheck = check;
        }

        private boolean isCheck = false;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
