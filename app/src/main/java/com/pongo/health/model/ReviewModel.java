package com.pongo.health.model;

public class ReviewModel {
    String title;
    String description;
    int rating;
    String images;

    public ReviewModel(String header1, String header2, int rating, String images) {
        this.title = header1;
        this.description = header2;
        this.rating = rating;
        this.images = images;
    }

    public String getHeader1() {
        return title;
    }

    public String getHeader2() {
        return description;
    }

    public String getImages() {
        return images;
    }

    public int getRating() {
        return rating;
    }
}
