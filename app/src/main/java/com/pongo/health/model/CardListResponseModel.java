package com.pongo.health.model;

import java.util.List;

public class CardListResponseModel {

    /**
     * status : success
     * cardlist : [{"cardnumber":"xxxx-xxxx-xxxx-4242","exp_month":10,"exp_year":2021,"card_type":"Visa","customer":"cus_HUJsaBwcl02WWd","card_id":"card_1GvMjLCCvFNIowRPrDf5EMxm"},{"cardnumber":"xxxx-xxxx-xxxx-4242","exp_month":12,"exp_year":2024,"card_type":"Visa","customer":"cus_HUJsaBwcl02WWd","card_id":"card_1GvLEoCCvFNIowRPB6pavojF"}]
     */

    private String status;
    private List<CardlistBean> cardlist;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<CardlistBean> getCardlist() {
        return cardlist;
    }

    public void setCardlist(List<CardlistBean> cardlist) {
        this.cardlist = cardlist;
    }

    public static class CardlistBean {
        /**
         * cardnumber : xxxx-xxxx-xxxx-4242
         * exp_month : 10
         * exp_year : 2021
         * card_type : Visa
         * customer : cus_HUJsaBwcl02WWd
         * card_id : card_1GvMjLCCvFNIowRPrDf5EMxm
         */

        private String cardnumber;
        private int exp_month;
        private int exp_year;
        private String card_type;
        private String customer;
        private String card_id;

        public String getCardnumber() {
            return cardnumber;
        }

        public void setCardnumber(String cardnumber) {
            this.cardnumber = cardnumber;
        }

        public int getExp_month() {
            return exp_month;
        }

        public void setExp_month(int exp_month) {
            this.exp_month = exp_month;
        }

        public int getExp_year() {
            return exp_year;
        }

        public void setExp_year(int exp_year) {
            this.exp_year = exp_year;
        }

        public String getCard_type() {
            return card_type;
        }

        public void setCard_type(String card_type) {
            this.card_type = card_type;
        }

        public String getCustomer() {
            return customer;
        }

        public void setCustomer(String customer) {
            this.customer = customer;
        }

        public String getCard_id() {
            return card_id;
        }

        public void setCard_id(String card_id) {
            this.card_id = card_id;
        }
    }
}
