package com.pongo.health.model;

import java.util.List;

public class PastChatResponseModel {


    /**
     * status : success
     * detail : [{"chat_id":"93","first_name":"G","last_name":"S","vet_type":"technician","expertise":",Food animal (Cattle and Pigs)","area_description":"test","primary_hospital_name":"Raman Medical Store","state_issued":",pb,chd","mobile":"","chatstatus":"processing","userimage":"http://digittrix.com/staging/pongo/assets/images/avtar.png","token":["d0_v75UnQ0OD_pcJeVZ66o:APA91bG4KK4YWMohdWvBRCSxfuh88hQkdpfjEKqNQGRsFfbdXeiY7tVId4FsnB4hfkP8i4e9VPURDvCMXFkDp8op2-r6HJXy4qFclcfyK2xL9bMfWgI1nsTOb3y608r_7FaWn3am86Df"]}]
     */

    private String status;
    private List<DetailBean> detail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DetailBean> getDetail() {
        return detail;
    }

    public void setDetail(List<DetailBean> detail) {
        this.detail = detail;
    }

    public static class DetailBean {
        /**
         * chat_id : 93
         * first_name : G
         * last_name : S
         * vet_type : technician
         * expertise : ,Food animal (Cattle and Pigs)
         * area_description : test
         * primary_hospital_name : Raman Medical Store
         * state_issued : ,pb,chd
         * mobile :
         * "updatedate": "Aug 26, 2020",
         * chatstatus : processing
         * userimage : http://digittrix.com/staging/pongo/assets/images/avtar.png
         * token : ["d0_v75UnQ0OD_pcJeVZ66o:APA91bG4KK4YWMohdWvBRCSxfuh88hQkdpfjEKqNQGRsFfbdXeiY7tVId4FsnB4hfkP8i4e9VPURDvCMXFkDp8op2-r6HJXy4qFclcfyK2xL9bMfWgI1nsTOb3y608r_7FaWn3am86Df"]
         */

        private String chat_id;
        private String first_name;
        private String last_name;
        private String vet_type;
        private String expertise;
        private String area_description;
        private String primary_hospital_name;
        private String state_issued;
        private String mobile;
        private String chatstatus;
        private String userimage;
        private String updatedate;

        public String getUpdatedate() {
            return updatedate;
        }

        public void setUpdatedate(String updatedate) {
            this.updatedate = updatedate;
        }


        public String getServerchatid() {
            return serverchatid;
        }

        public void setServerchatid(String serverchatid) {
            this.serverchatid = serverchatid;
        }

        public String getPet_id() {
            return pet_id;
        }

        public void setPet_id(String pet_id) {
            this.pet_id = pet_id;
        }

        private String serverchatid;
        private String pet_id;
        private List<String> token;

        public String getChat_id() {
            return chat_id;
        }

        public void setChat_id(String chat_id) {
            this.chat_id = chat_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getVet_type() {
            return vet_type;
        }

        public void setVet_type(String vet_type) {
            this.vet_type = vet_type;
        }

        public String getExpertise() {
            return expertise;
        }

        public void setExpertise(String expertise) {
            this.expertise = expertise;
        }

        public String getArea_description() {
            return area_description;
        }

        public void setArea_description(String area_description) {
            this.area_description = area_description;
        }

        public String getPrimary_hospital_name() {
            return primary_hospital_name;
        }

        public void setPrimary_hospital_name(String primary_hospital_name) {
            this.primary_hospital_name = primary_hospital_name;
        }

        public String getState_issued() {
            return state_issued;
        }

        public void setState_issued(String state_issued) {
            this.state_issued = state_issued;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getChatstatus() {
            return chatstatus;
        }

        public void setChatstatus(String chatstatus) {
            this.chatstatus = chatstatus;
        }

        public String getUserimage() {
            return userimage;
        }

        public void setUserimage(String userimage) {
            this.userimage = userimage;
        }

        public List<String> getToken() {
            return token;
        }

        public void setToken(List<String> token) {
            this.token = token;
        }
    }
}
