package com.pongo.health.model;

import java.util.List;

public class BreedModel {

    /**
     * status : success
     * catbreed : [{"id":"2","type":"cat","breed":"breed2"},{"id":"3","type":"cat","breed":"breed3"}]
     * dogbreed : [{"id":"1","type":"dog","breed":"breed1"}]
     */

    private String status;
    private List<CatbreedBean> catbreed;
    private List<DogbreedBean> dogbreed;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<CatbreedBean> getCatbreed() {
        return catbreed;
    }

    public void setCatbreed(List<CatbreedBean> catbreed) {
        this.catbreed = catbreed;
    }

    public List<DogbreedBean> getDogbreed() {
        return dogbreed;
    }

    public void setDogbreed(List<DogbreedBean> dogbreed) {
        this.dogbreed = dogbreed;
    }

    public static class CatbreedBean {
        /**
         * id : 2
         * type : cat
         * breed : breed2
         */

        private String id;
        private String type;
        private String breed;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getBreed() {
            return breed;
        }

        public void setBreed(String breed) {
            this.breed = breed;
        }
    }

    public static class DogbreedBean {
        /**
         * id : 1
         * type : dog
         * breed : breed1
         */

        private String id;
        private String type;
        private String breed;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getBreed() {
            return breed;
        }

        public void setBreed(String breed) {
            this.breed = breed;
        }
    }
}
