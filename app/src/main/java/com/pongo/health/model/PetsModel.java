package com.pongo.health.model;

public class PetsModel {
    private String petYear;
    private String petName;
    private String petdescription;

    public PetsModel(String petYear, String petName, String petdescription) {
        this.petYear = petYear;
        this.petName = petName;
        this.petdescription = petdescription;
    }

    public String getPetYear() {
        return petYear;
    }

    public String getPetName() {
        return petName;
    }

    public String getPetdescription() {
        return petdescription;
    }
}
