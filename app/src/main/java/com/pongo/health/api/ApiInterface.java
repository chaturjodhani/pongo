package com.pongo.health.api;


import com.pongo.health.model.AddressModel;
import com.pongo.health.model.BreedModel;
import com.pongo.health.model.CardListResponseModel;
import com.pongo.health.model.CartResponseModel;
import com.pongo.health.model.CategoriesModel;
import com.pongo.health.model.CheckoutResponseModel;
import com.pongo.health.model.DoctorWaitingListModel;
import com.pongo.health.model.EarningModel;
import com.pongo.health.model.HomeModel;
import com.pongo.health.model.HospitalSearchModel;
import com.pongo.health.model.MarketResponseModel;
import com.pongo.health.model.NearestDoctorModel;
import com.pongo.health.model.NotificationResponseModel;
import com.pongo.health.model.OtpModel;
import com.pongo.health.model.PastChatResponseModel;
import com.pongo.health.model.PastVetSearchModel;
import com.pongo.health.model.PetModelResponse;
import com.pongo.health.model.PetRecommendationModel;
import com.pongo.health.model.PongoVetsModel;
import com.pongo.health.model.ProductResponseModel;
import com.pongo.health.model.SearchProductModel;
import com.pongo.health.model.SingleProductModel;
import com.pongo.health.model.StateListModel;
import com.pongo.health.model.TechChatResponseModel;
import com.pongo.health.model.TechDashboradResponseModel;
import com.pongo.health.model.TimeslotModel;
import com.pongo.health.model.TransactionModel;
import com.pongo.health.model.UserLoginModel;
import com.pongo.health.model.VetDashBoardResponseModel;
import com.pongo.health.model.VetTimeSlotModel;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface ApiInterface {
//    String param = "staging/super/api/";


    @FormUrlEncoded
    @POST("otp")
    Call<OtpModel> getOtp(@Field("phone_no") String phone, @Field("email_id") String email);

    @POST("signup")
    Call<ResponseBody> usersignup(@Body RequestBody body);

    @GET("market")
    Call<MarketResponseModel> getMarketData();

    @FormUrlEncoded
    @POST("catproducts")
    Call<ProductResponseModel> getCateProducts(@Field("cat_id") String cat_id, @Field("page_id") int page_id);

    @POST("add_pet")
    Call<ResponseBody> addPet(@Body RequestBody body);

    @POST("add_new_pet")
    Call<ResponseBody> addNewPet(@Body RequestBody body);

    @FormUrlEncoded
    @POST("userlogin")
    Call<UserLoginModel> userLogin(@Field("email_id") String email_id, @Field("password") String password, @Field("phone_no") String phone_no, @Field("device_token") String device_token, @Field("devicename") String devicename);

    @FormUrlEncoded
    @POST("single_product_detail")
    Call<SingleProductModel> getSingleProductData(@Field("user_id") String user_id, @Field("product_id") String product_id);

    @FormUrlEncoded
    @POST("home")
    Call<HomeModel> getHomeData(@Field("user_id") String user_id);

    @GET("breed")
    Call<BreedModel> getBreedData();

    @FormUrlEncoded
    @POST("get_homeandship_address")
    Call<AddressModel> getAddresses(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("add_home_address")
    Call<ResponseBody> addHomeAddress(@Field("user_id") String user_id, @Field("street_address") String street_address, @Field("apartment_detail") String apartment_detail, @Field("address_id") String address_id, @Field("city") String city, @Field("state") String state, @Field("zip_code") String zip_code);

    @FormUrlEncoded
    @POST("add_ship_address")
    Call<ResponseBody> addShipAddress(@Field("user_id") String user_id, @Field("street_address") String street_address, @Field("apartment_detail") String apartment_detail, @Field("address_id") String address_id, @Field("city") String city, @Field("state") String state, @Field("zip_code") String zip_code);

    @FormUrlEncoded
    @POST("update_contact")
    Call<ResponseBody> updateContact(@Field("user_id") String user_id, @Field("first_name") String first_name, @Field("last_name") String last_name, @Field("email_id") String email_id, @Field("phone_number") String phone_number);

    @FormUrlEncoded
    @POST("add_review")
    Call<ResponseBody> addReview(@Field("user_id") String user_id, @Field("title") String title, @Field("rating") String rating, @Field("review") String review, @Field("productid") String productid);

    @FormUrlEncoded
    @POST("user_social_login")
    Call<UserLoginModel> socialLogin(@Field("first_name") String firstname, @Field("last_name") String lastname, @Field("email_id") String emailid, @Field("socialid") String socialid, @Field("devicetoken") String devicetoken, @Field("devicename") String devicename);

    @FormUrlEncoded
    @POST("searchproduct")
    Call<SearchProductModel> getSearchProducts(@Field("search") String search);

    @FormUrlEncoded
    @POST("getrecommendedproducts")
    Call<PetRecommendationModel> getPetRecommendationData(@Field("pet_type") String pet_type, @Field("health_goal") String health_goal);

    @FormUrlEncoded
    @POST("user_notification")
    Call<NotificationResponseModel> getNotifications(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("cartlist")
    Call<CartResponseModel> getCartList(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("deletecartitem")
    Call<ResponseBody> deleteCartItem(@Field("cart_id") String cart_id);

    @FormUrlEncoded
    @POST("updatecart")
    Call<ResponseBody> updateCartItem(@Field("cart_id") String cart_id, @Field("product_id") String product_id, @Field("quantity") String quantity, @Field("userid") String userid, @Field("price") String price);

    /* @FormUrlEncoded
     @POST("addtocart")
     Call<ResponseBody> addToCart(@Field("product_id") String product_id, @Field("quantity") String quantity, @Field("strength") String strength, @Field("userid") String userid, @Field("price") String price);
 */
    @POST("addtocart")
    Call<ResponseBody> addToCart(@Body RequestBody body);

    @GET
    Call<HospitalSearchModel> getHospitalSearchData(@Url String url);

    @POST("checkout")
    Call<CheckoutResponseModel> checkout(@Body RequestBody body);

    @FormUrlEncoded
    @POST("pastchat")
    Call<PastChatResponseModel> pastChat(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("vettypelist")
    Call<PastChatResponseModel> getUserList(@Field("type") String type, @Field("key") String key);

    @FormUrlEncoded
    @POST("chatto")
    Call<ResponseBody> createChat(@Field("user_id") String user_id, @Field("chat_to") String chat_to, @Field("pet_id") String pet_id, @Field("chatid") String chatid, @Field("req_id") String req_id);

    @FormUrlEncoded
    @POST("checkonline")
    Call<ResponseBody> onlineStatus(@Field("user_id") String user_id, @Field("logintype") String logintype, @Field("lat") String lat, @Field("long") String longitute);

    @FormUrlEncoded
    @POST("techlist")
    Call<TechChatResponseModel> techChatList(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("requestor")
    Call<TechChatResponseModel> requestorChatList(@Field("user_id") String user_id);

    @GET("problem_list")
    Call<CategoriesModel> problemList();

    @FormUrlEncoded
    @POST("recievepatient")
    Call<ResponseBody> updateVetAvailability(@Field("recievetype") String recievetype, @Field("doctor_id") String doctor_id);

    @FormUrlEncoded
    @POST("doctorhomepage")
    Call<VetDashBoardResponseModel> vetDashBoard(@Field("doctor_id") String doctor_id);


    /*  @FormUrlEncoded
      @POST("nearestdoctor")
      Call<NearestDoctorModel> searchNearestDoctor(@Field("lat") String lat, @Field("long") String longi, @Field("problem") String problem);
  */
    @POST("nearestdoctor")
    Call<NearestDoctorModel> searchNearestDoctor(@Body RequestBody body);

    @POST("book_appointment")
    Call<ResponseBody> bookAppontment(@Body RequestBody body);

    @FormUrlEncoded
    @POST("twiliotoken")
    Call<ResponseBody> getToken(@Field("roomname") String roomname, @Field("identity") String identity);

    @FormUrlEncoded
    @POST("techhomepage")
    Call<TechDashboradResponseModel> techDashBoard(@Field("tech_id") String doctor_id);

    @FormUrlEncoded
    @POST("freeuser")
    Call<ResponseBody> searchTech(@Field("lat") String lat, @Field("long") String longi, @Field("user_id") String user_id, @Field("pet_id") String pet_id);

    @FormUrlEncoded
    @POST("completemark")
    Call<ResponseBody> chatCompleteMark(@Field("user_id") String user_id, @Field("chatto_id") String chatto_id, @Field("chat_id") String chat_id);

    /* @FormUrlEncoded
     @POST(param + "?action=user_login")
     Call<ResponseBody> userLogin(@Field("email") String email, @Field("password") String password, @Field("device_token") String device_token);
 */
    @FormUrlEncoded
    @POST("getpersonalinfo")
    Call<ResponseBody> getInformation(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("singlepet")
    Call<ResponseBody> getSinglePet(@Field("chat_id") String user_id);

    @FormUrlEncoded
    @POST("consultation")
    Call<ResponseBody> addNote(@Field("user_id") String user_id, @Field("tech_id") String tech_id, @Field("doctor_note") String doctor_note, @Field("products") String products, @Field("chat_id") String chat_id);

    @FormUrlEncoded
    @POST("suggested_products")
    Call<SearchProductModel> searchSuggested(@Field("key") String key);

    @FormUrlEncoded
    @POST("userfeedback")
    Call<ResponseBody> feedback(@Field("user_id") String user_id, @Field("tech_id") String tech_id, @Field("call_quality") String call_quality, @Field("consult_rating") String consult_rating, @Field("note") String note, @Field("chat_id") String chat_id);

    @POST("cardlist")
    Call<CardListResponseModel> cardList(@Body RequestBody body);

    @FormUrlEncoded
    @POST("savecard")
    Call<ResponseBody> saveCard(@Field("user_id") String user_id, @Field("user_email") String user_email, @Field("card_number") String card_number, @Field("card_expiry_month") String card_expiry_month, @Field("card_expiry_year") String card_expiry_year, @Field("card_cvv") String card_cvv, @Field("card_holder_name") String card_holder_name);

    @FormUrlEncoded
    @POST("updatepetdetail")
    Call<ResponseBody> updatePetInfo(@Field("pet_id") String pet_id, @Field("user_id") String user_id, @Field("pet_name") String pet_name, @Field("pet_type") String pet_type, @Field("breed") String breed, @Field("sex") String sex, @Field("birthday") String birthday, @Field("neutered") String neutered, @Field("microchipnumber") String microchipnumber);

    @FormUrlEncoded
    @POST("userpetlist")
    Call<PetModelResponse> getPets(@Field("user_id") String user_id);

    @GET("statelist")
    Call<StateListModel> getStateList();

    @FormUrlEncoded
    @POST("single_recommended")
    Call<ResponseBody> getSingleRcommend(@Field("consultid") String consultid);

    @POST("updatepetimg")
    Call<ResponseBody> updatePetImage(@Body RequestBody body);

    @FormUrlEncoded
    @POST("medical_records")
    Call<ResponseBody> addMedicalRecord(@Field("pet_id") String pet_id, @Field("user_id") String user_id, @Field("clinic") String clinic, @Field("phone") String phone, @Field("email") String email, @Field("zipcode") String zipcode, @Field("signature") String signature, @Field("note") String note);

    @FormUrlEncoded
    @POST("startvideo")
    Call<ResponseBody> startVideoCall(@Field("pet_id") String pet_id, @Field("doctor_id") String doctor_id, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("doctorwaitinglist")
    Call<DoctorWaitingListModel> doctorwaitingList(@Field("doctor_id") String doctor_id);

    @FormUrlEncoded
    @POST("patientwithbooked_appointment")
    Call<DoctorWaitingListModel> patientWithBookedAppointment(@Field("doctor_id") String doctor_id);

    @FormUrlEncoded
    @POST("getactiononbooking")
    Call<DoctorWaitingListModel> getActionRequired(@Field("doctor_id") String doctor_id);

    @FormUrlEncoded
    @POST("upcomingappointment")
    Call<DoctorWaitingListModel> upComingAppointment(@Field("doctor_id") String doctor_id);

    @FormUrlEncoded
    @POST("create_order")
    Call<ResponseBody> placeOrder(@Field("user_id") String user_id, @Field("address_type") String address_type, @Field("total_price") String total_price, @Field("shipping") String shipping, @Field("card_id") String card_id, @Field("shipping_total_price") String shipping_total_price, @Field("customer") String customer);

    @FormUrlEncoded
    @POST("autorenew")
    Call<ResponseBody> autorenew(@Field("product_id") String product_id, @Field("autorenew") String autorenew, @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("applycoupon")
    Call<ResponseBody> applyCoupon(@Field("coupon") String coupon, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("removecoupon")
    Call<ResponseBody> deleteCoupon(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("cancleappointment")
    Call<ResponseBody> cancelAppointment(@Field("chat_id") String chat_id);

    @FormUrlEncoded
    @POST("acceptappoints")
    Call<ResponseBody> acceptAppointment(@Field("chat_id") String chat_id);

    @FormUrlEncoded
    @POST("getsearchpastdoctor")
    Call<PastVetSearchModel> getPastVet(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("doctorsearch")
    Call<ResponseBody> getSearchVet(@Field("search") String search, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("recentdelete")
    Call<ResponseBody> deleteRecent(@Field("user_id") String user_id, @Field("id") String id);

    @FormUrlEncoded
    @POST("gettimeslot")
    Call<TimeslotModel> getTimeSlot(@Field("doctor_id") String doctor_id, @Field("date") String date);

    @FormUrlEncoded
    @POST("getapointmenttimeslot")
    Call<VetTimeSlotModel> getVetTimeSlot(@Field("doctor_id") String doctor_id, @Field("date") String date);

    @FormUrlEncoded
    @POST("updateapointmenttimeslot")
    Call<ResponseBody> updateVetTimeSlot(@Field("doctor_id") String doctor_id, @Field("time") String time, @Field("status") String status, @Field("date") String date);

    @POST("updatepersonalinfo")
    Call<ResponseBody> updateProfile(@Body RequestBody body);

    @FormUrlEncoded
    @POST("videowithuser")
    Call<ResponseBody> getChatDetail(@Field("user_id") String user_id, @Field("pet_id") String pet_id);

    @FormUrlEncoded
    @POST("yourtransaction")
    Call<TransactionModel> getTransactionList(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("alluserorder")
    Call<TransactionModel> getAllTransactionList(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("canclesubscribe")
    Call<ResponseBody> cancelSubscription(@Field("user_id") String user_id, @Field("subscribe_id") String subscribe_id);

    @FormUrlEncoded
    @POST("currentplan")
    Call<ResponseBody> getCurrentSubscription(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("createsubscription")
    Call<ResponseBody> buySubscription(@Field("user_id") String user_id, @Field("customer") String customer);

    @FormUrlEncoded
    @POST("mypongovet")
    Call<PongoVetsModel> pongoVets(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("get_payoutmethod")
    Call<ResponseBody> getPayoutInformation(@Field("doctor_id") String user_id);

    @POST("add_payoutmethod")
    Call<ResponseBody> addPayoutMethod(@Body RequestBody body);

    @FormUrlEncoded
    @POST("earning")
    Call<EarningModel> getEarning(@Field("doctor_id") String doctor_id, @Field("month") String month);

    @FormUrlEncoded
    @POST("seeallpet")
    Call<EarningModel> seeAllPatientHistory(@Field("doctor_id") String doctor_id, @Field("month") String month);

    @FormUrlEncoded
    @POST("petcases")
    Call<EarningModel> petCases(@Field("doctor_id") String doctor_id);

    @FormUrlEncoded
    @POST("patient_history")
    Call<EarningModel> patientHistory(@Field("pet_id") String pet_id);

    @FormUrlEncoded
    @POST("requestpongovet")
    Call<ResponseBody> requestAnotherVet(@Field("user_id") String user_id, @Field("hospital") String hospital, @Field("firstname") String firstname, @Field("lastname") String lastname);

    @FormUrlEncoded
    @POST("canclesubscribeproduct")
    Call<ResponseBody> cancelProductSubscription(@Field("orderid") String orderid, @Field("productid") String productid, @Field("user_id") String user_id, @Field("subscribe_id") String subscribe_id);

    @FormUrlEncoded
    @POST("calldisconnect")
    Call<ResponseBody> videoDisconnect(@Field("user_id") String userId, @Field("chatid") String chatid, @Field("pet_id") String pet_id);


    @FormUrlEncoded
    @POST("userconnect")
    Call<ResponseBody> updateVideocallStatus(@Field("chatid") String chatid);

    @FormUrlEncoded
    @POST("callduration")
    Call<ResponseBody> updateVideocallDuration(@Field("chat_id") String chatid);

}
