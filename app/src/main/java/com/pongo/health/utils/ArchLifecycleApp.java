package com.pongo.health.utils;

import android.app.Application;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.firebase.client.Firebase;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArchLifecycleApp extends Application implements LifecycleObserver {
    private Session session;
    private Firebase online_status_all_users;
    public static final String TAG = ArchLifecycleApp.class
            .getSimpleName();

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private GPSTracker gps;

    private static ArchLifecycleApp mInstance;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        session = new Session(this);
        gps = new GPSTracker(this);
        Firebase.setAndroidContext(this);
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackgrounded() {
        //App in background
        setChatStatus("offline");
        changeStatus("0");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onAppForegrounded() {
        // App in foreground
        setChatStatus("online");
        changeStatus("1");

    }

    private void setChatStatus(String status) {
        String userId = session.getuser_id();
        if (!TextUtils.isEmpty(userId)) {
            if (null == online_status_all_users) {
                online_status_all_users = new Firebase(Constants.firebaseUrl + "chatstatus/" + userId);
            }
            //on each user's device when connected they should indicate e.g. `linker` should tell everyone he's snooping around
            online_status_all_users.child("status").setValue(status);
            //also when he's not doing any snooping or if snooping goes bad he should also tell
        }
    }

    public void changeStatus(String status) {
        String userId = session.getuser_id();
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();
        if (!TextUtils.isEmpty(userId)) {
            ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).onlineStatus(userId, status,""+latitude,""+longitude).enqueue(new Callback<ResponseBody>() {
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.body() != null) {
                        String responseData = "";
                        try {
                            responseData = response.body().string();
                            if (!TextUtils.isEmpty(responseData)) {
                                JSONObject jsonObject = new JSONObject(responseData);
                               /* String status = jsonObject.getString("status");
                                String message = jsonObject.getString("detail");
                                if (!TextUtils.isEmpty(status)) {
                                    if (status.equalsIgnoreCase("success")) {

                                    }
                                }*/
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                public void onFailure(Call<ResponseBody> call, Throwable th) {
                    Log.e("cate", th.toString());
                }
            });
        }
    }


    public static synchronized ArchLifecycleApp getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

  /*  public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }*/

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
