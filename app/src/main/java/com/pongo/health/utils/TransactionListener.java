package com.pongo.health.utils;

import android.view.View;

public interface TransactionListener {
    void onClicked(View view, int position);

    void onCancelClicked(View view, int position);
}
