package com.pongo.health.utils;

import android.view.View;

public interface ItemClickListener {

    void onClicked(View view,int position);
}
