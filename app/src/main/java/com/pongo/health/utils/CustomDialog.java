package com.pongo.health.utils;

import android.app.Activity;
import android.app.Dialog;
import android.os.Build.VERSION;
import android.view.Window;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;


public class CustomDialog {
    Activity activity;
    Dialog dialog;

    public CustomDialog(Activity activity) {
        this.activity = activity;
    }

    public void show() {
        this.dialog = new Dialog(this.activity);
        this.dialog.requestWindowFeature(1);
        if (VERSION.SDK_INT >= 19) {
            Window window = this.dialog.getWindow();
            if (window != null) {
                window.getClass();
                window.setBackgroundDrawableResource(android.R.color.transparent);
            }
        }
        this.dialog.setCancelable(false);
        this.dialog.setContentView(R.layout.custom_loading_layout);
        final AppCompatImageView imageView = (AppCompatImageView) this.dialog.findViewById(R.id.custom_loading_imageView);
        this.activity.runOnUiThread(new Runnable() {
            public void run() {
                Glide.with(CustomDialog.this.activity).load(R.drawable.loader).into(imageView);
            }
        });
        if (!this.activity.isFinishing()) {
            this.dialog.show();
        }
    }

    public void hideDialog() {
        Dialog dialog = this.dialog;
        if (dialog != null && dialog.isShowing()) {
            this.dialog.dismiss();
        }
    }
}
