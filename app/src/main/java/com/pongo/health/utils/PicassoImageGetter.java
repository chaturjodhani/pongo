package com.pongo.health.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.util.Base64;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class PicassoImageGetter implements Html.ImageGetter {

    private TextView textView = null;
    private Activity mActivity = null;

    public PicassoImageGetter() {

    }

    public PicassoImageGetter(TextView target, Activity activity) {
        textView = target;
        mActivity = activity;
    }

    @Override
    public Drawable getDrawable(String source) {
        BitmapDrawablePlaceHolder drawables = new BitmapDrawablePlaceHolder();
        final BitmapDrawable[] drawable = new BitmapDrawable[1];
      /*  Picasso.with(App.get())
                .load(source)
                .placeholder(R.drawable.img_loading)
                .into(drawable);*/
//        Picasso.get().load(source).placeholder(R.mipmap.pongo_logo).into(drawable);
//        return drawable;

        Glide.with(mActivity)
                .asBitmap()
                .load(source)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                        imageView.setImageBitmap(resource);
                        drawable[0] = new BitmapDrawable(mActivity.getResources(), resource);
//                        drawables.setDrawable(ff);

                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });

//        byte[] data = decrypt(mActivity,source);
//        byte[] data = Base64.decode(source, Base64.DEFAULT);
//        byte[] data = source;
//        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
//        return new BitmapDrawable(mActivity.getResources(), bitmap);
        return  drawable[0];
    }

    private static byte[] decrypt(Context cont, String value) {
        try {
//            return new String(Base64.decode(value, Base64.DEFAULT));
            return Base64.decode(value, Base64.DEFAULT);
        } catch (IllegalArgumentException e) {
            // TODO: handle exception
            return null;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private class BitmapDrawablePlaceHolder extends BitmapDrawable implements Target {

        protected Drawable drawable;

        @Override
        public void draw(final Canvas canvas) {
            if (drawable != null) {
                drawable.draw(canvas);
            }
        }

        public void setDrawable(Drawable drawable) {
            this.drawable = drawable;
            int width = drawable.getIntrinsicWidth();
            int height = drawable.getIntrinsicHeight();
            drawable.setBounds(0, 0, width, height);
            setBounds(0, 0, width, height);
            if (textView != null) {
                textView.setText(textView.getText());
            }
        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            setDrawable(new BitmapDrawable(mActivity.getResources(), bitmap));
        }

        @Override
        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

        }


        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }

    }
}