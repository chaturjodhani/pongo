package com.pongo.health.utils;

import android.view.View;

public interface ActionListener {
    void onReviewClicked(View view, int position);

    void onAcceptClicked(View view, int position);

    void onCancelClicked(View view, int position);
}
