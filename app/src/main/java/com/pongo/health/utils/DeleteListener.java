package com.pongo.health.utils;

import android.view.View;

public interface DeleteListener {
    void onDeleteClicked(View view, int position);

    void onkeyClicked(View view, int position);
}
