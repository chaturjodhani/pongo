package com.pongo.health.utils;

import android.view.View;

public interface ItemUpdateListener {
    void onClicked(View view, int position);

    void onUpdateClicked(View view, int position, int quantity);
}
