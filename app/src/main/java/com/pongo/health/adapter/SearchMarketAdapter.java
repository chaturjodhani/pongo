package com.pongo.health.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.MarketResponseModel;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class SearchMarketAdapter extends RecyclerView.Adapter<SearchMarketAdapter.MyMarketSearch> {

    private Context context;
    private List<MarketResponseModel.ProductlistBean> medicationList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public SearchMarketAdapter(Context context, List<MarketResponseModel.ProductlistBean> medicationList,
                               ItemClickListener itemClickListener) {
        this.context = context;
        this.medicationList = medicationList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyMarketSearch onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyMarketSearch(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_market_search, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyMarketSearch holder, int position) {
        String image = medicationList.get(position).getProduct_image();

        Glide.with(context).load(image).into(holder.medicationIv);

        holder.medicationDescriptionTv.setText(medicationList.get(position).getDescription());
        holder.medicationPriceTv.setText("$" + medicationList.get(position).getOrignalprice());
        if (!TextUtils.isEmpty(medicationList.get(position).getDiscountprice())) {
            holder.medicationStrikePriceTv.setText("$" + medicationList.get(position).getDiscountprice());
        }
        holder.medicationStrikePriceTv.setPaintFlags(holder.medicationStrikePriceTv.getPaintFlags()
                | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.medicationNameTv.setText(medicationList.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return medicationList.size();
    }

    class MyMarketSearch extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.medication_name_tv)
        TextView medicationNameTv;
        @BindView(R.id.medication_description_tv)
        TextView medicationDescriptionTv;
        @BindView(R.id.medication_iv)
        CircleImageView medicationIv;
        @BindView(R.id.medication_price_tv)
        TextView medicationPriceTv;
        @BindView(R.id.medication_strike_price_tv)
        TextView medicationStrikePriceTv;
        ItemClickListener itemClickListener;


        public MyMarketSearch(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }
}
