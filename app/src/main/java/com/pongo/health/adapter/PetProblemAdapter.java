package com.pongo.health.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.CategoriesModel;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PetProblemAdapter extends RecyclerView.Adapter<PetProblemAdapter.MyPetProblems> {

    private Context context;
    private List<CategoriesModel.ProblemlistBean> petProblemList = new ArrayList<>();
    private ItemClickListener mItemClickListener;

    public PetProblemAdapter(Context context, List<CategoriesModel.ProblemlistBean> petProblemList, ItemClickListener itemClickListener) {
        this.context = context;
        this.petProblemList = petProblemList;
        mItemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyPetProblems onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyPetProblems(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_pet_problems, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyPetProblems holder, int position) {
        String image = petProblemList.get(position).getImage();
        if (!TextUtils.isEmpty(image)) {
//            Picasso.get().load(image).into(holder.petProblemTypeIv);
            Glide.with(context).load(image).into(holder.petProblemTypeIv);

        }
        holder.petProblemTypeTtv.setText(petProblemList.get(position).getName());
        if (petProblemList.get(position).isCheck()) {
            holder.checkTypeIv.setVisibility(View.VISIBLE);
        } else {
            holder.checkTypeIv.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return petProblemList.size();
    }

    class MyPetProblems extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.pet_problem_type_Iv)
        ImageView petProblemTypeIv;
        @BindView(R.id.multiple_checked_type_iv)
        ImageView checkTypeIv;
        @BindView(R.id.pet_problem_type_tv)
        TextView petProblemTypeTtv;
        private ItemClickListener itemClickListener;

        public MyPetProblems(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemClickListener = mItemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }
}
