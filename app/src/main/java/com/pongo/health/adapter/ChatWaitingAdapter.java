package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.CartModel;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatWaitingAdapter extends RecyclerView.Adapter<ChatWaitingAdapter.MyChatWaiting> {

    private Context context;
    private List<CartModel>chatWaiting = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public ChatWaitingAdapter(Context context, List<CartModel> chatWaiting,
                              ItemClickListener itemClickListener) {
        this.context = context;
        this.chatWaiting = chatWaiting;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyChatWaiting onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyChatWaiting(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_chat_waiting,parent,false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyChatWaiting holder, int position) {
        Glide.with(context).load(chatWaiting.get(position).getItemImage()).into(holder.cwPetIv);

//        Picasso.get().load(chatWaiting.get(position).getItemImage()).into(holder.cwPetIv);
        holder.cwPetDescTv.setText(chatWaiting.get(position).getItemDescription());
        holder.cwPetNameTv.setText(chatWaiting.get(position).getItemName());
        holder.cwPetType.setText(chatWaiting.get(position).getItemQuantity());
    }

    @Override
    public int getItemCount() {
        return chatWaiting.size();
    }


    class MyChatWaiting extends RecyclerView.ViewHolder implements View.OnClickListener{
        
        @BindView(R.id.cw_pet_iv) ImageView cwPetIv;
        @BindView(R.id.cw_pet_desc_tv) TextView cwPetDescTv;
        @BindView(R.id.cw_pet_type) TextView cwPetType;
        @BindView(R.id.cw_pet_name_tv) TextView cwPetNameTv;
        

        ItemClickListener itemClickListener;

        public MyChatWaiting(@NonNull View itemView,
                             ItemClickListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
          itemClickListener.onClicked(view,getAdapterPosition());
        }
    }
}
