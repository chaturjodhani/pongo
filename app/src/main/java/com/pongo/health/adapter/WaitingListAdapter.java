package com.pongo.health.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.CartModel;
import com.pongo.health.model.ChatListModel;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WaitingListAdapter extends RecyclerView.Adapter<WaitingListAdapter.MywaitingList> {

    private Context context;
    private List<ChatListModel> waitingList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private String mStatus;

    public WaitingListAdapter(Context context, List<ChatListModel> waitingList,
                              ItemClickListener itemClickListener, String status) {
        this.context = context;
        this.waitingList = waitingList;
        this.itemClickListener = itemClickListener;
        this.mStatus = status;
    }

    @NonNull
    @Override
    public MywaitingList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MywaitingList(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_waiting_list, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MywaitingList holder, int position) {
        String img = waitingList.get(position).getPetimage();
        if (!TextUtils.isEmpty(img)) {
//            Picasso.get().load(img).into(holder.wPetIv);
            Glide.with(context).load(img).into(holder.wPetIv);

        }
//        holder.wPetDescTv.setText(waitingList.get(position).get());
        holder.wPetDescTv.setText(waitingList.get(position).getCurrentproblem());
        holder.wPetNameTv.setText(waitingList.get(position).getPet_name() + " - " + waitingList.get(position).getPet_age());
        holder.wPetType.setText(waitingList.get(position).getPet_type() + " | " + waitingList.get(position).getBreed());
        String appointmentTime = waitingList.get(position).getBooking_time();
        if (!TextUtils.isEmpty(appointmentTime)) {
            holder.waitingTimeTv.setVisibility(View.VISIBLE);
            holder.waitingTimeTv.setText(waitingList.get(position).getBooking_date() + " " + waitingList.get(position).getBooking_time());

        }

    }

    @Override
    public int getItemCount() {
        return waitingList.size();
    }


    class MywaitingList extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.w_pet_iv)
        ImageView wPetIv;
        @BindView(R.id.w_pet_desc_tv)
        TextView wPetDescTv;
        @BindView(R.id.w_pet_type)
        TextView wPetType;
        @BindView(R.id.w_pet_name_tv)
        TextView wPetNameTv;
        @BindView(R.id.waiting_time_tv)
        TextView waitingTimeTv;
        @BindView(R.id.see_patient_tv)
        TextView seePatientTv;
        @BindView(R.id.color_view)
        View colorView;


        ItemClickListener itemClickListener;

        public MywaitingList(@NonNull View itemView,
                             ItemClickListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
            if (mStatus.equalsIgnoreCase("complete")) {
//                seePatientTv.setVisibility(View.GONE);
                seePatientTv.setText("View Chat");
                colorView.setBackgroundColor(Color.parseColor("#F24E32"));
            } else if (mStatus.equalsIgnoreCase("waiting")) {
                seePatientTv.setText("Begin Chat");
                colorView.setBackgroundColor(Color.parseColor("#4CD32B"));

            } else if (mStatus.equalsIgnoreCase("progress")) {
                seePatientTv.setText("Chat");
                colorView.setBackgroundColor(Color.parseColor("#FBDC71"));

            } else if (mStatus.equalsIgnoreCase("video")) {
                seePatientTv.setText("See Patient");
                colorView.setBackgroundColor(Color.parseColor("#FFFFFF"));

            } else if (mStatus.equalsIgnoreCase("waiting video")) {
                seePatientTv.setText("Start Video");
                colorView.setBackgroundColor(Color.parseColor("#4CD32B"));

            }
        }

        @Override
        public void onClick(View view) {
//            if (!mStatus.equalsIgnoreCase("complete")) {
            itemClickListener.onClicked(view, getAdapterPosition());
//            }
        }
    }
}
