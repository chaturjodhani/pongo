package com.pongo.health.adapter;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.model.SingleProductModel;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HighlightAdapter extends RecyclerView.Adapter<HighlightAdapter.petQunatity> {

    private Activity context;
    private List<SingleProductModel.SingleproductBean.PrescriptionBean> quantityList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public HighlightAdapter(Activity context, List<SingleProductModel.SingleproductBean.PrescriptionBean> quantityList, ItemClickListener itemClickListener) {
        this.context = context;
        this.quantityList = quantityList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public petQunatity onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new petQunatity(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.highlight_adapter, parent, false), itemClickListener);
    }


    @Override
    public void onBindViewHolder(@NonNull petQunatity holder, int position) {
        holder.presc_titleTv.setText(quantityList.get(position).getTitle());
        holder.brand_tv.setText("By " + quantityList.get(position).getBy() + " - " + quantityList.get(position).getDate());
        holder.descTv.setText(quantityList.get(position).getDescription());
        int rating = Integer.parseInt(quantityList.get(position).getRating());
        holder.ratingBar.setRating(rating);

    }

    @Override
    public int getItemCount() {
        return quantityList.size();
//        return 5;
    }

    class petQunatity extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemClickListener itemClickListener;
        @BindView(R.id.presc_title)
        TextView presc_titleTv;
        @BindView(R.id.brand_tv)
        TextView brand_tv;
        @BindView(R.id.des_tv)
        TextView descTv;
        @BindView(R.id.rating_bar)
        AppCompatRatingBar ratingBar;
        @BindView(R.id.root_view)
        LinearLayout mRootview;

        public petQunatity(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
//            itemView.setOnClickListener(this);
            DisplayMetrics metrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int width = metrics.widthPixels;
            ViewGroup.LayoutParams layoutParams = mRootview.getLayoutParams();
            layoutParams.width = width - 200;
            mRootview.setLayoutParams(layoutParams);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }
}

