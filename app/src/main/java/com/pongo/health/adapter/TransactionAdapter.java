package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.CartModel;
import com.pongo.health.model.TransactionModel;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.MyTransaction> {

    private Context context;
    private List<TransactionModel.YourtransactionBean> transactionList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public TransactionAdapter(Context context, List<TransactionModel.YourtransactionBean> transactionList,
                              ItemClickListener itemClickListener) {
        this.context = context;
        this.transactionList = transactionList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyTransaction onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyTransaction(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_transcation, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyTransaction holder, int position) {
        Glide.with(context).load(transactionList.get(position).getImage()).into(holder.transIv);

//        Picasso.get().load(transactionList.get(position).getItemImage()).into(holder.transIv);
        holder.transDateTv.setText("Date of Transaction: " + transactionList.get(position).getOrderdate());
        holder.transNameTv.setText("Order Number: " + transactionList.get(position).getOrderid());
        holder.transPriceTv.setText("Transaction amount: $" + transactionList.get(position).getTotal());
//        holder.transTypeTv.setText("By Card,"+transactionList.get(position).ge());
    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }


    class MyTransaction extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.trans_iv)
        CircleImageView transIv;
        @BindView(R.id.trans_name_tv)
        TextView transNameTv;
        @BindView(R.id.trans_date_tv)
        TextView transDateTv;
        @BindView(R.id.trans_type_tv)
        TextView transTypeTv;
        @BindView(R.id.trans_price_tv)
        TextView transPriceTv;

        ItemClickListener itemClickListener;

        public MyTransaction(@NonNull View itemView,
                             ItemClickListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }
}
