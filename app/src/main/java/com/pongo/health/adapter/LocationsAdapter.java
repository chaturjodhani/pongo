package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.model.OtpModel;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LocationsAdapter extends RecyclerView.Adapter<LocationsAdapter.MyLocation> {

    private Context context;
    private List<OtpModel.StatelistBean> locationList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public LocationsAdapter(Context context, List<OtpModel.StatelistBean> locationList, ItemClickListener itemClickListener) {
        this.context = context;
        this.locationList = locationList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyLocation onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyLocation(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_location, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyLocation holder, int position) {

        holder.locationTv.setText(locationList.get(position).getState());


    }

    @Override
    public int getItemCount() {
        return locationList.size();
    }


    class MyLocation extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.locations_tv)
        TextView locationTv;
        ItemClickListener itemClickListener;

        public MyLocation(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }
}
