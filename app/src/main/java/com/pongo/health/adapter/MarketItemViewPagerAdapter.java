package com.pongo.health.adapter;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.pongo.health.R;
import com.pongo.health.model.ViewPagerModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MarketItemViewPagerAdapter extends RecyclerView.Adapter<MarketItemViewPagerAdapter.MyItems> {

    private List<ViewPagerModel> viewPagerModelList = new ArrayList<>();
    private ViewPager2 viewPager2;


    public MarketItemViewPagerAdapter(List<ViewPagerModel> viewPagerModelList, ViewPager2 viewPager2) {
        this.viewPagerModelList = viewPagerModelList;
        this.viewPager2 = viewPager2;
    }

    @NonNull
    @Override
    public MyItems onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyItems(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_market_item_view_pager,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyItems holder, int position) {
            Picasso.get().load(viewPagerModelList.get(position).getImages()).into(holder.marketItemVpImg);
    }

    @Override
    public int getItemCount() {
        return viewPagerModelList.size();
    }


    class MyItems extends RecyclerView.ViewHolder {

        @BindView( R.id.market_item_vp_img) ImageView marketItemVpImg;


        public MyItems(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

    }


}


