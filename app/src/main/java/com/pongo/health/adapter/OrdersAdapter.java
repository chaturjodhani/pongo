package com.pongo.health.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.model.CheckoutResponseModel;
import com.pongo.health.ui.ActivityHome;
import com.pongo.health.ui.ActivityPayment;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.MyRecentSearches> {

    private Activity context;
    private List<CheckoutResponseModel.CheckoutBean> ordersList = new ArrayList<>();
    private ItemClickListener mItemClickListener;
    private CustomDialog mLoadingView;
    private Session mSession;

    public OrdersAdapter(Activity context, List<CheckoutResponseModel.CheckoutBean> ordersList, ItemClickListener itemClickListener) {
        mLoadingView = new CustomDialog(context);
        mSession = new Session(context);
        this.context = context;
        this.ordersList = ordersList;
        this.mItemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyRecentSearches onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyRecentSearches(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_payment_orders, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyRecentSearches holder, int position) {
        holder.ordersTv.setText(ordersList.get(position).getProduct_name());
        holder.ordersPriceTv.setText("$" + ordersList.get(position).getPrice());
        holder.quantTv.setText("Qty: " + ordersList.get(position).getQuantity());
        if (ordersList.get(position).getAutorenew().equalsIgnoreCase("yes")) {
            holder.refilLy.setVisibility(View.VISIBLE);
        } else {
            holder.refilLy.setVisibility(View.GONE);
        }
        if (ordersList.get(position).getIsautorenew().equalsIgnoreCase("yes")) {
            holder.mSwitch.setChecked(true);
        }
        holder.mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String autorenew = "";
                String product_id = ordersList.get(position).getProduct_id();
                if (isChecked) {
                    autorenew = "yes";
                } else {
                    autorenew = "no";
                }
                callAutoRenewApi(autorenew, product_id);
            }
        });

    }

    private void callAutoRenewApi(String isChecked, String product_id) {
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).autorenew(product_id, isChecked, id).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                                } else {
                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(context, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }


    class MyRecentSearches extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.orders_tv)
        TextView ordersTv;
        @BindView(R.id.orders_price_tv)
        TextView ordersPriceTv;
        @BindView(R.id.quant_tv)
        TextView quantTv;
        @BindView(R.id.delete)
        TextView deleteTv;
        @BindView(R.id.refil_layout)
        LinearLayout refilLy;
        @BindView(R.id.auto_switch)
        SwitchCompat mSwitch;
        ItemClickListener itemClickListener;

        public MyRecentSearches(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemClickListener = mItemClickListener;
            deleteTv.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.delete) {
                itemClickListener.onClicked(v, getAdapterPosition());
            }
        }
    }
}
