package com.pongo.health.adapter;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.model.HomeModel;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AppointmentsAdapter extends RecyclerView.Adapter<AppointmentsAdapter.MyAppointments> {

    private Activity context;
    private List<HomeModel.AppointmentBean> appointmentsList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public AppointmentsAdapter(Activity context, List<HomeModel.AppointmentBean> appointmentsList, ItemClickListener itemClickListener) {
        this.context = context;
        this.appointmentsList = appointmentsList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyAppointments onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyAppointments(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_appointments, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAppointments holder, int position) {

        holder.appointmentsTv.setText(appointmentsList.get(position).getCallon());
    }

    @Override
    public int getItemCount() {
        return appointmentsList.size();
    }


    class MyAppointments extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.appointments_tv)
        TextView appointmentsTv;
        @BindView(R.id.root_view)
        LinearLayout mRootview;
        ItemClickListener itemClickListener;

        public MyAppointments(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
            DisplayMetrics metrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int width = metrics.widthPixels;
//            ViewGroup.LayoutParams layoutParams = mRootview.getLayoutParams();
//            layoutParams.width = width - 130;
//            mRootview.setLayoutParams(layoutParams);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }
}
