package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.model.SingleProductModel;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.MySubscription> {

    private Context context;
    private List<SingleProductModel.SingleproductBean.FaqBean> subscriptionList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public FaqAdapter(Context context, List<SingleProductModel.SingleproductBean.FaqBean> subscriptionList,
                      ItemClickListener itemClickListener) {
        this.context = context;
        this.subscriptionList = subscriptionList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MySubscription onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MySubscription(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.faq_adapter, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MySubscription holder, int position) {

//        holder.questionTv.setText(subscriptionList.get(position).getQuestion());
        holder.answerTv.setText(subscriptionList.get(position).getAnswer());
    }

    @Override
    public int getItemCount() {
        return subscriptionList.size();
    }


    class MySubscription extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.question)
        TextView questionTv;
        @BindView(R.id.answer)
        TextView answerTv;

        ItemClickListener itemClickListener;

        public MySubscription(@NonNull View itemView,
                              ItemClickListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }
}

