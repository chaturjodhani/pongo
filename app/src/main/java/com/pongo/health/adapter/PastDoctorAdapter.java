package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.DoctorWaitingListModel;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class PastDoctorAdapter extends RecyclerView.Adapter<PastDoctorAdapter.MyCategories> {

    private Context context;
    private List<DoctorWaitingListModel.WatingBean.PastdoctorBean> pastDoctorList = new ArrayList<>();
    private ItemClickListener itemClickListener;


    public PastDoctorAdapter(Context context, List<DoctorWaitingListModel.WatingBean.PastdoctorBean> categoriesModelList,
                             ItemClickListener itemClickListener) {
        this.context = context;
        this.pastDoctorList = categoriesModelList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyCategories onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyCategories(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.past_doctor_adapter, parent, false),
                itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyCategories holder, int position) {
//        Picasso.get().load(imageList.get(position)).into(holder.typeIv);
        Glide.with(context).load(pastDoctorList.get(position).getUserimg()).into(holder.doctorIv);
        holder.nameTv.setText(pastDoctorList.get(position).getName());
        holder.issueTv.setText("Issue - " + pastDoctorList.get(position).getProblems());

    }

    @Override
    public int getItemCount() {
        if (null != pastDoctorList) {
            return pastDoctorList.size();
        } else {
            return 0;
        }
    }


    class MyCategories extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.vp_dr_iv)
        CircleImageView doctorIv;
        @BindView(R.id.vp_dr_name_tv)
        TextView nameTv;
        @BindView(R.id.issue)
        TextView issueTv;


        ItemClickListener itemClickListener;

        public MyCategories(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }
}



