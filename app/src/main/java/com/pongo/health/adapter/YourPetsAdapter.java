package com.pongo.health.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.HomeModel;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class YourPetsAdapter extends RecyclerView.Adapter {

    private Activity context;
    private List<HomeModel.PetlistBean> petsModelList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int getItemViewType(int position) {
        if (petsModelList.get(position).getType().equalsIgnoreCase("add")) {
            return 0;
        }
        return 1;
    }


    public YourPetsAdapter(Activity context, List<HomeModel.PetlistBean> petsModelList,
                           ItemClickListener itemClickListener) {
        this.context = context;
        this.petsModelList = petsModelList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 1) {
            return new MyPets(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.single_item_view_your_pets_, parent, false),
                    itemClickListener);

        }

        return new MyPetsAdd(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_your_pets_add, parent, false),
                itemClickListener);
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (petsModelList.get(position).getType().equalsIgnoreCase("add")) {
            MyPetsAdd viewHolder2 = (MyPetsAdd) holder;
        } else {
            MyPets viewHolder = (MyPets) holder;
            String image = petsModelList.get(position).getPetimage();
            if (!TextUtils.isEmpty(image)) {
//                Picasso.get().load(image).into(viewHolder.petImage);
                Glide.with(context).load(image).into(((MyPets) holder).petImage);

            }
            viewHolder.petYearTv.setText(petsModelList.get(position).getBreed() + " | " + petsModelList.get(position).getPet_age());
            if (!TextUtils.isEmpty(petsModelList.get(position).getPet_name())) {
                viewHolder.petNameTv.setText(petsModelList.get(position).getPet_name());
                viewHolder.petNameTv.setVisibility(View.VISIBLE);
            } else {
                viewHolder.petNameTv.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(petsModelList.get(position).getPet_note())) {
                viewHolder.petDescriptionTv.setText(petsModelList.get(position).getPet_note());
                viewHolder.petDescriptionTv.setVisibility(View.VISIBLE);
            } else {
                viewHolder.petDescriptionTv.setVisibility(View.GONE);
            }
        }

    }


    @Override
    public int getItemCount() {
        return petsModelList.size();
    }


    class MyPets extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemClickListener itemClickListener;
        @BindView(R.id.pet_year_tv)
        TextView petYearTv;
        @BindView(R.id.pet_name_tv)
        TextView petNameTv;
        @BindView(R.id.pet_image)
        CircleImageView petImage;
        @BindView(R.id.pet_description_tv)
        TextView petDescriptionTv;
        @BindView(R.id.root_view)
        LinearLayout mRootview;

        public MyPets(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
           /* DisplayMetrics metrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int width = metrics.widthPixels;
            ViewGroup.LayoutParams layoutParams = mRootview.getLayoutParams();
            layoutParams.width = width - 130;
            mRootview.setLayoutParams(layoutParams);*/
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }

    class MyPetsAdd extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemClickListener itemClickListener;
        @BindView(R.id.add_pet_ll)
        LinearLayout mRootview;

        public MyPetsAdd(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
          /*  DisplayMetrics metrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int width = metrics.widthPixels;
            ViewGroup.LayoutParams layoutParams = mRootview.getLayoutParams();
            layoutParams.width = width - 130;
            mRootview.setLayoutParams(layoutParams);*/
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());

        }
    }

}
