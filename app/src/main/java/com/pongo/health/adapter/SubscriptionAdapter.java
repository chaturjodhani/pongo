package com.pongo.health.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.CartModel;
import com.pongo.health.model.TransactionModel;
import com.pongo.health.utils.ItemClickListener;
import com.pongo.health.utils.TransactionListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubscriptionAdapter extends RecyclerView.Adapter<SubscriptionAdapter.MySubscription> {

    private Context context;
    private List<TransactionModel.SubscribeproductBean> subscriptionList = new ArrayList<>();
    private TransactionListener itemClickListener;

    public SubscriptionAdapter(Context context, List<TransactionModel.SubscribeproductBean> subscriptionList,
                               TransactionListener itemClickListener) {
        this.context = context;
        this.subscriptionList = subscriptionList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MySubscription onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MySubscription(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_subsrciption, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MySubscription holder, int position) {

//        Picasso.get().load(subscriptionList.get(position).getItemImage()).into(holder.subIv);
        Glide.with(context).load(subscriptionList.get(position).getImage()).into(holder.subIv);

        holder.subDateTv.setText("Date of Transaction: " + subscriptionList.get(position).getOrderdate());
        holder.subPriceTv.setText("Transaction amount: $" + subscriptionList.get(position).getTotal());
        String type = subscriptionList.get(position).getType();
        if (!TextUtils.isEmpty(type) && type.equalsIgnoreCase("plan")) {
            holder.subCancelTv.setVisibility(View.VISIBLE);
            holder.subNameTv.setText(subscriptionList.get(position).getOrderid());

        } else {
            holder.subCancelTv.setVisibility(View.GONE);
            holder.subNameTv.setText("Order Number: " + subscriptionList.get(position).getOrderid());

        }
//        holder.subTypeTv.setText(subscriptionList.get(position).get());
    }

    @Override
    public int getItemCount() {
        return subscriptionList.size();
    }


    class MySubscription extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.sub_iv)
        ImageView subIv;
        @BindView(R.id.sub_name_tv)
        TextView subNameTv;
        @BindView(R.id.sub_date_tv)
        TextView subDateTv;
        @BindView(R.id.sub_type_tv)
        TextView subTypeTv;
        @BindView(R.id.sub_price_tv)
        TextView subPriceTv;
        @BindView(R.id.sub_cancel_tv)
        TextView subCancelTv;

        TransactionListener itemClickListener;

        public MySubscription(@NonNull View itemView,
                              TransactionListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
            subCancelTv.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.sub_cancel_tv) {
                itemClickListener.onCancelClicked(view, getAdapterPosition());

            } else {
                itemClickListener.onClicked(view, getAdapterPosition());
            }
        }
    }
}
