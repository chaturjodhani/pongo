package com.pongo.health.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.PetRecommendationModel;
import com.pongo.health.model.PetsModel;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddPetRecommendationAdapter extends RecyclerView.Adapter<AddPetRecommendationAdapter.PetRecommendation> {

    private Context context;
    private List<PetRecommendationModel.ProductdataBean> recommendationList = new ArrayList<>();
    public int selectedPosition = -1;
    private ItemClickListener itemClickListener;
    private String petType;

    public AddPetRecommendationAdapter(Context context, List<PetRecommendationModel.ProductdataBean> recommendationList, ItemClickListener itemClickListener, String petType) {
        this.context = context;
        this.recommendationList = recommendationList;
        this.itemClickListener = itemClickListener;
        this.petType = petType;
    }


    @NonNull
    @Override
    public PetRecommendation onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PetRecommendation(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_add_pet_recommedndation, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull PetRecommendation holder, int position) {
        String image = recommendationList.get(position).getProduct_image();
        if (!TextUtils.isEmpty(image)) {
//            Picasso.get().load(image).into(holder.recommIv);
            Glide.with(context).load(image).into(holder.recommIv);

        }
        holder.recommNameTv.setText(recommendationList.get(position).getName());
//        holder.recommYearTv.setText(recommendationList.get(position).getDescription());
//        holder.priceTv.setText("");
//        holder.recommDescriptionTv.setText("Recommendation for " + petType);

        if (selectedPosition == position) {
            holder.recommendedLl.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shaped_selected));
            holder.recommNameTv.setTextColor(context.getResources().getColor(R.color.white_color));
            holder.recommYearTv.setTextColor(context.getResources().getColor(R.color.white_color));
            holder.priceTv.setTextColor(context.getResources().getColor(R.color.white_color));
            holder.recommDescriptionTv.setTextColor(context.getResources().getColor(R.color.white_color));
            holder.medicationPriceTv.setTextColor(context.getResources().getColor(R.color.white_color));
            holder.medicationStrikePriceTv.setTextColor(context.getResources().getColor(R.color.white_color));
//            holder.recommDescriptionTv.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shaped_selected));
            holder.recommCheckedIv.setVisibility(View.VISIBLE);
        } else {
            holder.recommendedLl.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shaped));
            holder.recommNameTv.setTextColor(context.getResources().getColor(R.color.black_color));
            holder.recommYearTv.setTextColor(context.getResources().getColor(R.color.text_color_blue));
            holder.priceTv.setTextColor(context.getResources().getColor(R.color.text_color_blue));
            holder.recommDescriptionTv.setTextColor(context.getResources().getColor(R.color.black_color));
            holder.medicationPriceTv.setTextColor(context.getResources().getColor(R.color.text_color_blue));
            holder.medicationStrikePriceTv.setTextColor(context.getResources().getColor(R.color.text_color_blue));
//            holder.recommDescriptionTv.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shaped));
            holder.recommCheckedIv.setVisibility(View.GONE);
        }

        holder.recommDescriptionTv.setText(recommendationList.get(position).getDescription());
        holder.medicationPriceTv.setText("$" + recommendationList.get(position).getOrignalprice());
        if (!TextUtils.isEmpty(recommendationList.get(position).getDiscountprice())) {
            holder.medicationStrikePriceTv.setText("$" + recommendationList.get(position).getDiscountprice());
        }
        holder.medicationStrikePriceTv.setPaintFlags(holder.medicationStrikePriceTv.getPaintFlags()
                | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.itemView.setOnClickListener(v -> {
            if (selectedPosition == position) {
                selectedPosition = -1;
            } else {
                selectedPosition = position;

            }
            notifyDataSetChanged();

        });
    }

    @Override
    public int getItemCount() {
        return recommendationList.size();
    }


    public class PetRecommendation extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.recomm_iv)
        ImageView recommIv;
        @BindView(R.id.recomm_name_tv)
        TextView recommNameTv;
        @BindView(R.id.recomm_year_tv)
        TextView recommYearTv;
        @BindView(R.id.recomm_description_tv)
        TextView recommDescriptionTv;
        @BindView(R.id.recomm_price_tv)
        TextView priceTv;
        @BindView(R.id.recomm_checked_iv)
        ImageView recommCheckedIv;
        @BindView(R.id.recommended_ll)
        LinearLayout recommendedLl;
        @BindView(R.id.medication_price_tv)
        TextView medicationPriceTv;
        @BindView(R.id.medication_strike_price_tv)
        TextView medicationStrikePriceTv;
        ItemClickListener itemClickListener;

        public PetRecommendation(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
//            itemView.setOnClickListener(this);
            this.itemClickListener = itemClickListener;

        }

        @Override
        public void onClick(View view) {
//            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }
}
