package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.PetsModel;
import com.pongo.health.model.PongoVetsModel;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PongoVetsAdapter extends RecyclerView.Adapter<PongoVetsAdapter.MyPongVets> {

    private Context context;
    private List<PongoVetsModel.DetailBean> vetsList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public PongoVetsAdapter(Context context, List<PongoVetsModel.DetailBean> vetsList, ItemClickListener itemClickListener) {
        this.context = context;
        this.vetsList = vetsList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyPongVets onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyPongVets(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_pongo_vets, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyPongVets holder, int position) {
        //  Picasso.get().load(categoriesModelList.get(position).getImage()).into(holder.drIv);
        Glide.with(context).load(vetsList.get(position).getUserimg()).into(holder.drIv);
        holder.drNameTv.setText(vetsList.get(position).getName());
        holder.drSeenDateTv.setText("seen Date : " + vetsList.get(position).getSeendate());
        holder.drPetNameTv.setText("Pet Name : " + vetsList.get(position).getPet_name());

    }

    @Override
    public int getItemCount() {
        return vetsList.size();
    }

    class MyPongVets extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.dr_name_tv)
        TextView drNameTv;
        @BindView(R.id.dr_seen_date_tv)
        TextView drSeenDateTv;
        @BindView(R.id.dr_pet_name_tv)
        TextView drPetNameTv;
        @BindView(R.id.dr_iv)
        ImageView drIv;
        @BindView(R.id.book_again_tv)
        TextView bookAgainTv;
        ItemClickListener itemClickListener;

        public MyPongVets(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemClickListener = itemClickListener;
            bookAgainTv.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }
}
