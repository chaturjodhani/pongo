package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.model.SingleProductModel;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BreedSizeAdapter extends RecyclerView.Adapter<BreedSizeAdapter.petStrength> {

    private Context context;
    private List<SingleProductModel.SingleproductBean.BreedSizeBean> strengthList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public BreedSizeAdapter(Context context, List<SingleProductModel.SingleproductBean.BreedSizeBean> strengthList,
                                     ItemClickListener itemClickListener) {
        this.context = context;
        this.strengthList = strengthList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public BreedSizeAdapter.petStrength onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BreedSizeAdapter.petStrength(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_medication_strength,parent,false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull BreedSizeAdapter.petStrength holder, int position) {
        holder.sStrengthTv.setText(strengthList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return strengthList.size();
    }

    class petStrength extends RecyclerView.ViewHolder implements View.OnClickListener{
        ItemClickListener itemClickListener;
        @BindView(R.id.s_strength_tv)
        TextView sStrengthTv;

        public petStrength(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view,getAdapterPosition());
        }
    }
}
