package com.pongo.health.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.pongo.health.fragment.medicationtabs.FaqFragment;
import com.pongo.health.fragment.medicationtabs.HighlightFragment;
import com.pongo.health.fragment.medicationtabs.IngredientFragment;
import com.pongo.health.fragment.medicationtabs.ReviewFragment;
import com.pongo.health.fragment.medicationtabs.SideEffectFragment;
import com.pongo.health.model.SingleProductModel;

public class TabAdapter extends FragmentPagerAdapter {

    private Context myContext;
    int totalTabs;
    private SingleProductModel mSingleProductModel;

    public TabAdapter(Context context, FragmentManager fm, int totalTabs, SingleProductModel singleProductModel) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        myContext = context;
        this.totalTabs = totalTabs;
        this.mSingleProductModel = singleProductModel;
    }

    // this is for fragment tabs
    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new HighlightFragment(mSingleProductModel);
            case 1:
                return new ReviewFragment(mSingleProductModel);
            case 2:
                return new FaqFragment(mSingleProductModel);
            case 3:
                return new IngredientFragment(mSingleProductModel);
            case 4:
                return new SideEffectFragment(mSingleProductModel);
            default:
                return null;
        }
    }

    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}