package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IssueAdapter extends RecyclerView.Adapter<IssueAdapter.MyCategories> {

    private Context context;
    private List<String> issueList = new ArrayList<>();
    private ItemClickListener itemClickListener;


    public IssueAdapter(Context context, List<String> categoriesModelList,
                        ItemClickListener itemClickListener) {
        this.context = context;
        this.issueList = categoriesModelList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public IssueAdapter.MyCategories onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new IssueAdapter.MyCategories(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.issue_adapter, parent, false),
                itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull IssueAdapter.MyCategories holder, int position) {
//        Picasso.get().load(imageList.get(position)).into(holder.typeIv);
   /*     if (!imageList.get(position).contains(".pdf")) {
            Glide.with(context).load(imageList.get(position)).into(holder.typeIv);

        }*/

        holder.issueTv.setText(issueList.get(position));

    }

    @Override
    public int getItemCount() {
        if (null != issueList) {
            return issueList.size();
        } else {
            return 0;
        }
    }


    class MyCategories extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.issue_name)
        TextView issueTv;


        ItemClickListener itemClickListener;

        public MyCategories(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            this.itemClickListener = itemClickListener;

        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }
}


