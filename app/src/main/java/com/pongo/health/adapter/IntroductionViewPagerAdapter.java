package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;


import com.pongo.health.R;
import com.pongo.health.model.ViewPagerModel;

import java.util.ArrayList;
import java.util.List;

public class IntroductionViewPagerAdapter extends PagerAdapter {

    Context context;
    List<ViewPagerModel> viewPagerModelList = new ArrayList<>();


    public IntroductionViewPagerAdapter(Context context, List<ViewPagerModel> viewPagerModelList) {
        this.context = context;
        this.viewPagerModelList = viewPagerModelList;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.custom_introdution_view_pager,null);

        ImageView image = layout.findViewById(R.id.vp_img);
       // TextView txtOne = layout.findViewById(R.id.intro_heading_text_tv);
        TextView txtTwo= layout.findViewById(R.id.intro_heading_text_second_tv);


        image.setImageResource(viewPagerModelList.get(position).getImages());
      //  txtOne.setText(viewPagerModelList.get(position).getHeader1());
        txtTwo.setText(viewPagerModelList.get(position).getHeader2());

        container.addView(layout);

        return layout;
    }


    @Override
    public int getCount() {
        return viewPagerModelList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}


