package com.pongo.health.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.HospitalSearchModel;
import com.pongo.health.model.MarketResponseModel;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class SearchHospitalAdapter extends RecyclerView.Adapter<SearchHospitalAdapter.MyMarketSearch> {

    private Context context;
    private List<HospitalSearchModel.ResultsBean> hospitalList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private int mPosition = -1;

    public SearchHospitalAdapter(Context context, List<HospitalSearchModel.ResultsBean> medicationList,
                                 ItemClickListener itemClickListener) {
        this.context = context;
        this.hospitalList = medicationList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyMarketSearch onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyMarketSearch(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hospitals_adapter, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyMarketSearch holder, int position) {
        List<HospitalSearchModel.ResultsBean.PhotosBean> imageArray = hospitalList.get(position).getPhotos();
        if (null != imageArray && imageArray.size() > 0) {
            int maxwidth = imageArray.get(0).getWidth();

            String photoreference = imageArray.get(0).getPhoto_reference();
            String key = "AIzaSyCIhWIx08t5ap5neBIX1B3npieSP9HQSdY";
            String image = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=" + maxwidth + "&photoreference=" + photoreference + "&key=" + key;
            Glide.with(context).load(image).into(holder.medicationIv);

        } else {
            Glide.with(context).load("https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png").into(holder.medicationIv);

        }

        holder.medicationDescriptionTv.setText(hospitalList.get(position).getVicinity());
        holder.medicationNameTv.setText(hospitalList.get(position).getName());
        if (mPosition == position) {
            holder.medicationDescriptionTv.setTextColor(context.getResources().getColor(R.color.white_color));
            holder.medicationNameTv.setTextColor(context.getResources().getColor(R.color.white_color));
            holder.mRootView.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shaped_selected));
        } else {
            holder.medicationDescriptionTv.setTextColor(context.getResources().getColor(R.color.sub_text_color));
            holder.medicationNameTv.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.mRootView.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shaped_two));

        }

    }

    @Override
    public int getItemCount() {
        return hospitalList.size();
    }

    class MyMarketSearch extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.trans_name_tv)
        TextView medicationNameTv;
        @BindView(R.id.trans_type_tv)
        TextView medicationDescriptionTv;
        @BindView(R.id.trans_iv)
        CircleImageView medicationIv;
        @BindView(R.id.root_view)
        LinearLayout mRootView;
        ItemClickListener itemClickListener;


        public MyMarketSearch(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mPosition = getAdapterPosition();
            itemClickListener.onClicked(view, getAdapterPosition());
            notifyDataSetChanged();
        }
    }
}