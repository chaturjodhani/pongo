package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.model.PastVetSearchModel;
import com.pongo.health.utils.DeleteListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecentSearchesAdapter extends RecyclerView.Adapter<RecentSearchesAdapter.MyRecentSearches> {

    private Context context;
    private List<PastVetSearchModel.RecentBean> recentSearchesList = new ArrayList<>();
    DeleteListener mListener;

    public RecentSearchesAdapter(Context context, List<PastVetSearchModel.RecentBean> recentSearchesList, DeleteListener deleteListener) {
        this.context = context;
        this.recentSearchesList = recentSearchesList;
        mListener = deleteListener;
    }

    @NonNull
    @Override
    public MyRecentSearches onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyRecentSearches(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_recent_searches, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyRecentSearches holder, int position) {
        holder.recentSearchesTv.setText(recentSearchesList.get(position).getSearch());
    }

    @Override
    public int getItemCount() {
        return recentSearchesList.size();
    }


    class MyRecentSearches extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.recent_searches_tv)
        TextView recentSearchesTv;
        @BindView(R.id.recent_searches_iv)
        ImageView recentSearchesIv;
        DeleteListener deleteListener;

        public MyRecentSearches(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            deleteListener = mListener;
            recentSearchesIv.setOnClickListener(this);
            recentSearchesTv.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.recent_searches_iv: {
                    deleteListener.onDeleteClicked(view, getAdapterPosition());
                    break;
                }
                case R.id.recent_searches_tv: {
                    deleteListener.onkeyClicked(view, getAdapterPosition());
                    break;
                }
            }
        }
    }
}
