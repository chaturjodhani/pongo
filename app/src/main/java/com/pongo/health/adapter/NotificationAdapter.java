package com.pongo.health.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.NotificationResponseModel;
import com.pongo.health.model.PetSizeModel;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyNotification> {

    private Context context;
    private List<NotificationResponseModel.NotifyBean> notificationList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public NotificationAdapter(Context context, List<NotificationResponseModel.NotifyBean> notificationList,
                               ItemClickListener itemClickListener) {
        this.context = context;
        this.notificationList = notificationList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyNotification onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyNotification(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_notification, parent, false), itemClickListener);

    }

    @Override
    public void onBindViewHolder(@NonNull MyNotification holder, int position) {
        String image = notificationList.get(position).getImage();
        if (!TextUtils.isEmpty(image)) {
//            Picasso.get().load(image).into(holder.notiPicImg);
            Glide.with(context).load(image).into(holder.notiPicImg);

        }
        holder.notiNameTv.setText(notificationList.get(position).getId());
        holder.notiDescTv.setText(notificationList.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }


    class MyNotification extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemClickListener itemClickListener;
        @BindView(R.id.noti_pic_img)
        ImageView notiPicImg;
        @BindView(R.id.noti_name_tv)
        TextView notiNameTv;
        @BindView(R.id.noti_desc_tv)
        TextView notiDescTv;


        public MyNotification(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());

        }
    }
}
