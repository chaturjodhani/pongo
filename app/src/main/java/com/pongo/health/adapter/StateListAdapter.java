package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.model.CheckoutResponseModel;
import com.pongo.health.model.StateListModel;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StateListAdapter extends RecyclerView.Adapter<StateListAdapter.MyRecentSearches> {

    private Context context;
    private List<StateListModel.StatelistBean> stateList = new ArrayList<>();
    private int mPosition = -1;
    private ItemClickListener mItemClickListener;

    public StateListAdapter(Context context, List<StateListModel.StatelistBean> ordersList, ItemClickListener itemClickListener) {
        this.context = context;
        this.stateList = ordersList;
        this.mItemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public StateListAdapter.MyRecentSearches onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new StateListAdapter.MyRecentSearches(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_list_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull StateListAdapter.MyRecentSearches holder, int position) {
        holder.cardTv.setText(stateList.get(position).getState());
//        holder.carnameTv.setText(cardList.get(position).getCustomer());
        if (mPosition == position) {
            holder.rootly.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shaped_two));
        } else {
            holder.rootly.setBackground(context.getResources().getDrawable(R.drawable.white_box));
        }

    }

    @Override
    public int getItemCount() {
        return stateList.size();
    }


    class MyRecentSearches extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.card_tv)
        TextView cardTv;
        @BindView(R.id.card_name)
        TextView carnameTv;
        @BindView(R.id.root_view)
        LinearLayout rootly;
        ItemClickListener itemClickListener;

        public MyRecentSearches(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemClickListener = mItemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mPosition = getAdapterPosition();
            itemClickListener.onClicked(view, getAdapterPosition());
        }

    }
}
