package com.pongo.health.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.CartModel;
import com.pongo.health.model.EarningModel;
import com.pongo.health.ui.AddNoteActivity;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PetCasesAdapter extends RecyclerView.Adapter<PetCasesAdapter.MyPetCasesList> {
    
    private Context context;
    private List<EarningModel.PethistoryBean> petCasesList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public PetCasesAdapter(Context context, List<EarningModel.PethistoryBean> petCasesList,
                           ItemClickListener itemClickListener) {
        this.context = context;
        this.petCasesList = petCasesList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyPetCasesList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyPetCasesList(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_pet_cases,parent,false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyPetCasesList holder, int position) {
        Glide.with(context).load(petCasesList.get(position).getPetimage()).into(holder.pcPetIv);

        holder.pcPetDescTv.setText(petCasesList.get(position).getProblemsissue());
        holder.pcPetNameTv.setText(petCasesList.get(position).getPet_name()+" - "+petCasesList.get(position).getPet_age());
        holder.pcPetType.setText(petCasesList.get(position).getPet_type()+  " | "+petCasesList.get(position).getBreed());
        holder.billingWaitingDateTv.setText(petCasesList.get(position).getDate());

    }

    @Override
    public int getItemCount() {
        return petCasesList.size();
    }


    class MyPetCasesList extends RecyclerView.ViewHolder implements View.OnClickListener{
        
        @BindView(R.id.pc_pet_iv) ImageView pcPetIv;
        @BindView(R.id.pc_pet_desc_tv) TextView pcPetDescTv;
        @BindView(R.id.pc_pet_type) TextView pcPetType;
        @BindView(R.id.pc_pet_name_tv) TextView pcPetNameTv;
        @BindView(R.id.pc_waiting_date_tv) TextView billingWaitingDateTv;
        @BindView(R.id.patient_history_tv) TextView seePatientTv;


        ItemClickListener itemClickListener;

        public MyPetCasesList(@NonNull View itemView,
                             ItemClickListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            this.itemClickListener = itemClickListener;
            seePatientTv.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
          itemClickListener.onClicked(view,getAdapterPosition());
        }
    }
}
