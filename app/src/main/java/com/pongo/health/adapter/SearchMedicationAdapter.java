package com.pongo.health.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.ProductResponseModel;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class SearchMedicationAdapter extends RecyclerView.Adapter<SearchMedicationAdapter.MySearchMedication> {
    private Context context;
    private List<ProductResponseModel.ProductlistBean> searchMedicationList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public SearchMedicationAdapter(Context context, List<ProductResponseModel.ProductlistBean> searchMedicationList,
                                   ItemClickListener itemClickListener) {
        this.context = context;
        this.searchMedicationList = searchMedicationList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MySearchMedication onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MySearchMedication(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_medication_search, parent, false), itemClickListener);
    }


    @Override
    public void onBindViewHolder(@NonNull MySearchMedication holder, int position) {
        String image = searchMedicationList.get(position).getProduct_image();
        if (!TextUtils.isEmpty(image)) {
//            Picasso.get().load(image).into(holder.searchMedicationIv);
            Glide.with(context).load(image).into(holder.searchMedicationIv);

        }
        holder.searchMedicationDescriptionTv.setText(searchMedicationList.get(position).getDescription());
        holder.searchMedicationPriceTv.setText("$" + searchMedicationList.get(position).getOrignalprice());
        holder.searchMedicationNameTv.setText(searchMedicationList.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return searchMedicationList.size();
    }

    class MySearchMedication extends RecyclerView.ViewHolder {

        @BindView(R.id.search_medication_iv)
        CircleImageView searchMedicationIv;
        @BindView(R.id.search_medication_name_tv)
        TextView searchMedicationNameTv;
        @BindView(R.id.search_medication_description_tv)
        TextView searchMedicationDescriptionTv;
        @BindView(R.id.search_medication_price_tv)
        TextView searchMedicationPriceTv;
        ItemClickListener itemClickListener;

        public MySearchMedication(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onClicked(view, getAdapterPosition());
                }
            });
        }


    }
}
