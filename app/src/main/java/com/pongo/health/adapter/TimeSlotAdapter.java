package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.model.TimeslotModel;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.petQunatity> {

    private Context context;
    private List<TimeslotModel.TimeslotsBean> quantityList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    public int mPosition = -1;

    public TimeSlotAdapter(Context context, List<TimeslotModel.TimeslotsBean> quantityList, ItemClickListener itemClickListener) {
        this.context = context;
        this.quantityList = quantityList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public petQunatity onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new petQunatity(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.time_slot_adapter, parent, false), itemClickListener);
    }


    @Override
    public void onBindViewHolder(@NonNull petQunatity holder, int position) {
        holder.sQuantityTv.setText(quantityList.get(position).getStart());
        if (mPosition == position) {
            holder.sQuantityTv.setBackground(context.getResources().getDrawable(R.drawable.rectangle_blue_shape));
            holder.sQuantityTv.setTextColor(context.getResources().getColor(R.color.white_color));
        } else {
            holder.sQuantityTv.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shaped_two));
            holder.sQuantityTv.setTextColor(context.getResources().getColor(R.color.black_color));

        }

    }

    @Override
    public int getItemCount() {
        return quantityList.size();
    }

    class petQunatity extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemClickListener itemClickListener;
        @BindView(R.id.quantity_tv)
        TextView sQuantityTv;

        public petQunatity(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mPosition = getAdapterPosition();
            itemClickListener.onClicked(view, getAdapterPosition());
            notifyDataSetChanged();
        }
    }
}
