package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.model.CardListResponseModel;
import com.pongo.health.model.CheckoutResponseModel;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentCardAdapter extends RecyclerView.Adapter<PaymentCardAdapter.MyRecentSearches> {

    private Context context;
    private List<CardListResponseModel.CardlistBean> cardList = new ArrayList<>();
    private int mPosition = -1;
    private ItemClickListener mItemClickListener;

    public PaymentCardAdapter(Context context, List<CardListResponseModel.CardlistBean> ordersList, ItemClickListener itemClickListener) {
        this.context = context;
        this.cardList = ordersList;
        this.mItemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyRecentSearches onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyRecentSearches(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_list_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyRecentSearches holder, int position) {
        holder.cardTv.setText(cardList.get(position).getCardnumber());
        holder.carnameTv.setText(cardList.get(position).getCard_type());
       /* if (mPosition == position) {
            holder.rootly.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shaped_two));
        } else {
            holder.rootly.setBackground(context.getResources().getDrawable(R.drawable.grey_rectangle));
        }
*/
    }

    @Override
    public int getItemCount() {
        return cardList.size();
    }


    class MyRecentSearches extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.card_tv)
        TextView cardTv;
        @BindView(R.id.card_name)
        TextView carnameTv;
        @BindView(R.id.root_view)
        LinearLayout rootly;
        ItemClickListener itemClickListener;

        public MyRecentSearches(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemClickListener = mItemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mPosition = getAdapterPosition();
            itemClickListener.onClicked(view, getAdapterPosition());
        }

    }
}

