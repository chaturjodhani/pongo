package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.model.BreedDynamicModel;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BreedAdapter extends RecyclerView.Adapter<BreedAdapter.MySubscription> {

    private Context context;
    private List<BreedDynamicModel> subscriptionList ;
    private ItemClickListener itemClickListener;

    public BreedAdapter(Context context, List<BreedDynamicModel> subscriptionList,
                         ItemClickListener itemClickListener) {
        this.context = context;
        this.subscriptionList = subscriptionList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public BreedAdapter.MySubscription onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BreedAdapter.MySubscription(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.breed_adapter, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull BreedAdapter.MySubscription holder, int position) {

        holder.breedTv.setText(subscriptionList.get(position).getBreed());
    }

    @Override
    public int getItemCount() {
        return subscriptionList.size();
    }


    class MySubscription extends RecyclerView.ViewHolder implements View.OnClickListener {


        @BindView(R.id.breed)
        TextView breedTv;

        ItemClickListener itemClickListener;

        public MySubscription(@NonNull View itemView,
                              ItemClickListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }

    public void filterList(List<BreedDynamicModel> filterdNames) {
        this.subscriptionList = filterdNames;
        notifyDataSetChanged();
    }
}
