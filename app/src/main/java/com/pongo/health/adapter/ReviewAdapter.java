package com.pongo.health.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.ReviewModel;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.MySubscription> {

    private Context context;
    private List<ReviewModel> subscriptionList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public ReviewAdapter(Context context, List<ReviewModel> subscriptionList,
                         ItemClickListener itemClickListener) {
        this.context = context;
        this.subscriptionList = subscriptionList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MySubscription onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MySubscription(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_adapter, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MySubscription holder, int position) {
        String img = subscriptionList.get(position).getImages();
        if (!TextUtils.isEmpty(img)) {
//            Picasso.get().load(img).into(holder.subIv);
            Glide.with(context).load(img).into(holder.subIv);

        }
        holder.subDateTv.setText(subscriptionList.get(position).getHeader2());
        holder.subNameTv.setText(subscriptionList.get(position).getHeader1());
        holder.ratingText.setText("( " + subscriptionList.get(position).getRating() + " )");
        holder.ratingBar.setRating(subscriptionList.get(position).getRating());
    }

    @Override
    public int getItemCount() {
        return subscriptionList.size();
    }


    class MySubscription extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.sub_iv)
        CircleImageView subIv;
        @BindView(R.id.sub_name_tv)
        TextView subNameTv;
        @BindView(R.id.sub_tv)
        TextView subDateTv;
        @BindView(R.id.rating_text)
        TextView ratingText;
        @BindView(R.id.rating_bar)
        AppCompatRatingBar ratingBar;

        ItemClickListener itemClickListener;

        public MySubscription(@NonNull View itemView,
                              ItemClickListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }
}
