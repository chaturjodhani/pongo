package com.pongo.health.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.DynamicPagerModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PrescribingPagerAdapter extends PagerAdapter {

    Context context;
    List<DynamicPagerModel> viewPagerModelList = new ArrayList<>();


    public PrescribingPagerAdapter(Context context, List<DynamicPagerModel> viewPagerModelList) {
        this.context = context;
        this.viewPagerModelList = viewPagerModelList;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.custom_medication_view_pager, null);

        ImageView image = layout.findViewById(R.id.medication_vp_img);
        String imageUrl = viewPagerModelList.get(position).getImages();
        if (!TextUtils.isEmpty(imageUrl))
        {
//            Picasso.get().load(imageUrl).into(image);
            Glide.with(context).load(imageUrl).into(image);

        }
//        image.setImageResource(viewPagerModelList.get(position).getImages());
        container.addView(layout);

        return layout;
    }


    @Override
    public int getCount() {
        return viewPagerModelList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}



