package com.pongo.health.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.CartResponseModel;
import com.pongo.health.utils.ItemUpdateListener;
import com.pongo.health.utils.MySpinner;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyCartItems> {

    private Context context;
    private List<CartResponseModel.CartlistBean> cartList = new ArrayList<>();
    private ItemUpdateListener mItemClickListener;
    public boolean apiCalled = false;
//    ArrayAdapter userAdapter;

    public CartAdapter(Context context, List<CartResponseModel.CartlistBean> cartList, ItemUpdateListener itemClickListener) {
        this.context = context;
        this.cartList = cartList;
        this.mItemClickListener = itemClickListener;

    }

    @NonNull
    @Override
    public MyCartItems onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyCartItems(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_cart_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyCartItems holder, int position) {
        String image = cartList.get(position).getProduct_image();
        if (!TextUtils.isEmpty(image)) {
//            Picasso.get().load(image).into(holder.cartItemIv);
            Glide.with(context).load(image).into(holder.cartItemIv);

        }
        holder.cartItemDescriptionTv.setText(cartList.get(position).getDescription());
        holder.cartItemNameTv.setText(cartList.get(position).getName());
        holder.cartItemQuantityTv.setText("$" + cartList.get(position).getPrice());
        ArrayList<String> arrayList = new ArrayList<>();
        if (cartList.get(position).getQuantityarray() != null && cartList.get(position).getQuantityarray().size() > 0) {
            for (int i = 0; i < cartList.get(position).getQuantityarray().size(); i++) {
                arrayList.add(cartList.get(position).getQuantityarray().get(i).getQuantity());
            }
        }
        ArrayAdapter userAdapter = (ArrayAdapter) holder.cartItemSpinner.getAdapter();
        if (null == userAdapter) {
            userAdapter = new ArrayAdapter(context, R.layout.spinner, arrayList);
            userAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        ArrayAdapter<CartResponseModel.CartlistBean.QuantityarrayBean> userAdapter = new ArrayAdapter<CartResponseModel.CartlistBean.QuantityarrayBean>(context,
//                R.layout.spinner,  cartList.get(position).getQuantityarray());

            int pos = userAdapter.getPosition(cartList.get(position).getQuantity());
//            if (!apiCalled) {

            holder.cartItemSpinner.setAdapter(userAdapter);
            holder.cartItemSpinner.setSelection(pos, true);
            holder.cartItemSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int sposition, long id) {
                    // Get the value selected by the user
                    // e.g. to store it as a field or immediately call a method
//                User user = (User) parent.getSelectedItem();
//                    if (!apiCalled) {
//                    Toast.makeText(context, parent.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                    mItemClickListener.onUpdateClicked(view, position, Integer.parseInt(parent.getSelectedItem().toString()));
                  /*  } else {
                        apiCalled = false;
                    }
*/
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
//        }
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }


    class MyCartItems extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.cart_item_iv)
        ImageView cartItemIv;
        @BindView(R.id.cart_item_description_tv)
        TextView cartItemDescriptionTv;
        @BindView(R.id.cart_item_name_tv)
        TextView cartItemNameTv;
        @BindView(R.id.cart_item_quantity_tv)
        TextView cartItemQuantityTv;
        @BindView(R.id.remove_txt)
        TextView removeTv;
        @BindView(R.id.cart_item_spinner)
        MySpinner cartItemSpinner;
        @BindView(R.id.remove_ll)
        LinearLayout removeLl;
        @BindView(R.id.wishlist_ll)
        LinearLayout wishlistLl;
        ItemUpdateListener itemClickListener;
        private boolean isClick = false;

        public MyCartItems(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemClickListener = mItemClickListener;
            removeTv.setOnClickListener(this);
          /*  cartItemSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Toast.makeText(context, parent.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                            notifyDataSetChanged();
//                            mItemClickListener.onUpdateClicked(view,  getAdapterPosition(), Integer.parseInt(parent.getSelectedItem().toString()));
                }
            });*/


        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.remove_txt: {
                    itemClickListener.onClicked(view, getAdapterPosition());
                    break;
                }
            }
        }
    }
}
