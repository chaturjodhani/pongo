package com.pongo.health.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.PastChatResponseModel;
import com.pongo.health.model.PetsModel;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class VeterinaryProfessionalAdapter extends RecyclerView.Adapter<VeterinaryProfessionalAdapter.MyVeterinary> {

    private Context context;
    private List<PastChatResponseModel.DetailBean> pastVeterinaryList = new ArrayList<>();
    private ItemClickListener mItemClickListener;

    public VeterinaryProfessionalAdapter(Context context, List<PastChatResponseModel.DetailBean> pastVeterinaryList, ItemClickListener itemClickListener) {
        this.context = context;
        this.pastVeterinaryList = pastVeterinaryList;
        this.mItemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyVeterinary onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyVeterinary(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_veterniary_professional, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyVeterinary holder, int position) {
        String image = pastVeterinaryList.get(position).getUserimage();
        if (!TextUtils.isEmpty(image)) {
//            Picasso.get().load(image).into(holder.vpDrIv);
            Glide.with(context).load(image).into(holder.vpDrIv);

        }
        holder.vpDescriptionTv.setText(pastVeterinaryList.get(position).getArea_description());
        holder.vpExperienceTv.setText("Expertise: "+pastVeterinaryList.get(position).getExpertise());
        if (!TextUtils.isEmpty(pastVeterinaryList.get(position).getUpdatedate())) {
            holder.chatDate.setText(pastVeterinaryList.get(position).getUpdatedate());
            holder.chatDate.setVisibility(View.VISIBLE);
        } else {
            holder.chatDate.setVisibility(View.GONE);
        }
        holder.vpDrNameTv.setText(pastVeterinaryList.get(position).getFirst_name() + " " + pastVeterinaryList.get(position).getLast_name());


    }

    @Override
    public int getItemCount() {
        return pastVeterinaryList.size();
    }

    class MyVeterinary extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.vp_dr_iv)
        CircleImageView vpDrIv;
        @BindView(R.id.vp_description_tv)
        TextView vpDescriptionTv;
        @BindView(R.id.vp_dr_name_tv)
        TextView vpDrNameTv;
        @BindView(R.id.doctor_experience_tv)
        TextView vpExperienceTv;
        @BindView(R.id.waiting_time_tv)
        TextView chatDate;
        @BindView(R.id.vp_we_chat_iv)
        ImageView vpWeChatIv;
        ItemClickListener itemClickListener;

        public MyVeterinary(@NonNull View itemView) {
            super(itemView);
            this.itemClickListener = mItemClickListener;
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }
}
