package com.pongo.health.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pongo.health.R;
import com.pongo.health.model.PastVetSearchModel;
import com.pongo.health.model.PetsModel;
import com.pongo.health.ui.ActivityVetAvailableTime;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PastPongoVetsAdapter extends RecyclerView.Adapter<PastPongoVetsAdapter.MyPongVets> {

    private Context context;
    private List<PastVetSearchModel.PastdoctorBean> vetsList = new ArrayList<>();
    private String screen = "";

    public PastPongoVetsAdapter(Context context, List<PastVetSearchModel.PastdoctorBean> vetsList, String screenFrom) {
        this.context = context;
        this.vetsList = vetsList;
        screen = screenFrom;
    }

    @NonNull
    @Override
    public MyPongVets onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyPongVets(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_past_pongo_vents, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyPongVets holder, int position) {
        //  Picasso.get().load(categoriesModelList.get(position).getImage()).into(holder.drIv);
        Glide.with(context).load(vetsList.get(position).getUserimage()).apply(RequestOptions.circleCropTransform()).into(holder.pvDrIv);

//        holder.pvDrDescription.setText(vetsList.get(position).getPetdescription());
        holder.pvDrSpecialistTv.setText(vetsList.get(position).getExpertise() + " (" + vetsList.get(position).getDate() + ")");
        holder.pvDrNameTv.setText(vetsList.get(position).getName());
        if (!TextUtils.isEmpty(vetsList.get(position).getCallprice())) {
            holder.priceTv.setText("$" + vetsList.get(position).getCallprice());
        }
        holder.bookAppointmentTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String vetId = vetsList.get(position).getUserId();
                context.startActivity(new Intent(context, ActivityVetAvailableTime.class)
                        .putExtra("vet_id", vetId)
                        .putExtra("screen", screen));
            }
        });

    }

    @Override
    public int getItemCount() {
        return vetsList.size();
    }

    class MyPongVets extends RecyclerView.ViewHolder {

        @BindView(R.id.pv_dr_name_tv)
        TextView pvDrNameTv;
        @BindView(R.id.pv_dr_specialist_tv)
        TextView pvDrSpecialistTv;
        @BindView(R.id.pv_dr_description)
        TextView pvDrDescription;
        @BindView(R.id.pv_dr_iv)
        ImageView pvDrIv;
        @BindView(R.id.view_profile_tv)
        TextView viewProfileTv;
        @BindView(R.id.blue_btn_tv)
        TextView bookAppointmentTv;
        @BindView(R.id.price_tv)
        TextView priceTv;


        public MyPongVets(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            bookAppointmentTv.setText("Book Appointment");
        }
    }
}
