package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.PetSizeModels;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddPetHealthGoalAdapter extends RecyclerView.Adapter<AddPetHealthGoalAdapter.MyPetList>{

    private Context context;
    private List<PetSizeModels> petSizList;
//    private  int lastPosition = -1;

    public AddPetHealthGoalAdapter(Context context, List<PetSizeModels> petSizList) {
        this.context = context;
        this.petSizList = petSizList;
    }

    @NonNull
    @Override
    public MyPetList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyPetList(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_pet_health_goal,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyPetList holder, int position) {

//        Picasso.get().load(petSizList.get(position).getImage()).into(holder.healthGoalIv);
        Glide.with(context).load(petSizList.get(position).getImage()).into(holder.healthGoalIv);

        holder.healthGoalTv.setText(petSizList.get(position).getSizeType());
        if (petSizList.get(position).isSelected()) {
            holder.checkedHealthIv.setVisibility(View.VISIBLE);
        } else {
            holder.checkedHealthIv.setVisibility(View.INVISIBLE);
        }
        holder.healthGoalLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (petSizList.get(position).isSelected()) {
                    holder.checkedHealthIv.setVisibility(View.GONE);
                    petSizList.get(position).setSelected(false);
                } else {
                    holder.checkedHealthIv.setVisibility(View.VISIBLE);
                    petSizList.get(position).setSelected(true);

                }
                notifyDataSetChanged();
//                lastPosition = holder.getAdapterPosition();
            }
        });


    }

    @Override
    public int getItemCount() {
        return petSizList.size();
    }


    class MyPetList extends RecyclerView.ViewHolder{

        @BindView(R.id.health_goal_iv) ImageView healthGoalIv;
        @BindView(R.id.health_goal_tv) TextView healthGoalTv;
        @BindView(R.id.checked_health_iv) ImageView checkedHealthIv;
        @BindView(R.id.health_goal_ll) LinearLayout healthGoalLl;

        public MyPetList(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
