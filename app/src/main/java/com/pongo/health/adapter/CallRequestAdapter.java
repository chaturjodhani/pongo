package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.ChatListModel;
import com.pongo.health.utils.ActionListener;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CallRequestAdapter extends RecyclerView.Adapter<CallRequestAdapter.MycallingList> {

    private Context context;
    private List<ChatListModel> callingList = new ArrayList<>();
    private ActionListener itemClickListener;
    private String type;

    public CallRequestAdapter(Context context, List<ChatListModel> callingList,
                              ActionListener itemClickListener, String action) {
        this.context = context;
        this.callingList = callingList;
        this.itemClickListener = itemClickListener;
        type = action;
    }

    @NonNull
    @Override
    public MycallingList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MycallingList(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_call_request, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MycallingList holder, int position) {
        Glide.with(context).load(callingList.get(position).getPetimage()).into(holder.cPetIv);

//        Picasso.get().load(callingList.get(position).getItemImage()).into(holder.cPetIv);
        holder.cPetDescTv.setText(callingList.get(position).getCurrentproblem());
        holder.cWaitingTimeTv.setText(callingList.get(position).getBooking_date() + " " + callingList.get(position).getBooking_time());
        holder.cPetNameTv.setText(callingList.get(position).getPet_name() + " - " + callingList.get(position).getPet_age());
        holder.cPetType.setText(callingList.get(position).getPet_type() + " | " + callingList.get(position).getBreed());
    }

    @Override
    public int getItemCount() {
        return callingList.size();
    }


    class MycallingList extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.c_pet_iv)
        ImageView cPetIv;
        @BindView(R.id.c_pet_desc_tv)
        TextView cPetDescTv;
        @BindView(R.id.c_pet_type)
        TextView cPetType;
        @BindView(R.id.c_pet_name_tv)
        TextView cPetNameTv;
        @BindView(R.id.c_waiting_time_tv)
        TextView cWaitingTimeTv;
        @BindView(R.id.cancel_tv)
        TextView cancelTv;
        @BindView(R.id.accept_tv)
        TextView acceptTv;
        @BindView(R.id.review_tv)
        TextView reviewTv;
        @BindView(R.id.review)
        TextView reviewUpcomingTv;


        ActionListener itemClickListener;

        public MycallingList(@NonNull View itemView,
                             ActionListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
            cancelTv.setOnClickListener(this);
            acceptTv.setOnClickListener(this);
            reviewTv.setOnClickListener(this);
            reviewUpcomingTv.setOnClickListener(this);
            if (type.equalsIgnoreCase("upcoming")) {
                cancelTv.setVisibility(View.GONE);
                acceptTv.setVisibility(View.GONE);
                reviewTv.setVisibility(View.GONE);
                reviewUpcomingTv.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.accept_tv: {
                    itemClickListener.onAcceptClicked(view, getAdapterPosition());

                    break;
                }
                case R.id.cancel_tv: {
                    itemClickListener.onCancelClicked(view, getAdapterPosition());

                    break;
                }
                case R.id.review_tv: {
                    itemClickListener.onReviewClicked(view, getAdapterPosition());

                    break;
                }
                case R.id.review: {
                    itemClickListener.onReviewClicked(view, getAdapterPosition());

                    break;
                }
            }
        }
    }
}
