package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AttachmentAdapter extends RecyclerView.Adapter<AttachmentAdapter.MyCategories> {

    private Context context;
    private List<String> imageList = new ArrayList<>();
    private ItemClickListener itemClickListener;


    public AttachmentAdapter(Context context, List<String> categoriesModelList,
                             ItemClickListener itemClickListener) {
        this.context = context;
        this.imageList = categoriesModelList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public AttachmentAdapter.MyCategories onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AttachmentAdapter.MyCategories(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.images_adapter, parent, false),
                itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull AttachmentAdapter.MyCategories holder, int position) {
//        Picasso.get().load(imageList.get(position)).into(holder.typeIv);
        if (!imageList.get(position).contains(".pdf")) {
            Glide.with(context).load(imageList.get(position)).into(holder.typeIv);

        }

//        holder.typeTv.setText(categoriesModelList.get(position).getCat_name());

    }

    @Override
    public int getItemCount() {
        if (null != imageList) {
            return imageList.size();
        } else {
            return 0;
        }
    }


    class MyCategories extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.type_iv)
        ImageView typeIv;
        @BindView(R.id.delete_iv)
        ImageView deleteIv;


        ItemClickListener itemClickListener;

        public MyCategories(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            this.itemClickListener = itemClickListener;
            deleteIv.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }
}


