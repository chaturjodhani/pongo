package com.pongo.health.adapter;

import android.app.Activity;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.HomeModel;
import com.pongo.health.model.PetModelResponse;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class PetListAdapter extends RecyclerView.Adapter {

    private Activity context;
    private List<PetModelResponse.DataBean> petsModelList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private int mPosition = -1;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int getItemViewType(int position) {
        return 1;
    }


    public PetListAdapter(Activity context, List<PetModelResponse.DataBean> petsModelList,
                          ItemClickListener itemClickListener) {
        this.context = context;
        this.petsModelList = petsModelList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new PetListAdapter.MyPets(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pets_adapter_layout, parent, false),
                itemClickListener);

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        MyPets viewHolder = (MyPets) holder;
        String image = petsModelList.get(position).getPetimage();
        if (!TextUtils.isEmpty(image)) {
            Glide.with(context)
                    .load(image)
                    .into(viewHolder.petImage);
        }
        viewHolder.petNameTv.setText(petsModelList.get(position).getPet_name());
        String type = petsModelList.get(position).getPet_type();
        if (!TextUtils.isEmpty(type))
        {
           type = type.substring(0, 1).toUpperCase() + type.substring(1).toLowerCase();
        }
        viewHolder.petDescriptionTv.setText(type+" - "+petsModelList.get(position).getBreed()+" ("+petsModelList.get(position).getPet_age() + ")" );
        if (mPosition == position) {
            viewHolder.mRootview.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shaped_two));
        } else {
            viewHolder.mRootview.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shaped));
        }
    }


    @Override
    public int getItemCount() {
        return petsModelList.size();
    }


    class MyPets extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemClickListener itemClickListener;
        @BindView(R.id.pet_name_tv)
        TextView petNameTv;
        @BindView(R.id.pet_image)
        CircleImageView petImage;
        @BindView(R.id.pet_description_tv)
        TextView petDescriptionTv;
        @BindView(R.id.root_view)
        LinearLayout mRootview;

        public MyPets(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
           /* DisplayMetrics metrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int width = metrics.widthPixels;
            ViewGroup.LayoutParams layoutParams = mRootview.getLayoutParams();
            layoutParams.width = width - 130;
            mRootview.setLayoutParams(layoutParams);*/
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            mPosition = getAdapterPosition();
            itemClickListener.onClicked(view, getAdapterPosition());
            notifyDataSetChanged();
        }
    }

}

