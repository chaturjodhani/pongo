package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.PetSizeModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddPetSizeAdapter extends RecyclerView.Adapter<AddPetSizeAdapter.MyPetList>{

    private Context context;
    private List<PetSizeModel> petSizList = new ArrayList<>();
    public int selectedPosition = -1;

    public AddPetSizeAdapter(Context context, List<PetSizeModel> petSizList) {
        this.context = context;
        this.petSizList = petSizList;
    }

    @NonNull
    @Override
    public MyPetList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyPetList(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_pet_size,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyPetList holder, int position) {

//        Picasso.get().load(petSizList.get(position).getImage()).into(holder.petTypeIv);
        Glide.with(context).load(petSizList.get(position).getImage()).into(holder.petTypeIv);

        holder.petTypeTv.setText(petSizList.get(position).getSizeType());
        holder.petLengthSizeTv.setText(petSizList.get(position).getLength());

        if(selectedPosition==position) {
            holder.petSizeLl.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shaped_two));
        }
        else {
            holder.petSizeLl.setBackground(context.getResources().getDrawable(R.drawable.rectangle_shaped));
        }
        holder.itemView.setOnClickListener(v -> {
            selectedPosition=position;
            notifyDataSetChanged();

        });
    }

    @Override
    public int getItemCount() {
        return petSizList.size();
    }


    class MyPetList extends RecyclerView.ViewHolder{

        @BindView(R.id.pet_type_iv) ImageView petTypeIv;
        @BindView(R.id.pet_type_tv) TextView petTypeTv;
        @BindView(R.id.pet_length_size_tv) TextView petLengthSizeTv;
        @BindView(R.id.pet_size_ll) LinearLayout petSizeLl;

        public MyPetList(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
