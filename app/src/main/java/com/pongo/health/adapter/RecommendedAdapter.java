package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.HomeModel;
import com.pongo.health.ui.FeedbackActivity;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class RecommendedAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<HomeModel.BlogBean> recommendedList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private static int TYPE_BLOG = 1;
    private static int TYPE_PRODUCT = 2;

    public RecommendedAdapter(Context context, List<HomeModel.BlogBean> recommendedList,
                              ItemClickListener itemClickListener) {
        this.context = context;
        this.recommendedList = recommendedList;
        this.itemClickListener = itemClickListener;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       /* return new MyRecommended(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_recommended_articles, parent, false),
                itemClickListener);*/

        if (viewType == TYPE_BLOG) { // for call layout
            return new BlogView(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.single_item_view_recommended_articles, parent, false),
                    itemClickListener);

        } else { // for email layout
            return new ProductView(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.product_article_view, parent, false),
                    itemClickListener);
        }
    }

    @Override
    public int getItemViewType(int position) {
        String type = recommendedList.get(position).getType();
        if (type.equalsIgnoreCase("blog")) {
            return TYPE_BLOG;
        } else {
            return TYPE_PRODUCT;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (getItemViewType(position) == TYPE_BLOG) {
            ((BlogView) viewHolder).setBlogDetails(recommendedList.get(position));
        } else {
            ((ProductView) viewHolder).setProductDetails(recommendedList.get(position));
        }


    }


    @Override
    public int getItemCount() {
        return recommendedList.size();
    }

     class BlogView extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.blog_iv)
        CircleImageView blogIv;
        @BindView(R.id.blog_img)
        ImageView blogImage;
        @BindView(R.id.blog_date_tv)
        TextView blogDateTv;
        @BindView(R.id.title_blog_tv)
        TextView titleBlogTv;


        ItemClickListener itemClickListener;

        public BlogView(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }

        private void setBlogDetails(HomeModel.BlogBean model) {
            Glide.with(context).load(model.getUserimg()).into(blogIv);
            if (model.getBlogimage().size()>0)
            {
                Glide.with(context).load(model.getBlogimage().get(0)).into(blogImage);
            }
//            Picasso.get().load(model.getUserimg()).into(blogIv);
//            Picasso.get().load(model.getBlogimage().get(0)).into(blogImage);
            blogDateTv.setText(model.getDate());
            titleBlogTv.setText(model.getTitle());
        }
    }

    class ProductView extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.blog_iv)
        CircleImageView blogIv;
        @BindView(R.id.blog_img_one)
        ImageView blogImageOne;
        @BindView(R.id.blog_img_two)
        ImageView blogImageTwo;
        @BindView(R.id.blog_date_tv)
        TextView blogDateTv;
        @BindView(R.id.title_blog_tv)
        TextView titleBlogTv;


        ItemClickListener itemClickListener;

        public ProductView(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }

        private void setProductDetails(HomeModel.BlogBean model) {
            Glide.with(context).load(model.getUserimg()).into(blogIv);
            if (model.getBlogimage().size()>0)
            {
                Glide.with(context).load(model.getBlogimage().get(0)).into(blogImageOne);
            } if (model.getBlogimage().size()>1)
            {
                Glide.with(context).load(model.getBlogimage().get(1)).into(blogImageTwo);
            }
//            Picasso.get().load(model.getBlogimage().get(0)).into(blogImageOne);
//            Picasso.get().load(model.getBlogimage().get(1)).into(blogImageTwo);
            blogDateTv.setText(model.getDate());
            titleBlogTv.setText(model.getTitle());
        }
    }
}
