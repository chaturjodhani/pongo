package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.model.SingleProductModel;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MedicationStrengthAdapter extends RecyclerView.Adapter<MedicationStrengthAdapter.petStrength> {

    private Context context;
    private List<SingleProductModel.SingleproductBean.StrengthBean> strengthList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private int mPosition = -1;

    public MedicationStrengthAdapter(Context context, List<SingleProductModel.SingleproductBean.StrengthBean> strengthList,
                                     ItemClickListener itemClickListener, int mselectedVariationPosition) {
        this.context = context;
        this.strengthList = strengthList;
        this.itemClickListener = itemClickListener;
        mPosition = mselectedVariationPosition;
    }

    @NonNull
    @Override
    public petStrength onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new petStrength(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_medication_strength, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull petStrength holder, int position) {
        if (position == mPosition) {
            holder.radioButton.setChecked(true);
        } else {
            holder.radioButton.setChecked(false);
        }
        holder.sStrengthTv.setText(strengthList.get(position).getName());
        holder.dosageTv.setText(strengthList.get(position).getDoseage()+" in dosage");
        holder.priceTv.setText("$" + strengthList.get(position).getPrice());
    }

    @Override
    public int getItemCount() {
        return strengthList.size();
    }

    class petStrength extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemClickListener itemClickListener;
        @BindView(R.id.s_strength_tv)
        TextView sStrengthTv;
        @BindView(R.id.price_tv)
        TextView priceTv;
        @BindView(R.id.dosage_tv)
        TextView dosageTv;
        @BindView(R.id.radio_button)
        RadioButton radioButton;

        public petStrength(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
            mPosition = getAdapterPosition();
            notifyDataSetChanged();
        }
    }
}
