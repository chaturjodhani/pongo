package com.pongo.health.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.MarketResponseModel;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class CategoriesTypeAdapter extends RecyclerView.Adapter<CategoriesTypeAdapter.MyCategories> {

    private Context context;
    private List<MarketResponseModel.CategorylistBean> categoriesModelList = new ArrayList<>();
    private ItemClickListener itemClickListener;


    public CategoriesTypeAdapter(Context context, List<MarketResponseModel.CategorylistBean> categoriesModelList,
                                 ItemClickListener itemClickListener) {
        this.context = context;
        this.categoriesModelList = categoriesModelList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyCategories onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyCategories(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_categories_type, parent, false),
                itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyCategories holder, int position) {
//        Picasso.get().load(categoriesModelList.get(position).getCat_image()).into(holder.typeIv);
        Glide.with(context).load(categoriesModelList.get(position).getCat_image()).into(holder.typeIv);

        holder.typeTv.setText(categoriesModelList.get(position).getCat_name());

    }

    @Override
    public int getItemCount() {
        return categoriesModelList.size();
    }


    class MyCategories extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.type_tv)
        TextView typeTv;
        @BindView(R.id.type_iv)
        CircleImageView typeIv;


        ItemClickListener itemClickListener;

        public MyCategories(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
            this.itemClickListener = itemClickListener;

        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }
}
