package com.pongo.health.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.CartModel;
import com.pongo.health.model.EarningModel;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BillingAdapter extends RecyclerView.Adapter<BillingAdapter.MyBillingList> {
    
    private Context context;
    private List<EarningModel.PethistoryBean> billingList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    public BillingAdapter(Context context, List<EarningModel.PethistoryBean> billingList,
                          ItemClickListener itemClickListener) {
        this.context = context;
        this.billingList = billingList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyBillingList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyBillingList(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_billings,parent,false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyBillingList holder, int position) {

//        Picasso.get().load(billingList.get(position).getItemImage()).into(holder.bPetIv);
        Glide.with(context).load(billingList.get(position).getPetimage()).into(holder.bPetIv);

        holder.bPetDescTv.setText(billingList.get(position).getProblemsissue());
        holder.bPetNameTv.setText(billingList.get(position).getPet_name()+" - "+billingList.get(position).getPet_age());
        holder.bPetType.setText(billingList.get(position).getPet_type()+  " | "+billingList.get(position).getBreed());
        holder.billingWaitingDateTv.setText(billingList.get(position).getDate());
        String price = billingList.get(position).getPrice();
        if (!TextUtils.isEmpty(price)) {
            holder.seePatientTv.setText("$" + price);
        }
    }

    @Override
    public int getItemCount() {
        return billingList.size();
    }


    class MyBillingList extends RecyclerView.ViewHolder implements View.OnClickListener{
        
        @BindView(R.id.b_pet_iv) ImageView bPetIv;
        @BindView(R.id.b_pet_desc_tv) TextView bPetDescTv;
        @BindView(R.id.b_pet_type) TextView bPetType;
        @BindView(R.id.b_pet_name_tv) TextView bPetNameTv;
        @BindView(R.id.billing_waiting_date_tv) TextView billingWaitingDateTv;
        @BindView(R.id.see_patient_tv) TextView seePatientTv;


        ItemClickListener itemClickListener;

        public MyBillingList(@NonNull View itemView,
                             ItemClickListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
          itemClickListener.onClicked(view,getAdapterPosition());
        }
    }
}
