package com.pongo.health.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.model.ProductResponseModel;
import com.pongo.health.utils.ItemClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class SubCategoriesAdapter extends RecyclerView.Adapter<SubCategoriesAdapter.MySubCategories> {

    private Context context;
    private List<ProductResponseModel.FilterBean> subCategoriesList;
    private ItemClickListener itemClickListener;

    public SubCategoriesAdapter(Context context, List<ProductResponseModel.FilterBean> subCategoriesList, ItemClickListener itemClickListener) {
        this.context = context;
        this.subCategoriesList = subCategoriesList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MySubCategories onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MySubCategories(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view_sub_catgories, parent, false), itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MySubCategories holder, int position) {
        String image = subCategoriesList.get(position).getIcon();
       if(!TextUtils.isEmpty(image)) {
           Glide.with(context).load(image).into(holder.appointmentsTv);
       }
        holder.subCatTv.setText(subCategoriesList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        if (null!=subCategoriesList) {
            return subCategoriesList.size();
        }else
        {
            return 0;
        }
    }


    class MySubCategories extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.sub_cat_iv)
        ImageView appointmentsTv;
        @BindView(R.id.sub_cat_tv)
        TextView subCatTv;
        ItemClickListener itemClickListener;

        public MySubCategories(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClicked(view, getAdapterPosition());
        }
    }
}
