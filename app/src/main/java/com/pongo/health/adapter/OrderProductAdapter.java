package com.pongo.health.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.model.CheckoutResponseModel;
import com.pongo.health.model.TransactionModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.ItemClickListener;
import com.pongo.health.utils.ItemUpdateListener;
import com.pongo.health.utils.MySpinner;
import com.pongo.health.utils.TransactionListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderProductAdapter extends RecyclerView.Adapter<OrderProductAdapter.MyRecentSearches> {

    private Activity context;
    private List<TransactionModel.SubscribeproductBean.ProductsBeanX> ordersList = new ArrayList<>();
    private TransactionListener mItemClickListener;
    private CustomDialog mLoadingView;
    private Session mSession;

    public OrderProductAdapter(Activity context, List<TransactionModel.SubscribeproductBean.ProductsBeanX> ordersList, TransactionListener itemClickListener) {
        mLoadingView = new CustomDialog(context);
        mSession = new Session(context);
        this.context = context;
        this.ordersList = ordersList;
        this.mItemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public MyRecentSearches onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyRecentSearches(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_order_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyRecentSearches holder, int position) {
        String image = ordersList.get(position).getImage();
        if (!TextUtils.isEmpty(image)) {
//            Picasso.get().load(image).into(holder.cartItemIv);
            Glide.with(context).load(image).into(holder.cartItemIv);

        }
        holder.cartItemDescriptionTv.setText("$" + ordersList.get(position).getPrice());
        holder.cartItemNameTv.setText(ordersList.get(position).getProduct_name());
        holder.cartItemQuantityTv.setText(ordersList.get(position).getQuantity());
        String subscribeid = ordersList.get(position).getSubscribeid();
        if (!TextUtils.isEmpty(subscribeid)) {
            holder.cancelTv.setVisibility(View.VISIBLE);
        } else {
            holder.cancelTv.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }


    class MyRecentSearches extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.cart_item_iv)
        ImageView cartItemIv;
        @BindView(R.id.cart_item_description_tv)
        TextView cartItemDescriptionTv;
        @BindView(R.id.cart_item_name_tv)
        TextView cartItemNameTv;
        @BindView(R.id.cart_item_quantity_tv)
        TextView cartItemQuantityTv;
        @BindView(R.id.sub_cancel_tv)
        TextView cancelTv;
        TransactionListener itemClickListener;


        public MyRecentSearches(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemClickListener = mItemClickListener;
            cancelTv.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.sub_cancel_tv) {
                itemClickListener.onCancelClicked(v, getAdapterPosition());
            }
        }
    }
}

