package com.pongo.health;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;


public class Session {

    private SharedPreferences prefs;

    public Session(Context cntx) {

        prefs = cntx.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);

    }

    public void setuser_id(String user_id) {
        prefs.edit().putString("user_id", user_id).apply();
    }

    public String getuser_id() {
        String user_id = prefs.getString("user_id", "");
        if (TextUtils.isEmpty(user_id)) {
            user_id = "";
        }
        return user_id;
    }

   /* public void logout() {
        prefs.edit().clear().apply();
    }*/

    public void setfirt_name(String userFirtname) {
        prefs.edit().putString("firstname", userFirtname).apply();
    }

    public void setlast_name(String userLastName) {
        prefs.edit().putString("lastname", userLastName).apply();
    }

    public void setuser_pic(String userImage) {
        prefs.edit().putString("user_image", userImage).apply();
    }

    public String getfirst_name() {
        String name = prefs.getString("firstname", "");
        return name;
    }

    public String getlast_name() {
        String lastname = prefs.getString("lastname", "");
        return lastname;
    }

    public String getuser_image() {
        String image = prefs.getString("user_image", "");
        return image;
    }

    public void setuser_login(boolean userPhone) {
        prefs.edit().putBoolean("user_login", userPhone).apply();
    }

    public boolean getuser_login() {
        boolean phone = prefs.getBoolean("user_login", false);

        return phone;
    }

    public void setuser_phone(String userPhone) {
        prefs.edit().putString("user_phone", userPhone).apply();
    }

    public String getuser_phone() {
        String phone = prefs.getString("user_phone", "");

        return phone;
    }

    public void setuser_email(String userPhone) {
        prefs.edit().putString("user_email", userPhone).apply();
    }

    public String getuser_email() {
        String phone = prefs.getString("user_email", "");

        return phone;
    }

    public void setToken(String token) {

        prefs.edit().putString("token", token).apply();
    }

    public String getToken() {

        return prefs.getString("token", "");
    }

    public void setuser_type(String type) {
        prefs.edit().putString("user_type", type).apply();
    }

    public String getuser_type() {
        String user_type = prefs.getString("user_type", "");

        return user_type;
    }

    public void setuserPaid(String type) {
        prefs.edit().putString("user_paid", type).apply();
    }

    public String getuserPaid() {
        String user_type = prefs.getString("user_paid", "");

        return user_type;
    }

    /*   public void setRecent(List<String> list) {
      Gson gson = new Gson();
      String json = gson.toJson(list);
      prefs.edit().putString("recent", json).apply();
  }

  public List<String> getRecent() {
      List<String> idList = new ArrayList<>();
      Gson gson = new Gson();
      String response = prefs.getString("recent", "");
      if (!TextUtils.isEmpty(response)) {
          idList = gson.fromJson(response,
                  new TypeToken<List<String>>() {
                  }.getType());
      }
      return idList;
  }





  public void setuser_image(String pic) {
      prefs.edit().putString("user_image", pic).apply();
  }

  public String getuser_image() {
      String pic = prefs.getString("user_image", "");

      return pic;
  }

  public void setuser_remember(boolean userPhone) {
      prefs.edit().putBoolean("user_remember", userPhone).apply();
  }

  public boolean getuser_remember() {
      boolean phone = prefs.getBoolean("user_remember", false);

      return phone;
  }

  public boolean getuser_interest() {
      boolean interest = prefs.getBoolean("user_interest", false);

      return interest;
  }

  public void setuser_interest(boolean userPhone) {
      prefs.edit().putBoolean("user_interest", userPhone).apply();
  }
*/
    public String getuser_language() {
        String language = prefs.getString("user_language", "fr");
        return language;
    }

    public void setuser_language(String userLanguage) {
        prefs.edit().putString("user_language", userLanguage).apply();
    }

    public void clear() {
        String token = prefs.getString("token", "");
        prefs.edit().clear().apply();
        setToken(token);
    }

}



