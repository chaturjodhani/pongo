package com.pongo.health.fragment.medicationtabs;

import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.adapter.ReviewAdapter;
import com.pongo.health.base.BaseFragment;
import com.pongo.health.model.ReviewModel;
import com.pongo.health.model.SingleProductModel;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewFragment extends BaseFragment {
    @BindView(R.id.rv_list)
    RecyclerView RvList;
    private List<ReviewModel> modelList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private SingleProductModel mSingleProductModel;

    public ReviewFragment(SingleProductModel singleProductModel) {
        this.mSingleProductModel = singleProductModel;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_review, container, false);
        ButterKnife.bind(this, view);
        if (mSingleProductModel.getSingleproduct().get(0).getReviews().size() > 0) {
            setData();
        }
        return view;
    }

    private void setData() {
        modelList.clear();
        for (int i = 0; i < mSingleProductModel.getSingleproduct().get(0).getReviews().size(); i++) {
            String title = mSingleProductModel.getSingleproduct().get(0).getReviews().get(i).getName();
            String description = mSingleProductModel.getSingleproduct().get(0).getReviews().get(i).getDescription();
            int rating = Integer.parseInt(mSingleProductModel.getSingleproduct().get(0).getReviews().get(i).getRating());
            String image = mSingleProductModel.getSingleproduct().get(0).getReviews().get(i).getReviewimage();
            modelList.add(new ReviewModel(title,
                    description, rating,
                    image));
        }
        itemClickListener = new ItemClickListener() {

            @Override
            public void onClicked(View view, int postion) {
//                RvList.setText(strengthList.get(postion));
                openPopup(modelList.get(postion).getHeader2());
            }
        };
        RvList.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));

        RvList.setAdapter(new ReviewAdapter(activity, modelList, itemClickListener));
    }

    private void openPopup(String header2) {


        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(activity);
        View promptsView = li.inflate(R.layout.review_desc_popup, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                activity);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final TextView closeText = promptsView
                .findViewById(R.id.close);
        TextView descText = promptsView
                .findViewById(R.id.desc);
        descText.setText(header2);
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();


        closeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        // show it
        alertDialog.show();
    }

}
