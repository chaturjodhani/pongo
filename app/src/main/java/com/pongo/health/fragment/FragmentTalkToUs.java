package com.pongo.health.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.pongo.health.R;
import com.pongo.health.base.BaseFragment;
import com.pongo.health.ui.ActivityAddPrimaryHospital;
import com.pongo.health.ui.ActivityPetProblems;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentTalkToUs extends BaseFragment {

    @BindView(R.id.blue_btn_tv) TextView talkContinueTv;
    @BindView(R.id.my_vet_ll) LinearLayout myVetLl;
    @BindView(R.id.my_vet_tv) TextView myVetTv;
    @BindView(R.id.my_vet_profile_description_tv) TextView myVetProfileDescriptiontv;
    @BindView(R.id.within_tv) TextView withinTv;
    @BindView(R.id.within_ll) LinearLayout withinLl;
    @BindView(R.id.first_available_vet_ll) LinearLayout firstAvailableVetLl;
    @BindView(R.id.first_available_vet_tv) TextView firstAvailableVetTv;
    @BindView(R.id.first_available_vet_desc_tv) TextView firstAvailableVetDescTv;
    @BindView(R.id.minutes_ll) LinearLayout minutesLl;
    @BindView(R.id.minutes_tv) TextView minutesTv;

    private int position;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_talk_to_us, container, false);
        ButterKnife.bind(this, view);
        talkContinueTv.setText(R.string.continue_);
        talkContinueTv.setOnClickListener(this);
        myVetLl.setOnClickListener(this);
        firstAvailableVetLl.setOnClickListener(this);
        return view;
    }

    public static FragmentTalkToUs newInstance() {
        return new FragmentTalkToUs();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.blue_btn_tv:
                if(position==1){
                    startActivity(new Intent(activity, ActivityAddPrimaryHospital.class));
                }
                else if(position == 2){
                    startActivity(new Intent(activity, ActivityPetProblems.class));
                }
                else{
                    Toast.makeText(activity, "Please Select Anyone", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.my_vet_ll:
               position = 1;
               myVetTv.setTextColor(getResources().getColor(R.color.white_color));
               myVetProfileDescriptiontv.setTextColor(getResources().getColor(R.color.white_color));
               withinTv.setTextColor(getResources().getColor(R.color.white_color));
               withinLl.setBackground(getResources().getDrawable(R.drawable.rectangle_shaped_white));
               myVetLl.setBackground(getResources().getDrawable(R.drawable.rectangle_shaped_selected));

                firstAvailableVetDescTv.setTextColor(getResources().getColor(R.color.colorPrimary));
                firstAvailableVetTv.setTextColor(getResources().getColor(R.color.colorPrimary));
                minutesTv.setTextColor(getResources().getColor(R.color.colorPrimary));
                minutesLl.setBackground(getResources().getDrawable(R.drawable.rectangle_shaped));
                firstAvailableVetLl.setBackground(getResources().getDrawable(R.drawable.rectangle_shaped));
                break;

            case R.id.first_available_vet_ll:
                position = 2;

                myVetTv.setTextColor(getResources().getColor(R.color.colorPrimary));
                myVetProfileDescriptiontv.setTextColor(getResources().getColor(R.color.colorPrimary));
                withinTv.setTextColor(getResources().getColor(R.color.colorPrimary));
                withinLl.setBackground(getResources().getDrawable(R.drawable.rectangle_shaped));
                myVetLl.setBackground(getResources().getDrawable(R.drawable.rectangle_shaped));

                firstAvailableVetDescTv.setTextColor(getResources().getColor(R.color.white_color));
                firstAvailableVetTv.setTextColor(getResources().getColor(R.color.white_color));
                minutesTv.setTextColor(getResources().getColor(R.color.white_color));
                minutesLl.setBackground(getResources().getDrawable(R.drawable.rectangle_shaped_white));
                firstAvailableVetLl.setBackground(getResources().getDrawable(R.drawable.rectangle_shaped_selected));
                break;
        }
    }
}
