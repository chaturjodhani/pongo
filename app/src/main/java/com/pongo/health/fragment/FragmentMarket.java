package com.pongo.health.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.adapter.CategoriesTypeAdapter;
import com.pongo.health.adapter.SearchMarketAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseFragment;
import com.pongo.health.model.MarketResponseModel;
import com.pongo.health.model.SearchProductModel;
import com.pongo.health.ui.ActivityCart;
import com.pongo.health.ui.ActivityMedicationNext;
import com.pongo.health.ui.ActivityMedicationSearch;
import com.pongo.health.ui.ActivitySingleItemMarket;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMarket extends BaseFragment {
    @BindView(R.id.tb_cart)
    ImageView tbCartIv;
    @BindView(R.id.market_categories_rv)
    RecyclerView marketCategoriesRv;
    @BindView(R.id.searches_medication_rv)
    RecyclerView searchesMedicationRv;
    @BindView(R.id.search_second_et)
    EditText searchEt;
    private List<MarketResponseModel.CategorylistBean> categoriesModelList = new ArrayList<>();
    private List<MarketResponseModel.ProductlistBean> medicationList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private CustomDialog mLoadingView;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_market, container, false);
        ButterKnife.bind(this, view);

        searchEt.setHint("Type your product name");
        searchesMedicationRv.setNestedScrollingEnabled(false);
        mLoadingView = new CustomDialog(activity);
        if (InternetConnection.checkConnection(activity)) {
            getProduct();
        } else {
            Toast.makeText(activity, "No Internet available", Toast.LENGTH_SHORT).show();
        }
        setSearchListener();
        gotoCartScreen();
        return view;

    }

    private void gotoCartScreen() {
        tbCartIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(activity, ActivityCart.class));

            }
        });
    }

    private void setSearchListener() {
       /* searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                getSearch(editable.toString());
            }
        });*/

        searchEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String search = searchEt.getText().toString();
                    getSearch(search);
                    return true;
                }
                return false;
            }
        });
    }

    private void getProduct() {
        this.mLoadingView.show();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getMarketData().enqueue(new Callback<MarketResponseModel>() {
            public void onResponse(Call<MarketResponseModel> call, Response<MarketResponseModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((MarketResponseModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        setCategories(response.body().getCategorylist());
                        setMarketSearches(response.body().getProductlist());
                    }
                }

            }

            public void onFailure(Call<MarketResponseModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(activity, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void getSearch(String s) {
        this.mLoadingView.show();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getSearchProducts(s).enqueue(new Callback<SearchProductModel>() {
            public void onResponse(Call<SearchProductModel> call, Response<SearchProductModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((SearchProductModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        medicationList.clear();
                        List<MarketResponseModel.ProductlistBean> productlist = new ArrayList<>();
                        for (int i = 0; i < response.body().getProductdata().size(); i++) {
                            MarketResponseModel.ProductlistBean productlistBean = new MarketResponseModel.ProductlistBean();
                            productlistBean.setId(response.body().getProductdata().get(i).getId());
                            productlistBean.setId(response.body().getProductdata().get(i).getId());
                            productlistBean.setName(response.body().getProductdata().get(i).getName());
                            productlistBean.setDescription(response.body().getProductdata().get(i).getDescription());
                            productlistBean.setOrignalprice(response.body().getProductdata().get(i).getOrignalprice());
                            productlistBean.setDiscountprice(response.body().getProductdata().get(i).getDiscountprice());
                            productlistBean.setProduct_type(response.body().getProductdata().get(i).isProduct_type());
                            productlistBean.setProduct_image(response.body().getProductdata().get(i).getProduct_image());
                            productlist.add(productlistBean);
                        }
                        setMarketSearches(productlist);
                    }
                }

            }

            public void onFailure(Call<SearchProductModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(activity, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }


    public static FragmentMarket newInstance() {
        return new FragmentMarket();
    }

    private void setCategories(List<MarketResponseModel.CategorylistBean> categorylist) {
        marketCategoriesRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false));
//        categoriesModelList.add(new CategoriesModel(R.drawable.rx_img, "Rx"));
//        categoriesModelList.add(new CategoriesModel(R.drawable.food_img, "Food"));
//        categoriesModelList.add(new CategoriesModel(R.drawable.toy_store_img, "Supplies"));
//        categoriesModelList.add(new CategoriesModel(R.drawable.insurance_img, "Insurance"));

        categoriesModelList = categorylist;
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                String id = categoriesModelList.get(position).getCat_id();
                String name = categoriesModelList.get(position).getCat_name();
                Intent intent = new Intent(activity, ActivityMedicationSearch.class);
                intent.putExtra("id", id);
                intent.putExtra("name", name);
                startActivity(intent);
            }
        };
        marketCategoriesRv.setAdapter(new CategoriesTypeAdapter(activity, categoriesModelList, itemClickListener));
    }

    private void setMarketSearches(List<MarketResponseModel.ProductlistBean> productlist) {
        searchesMedicationRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 4; i++) {
            medicationList.add(new PetsModel("$45.99", "Ophtha Care",
                    "lorem ipsum is a simply dummy text of printing and type setting industry"));
        }*/
        medicationList = productlist;
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                String id = medicationList.get(position).getId();
                String name = medicationList.get(position).getName();
                if (medicationList.get(position).isProduct_type()) {
                    startActivity(new Intent(activity, ActivityMedicationNext.class).putExtra("id", id).putExtra("name", name));
                } else {
                    name = medicationList.get(position).getCategory();
                    String subcate = medicationList.get(position).getSubcategory();
                    if (!TextUtils.isEmpty(subcate)) {
                        name = name + " > " + subcate;
                    }
                    startActivity(new Intent(activity, ActivitySingleItemMarket.class).putExtra("id", id).putExtra("name", name));
                }
            }
        };

        searchesMedicationRv.setAdapter(new SearchMarketAdapter(activity, medicationList, itemClickListener));
    }
}


