package com.pongo.health.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseFragment;
import com.pongo.health.ui.ActivityAddress;
import com.pongo.health.ui.ActivityContactInfo;
import com.pongo.health.ui.ActivityInviteFriend;
import com.pongo.health.ui.ActivityLogin;
import com.pongo.health.ui.ActivityMyPongoVets;
import com.pongo.health.ui.ActivityNotification;
import com.pongo.health.ui.ActivityPaymentMethod;
import com.pongo.health.ui.ActivityTransactionSubscription;
import com.pongo.health.utils.GPSTracker;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentProfile extends BaseFragment {

    @BindView(R.id.notification_ll)
    LinearLayout notificationLl;
    @BindView(R.id.contact_info_ll)
    LinearLayout contactInfoLl;
    @BindView(R.id.contact_pongo_ll)
    LinearLayout contactPongoLl;
    @BindView(R.id.address_ll)
    LinearLayout addressLl;
    @BindView(R.id.referral_ll)
    LinearLayout referralLl;
    @BindView(R.id.transaction_ll)
    LinearLayout transactionLl;
    @BindView(R.id.pongo_vets_ll)
    LinearLayout pongoVetsLl;
    @BindView(R.id.payment_ll)
    LinearLayout paymentLl;
    @BindView(R.id.logout_ll)
    LinearLayout logoutLl;
    @BindView(R.id.user_name)
    TextView userNameTv;
    private Session mSession;
    private GPSTracker gps;
    private GoogleSignInClient mGoogleSignInClient;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        gps = new GPSTracker(activity);
        mSession = new Session(activity);
        notificationLl.setOnClickListener(this);
        contactInfoLl.setOnClickListener(this);
        contactPongoLl.setOnClickListener(this);
        addressLl.setOnClickListener(this);
        referralLl.setOnClickListener(this);
        transactionLl.setOnClickListener(this);
        pongoVetsLl.setOnClickListener(this);
        paymentLl.setOnClickListener(this);
        logoutLl.setOnClickListener(this);
        userNameTv.setText("Hello, " + mSession.getfirst_name());
        googleIniTialize();
        return view;
    }

    private void googleIniTialize() {
        this.mGoogleSignInClient = GoogleSignIn.getClient(activity, new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build());
    }

    public static FragmentProfile newInstance() {
        return new FragmentProfile();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.notification_ll:
                startActivity(new Intent(activity, ActivityNotification.class));
                break;
            case R.id.contact_info_ll:
                startActivity(new Intent(activity, ActivityContactInfo.class));
                break;
            case R.id.contact_pongo_ll:
                sendEmail();
                break;
            case R.id.address_ll:
                startActivity(new Intent(activity, ActivityAddress.class));
                break;
            case R.id.referral_ll:
                startActivity(new Intent(activity, ActivityInviteFriend.class));
                break;
            case R.id.transaction_ll:
                startActivity(new Intent(activity, ActivityTransactionSubscription.class));
                break;
            case R.id.pongo_vets_ll:
                startActivity(new Intent(activity, ActivityMyPongoVets.class));
                break;
            case R.id.payment_ll:
                startActivity(new Intent(activity, ActivityPaymentMethod.class));
                break;
            case R.id.logout_ll:
                changeStatus("0");
                mSession.clear();
                googleLogout();
                Intent intent = new Intent(activity, ActivityLogin.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;

        }
    }

    private void googleLogout() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(activity, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
    }

    private void sendEmail() {
        Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        intent.putExtra(Intent.EXTRA_TEXT, "");
        intent.setData(Uri.parse("mailto:support@pongohealth.com")); // or just "mailto:" for blank
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        startActivity(intent);
    }

    private void changeStatus(String status) {
        String userId = mSession.getuser_id();
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();
        if (!TextUtils.isEmpty(userId)) {
            ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).onlineStatus(userId, status, "" + latitude, "" + longitude).enqueue(new Callback<ResponseBody>() {
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.body() != null) {
                        String responseData = "";
                        try {
                            responseData = response.body().string();
                            if (!TextUtils.isEmpty(responseData)) {
                                JSONObject jsonObject = new JSONObject(responseData);
                               /* String status = jsonObject.getString("status");
                                String message = jsonObject.getString("detail");
                                if (!TextUtils.isEmpty(status)) {
                                    if (status.equalsIgnoreCase("success")) {

                                    }
                                }*/
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                public void onFailure(Call<ResponseBody> call, Throwable th) {
                    Log.e("cate", th.toString());
                }
            });
        }
    }

}


