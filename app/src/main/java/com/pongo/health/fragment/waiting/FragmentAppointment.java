package com.pongo.health.fragment.waiting;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.WaitingListAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseFragment;
import com.pongo.health.model.CartModel;
import com.pongo.health.model.ChatListModel;
import com.pongo.health.model.DoctorWaitingListModel;
import com.pongo.health.ui.vet.ActivityPatientDetail;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentAppointment extends BaseFragment {

    @BindView(R.id.appointment_list_rv)
    RecyclerView appointmentListRv;
    private List<ChatListModel> appointmentList = new ArrayList<>();
    private List<DoctorWaitingListModel.WatingBean> waitingBean = new ArrayList<>();
    private CustomDialog mLoadingView;
    private Session mSession;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmnet_appointment, container, false);
        ButterKnife.bind(this, view);
        mSession = new Session(activity);
        mLoadingView = new CustomDialog(activity);

        return view;

    }

    public static FragmentAppointment newInstance() {
        return new FragmentAppointment();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (InternetConnection.checkConnection(activity)) {
            getChatData();
        } else {
            Toast.makeText(activity, "No Internet available", Toast.LENGTH_SHORT).show();
        }
    }


    private void getChatData() {
        appointmentList.clear();
        setAppointmentRv();
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).patientWithBookedAppointment(id).enqueue(new Callback<DoctorWaitingListModel>() {
            public void onResponse(Call<DoctorWaitingListModel> call, Response<DoctorWaitingListModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((DoctorWaitingListModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        waitingBean = response.body().getWating();
                        if (null != waitingBean && waitingBean.size() > 0) {
                            createWaitingList();
                        }

                    }
                }

            }

            public void onFailure(Call<DoctorWaitingListModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(activity, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void createWaitingList() {
        appointmentList.clear();
        for (int i = 0; i < waitingBean.size(); i++) {
            ChatListModel chatmodel = new ChatListModel();
            chatmodel.setBreed(waitingBean.get(i).getBreed());
            chatmodel.setId(waitingBean.get(i).getPet_id());
            chatmodel.setPet_age(waitingBean.get(i).getPet_age());
            chatmodel.setPet_name(waitingBean.get(i).getPet_name());
            chatmodel.setPet_type(waitingBean.get(i).getPet_type());
            chatmodel.setPetimage(waitingBean.get(i).getPetimage());
            chatmodel.setStatus(waitingBean.get(i).getStatus());
            chatmodel.setUser_id(waitingBean.get(i).getUser_id());
            chatmodel.setToken(new ArrayList<>());
            chatmodel.setChatid(waitingBean.get(i).getChatid());
            chatmodel.setCurrentproblem(waitingBean.get(i).getCurrentproblem());
            chatmodel.setBooking_date(waitingBean.get(i).getBooking_date());
            chatmodel.setBooking_time(waitingBean.get(i).getBooking_time());
            appointmentList.add(chatmodel);
        }
        setAppointmentRv();
    }

    /*private void setAppointmentRv(){
        appointmentListRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL,false));
        for (int i=0;i<3;i++){
            ChatListModel chatmodel = new ChatListModel();
            chatmodel.setBreed("pug");
            chatmodel.setId("");
            chatmodel.setPet_age("5 yrs");
            chatmodel.setPet_name("Tommy");
            chatmodel.setPet_type("Dog");
            chatmodel.setPetimage("");
            chatmodel.setStatus("");
            chatmodel.setUser_id("");
            appointmentList.add(chatmodel);

        }
        ItemClickListener itemClickListener = (view, position) -> startActivity(new Intent(activity, ActivityPatientDetail.class));

        appointmentListRv.setAdapter(new WaitingListAdapter(activity, appointmentList, itemClickListener,"waiting"));
    }*/

    private void setAppointmentRv() {
        appointmentListRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));


        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                Gson gson = new Gson();
                String myJson = gson.toJson(waitingBean.get(position));
                startActivity(new Intent(activity, ActivityPatientDetail.class)
                        .putExtra("model", myJson)
                        .putExtra("type", ""));
            }
        };
        appointmentListRv.setAdapter(new WaitingListAdapter(activity, appointmentList, itemClickListener, "waiting video"));
    }
}
