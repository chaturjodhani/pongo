package com.pongo.health.fragment.calling_request;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.CallRequestAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseFragment;
import com.pongo.health.model.CartModel;
import com.pongo.health.model.ChatListModel;
import com.pongo.health.model.DoctorWaitingListModel;
import com.pongo.health.ui.vet.ActivityPatientDetail;
import com.pongo.health.utils.ActionListener;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentUpcomingAppointments extends BaseFragment {

    @BindView(R.id.upcoming_appointment_rv)
    RecyclerView upcomingAppointmentRv;
    private List<ChatListModel> appointmentsList = new ArrayList<>();
    private List<DoctorWaitingListModel.WatingBean> waitingBean = new ArrayList<>();
    private CustomDialog mLoadingView;
    private Session mSession;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmnet_upcoming_appointement, container, false);
        ButterKnife.bind(this, view);
        mSession = new Session(activity);
        mLoadingView = new CustomDialog(activity);

        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        if (InternetConnection.checkConnection(activity)) {
            getChatData();
        } else {
            Toast.makeText(activity, "No Internet available", Toast.LENGTH_SHORT).show();
        }
    }

    public static FragmentUpcomingAppointments newInstance() {
        return new FragmentUpcomingAppointments();
    }

    private void setWaitingRv() {
        upcomingAppointmentRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));


        ActionListener itemClickListener = new ActionListener() {
            @Override
            public void onReviewClicked(View view, int position) {
                Gson gson = new Gson();
                String myJson = gson.toJson(waitingBean.get(position));
                startActivity(new Intent(activity, ActivityPatientDetail.class)
                        .putExtra("model", myJson)
                        .putExtra("type", "hide"));
            }

            @Override
            public void onAcceptClicked(View view, int position) {

            }

            @Override
            public void onCancelClicked(View view, int position) {

            }


        };
        upcomingAppointmentRv.setAdapter(new CallRequestAdapter(activity, appointmentsList, itemClickListener, "upcoming"));
    }

    private void getChatData() {
        appointmentsList.clear();
        setWaitingRv();
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).upComingAppointment(id).enqueue(new Callback<DoctorWaitingListModel>() {
            public void onResponse(Call<DoctorWaitingListModel> call, Response<DoctorWaitingListModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((DoctorWaitingListModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        waitingBean = response.body().getWating();
                        if (null != waitingBean && waitingBean.size() > 0) {
                            createWaitingList();
                        }

                    }
                }

            }

            public void onFailure(Call<DoctorWaitingListModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(activity, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void createWaitingList() {
        appointmentsList.clear();
        for (int i = 0; i < waitingBean.size(); i++) {
            ChatListModel chatmodel = new ChatListModel();
            chatmodel.setBreed(waitingBean.get(i).getBreed());
            chatmodel.setId(waitingBean.get(i).getPet_id());
            chatmodel.setPet_age(waitingBean.get(i).getPet_age());
            chatmodel.setPet_name(waitingBean.get(i).getPet_name());
            chatmodel.setPet_type(waitingBean.get(i).getPet_type());
            chatmodel.setPetimage(waitingBean.get(i).getPetimage());
            chatmodel.setStatus(waitingBean.get(i).getStatus());
            chatmodel.setUser_id(waitingBean.get(i).getUser_id());
            chatmodel.setToken(new ArrayList<>());
            chatmodel.setChatid(waitingBean.get(i).getChatid());
            chatmodel.setCurrentproblem(waitingBean.get(i).getCurrentproblem());
            chatmodel.setBooking_date(waitingBean.get(i).getBooking_date());
            chatmodel.setBooking_time(waitingBean.get(i).getBooking_time());
            appointmentsList.add(chatmodel);
        }
        setWaitingRv();
    }

   /* private void setWaitingRv(){
        upcomingAppointmentRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL,false));
        for (int i=0;i<3;i++){
            appointmentsList.add(new CartModel(R.drawable.senior,"Tommy - yrs","Robies",
                    "Dog | pug"));

        }
        ItemClickListener itemClickListener = (view, position) -> startActivity(new Intent(activity, ActivityPatientDetail.class));

        upcomingAppointmentRv.setAdapter(new CallRequestAdapter(activity, appointmentsList, itemClickListener));
    }*/
}
