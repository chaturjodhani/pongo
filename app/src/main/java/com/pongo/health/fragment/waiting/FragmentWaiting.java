package com.pongo.health.fragment.waiting;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.gson.Gson;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.WaitingListAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseFragment;
import com.pongo.health.model.CartModel;
import com.pongo.health.model.ChatListModel;
import com.pongo.health.model.DoctorWaitingListModel;
import com.pongo.health.model.TechChatResponseModel;
import com.pongo.health.ui.ChatViewActivity;
import com.pongo.health.ui.pet_parent.ActivityVeterinaryClinics;
import com.pongo.health.ui.vet.ActivityChatWaiting;
import com.pongo.health.ui.vet.ActivityPatientDetail;
import com.pongo.health.utils.Constants;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentWaiting extends BaseFragment {

    @BindView(R.id.waiting_list_rv)
    RecyclerView waitingListRv;
    private List<ChatListModel> waitingList = new ArrayList<>();
    private List<DoctorWaitingListModel.WatingBean> waitingBean = new ArrayList<>();
    private CustomDialog mLoadingView;
    private Session mSession;
    Firebase reference1;
    ValueEventListener valueEventListener;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmnet_waiting, container, false);
        ButterKnife.bind(this, view);
        mSession = new Session(activity);
        mLoadingView = new CustomDialog(activity);
        String userId = mSession.getuser_id();
        reference1 = new Firebase(Constants.firebaseUrl + "videowatinglist/" + userId);
        eventListener();
        return view;

    }

    public static FragmentWaiting newInstance() {
        return new FragmentWaiting();
    }

    private void eventListener() {
       /* reference1.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                try {
                    Map map = dataSnapshot.getValue(Map.class);

//                            *  : [{"id":"135","name":"Vet Testing","userimg":"http://digittrix.com/staging/pongo/assets/images/avtar.png","problems":"Behavioral,Heartworm"}]

                    String chatid = Objects.requireNonNull(map.get("chatid")).toString();
                    String pastdoctor = Objects.requireNonNull(map.get("pastdoctor")).toString();
                    String currentissue = Objects.requireNonNull(map.get("currentissue")).toString();
                    String attachments = Objects.requireNonNull(map.get("attachments")).toString();
                    String chatroomname = Objects.requireNonNull(map.get("chatroomname")).toString();
                    String pet_id = Objects.requireNonNull(map.get("pet_id")).toString();
                    String user_id = Objects.requireNonNull(map.get("user_id")).toString();
                    String pet_name = Objects.requireNonNull(map.get("pet_name")).toString();
                    String pet_type = Objects.requireNonNull(map.get("pet_type")).toString();
                    String petimage = Objects.requireNonNull(map.get("petimage")).toString();
                    String breed = Objects.requireNonNull(map.get("breed")).toString();
                    String status = Objects.requireNonNull(map.get("status")).toString();
                    String pet_age = Objects.requireNonNull(map.get("pet_age")).toString();
//                    for (int i = 0; i < waitingBean.size(); i++) {
                    DoctorWaitingListModel.WatingBean chatmodel = new DoctorWaitingListModel.WatingBean();
                    chatmodel.setBreed(breed);
                    chatmodel.setPet_id(pet_id);
                    chatmodel.setPet_age(pet_age);
                    chatmodel.setPet_name(pet_name);
                    chatmodel.setPet_type(pet_type);
                    chatmodel.setPetimage(petimage);
                    chatmodel.setStatus(status);
                    chatmodel.setUser_id(user_id);
//                        chatmodel.sett(new ArrayList<>());
                    chatmodel.setChatid(chatid);
                    chatmodel.setCurrentproblem(currentissue);
//            chatmodel.setBooking_date(waitingBean.get(i).getBooking_date());
//            chatmodel.setBooking_time(waitingBean.get(i).getBooking_time());
                    waitingBean.add(chatmodel);
//                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.i("data", s);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.i("data", dataSnapshot.toString());

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.i("data", s);

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.i("data", firebaseError.getMessage());

            }
        });
*/

        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Log.e("Count ", "" + snapshot.getChildrenCount());
                try {
                    waitingBean.clear();
                  /*  for (DataSnapshot mainSnapshot : snapshot.getChildren()) {
                        for (DataSnapshot postSnapshot : mainSnapshot.getChildren()) {
                            DoctorWaitingListModel.WatingBean post = postSnapshot.getValue(DoctorWaitingListModel.WatingBean.class);
                            waitingBean.add(post);
                            Log.e("Get Data", post.getBreed());
                        }

                    }
                    */
//                    for (DataSnapshot mainSnapshot : snapshot.getChildren()) {
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        DoctorWaitingListModel.WatingBean post = postSnapshot.getValue(DoctorWaitingListModel.WatingBean.class);
                        waitingBean.add(post);
                        Log.e("Get Data", post.getBreed());
                    }

//                    }
                    Collections.reverse(waitingBean);
                    createWaitingList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e("The read failed: ", firebaseError.getMessage());
            }
        };
        reference1.addValueEventListener(valueEventListener);

    }


    @Override
    public void onResume() {
        super.onResume();
      /*  if (InternetConnection.checkConnection(activity)) {
            getChatData();
        } else {
            Toast.makeText(activity, "No Internet available", Toast.LENGTH_SHORT).show();
        }*/
    }

    private void setWaitingRv() {
        waitingListRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));


        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                Gson gson = new Gson();
                String myJson = gson.toJson(waitingBean.get(position));
                startActivity(new Intent(activity, ActivityPatientDetail.class)
                        .putExtra("model", myJson)
                        .putExtra("type", ""));
            }
        };
        waitingListRv.setAdapter(new WaitingListAdapter(activity, waitingList, itemClickListener, "waiting video"));
    }

    private void getChatData() {
        waitingList.clear();
        setWaitingRv();
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).doctorwaitingList(id).enqueue(new Callback<DoctorWaitingListModel>() {
            public void onResponse(Call<DoctorWaitingListModel> call, Response<DoctorWaitingListModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((DoctorWaitingListModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        waitingBean = response.body().getWating();
                        if (null != waitingBean && waitingBean.size() > 0) {
                            createWaitingList();
                        }

                    }
                }

            }

            public void onFailure(Call<DoctorWaitingListModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(activity, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void createWaitingList() {
        waitingList.clear();
        for (int i = 0; i < waitingBean.size(); i++) {
            ChatListModel chatmodel = new ChatListModel();
            chatmodel.setBreed(waitingBean.get(i).getBreed());
            chatmodel.setId(waitingBean.get(i).getPet_id());
            chatmodel.setPet_age(waitingBean.get(i).getPet_age());
            chatmodel.setPet_name(waitingBean.get(i).getPet_name());
            chatmodel.setPet_type(waitingBean.get(i).getPet_type());
            chatmodel.setPetimage(waitingBean.get(i).getPetimage());
            chatmodel.setStatus(waitingBean.get(i).getStatus());
            chatmodel.setUser_id(waitingBean.get(i).getUser_id());
            chatmodel.setToken(new ArrayList<>());
            chatmodel.setChatid(waitingBean.get(i).getChatid());
            chatmodel.setCurrentproblem(waitingBean.get(i).getCurrentproblem());
//            chatmodel.setBooking_date(waitingBean.get(i).getBooking_date());
//            chatmodel.setBooking_time(waitingBean.get(i).getBooking_time());
            waitingList.add(chatmodel);
        }
        setWaitingRv();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != reference1) {
            reference1.addValueEventListener(valueEventListener);

        }
    }
}
