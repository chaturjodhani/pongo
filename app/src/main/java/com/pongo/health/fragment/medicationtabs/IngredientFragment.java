package com.pongo.health.fragment.medicationtabs;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pongo.health.R;
import com.pongo.health.adapter.IngredientAdapter;
import com.pongo.health.base.BaseFragment;
import com.pongo.health.model.SingleProductModel;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class IngredientFragment extends BaseFragment {
    @BindView(R.id.blue_btn_tv)
    TextView cartTv;
    @BindView(R.id.fq_list)
    RecyclerView FqList;
    private List<SingleProductModel.SingleproductBean.IngredientsBean> faqList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private SingleProductModel mSingleProductModel;
    public IngredientFragment(SingleProductModel singleProductModel) {
        this.mSingleProductModel = singleProductModel;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ingredient, container, false);
        ButterKnife.bind(this, view);

        cartTv.setText("Add to Cart");
        if (mSingleProductModel.getSingleproduct().get(0).getFaq().size() > 0) {
            setData();
        }
        return view;
    }

    private void setData() {
        faqList = mSingleProductModel.getSingleproduct().get(0).getIngredients();
        itemClickListener = new ItemClickListener() {

            @Override
            public void onClicked(View view, int postion) {
//                RvList.setText(strengthList.get(postion));
            }
        };
        FqList.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));

        FqList.setAdapter(new IngredientAdapter(activity, faqList, itemClickListener));
    }
}
