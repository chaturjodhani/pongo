package com.pongo.health.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.AppointmentsAdapter;
import com.pongo.health.adapter.HomeCateAdapter;
import com.pongo.health.adapter.RecommendedAdapter;
import com.pongo.health.adapter.YourPetsAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseFragment;
import com.pongo.health.model.HomeModel;
import com.pongo.health.ui.ActivityBlog;
import com.pongo.health.ui.ActivityMedicationSearch;
import com.pongo.health.ui.ActivityPetProblems;
import com.pongo.health.ui.RecommendProductDetailActivity;
import com.pongo.health.ui.addpet.ActivityPetName;
import com.pongo.health.ui.pet_parent.ActivityYourPetSingleType;
import com.pongo.health.ui.vet.VideocallActivity;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentHome extends BaseFragment {

    @BindView(R.id.categories_type_rv)
    RecyclerView categoriesTypeRv;
    @BindView(R.id.user_name)
    TextView userName;
    @BindView(R.id.no_apt_txt)
    TextView npAptText;
    @BindView(R.id.your_pets_rv)
    RecyclerView yourPetsRv;
    @BindView(R.id.recommended_articles_rv)
    RecyclerView recommendedArticlesRv;
    @BindView(R.id.add_pet_ll)
    LinearLayout addPetLl;
    @BindView(R.id.upcoming_appointment_rv)
    RecyclerView upcomingAppointmentRv;

    private List<HomeModel.CategoriesBean> categoriesModelList = new ArrayList<>();
    private List<HomeModel.PetlistBean> petsModelList = new ArrayList<>();
    private List<HomeModel.BlogBean> recommmendedList = new ArrayList<>();
    private List<HomeModel.AppointmentBean> appointmentsList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private CustomDialog mLoadingView;
    private Session mSession;

    //This method gets called whenever we attach fragment to the activity

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
//        ViewGroup.LayoutParams layoutParams = addPetLl.getLayoutParams();
//        layoutParams.width = width - 130;
       /* ViewGroup.LayoutParams recycleParams = yourPetsRv.getLayoutParams();
        recycleParams.width = width - 130;
*/
//        addPetLl.setLayoutParams(layoutParams);
//        yourPetsRv.setLayoutParams(recycleParams);
        yourPetsRv.setNestedScrollingEnabled(false);
        recommendedArticlesRv.setNestedScrollingEnabled(false);
        categoriesTypeRv.setNestedScrollingEnabled(false);
        upcomingAppointmentRv.setNestedScrollingEnabled(false);
        mSession = new Session(activity);
        mLoadingView = new CustomDialog(activity);
        userName.setText("Hello, " + mSession.getfirst_name());


        addPetLl.setOnClickListener(view1 -> startActivity(new Intent(activity, ActivityPetName.class)));

        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        if (InternetConnection.checkConnection(activity)) {
            getHomeData();
        } else {
            Toast.makeText(activity, "No Internet available", Toast.LENGTH_SHORT).show();
        }

    }

    public static FragmentHome newInstance() {
        return new FragmentHome();
    }

    private void setCategories() {
        categoriesTypeRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false));

        //   categoriesModelList.add(new CategoriesModel(R.drawable.schedule_img,"Schedules"));
       /* categoriesModelList.add(new CategoriesModel(R.drawable.rx_img, "Rx"));
        categoriesModelList.add(new CategoriesModel(R.drawable.food_img, "Food"));
        categoriesModelList.add(new CategoriesModel(R.drawable.toy_store_img, "Supplies"));
        categoriesModelList.add(new CategoriesModel(R.drawable.insurance_img, "Insurance"));
*/

        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                String id = categoriesModelList.get(position).getCat_id();
                String name = categoriesModelList.get(position).getCat_name();
                Intent intent = new Intent(activity, ActivityMedicationSearch.class);
                intent.putExtra("id", id);
                intent.putExtra("name", name);
                startActivity(intent);
            }
        };
        categoriesTypeRv.setAdapter(new HomeCateAdapter(activity, categoriesModelList, itemClickListener));
    }

    private void setPetList() {
        yourPetsRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false));

       /* for (int i = 0; i < 3; i++) {
            petsModelList.add(new PetsModel("7 year old", "Roberts", "i have hunted that i woul."));

        }*/
        HomeModel.PetlistBean addPet = new HomeModel.PetlistBean();
        addPet.setType("add");
        petsModelList.add(addPet);
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                String type = petsModelList.get(position).getType();
                if (type.equalsIgnoreCase("add")) {
                    startActivity(new Intent(activity, ActivityPetName.class));

                } else {
                    Intent intent = new Intent(activity, ActivityYourPetSingleType.class);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(petsModelList.get(position));
                    intent.putExtra("model", myJson);
                    startActivity(intent);
//                    startActivity(new Intent(activity, ActivityYourPetSingleType.class));
                }
            }
        };
//        itemClickListener = (view, position) -> startActivity(new Intent(activity, ActivityYourPetSingleType.class));
        yourPetsRv.setAdapter(new YourPetsAdapter(activity, petsModelList, itemClickListener));
    }

    private void setrecommended() {
        recommendedArticlesRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false));
      /*  for (int i = 0; i < 4; i++) {
            recommmendedList.add(new PetsModel("", "2nd April ,2020", "Title of the blog"));
        }*/
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                String type = recommmendedList.get(position).getType();
                if (type.equalsIgnoreCase("blog")) {
                    Intent intent = new Intent(activity, ActivityBlog.class);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(recommmendedList.get(position));
                    intent.putExtra("model", myJson);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(activity, RecommendProductDetailActivity.class);
                    String consultId = recommmendedList.get(position).getConsultid();
                    intent.putExtra("id", consultId);
                    startActivity(intent);
                }
            }
        };
        recommendedArticlesRv.setAdapter(new RecommendedAdapter(activity, recommmendedList, itemClickListener));
    }

    private void setAppointments() {
        upcomingAppointmentRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false));
       /* for (int i = 0; i < 4; i++) {
            appointmentsList.add("call on 03:25  Starting in 00:30 minutes");
        }*/
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                if (appointmentsList.get(position).isCall_availabilty()) {
                    updateCallstatus(position);
                } else {
                    Toast.makeText(activity, "Vet is not available yet", Toast.LENGTH_SHORT).show();
                }

            }
        };
        upcomingAppointmentRv.setAdapter(new AppointmentsAdapter(activity, appointmentsList, itemClickListener));

    }


    private void getHomeData() {
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getHomeData(id).enqueue(new Callback<HomeModel>() {
            public void onResponse(Call<HomeModel> call, Response<HomeModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((HomeModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        categoriesModelList = response.body().getCategories();
                        petsModelList = response.body().getPetlist();
                        recommmendedList = response.body().getBlog();
                        appointmentsList = response.body().getAppointment();
                        setCategories();
                       /* if (petsModelList.size() == 0) {
                            yourPetsRv.setVisibility(View.GONE);
                        } else {
                            yourPetsRv.setVisibility(View.VISIBLE);
                            setPetList();
                        }*/
                        setPetList();
                        setrecommended();
                        setAppointments();
                        if (null != appointmentsList && appointmentsList.size() > 0) {
                            npAptText.setVisibility(View.GONE);
                            upcomingAppointmentRv.setVisibility(View.VISIBLE);

                        } else {
                            npAptText.setVisibility(View.VISIBLE);
                            upcomingAppointmentRv.setVisibility(View.GONE);

                        }

                    }
                }

            }

            public void onFailure(Call<HomeModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(activity, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }


    private void updateCallstatus(int position) {
        mLoadingView.show();
        String chatid = appointmentsList.get(position).getChatid();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).updateVideocallStatus(chatid).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Intent intent = new Intent(activity, VideocallActivity.class);
                                    intent.putExtra("user_id", appointmentsList.get(position).getPet_id());
                                    intent.putExtra("pet_name", appointmentsList.get(position).getPet_name());
                                    intent.putExtra("appointment", "yes");
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(activity, "Failed to connect", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }


}
