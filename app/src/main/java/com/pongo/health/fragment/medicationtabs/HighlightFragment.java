package com.pongo.health.fragment.medicationtabs;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.BreedSizeAdapter;
import com.pongo.health.adapter.HighlightAdapter;
import com.pongo.health.adapter.MedicationQuantityAdapter;
import com.pongo.health.adapter.MedicationStrengthAdapter;
import com.pongo.health.adapter.PrescribingPagerAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseFragment;
import com.pongo.health.model.CheckoutResponseModel;
import com.pongo.health.model.DynamicPagerModel;
import com.pongo.health.model.SingleProductModel;
import com.pongo.health.ui.ActivityAddress;
import com.pongo.health.ui.ActivityCart;
import com.pongo.health.ui.ActivityPdfView;
import com.pongo.health.ui.ActivitySingleItemMarket;
import com.pongo.health.ui.checkout.ActivityCheckOutPrescribed;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HighlightFragment extends BaseFragment {

    @BindView(R.id.breed_rv)
    RecyclerView breedRv;
    @BindView(R.id.quantity_rv)
    RecyclerView quantityRv;
    @BindView(R.id.pre_rv)
    RecyclerView preRv;
    @BindView(R.id.blue_btn_tv)
    TextView addCartTv;
    @BindView(R.id.fav_iv)
    ImageView favIv;
    @BindView(R.id.strength_tv)
    TextView strengthTv;
    @BindView(R.id.strength_fl)
    FrameLayout strengthFl;
    @BindView(R.id.breed_tv)
    TextView breedTv;
    @BindView(R.id.breed_fl)
    FrameLayout breedFl;
    @BindView(R.id.quantity_fl)
    FrameLayout quantityFl;
    @BindView(R.id.quantity_tv)
    TextView quantityTv;
    @BindView(R.id.special_shipping)
    TextView specialShipTv;
    @BindView(R.id.medication_vp)
    ViewPager medicationVp;
    @BindView(R.id.strength_dp_iv)
    ImageView strengthDpIv;
    @BindView(R.id.breed_dp_iv)
    ImageView breedDpIv;
    @BindView(R.id.quantity_dp_iv)
    ImageView quantityIv;
    @BindView(R.id.tabs_layout)
    TabLayout tabsLayout;
    @BindView(R.id.title)
    TextView titleTv;
    @BindView(R.id.rating_count)
    TextView ratingTv;
    @BindView(R.id.price)
    TextView priceTv;
    @BindView(R.id.description)
    TextView descriptionTv;
    @BindView(R.id.key_benifit)
    TextView keyBenifitTv;
    @BindView(R.id.precausion)
    TextView precausionTv;
    @BindView(R.id.review_count)
    TextView review_countTv;
    @BindView(R.id.rating_txt)
    TextView rating_txtTv;
    @BindView(R.id.percent_txt)
    TextView percentTxt;
    @BindView(R.id.sub_name_tv)
    TextView subpercentTxt;
    @BindView(R.id.item_num_tv)
    TextView itemNoTxt;
    @BindView(R.id.common_brand_tv)
    TextView commonBarndTxt;
    @BindView(R.id.generic_tv)
    TextView genericTxt;
    @BindView(R.id.use_with_tv)
    TextView use_withTxt;
    @BindView(R.id.adminstartion_tv)
    TextView adminstartion_Txt;
    @BindView(R.id.brand_tv)
    TextView brand_Txt;
    @BindView(R.id.product_tv)
    TextView product_Txt;
    @BindView(R.id.health_tv)
    TextView health_Txt;
    @BindView(R.id.drug_tv)
    TextView drug_Txt;
    @BindView(R.id.review_btn_tv)
    TextView review_Txt;
    @BindView(R.id.rating)
    AppCompatRatingBar ratingBar;
    @BindView(R.id.bottom_ratingbar)
    AppCompatRatingBar bottomratingBar;
    @BindView(R.id.presc_layout)
    LinearLayout preLayout;
    @BindView(R.id.presc_view_bottom)
    LinearLayout prescBottomLayout;
    @BindView(R.id.presc_info_layout)
    LinearLayout prescInfoLayout;
    @BindView(R.id.patient_info_layout)
    LinearLayout patientInfoLayout;
    @BindView(R.id.pdf_view)
    LinearLayout pdfLayout;
    @BindView(R.id.circularProgressBar)
    ProgressBar progressCircle;
    @BindView(R.id.rating_one)
    ProgressBar progressOne;
    @BindView(R.id.rating_two)
    ProgressBar progressTwo;
    @BindView(R.id.rating_three)
    ProgressBar progressThree;
    @BindView(R.id.rating_four)
    ProgressBar progressFour;
    @BindView(R.id.rating_five)
    ProgressBar progressFive;
    private CustomDialog mLoadingView;
    private Session mSession;
    private List<SingleProductModel.SingleproductBean.StrengthBean> strengthList = new ArrayList<>();
    private List<SingleProductModel.SingleproductBean.BreedSizeBean> breedList = new ArrayList<>();
    private List<SingleProductModel.SingleproductBean.PrescriptionBean> prescList = new ArrayList<>();
    private List<String> quantityList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private boolean isChecked = false;
    private boolean breedChecked = false;
    private boolean quantityChecked = false;
    private SingleProductModel mSingleProductModel;
    private List<DynamicPagerModel> viewPagerModelList = new ArrayList<>();
    private int mselectedVariationPosition = 0;
    private int mQuantityPosition = -1;
    private int tempPosition = 0;
    private float totalPrice;

    public HighlightFragment(SingleProductModel singleProductModel) {
        // Required empty public constructor
        this.mSingleProductModel = singleProductModel;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_highlight, container, false);
        ButterKnife.bind(this, view);
        mLoadingView = new CustomDialog(activity);
        mSession = new Session(activity);
        addCartTv.setText("Add to Cart");
        if (mSingleProductModel.getSingleproduct().get(0).getStrength().size() > 0) {
            strengthList = mSingleProductModel.getSingleproduct().get(0).getStrength();
            strengthTv.setText(strengthList.get(mselectedVariationPosition).getName());
        }
        if (mSingleProductModel.getSingleproduct().get(0).getBreed_size().size() > 0) {
            breedList = mSingleProductModel.getSingleproduct().get(0).getBreed_size();
            setBreed();
        }
        if (mSingleProductModel.getSingleproduct().get(0).getPrescription().size() > 0) {
            prescList = mSingleProductModel.getSingleproduct().get(0).getPrescription();
            setPres();
        }
        setQuantity();
        if (mSingleProductModel.getSingleproduct().get(0).getPrescription_item().equalsIgnoreCase("yes")) {
            preLayout.setVisibility(View.VISIBLE);
            prescBottomLayout.setVisibility(View.VISIBLE);
        } else {
            preLayout.setVisibility(View.GONE);
            prescBottomLayout.setVisibility(View.GONE);
        }

        viewPagerModelList.clear();
        for (int i = 0; i < mSingleProductModel.getSingleproduct().get(0).getProduct_image().size(); i++) {
            viewPagerModelList.add(new DynamicPagerModel("",
                    "",
                    mSingleProductModel.getSingleproduct().get(0).getProduct_image().get(i).getImage()));
        }
        setTextData();
        tabsLayout.setupWithViewPager(medicationVp);
        medicationVp.setAdapter(new PrescribingPagerAdapter(activity, viewPagerModelList));
        addCartTv.setOnClickListener(this);
        strengthFl.setOnClickListener(this);
        breedFl.setOnClickListener(this);
        quantityFl.setOnClickListener(this);
        review_Txt.setOnClickListener(this);
        patientInfoLayout.setOnClickListener(this);
        prescInfoLayout.setOnClickListener(this);

        return view;
    }

    private void setTextData() {
        SingleProductModel.SingleproductBean databean = mSingleProductModel.getSingleproduct().get(0);
        if (databean.isPrescribing_show()) {
            pdfLayout.setVisibility(View.VISIBLE);
        } else {
            pdfLayout.setVisibility(View.GONE);

        }
        titleTv.setText(databean.getName());
        specialShipTv.setText(databean.getSpecial_shipping());
        ratingTv.setText(String.valueOf(databean.getReview_count()));
        priceTv.setText("$" + databean.getOrignalprice());
        int reviewpercentage = databean.getReviewpercentage();
        progressCircle.setProgress(reviewpercentage);
        int rating = databean.getReview_rating();
        ratingBar.setRating(rating);
        bottomratingBar.setRating(rating);
        descriptionTv.setText(databean.getDescription());
        keyBenifitTv.setText(databean.getKeybenifits());
        precausionTv.setText(databean.getPrecautions());
        review_countTv.setText(databean.getReview_count() + " Customer Reviews");
        rating_txtTv.setText(String.valueOf(databean.getReview_rating()));
        percentTxt.setText(databean.getReviewpercentage() + "%");
        subpercentTxt.setText(databean.getReviewpercentage() + "% of Reviewers");
        itemNoTxt.setText(databean.getItem_number());
        commonBarndTxt.setText(databean.getCommon_brand_name());
        genericTxt.setText(databean.getGeneric_name());
        use_withTxt.setText(databean.getFor_use_with());
        adminstartion_Txt.setText(databean.getAdministartion_form());
        brand_Txt.setText(databean.getBrand());
        product_Txt.setText(databean.getProduct_form());
        health_Txt.setText(databean.getHealthcondition());
        drug_Txt.setText(databean.getDrug_type());
        progressOne.setProgress(databean.getStar1());
        progressTwo.setProgress(databean.getStar2());
        progressThree.setProgress(databean.getStar3());
        progressFour.setProgress(databean.getStar4());
        progressFive.setProgress(databean.getStar5());
    }

    private void setPres() {
        preRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false));

        itemClickListener = new ItemClickListener() {

            @Override
            public void onClicked(View view, int postion) {
                // strengthTv.setText(strengthList.get(postion));
            }
        };

        preRv.setAdapter(new HighlightAdapter(activity, prescList, itemClickListener));


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                if (InternetConnection.checkConnection(activity)) {
                    if (mQuantityPosition == -1) {
                        Toast.makeText(activity, "Please select quantity", Toast.LENGTH_SHORT).show();
                    } else {
                        String name = mSingleProductModel.getSingleproduct().get(0).getName();
                        if (mSingleProductModel.getSingleproduct().get(0).getPrescription_item().equalsIgnoreCase("yes")) {
                            String prodId = mSingleProductModel.getSingleproduct().get(0).getId();
                            String quantity = quantityTv.getText().toString();
                            String strength = strengthTv.getText().toString();
                            String price = String.valueOf(totalPrice);
                            startActivity(new Intent(activity, ActivityCheckOutPrescribed.class)
                                    .putExtra("name", name)
                                    .putExtra("prodId", prodId)
                                    .putExtra("quantity", quantity)
                                    .putExtra("strength", strength)
                                    .putExtra("price", price)
                            );

                        } else {
                            addToCart();

                        }
                    }
                } else {
                    Toast.makeText(activity, "No Internet available", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.strength_fl:
                if (mSingleProductModel.getSingleproduct().get(0).getStrength().size() > 0) {
                    strengthList = mSingleProductModel.getSingleproduct().get(0).getStrength();
                    variationPopup();
                } else {
                    Toast.makeText(activity, "No variation available", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.breed_fl:
                breedchecked();
                break;
            case R.id.quantity_fl:
                quantitychecked();
                break;
            case R.id.review_btn_tv:
                openPopup();
                break;
            case R.id.presc_info_layout:
                Intent intent = new Intent(activity, ActivityPdfView.class);
                String url = mSingleProductModel.getSingleproduct().get(0).getPrescribing_info();
                intent.putExtra("name", "Prescribing Information");
                intent.putExtra("url", url);
                if (!TextUtils.isEmpty(url)) {
                    startActivity(intent);
                } else {
                    Toast.makeText(activity, "No pdf available", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.patient_info_layout:
                Intent intent2 = new Intent(activity, ActivityPdfView.class);
                String url2 = mSingleProductModel.getSingleproduct().get(0).getPatient_info_sheet();
                intent2.putExtra("name", "Patient Information Sheet");
                intent2.putExtra("url", url2);
                if (!TextUtils.isEmpty(url2)) {
                    startActivity(intent2);
                } else {
                    Toast.makeText(activity, "No pdf available", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    private void setBreed() {

        breedRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
      /*  strengthList.add("8mg");
        strengthList.add("10mg");
        strengthList.add("12mg");
        strengthList.add("16mg");*/

        itemClickListener = new ItemClickListener() {

            @Override
            public void onClicked(View view, int postion) {
                breedTv.setText(breedList.get(postion).getName());
                breedRv.setVisibility(View.GONE);

            }
        };

        breedRv.setAdapter(new BreedSizeAdapter(activity, breedList, itemClickListener));


    }

    private void setQuantity() {

        quantityRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
        if (mSingleProductModel.getSingleproduct().get(0).getQuantityarray().size() > 0) {
            List<SingleProductModel.SingleproductBean.QuantityarrayBean> quantity = mSingleProductModel.getSingleproduct().get(0).getQuantityarray();
            for (int i = 0; i < quantity.size(); i++) {
                quantityList.add(quantity.get(i).getQuantity());
            }
        }
      /*  quantityList.add("1");
        quantityList.add("2");
        quantityList.add("3");
        quantityList.add("4");
        quantityList.add("5");
        quantityList.add("6");*/


        itemClickListener = new ItemClickListener() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onClicked(View view, int postion) {
                mQuantityPosition = postion;
                quantityTv.setText(quantityList.get(postion));
                quantityRv.setVisibility(View.GONE);
                int quant = Integer.parseInt(quantityList.get(postion));
                float variationprice = Float.parseFloat(strengthList.get(mselectedVariationPosition).getPrice());
                totalPrice = quant * variationprice;
                priceTv.setText("$" + String.format("%.2f", totalPrice));
            }
        };

        quantityRv.setAdapter(new MedicationQuantityAdapter(activity, quantityList, itemClickListener));


    }

 /*   private void checked() {
        if (isChecked) {
            strengthDpIv.setRotation(0);
            strengthRv.setVisibility(View.GONE);
            isChecked = false;
        } else {
            strengthDpIv.setRotation(180);
            strengthRv.setVisibility(View.VISIBLE);
            isChecked = true;
        }
    }*/

    private void breedchecked() {
        if (breedChecked) {
            breedDpIv.setRotation(0);
            breedRv.setVisibility(View.GONE);
            breedChecked = false;
        } else {
            breedDpIv.setRotation(180);
            breedRv.setVisibility(View.VISIBLE);
            breedChecked = true;
        }
    }

    private void quantitychecked() {
        if (quantityChecked) {
            quantityIv.setRotation(0);
            quantityRv.setVisibility(View.GONE);
            quantityChecked = false;
        } else {
            quantityIv.setRotation(180);
            quantityRv.setVisibility(View.VISIBLE);
            quantityChecked = true;
        }
    }


    private void openPopup() {


        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(activity);
        View promptsView = li.inflate(R.layout.review_popup, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                activity);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final TextView closeText = promptsView
                .findViewById(R.id.close);
        TextView sendText = promptsView
                .findViewById(R.id.send);
        AppCompatEditText titleEt = promptsView
                .findViewById(R.id.title_et);
        AppCompatEditText reviewEt = promptsView
                .findViewById(R.id.review_et);
        RatingBar ratingBar = promptsView
                .findViewById(R.id.rating_popup);
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();


        closeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        sendText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (InternetConnection.checkConnection(activity)) {
                    String title = titleEt.getText().toString().trim();
                    String review = reviewEt.getText().toString().trim();
                    int rating = (int) ratingBar.getRating();

                    if (title.isEmpty()) {
                        titleEt.requestFocus();
                        titleEt.setError("Please enter title");

                    } else if (rating <= 0) {
                        Toast.makeText(activity, "Select Rating", Toast.LENGTH_SHORT).show();
                    } else {
                        alertDialog.dismiss();
                        sendReview(title, review, rating);
                    }
                } else {
                    Toast.makeText(activity, "No Internet available", Toast.LENGTH_SHORT).show();
                }
            }
        });
        // show it
        alertDialog.show();
    }

    private void variationPopup() {


        // get prompts.xml view
//        LayoutInflater li = LayoutInflater.from(activity);
//        View promptsView = li.inflate(R.layout.variation_popup, null);

//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//                activity);
        AlertDialog promptsView = new MaterialAlertDialogBuilder(activity, R.style.MyRounded_MaterialComponents_MaterialAlertDialog)  // for fragment you can use getActivity() instead of this
                .setView(R.layout.variation_popup) // custom layout is here
                .show();
        // set prompts.xml to alertdialog builder
//        alertDialogBuilder.setView(promptsView);

        final TextView closeText = promptsView
                .findViewById(R.id.cancel);
        TextView setText = promptsView
                .findViewById(R.id.set);
        RecyclerView strengthRv = promptsView
                .findViewById(R.id.strength_rv);

        // create alert dialog
//        AlertDialog alertDialog = alertDialogBuilder.create();
        setStrength(strengthRv);

        closeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                promptsView.dismiss();
            }
        });
        setText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mselectedVariationPosition = tempPosition;
                strengthTv.setText(strengthList.get(mselectedVariationPosition).getName());
                int quant = 1;
                if (mQuantityPosition >= 0) {
                    quant = Integer.parseInt(quantityList.get(mQuantityPosition));
                }
                float variationprice = Float.parseFloat(strengthList.get(mselectedVariationPosition).getPrice());
                totalPrice = quant * variationprice;
                priceTv.setText("$" + String.format("%.2f", totalPrice));
                promptsView.dismiss();
            }
        });

        // show it
//        alertDialog.show();
    }

    private void setStrength(RecyclerView strengthRv) {

        strengthRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
      /*  strengthList.add("8mg");
        strengthList.add("10mg");
        strengthList.add("12mg");
        strengthList.add("16mg");*/

        itemClickListener = new ItemClickListener() {

            @Override
            public void onClicked(View view, int postion) {
                tempPosition = postion;
//                strengthRv.setVisibility(View.GONE);
            }
        };

        strengthRv.setAdapter(new MedicationStrengthAdapter(activity, strengthList, itemClickListener, mselectedVariationPosition));


    }

    private void sendReview(String title, String review, int rating) {
        this.mLoadingView.show();
        String userid = mSession.getuser_id();
        String prodId = mSingleProductModel.getSingleproduct().get(0).getId();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).addReview(userid, title, String.valueOf(rating), review, prodId).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();


                                } else {
                                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(activity, "Failed to connect", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void addToCart() {
        this.mLoadingView.show();
        String prescription_from = "";
        String pharmacyName = "";
        String hospitalName = "";
        String have_allergy = "";
        String other_medication = "";
        String medical_condition = "";
        String specify = "";
        String prodId = mSingleProductModel.getSingleproduct().get(0).getId();
        String quantity = quantityTv.getText().toString();
        String strength = strengthTv.getText().toString();
        String price = String.valueOf(totalPrice);
        ArrayList<String> imagelist = new ArrayList<>();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("userid", mSession.getuser_id());
        builder.addFormDataPart("existing_status", "no");
        builder.addFormDataPart("prescription_from", prescription_from);
        builder.addFormDataPart("transfer_pharmacy_name", pharmacyName);
        builder.addFormDataPart("primary_hospital_name", hospitalName);
        builder.addFormDataPart("have_allergy", have_allergy);
        builder.addFormDataPart("other_medication", other_medication);
        builder.addFormDataPart("medical_condition", medical_condition);
        builder.addFormDataPart("specify", specify);
        builder.addFormDataPart("product_id", prodId);
        builder.addFormDataPart("quantity", quantity);
        builder.addFormDataPart("strength", strength);
        builder.addFormDataPart("price", price);
        builder.addFormDataPart("click_image[]", "");

        MultipartBody requestBody = builder.build();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.addToCart(requestBody);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                 /*   startActivity(new Intent(activity, ActivityAddress.class)
                                            .putExtra("existing_status", "no")
                                    );*/
                                    startActivity(new Intent(activity, ActivityCart.class));

                                } else {
                                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mLoadingView.hideDialog();
                Toast.makeText(activity, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", t.toString());
            }

        });

/*
        String userid = mSession.getuser_id();
        String prodId = mSingleProductModel.getSingleproduct().get(0).getId();
        String quantity = quantityTv.getText().toString();
        String strength = strengthTv.getText().toString();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).addToCart(prodId, quantity, strength, userid, String.valueOf(totalPrice)).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    String name = mSingleProductModel.getSingleproduct().get(0).getName();
                                    if (mSingleProductModel.getSingleproduct().get(0).getPrescription_item().equalsIgnoreCase("yes")) {
                                        startActivity(new Intent(activity, ActivityCheckOutPrescribed.class)
                                                .putExtra("name", name));

                                    } else {
                                        startActivity(new Intent(activity, ActivityPayment.class)
                                                .putExtra("name", name)
                                                .putExtra("existing_status", "no")
                                        );

                                    }
                                } else {
                                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(activity, "Failed to connect", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });*/
    }

}
