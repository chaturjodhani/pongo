package com.pongo.health.fragment.calling_request;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.CallRequestAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseFragment;
import com.pongo.health.model.ChatListModel;
import com.pongo.health.model.DoctorWaitingListModel;
import com.pongo.health.ui.ActivityAddPrimaryHospital;
import com.pongo.health.ui.vet.ActivityPatientDetail;
import com.pongo.health.utils.ActionListener;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentActionRequired extends BaseFragment {

    @BindView(R.id.action_required_rv)
    RecyclerView actionRequiredRv;
    private List<ChatListModel> actionRequiredList = new ArrayList<>();
    private List<DoctorWaitingListModel.WatingBean> waitingBean = new ArrayList<>();
    private CustomDialog mLoadingView;
    private Session mSession;
    CallRequestAdapter callRequestAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmnet_action_required, container, false);
        ButterKnife.bind(this, view);
        mSession = new Session(activity);
        mLoadingView = new CustomDialog(activity);

        return view;

    }

    public static FragmentActionRequired newInstance() {
        return new FragmentActionRequired();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (InternetConnection.checkConnection(activity)) {
            getChatData();
        } else {
            Toast.makeText(activity, "No Internet available", Toast.LENGTH_SHORT).show();
        }
    }

    private void setWaitingRv() {
        actionRequiredRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));


        ActionListener itemClickListener = new ActionListener() {
            @Override
            public void onReviewClicked(View view, int position) {
                Gson gson = new Gson();
                String myJson = gson.toJson(waitingBean.get(position));
                startActivity(new Intent(activity, ActivityPatientDetail.class)
                        .putExtra("model", myJson)
                        .putExtra("type", "hide"));
            }

            @Override
            public void onAcceptClicked(View view, int position) {
                String chatId = waitingBean.get(position).getChatid();
                acceptAppointment(chatId, position);
            }

            @Override
            public void onCancelClicked(View view, int position) {
                String chatId = waitingBean.get(position).getChatid();
                cancelAppointment(chatId, position);
            }


        };
        callRequestAdapter = new CallRequestAdapter(activity, actionRequiredList, itemClickListener, "action");
        actionRequiredRv.setAdapter(callRequestAdapter);
    }

    private void cancelAppointment(String id, int position) {
        this.mLoadingView.show();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).cancelAppointment(id).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                                    waitingBean.remove(position);
                                    actionRequiredList.remove(position);
                                    callRequestAdapter.notifyDataSetChanged();

                                } else {
                                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(activity, "Failed to cancel appointment", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void acceptAppointment(String id, int position) {
        this.mLoadingView.show();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).acceptAppointment(id).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                                    waitingBean.remove(position);
                                    actionRequiredList.remove(position);
                                    callRequestAdapter.notifyDataSetChanged();
                                } else {
                                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(activity, "Failed to accept appointment", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void getChatData() {
        actionRequiredList.clear();
        setWaitingRv();
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getActionRequired(id).enqueue(new Callback<DoctorWaitingListModel>() {
            public void onResponse(Call<DoctorWaitingListModel> call, Response<DoctorWaitingListModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((DoctorWaitingListModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        waitingBean = response.body().getWating();
                        if (null != waitingBean && waitingBean.size() > 0) {
                            createWaitingList();
                        }

                    }
                }

            }

            public void onFailure(Call<DoctorWaitingListModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(activity, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void createWaitingList() {
        actionRequiredList.clear();
        for (int i = 0; i < waitingBean.size(); i++) {
            ChatListModel chatmodel = new ChatListModel();
            chatmodel.setBreed(waitingBean.get(i).getBreed());
            chatmodel.setId(waitingBean.get(i).getPet_id());
            chatmodel.setPet_age(waitingBean.get(i).getPet_age());
            chatmodel.setPet_name(waitingBean.get(i).getPet_name());
            chatmodel.setPet_type(waitingBean.get(i).getPet_type());
            chatmodel.setPetimage(waitingBean.get(i).getPetimage());
            chatmodel.setStatus(waitingBean.get(i).getStatus());
            chatmodel.setUser_id(waitingBean.get(i).getUser_id());
            chatmodel.setToken(new ArrayList<>());
            chatmodel.setChatid(waitingBean.get(i).getChatid());
            chatmodel.setCurrentproblem(waitingBean.get(i).getCurrentproblem());
            chatmodel.setBooking_date(waitingBean.get(i).getBooking_date());
            chatmodel.setBooking_time(waitingBean.get(i).getBooking_time());
            actionRequiredList.add(chatmodel);
        }
        setWaitingRv();
    }

   /* private void setWaitingRv(){
        actionRequiredRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL,false));
        for (int i=0;i<2;i++){
            actionRequiredList.add(new CartModel(R.drawable.senior,"Tommy - yrs","Robies",
                    "Dog | pug"));

        }
        ItemClickListener itemClickListener = (view, position) -> startActivity(new Intent(activity, ActivityPatientDetail.class));

        actionRequiredRv.setAdapter(new CallRequestAdapter(activity, actionRequiredList, itemClickListener));
    }*/
}
