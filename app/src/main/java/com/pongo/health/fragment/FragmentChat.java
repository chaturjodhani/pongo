package com.pongo.health.fragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.VeterinaryProfessionalAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseFragment;
import com.pongo.health.model.PastChatResponseModel;
import com.pongo.health.model.PetsModel;
import com.pongo.health.ui.ChatViewActivity;
import com.pongo.health.ui.GetDoctorsListActivity;
import com.pongo.health.ui.NearbyChatActivity;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentChat extends BaseFragment {

    @BindView(R.id.first_vp_tv)
    TextView firstVpTv;
    @BindView(R.id.search_vp_tv)
    TextView searchVpTv;
    @BindView(R.id.past_vp_rv)
    RecyclerView pastVpRv;
    @BindView(R.id.online_layout)
    LinearLayout onlineLayout;
    @BindView(R.id.all_layout)
    LinearLayout allLayout;
    private List<PastChatResponseModel.DetailBean> veterinaryList = new ArrayList<>();
    String experience;
    private ItemClickListener itemClickListener;
    private CustomDialog mLoadingView;
    private Session mSession;


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        ButterKnife.bind(this, view);

        pastVpRv.setNestedScrollingEnabled(false);
        mLoadingView = new CustomDialog(activity);
        mSession = new Session(activity);

        allLayout.setOnClickListener(this);
        onlineLayout.setOnClickListener(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (InternetConnection.checkConnection(activity)) {
            getPastList();
        } else {
            Toast.makeText(activity, "No Internet available", Toast.LENGTH_SHORT).show();
        }
    }

    public static FragmentChat newInstance() {
        return new FragmentChat();
    }


    private void setVeterinaryList() {
        pastVpRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                createChat(position);
            }
        };

        pastVpRv.setAdapter(new VeterinaryProfessionalAdapter(activity, veterinaryList, itemClickListener));
    }

    private void getPastList() {
        this.mLoadingView.show();
        String userId = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).pastChat(userId).enqueue(new Callback<PastChatResponseModel>() {
            public void onResponse(Call<PastChatResponseModel> call, Response<PastChatResponseModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((PastChatResponseModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        veterinaryList = response.body().getDetail();
                        setVeterinaryList();

                    }
                }

            }

            public void onFailure(Call<PastChatResponseModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(activity, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }


    private void createChat(int position) {
        this.mLoadingView.show();
        PastChatResponseModel.DetailBean detailBean = veterinaryList.get(position);
        String userId = mSession.getuser_id();
        String chatId = detailBean.getChat_id();
        String serverchatId = detailBean.getServerchatid();
        String petid = detailBean.getPet_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).createChat(userId, chatId,petid,serverchatId,userId).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    if (message.equalsIgnoreCase("chat start")) {
                                        String chatid = jsonObject.getString("chatid");
                                        PastChatResponseModel.DetailBean detailBean = veterinaryList.get(position);
                                        Intent intent = new Intent(activity, ChatViewActivity.class);
                                        intent.putExtra("chat_id", detailBean.getChat_id());
                                        intent.putExtra("name", detailBean.getFirst_name() + " " + detailBean.getLast_name());
                                        intent.putExtra("profileimage", detailBean.getUserimage());
                                        intent.putExtra("status", detailBean.getChatstatus());
                                        intent.putExtra("chatid", chatid);
                                        intent.putStringArrayListExtra("token", (ArrayList<String>) detailBean.getToken());
                                        startActivity(intent);

                                    } else {
                                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                    Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(activity, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.online_layout: {
                Intent intent = new Intent(activity, NearbyChatActivity.class);
                intent.putExtra("type", "online");
                startActivity(intent);
                break;
            }
            case R.id.all_layout: {
                Intent intent = new Intent(activity, GetDoctorsListActivity.class);
                intent.putExtra("type", "all");
                startActivity(intent);
                break;
            }
        }
    }
}
