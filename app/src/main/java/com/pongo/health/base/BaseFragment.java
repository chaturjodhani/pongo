package com.pongo.health.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class BaseFragment extends Fragment implements View.OnClickListener {

    protected Activity activity;
    protected ProgressDialog progressDialog;
   // Session session;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

       //  initLocale();
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    protected  void  setProgressDialog(){

        progressDialog = new ProgressDialog(activity);
        progressDialog.setTitle("please Wait");
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(context instanceof Activity)
        {
            activity=(Activity)context;
        }
    }
    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        activity=activity;
    }
   /* public void initLocale(){
        session = new Session(activity);
        String language = session.getuser_language();
        LocalUtil.updateResources(activity, language);
        if (language.equals("ar")) {
            activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else {
           activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }*/
  //  }


    @Override
    public void onClick(View v) {

    }

}
