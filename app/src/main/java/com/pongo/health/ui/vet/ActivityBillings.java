package com.pongo.health.ui.vet;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.dewinjm.monthyearpicker.MonthYearPickerDialog;
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialogFragment;
import com.google.gson.Gson;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.BillingAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.CartModel;
import com.pongo.health.model.EarningModel;
import com.pongo.health.model.PongoVetsModel;
import com.pongo.health.ui.ActivityMyPongoVets;
import com.pongo.health.ui.AlltransactionActivity;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityBillings extends BaseActivity {

    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.select_month)
    TextView selectMonthTv;
    @BindView(R.id.earning_month)
    TextView earnMonthTv;
    @BindView(R.id.earning)
    TextView earnTv;
    @BindView(R.id.patient_seen)
    TextView patientSeenTv;
    @BindView(R.id.history_title)
    TextView historyTv;
    @BindView(R.id.see_all)
    TextView seeAllTv;
    @BindView(R.id.billings_rv)
    RecyclerView billingsRv;
    private List<EarningModel.PethistoryBean> billingList = new ArrayList<>();
    MonthYearPickerDialogFragment monthPicker;
    BillingAdapter billingAdapter;
    private CustomDialog mLoadingView;
    private Session mSession;
    int yearSelected;
    int monthSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);
        createMonthPicker();
        setBillingRv();
        if (InternetConnection.checkConnection(ActivityBillings.this)) {
            getEarningData();
        } else {
            Toast.makeText(ActivityBillings.this, "No Internet available", Toast.LENGTH_SHORT).show();
        }

    }

    private void createMonthPicker() {
        //Set default values
        Calendar calendar = Calendar.getInstance();
        yearSelected = calendar.get(Calendar.YEAR);
        monthSelected = calendar.get(Calendar.MONTH) + 1;

        monthPicker = MonthYearPickerDialogFragment
                .getInstance(monthSelected-1, yearSelected);

        setMonthListener();

    }

    private void setMonthListener() {
        monthPicker.setOnDateSetListener(new MonthYearPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int year, int monthOfYear) {
                yearSelected = year;
                monthSelected = monthOfYear + 1;
                getEarningData();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.select_month:
                monthPicker.show(getSupportFragmentManager(), null);
                break;
            case R.id.tb_iv:
                clearActivityStack();
                break;
            case R.id.see_all:
                String month = "";
                if (monthSelected > 10) {
                    month = yearSelected + "-" + monthSelected;
                } else {
                    month = yearSelected + "-0" + monthSelected;
                }
                startActivity(new Intent(this, AllPatientHistoryActivity.class)
                        .putExtra("month", month));
                break;
        }

    }


    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("Billing");
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        selectMonthTv.setOnClickListener(this);
        seeAllTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_billings;
    }

    private void getEarningData() {
        this.mLoadingView.show();
        billingList.clear();
        billingAdapter.notifyDataSetChanged();
        String id = mSession.getuser_id();

        String month = "";
        if (monthSelected > 10) {
            month = yearSelected + "-" + monthSelected;
        } else {
            month = yearSelected + "-0" + monthSelected;
        }

        String monthName = DateTime.now().withMonthOfYear(monthSelected).toString("MMM");

        historyTv.setText(monthName + " Patient history");
//        earnMonthTv.setText(monthName + " Earnings");
        earnMonthTv.setText(monthName + " Units");
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getEarning(id, month).enqueue(new Callback<EarningModel>() {
            public void onResponse(Call<EarningModel> call, Response<EarningModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((EarningModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {

                        billingList = response.body().getPethistory();
                        String earn = response.body().getEarning();
                        int point = response.body().getPatient_seen();
                        patientSeenTv.setText(String.valueOf(point));
                        if (!TextUtils.isEmpty(earn)) {
//                            earnTv.setText("$" + earn);
                            earnTv.setText( earn);
                        }
                        if (null != billingList && billingList.size() > 0) {
                            setBillingRv();
                            seeAllTv.setVisibility(View.VISIBLE);
                        } else {
                            seeAllTv.setVisibility(View.GONE);
                        }

                    }
                }

            }

            public void onFailure(Call<EarningModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityBillings.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void setBillingRv() {
        billingsRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 3; i++) {
            billingList.add(new CartModel(R.drawable.senior, "Tommy - yrs", "Robies",
                    "Dog | pug"));

        }*/
        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
//                startActivity(new Intent(ActivityBillings.this, ActivityPatientDetail.class));
                Gson gson = new Gson();
                String myJson = gson.toJson(billingList.get(position));
                startActivity(new Intent(ActivityBillings.this, ActivityPatientDetail.class)
                        .putExtra("model", myJson)
                        .putExtra("type", "hide"));
            }
        };
        billingAdapter = new BillingAdapter(this, billingList, itemClickListener);
        billingsRv.setAdapter(billingAdapter);
    }


}
