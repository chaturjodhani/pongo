package com.pongo.health.ui.vet;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pongo.health.BuildConfig;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.PastChatResponseModel;
import com.pongo.health.ui.ChatViewActivity;
import com.pongo.health.ui.addpet.ActivityPetName;
import com.pongo.health.ui.pet_parent.ActivityYourPetSingleType;
import com.pongo.health.utils.Constants;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.UsPhoneNumberFormatter;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityProfile extends BaseActivity {

    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.change_image)
    ImageView changeIv;
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.txt_price)
    TextView priceTv;
    @BindView(R.id.price_layout)
    LinearLayout priceLayout;
    private CustomDialog mLoadingView;
    private Session mSession;
    ImageView img_profile;
    EditText Firsttxt_name, Lasttxt_name, txt_email, txt_number, txt_licence, txt_hospital;
    String mCurrentPhotoPath;
    String res;
    String imageUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(ActivityProfile.this);
        mSession = new Session(ActivityProfile.this);
        init_();
        if (InternetConnection.checkConnection(ActivityProfile.this)) {
            getInfo();
        } else {
            Toast.makeText(ActivityProfile.this, "No Internet available", Toast.LENGTH_SHORT).show();
        }

    }

    private void init_() {
        Firsttxt_name = findViewById(R.id.frst_txt_name);
        Lasttxt_name = findViewById(R.id.last_txt_name);
        txt_email = findViewById(R.id.txt_email);
        txt_number = findViewById(R.id.txt_number);
        txt_licence = findViewById(R.id.txt_licence);
        txt_hospital = findViewById(R.id.txt_hospital);
        img_profile = findViewById(R.id.img_profile);
        UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(txt_number, "+1 (###) ###-####");
        txt_number.addTextChangedListener(addLineNumberFormatter);
        String type = mSession.getuser_type();
        if (!TextUtils.isEmpty(type) && type.equalsIgnoreCase("vet")) {
            priceLayout.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                clearActivityStack();
                break;
            case R.id.blue_btn_tv:
                UpdateProfile();
                break;
            case R.id.change_image:
                if (!checkPermission()) {
                    selectImage();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            102);
                }
                break;

        }

    }

    private boolean checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
        ) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 102) {
            if (grantResults.length > 0) {

                boolean recordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (recordAccepted) {
                    selectImage();
                }
            }
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (cameraIntent.resolveActivity(getPackageManager()) != null) {
// Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
// Error occurred while creating the File
                        }
// Continue only if the File was successfully created
                        if (photoFile != null) {
                            Uri outputFileUri = FileProvider.getUriForFile(ActivityProfile.this, BuildConfig.APPLICATION_ID, photoFile);
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                            startActivityForResult(cameraIntent, 0);
                        }
                    }


                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:


                    if (resultCode == Activity.RESULT_OK) {

                        File file = new File(Objects.requireNonNull(mCurrentPhotoPath));
                        String fileName = Constants.compressImage(mCurrentPhotoPath, ActivityProfile.this);
//                        Bitmap bitmap = BitmapFactory.decodeFile(fileName);
//                        bitmap = getResizedBitmap(bitmap, 150);
//                        String path = mCurrentPhotoPath;
                        if (!TextUtils.isEmpty(fileName)) {
                            res = mCurrentPhotoPath;
                        } else {
                            res = fileName;
                        }
                        Glide.with(ActivityProfile.this).load(res).into(img_profile);
                    }

                    break;

                case 1:

                    if (resultCode == RESULT_OK && data != null) {

                        Uri selectedImage = data.getData();

                        if (selectedImage != null) {
                            String fileName = Constants.compressImage(selectedImage.toString(), ActivityProfile.this);
                            if (!TextUtils.isEmpty(fileName)) {
                                res = fileName;
                            } else {
                                res = mCurrentPhotoPath;
                            }
                            Glide.with(ActivityProfile.this).load(res).into(img_profile);
                        }
                    }

                    break;
            }
        }
    }

    private File createImageFile() throws IOException {
// Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName, // prefix
                ".jpg", // suffix
                storageDir // directory
        );

// Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();

        return image;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("Profile");
        continueTv.setText(R.string.continue_);

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        continueTv.setOnClickListener(this);
        changeIv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_profile;
    }

    private void getInfo() {
        this.mLoadingView.show();
        String userId = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getInformation(userId).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    JSONObject detaill_object = jsonObject.getJSONObject("detaill");
                                    String email = detaill_object.getString("email");
                                    String first_name = detaill_object.getString("first_name");
                                    String last_name = detaill_object.getString("last_name");
                                    imageUrl = detaill_object.getString("image");
                                    String hospital = detaill_object.getString("hospital");
                                    String phone = detaill_object.getString("phone");
                                    String type = mSession.getuser_type();
                                    if (!TextUtils.isEmpty(type) && type.equalsIgnoreCase("vet")) {
                                        String callprice = detaill_object.getString("callprice");
                                        priceTv.setText(callprice);
                                    }
                                    Firsttxt_name.setText(first_name);
                                    Lasttxt_name.setText(last_name);
                                    txt_email.setText(email);
                                    txt_number.setText(phone);
                                    txt_hospital.setText(hospital);
//                                    Picasso.get().load(image).into(img_profile);
                                    Glide.with(ActivityProfile.this).load(imageUrl).into(img_profile);

                                    JSONArray license_number_Array = detaill_object.getJSONArray("license_number");
                                    {
                                        if (license_number_Array.length() > 0) {
                                            String number = "";
                                            for (int i = 0; i < license_number_Array.length(); i++) {
                                                if (i == license_number_Array.length() - 1) {
                                                    number = number + license_number_Array.get(i).toString();
                                                } else {
                                                    number = number + license_number_Array.get(i).toString() + ",";
                                                }
                                            }
                                            txt_licence.setText(number);
                                        }
                                    }


                                } else {
                                    Toast.makeText(ActivityProfile.this, "Opps .. Something went wrong", Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivityProfile.this, "Opps .. Something went wrong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityProfile.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void UpdateProfile() {
        mLoadingView.show();
        String userId = mSession.getuser_id();
        String email = txt_email.getText().toString();
        String firstName = Firsttxt_name.getText().toString();
        String lastName = Lasttxt_name.getText().toString();
        String licence = txt_licence.getText().toString();
        String hospital = txt_hospital.getText().toString();
        String phone = txt_number.getText().toString();
        String s = "";
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(phone); //editText.getText().toString()
        while (m.find()) {
            s = s + m.group(0);
        }
        s = s.substring(1);
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", userId);
        builder.addFormDataPart("email", email);
        builder.addFormDataPart("first_name", firstName);
        builder.addFormDataPart("last_name", lastName);
        builder.addFormDataPart("license_number", licence);
        builder.addFormDataPart("hospital", hospital);
        builder.addFormDataPart("phone", s);
        String type = mSession.getuser_type();
        if (!TextUtils.isEmpty(type) && type.equalsIgnoreCase("vet")) {
            String callprice = priceTv.getText().toString();
            builder.addFormDataPart("callprice", callprice);

        } else {
            builder.addFormDataPart("callprice", "");
        }

//        File file = new File(res);
//        builder.addFormDataPart("img_identification", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));


        if (!TextUtils.isEmpty(res)) {
            File file = new File(res);
            builder.addFormDataPart("user_image", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));

        } else {
            builder.addFormDataPart("user_image", "");
        }
//        builder.addFormDataPart("flag", filePath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), filePath));


        MultipartBody requestBody = builder.build();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.updateProfile(requestBody);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detaill");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(ActivityProfile.this, message, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(ActivityProfile.this, message, Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(ActivityProfile.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityProfile.this, "Failed to update profile", Toast.LENGTH_SHORT).show();
                Log.e("cate", t.toString());
            }

        });

    }

}
