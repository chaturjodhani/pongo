package com.pongo.health.ui.addpet;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.BreedAdapter;
import com.pongo.health.adapter.LocationsAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.BreedDynamicModel;
import com.pongo.health.model.BreedModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityPetType extends BaseActivity {

    @BindView(R.id.strength_tv)
    TextView strengthTv;
    @BindView(R.id.breed_name)
    TextView breedTv;
    @BindView(R.id.blue_btn_tv)
    TextView nextTv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.pet_name)
    TextView petnameTv;
    @BindView(R.id.pet_img)
    CircleImageView petImage;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.dog_ll)
    LinearLayout dogLl;
    @BindView(R.id.list_layout)
    LinearLayout listLayout;
    @BindView(R.id.cat_ll)
    LinearLayout catLl;
    @BindView(R.id.strength_fl)
    FrameLayout breedLayout;
    @BindView(R.id.breed_rv)
    RecyclerView breedRv;
    @BindView(R.id.search_second_et)
    EditText searchEt;
    private BreedModel modelList;
    private List<BreedDynamicModel> selectionList = new ArrayList<>();

    private String petname;
    private String petimage;
    private String petType = "";
    private CustomDialog mLoadingView;
    private Session mSession;
    private String breedSeleceted = "";
    BreedAdapter breedAdapter;
    ItemClickListener itemClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        if (null != intent) {
            petname = intent.getStringExtra("pet_name");
            petimage = intent.getStringExtra("pet_image");
        }
        breedTv.setVisibility(View.GONE);
        searchEt.setHint("search breed");
        petnameTv.setText(petname + " is a:");
        breedTv.setText(petname + "'s breed is:");
        Glide.with(this).load(petimage).apply(RequestOptions.circleCropTransform()).into(petImage);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);
        if (InternetConnection.checkConnection(this)) {
            getBreedData();
        } else {
            Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
        searchListener();
    }

    private void searchListener() {
        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString());
            }
        });
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        List<BreedDynamicModel> filterdNames = new ArrayList<>();
        selectionList.clear();
        if (null != modelList) {
            if (petType.equalsIgnoreCase("dog")) {
                //looping through existing elements
                for (BreedModel.DogbreedBean s : modelList.getDogbreed()) {
                    //if the existing elements contains the search input
                    if (s.getBreed().toLowerCase().contains(text.toLowerCase())) {
                        //adding the element to filtered list
                        String type = s.getType();
                        String id = s.getId();
                        String breed = s.getBreed();
                        selectionList.add(new BreedDynamicModel(type, id, breed));
                    }
                }
            } else {
                for (BreedModel.CatbreedBean s : modelList.getCatbreed()) {
                    //if the existing elements contains the search input
                    if (s.getBreed().toLowerCase().contains(text.toLowerCase())) {
                        //adding the element to filtered list
                        String type = s.getType();
                        String id = s.getId();
                        String breed = s.getBreed();
                        selectionList.add(new BreedDynamicModel(type, id, breed));
                    }
                }
            }
        }


        //calling a method of the adapter class and passing the filtered list
        breedAdapter.filterList(selectionList);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                goBack();
                break;
            case R.id.blue_btn_tv:
                verify_data();
                break;
            case R.id.dog_ll:
                breedTv.setVisibility(View.VISIBLE);
                listLayout.setVisibility(View.VISIBLE);
                dogLl.setBackground(getResources().getDrawable(R.drawable.rectangle_shaped_two));
                catLl.setBackground(getResources().getDrawable(R.drawable.rectangle_shaped));
                if (!petType.equalsIgnoreCase("dog")) {
                    petType = "dog";
                    breedSeleceted = "";
                    strengthTv.setText("");
                    setBreedRv();
                }
                break;
            case R.id.cat_ll:
                breedTv.setVisibility(View.VISIBLE);
                listLayout.setVisibility(View.VISIBLE);
                dogLl.setBackground(getResources().getDrawable(R.drawable.rectangle_shaped));
                catLl.setBackground(getResources().getDrawable(R.drawable.rectangle_shaped_two));
                if (!petType.equalsIgnoreCase("cat")) {
                    petType = "cat";
                    breedSeleceted = "";
                    strengthTv.setText("");
                    setBreedRv();
                }
                break;
            case R.id.strength_fl:
                if (null != modelList) {
                    if (!TextUtils.isEmpty(petType)) {
                        openPopup();
                    } else {
                        Toast.makeText(this, "please select pet type first", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "no breed data available", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void setBreedRv() {
        selectionList.clear();
        if (petType.equalsIgnoreCase("dog")) {
            for (int i = 0; i < modelList.getDogbreed().size(); i++) {
                String type = modelList.getDogbreed().get(i).getType();
                String id = modelList.getDogbreed().get(i).getId();
                String breed = modelList.getDogbreed().get(i).getBreed();
                selectionList.add(new BreedDynamicModel(type, id, breed));
            }
        } else {
            for (int i = 0; i < modelList.getCatbreed().size(); i++) {
                String type = modelList.getCatbreed().get(i).getType();
                String id = modelList.getCatbreed().get(i).getId();
                String breed = modelList.getCatbreed().get(i).getBreed();
                selectionList.add(new BreedDynamicModel(type, id, breed));
            }
        }
        breedRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        itemClickListener = new ItemClickListener() {

            @Override
            public void onClicked(View view, int postion) {
                breedSeleceted = selectionList
                        .get(postion).getBreed();
                strengthTv.setText(selectionList
                        .get(postion).getBreed());
            }
        };
        breedAdapter = new BreedAdapter(this, selectionList, itemClickListener);
        breedRv.setAdapter(breedAdapter);


    }

    private void openPopup() {
        selectionList.clear();
        if (petType.equalsIgnoreCase("dog")) {
            for (int i = 0; i < modelList.getDogbreed().size(); i++) {
                String type = modelList.getDogbreed().get(i).getType();
                String id = modelList.getDogbreed().get(i).getId();
                String breed = modelList.getDogbreed().get(i).getBreed();
                selectionList.add(new BreedDynamicModel(type, id, breed));
            }
        } else {
            for (int i = 0; i < modelList.getCatbreed().size(); i++) {
                String type = modelList.getCatbreed().get(i).getType();
                String id = modelList.getCatbreed().get(i).getId();
                String breed = modelList.getCatbreed().get(i).getBreed();
                selectionList.add(new BreedDynamicModel(type, id, breed));
            }
        }

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.breed_popup, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final TextView closeText = promptsView
                .findViewById(R.id.close);

        RecyclerView BreedList = promptsView
                .findViewById(R.id.breed_list);
        ItemClickListener itemClickListener;
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        itemClickListener = new ItemClickListener() {

            @Override
            public void onClicked(View view, int postion) {
                breedSeleceted = selectionList
                        .get(postion).getBreed();
                strengthTv.setText(selectionList
                        .get(postion).getBreed());
                alertDialog.dismiss();
            }
        };
        BreedList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        BreedList.setAdapter(new BreedAdapter(this, selectionList, itemClickListener));


        closeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        // show it
        alertDialog.show();
    }

    private void verify_data() {
        if (petType.isEmpty()) {
            Toast.makeText(this, "Please select pet type", Toast.LENGTH_SHORT).show();

        } else if (TextUtils.isEmpty(breedSeleceted)) {
            Toast.makeText(this, "Please select breed", Toast.LENGTH_SHORT).show();

        } else {
            if (petType.equalsIgnoreCase("cat")) {
                Intent intent = new Intent(ActivityPetType.this, ActivityCatSize.class);
                intent.putExtra("pet_name", petname);
                intent.putExtra("pet_image", petimage);
                intent.putExtra("pet_type", petType);
                intent.putExtra("breed", breedSeleceted);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(ActivityPetType.this, ActivityPetSize.class);
                intent.putExtra("pet_name", petname);
                intent.putExtra("pet_image", petimage);
                intent.putExtra("pet_type", petType);
                intent.putExtra("breed", breedSeleceted);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    protected void setText() {
        tbTv.setText(R.string.add_pet);
        nextTv.setText(R.string.next);
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        nextTv.setOnClickListener(this);
        dogLl.setOnClickListener(this);
        catLl.setOnClickListener(this);
        breedLayout.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_add_pet_type;
    }


    private void getBreedData() {
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getBreedData().enqueue(new Callback<BreedModel>() {
            public void onResponse(Call<BreedModel> call, Response<BreedModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((BreedModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        modelList = response.body();
                    }
                }

            }

            public void onFailure(Call<BreedModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityPetType.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        Intent intent = new Intent(ActivityPetType.this, ActivityPetName.class);
        startActivity(intent);
        finish();
    }
}
