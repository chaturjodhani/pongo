package com.pongo.health.ui;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.UserLoginModel;
import com.pongo.health.ui.vet.ActivityTechnician;
import com.pongo.health.ui.vet.ActivityVetDashboard;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmailLoginActivity extends BaseActivity {

    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.blue_btn_tv)
    TextView submitTv;
    @BindView(R.id.phone_number_et)
    AppCompatEditText mPhoneEdit;
    @BindView(R.id.password_et)
    AppCompatEditText mPasswordEdit;
    private String mLogintype = "";
    private String mPhone = "";
    private String mPassword = "";
    private CustomDialog mLoadingView;
    private Session mSession;
    private static final int REQUEST_ACCESS_FINE_LOCATIONMobile = 110;
    boolean hasPermissionLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        if (null != intent) {
            mLogintype = intent.getStringExtra("login_type");
        }
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
        hasPermissionLocation = (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.blue_btn_tv:
                if (InternetConnection.checkConnection(this)) {
                    mPhone = mPhoneEdit.getText().toString().trim();
                    mPassword = mPasswordEdit.getText().toString().trim();

                    if (mPhone.isEmpty()) {
                        Toast.makeText(this, "Please enter your email", Toast.LENGTH_SHORT).show();
                    } else if (mPassword.isEmpty()) {
                        Toast.makeText(this, "Please enter your password", Toast.LENGTH_SHORT).show();
                    } else {
                        if (!hasPermissionLocation) {
                            ActivityCompat.requestPermissions(this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_ACCESS_FINE_LOCATIONMobile);
                        } else {
                            getUserLogin();
                        }
                    }
                } else {
                    Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
//                startActivity(new Intent(this, ActivityVetJoin.class));
                break;
        }
    }

    @Override
    protected void setText() {
        submitTv.setText(R.string.submit);
    }

    @Override
    protected void setOnClick() {
        vTbIv.setOnClickListener(this);
        submitTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_email_login;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case REQUEST_ACCESS_FINE_LOCATIONMobile: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getUserLogin();
                } else {
                    Toast.makeText(EmailLoginActivity.this, "The app was not allowed to get your location. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();

                }
                break;
            }

        }


    }

    private void getUserLogin() {
        this.mLoadingView.show();
        String token = mSession.getToken();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).userLogin(mPhone, mPassword, "", token, "android").enqueue(new Callback<UserLoginModel>() {
            public void onResponse(Call<UserLoginModel> call, Response<UserLoginModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((UserLoginModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        String type = response.body().getUserdetail().getVet_type();
                        String userId = response.body().getUserdetail().getId();
                        Intent intent;
                        if (TextUtils.isEmpty(type)) {
                            intent = new Intent(EmailLoginActivity.this, ActivityHome.class);
                            mSession.setuser_id(userId);
                            mSession.setuser_login(true);
                            mSession.setfirt_name(response.body().getUserdetail().getFirst_name());
                            mSession.setlast_name(response.body().getUserdetail().getLast_name());
                            mSession.setuser_email(mPhone);
                            mSession.setuser_phone(response.body().getUserdetail().getPhone());
                            mSession.setuser_type(type);
                            mSession.setuserPaid(response.body().getUserdetail().getUsertype());
                            startActivity(intent);
                        } else {

                            if ("vet".equals(type)) {
                                intent = new Intent(EmailLoginActivity.this, ActivityVetDashboard.class);
                            } else {
                                intent = new Intent(EmailLoginActivity.this, ActivityTechnician.class);
                            }
                            mSession.setuser_id(userId);
                            mSession.setuser_login(true);
                            mSession.setfirt_name(response.body().getUserdetail().getFirst_name());
                            mSession.setlast_name(response.body().getUserdetail().getLast_name());
                            mSession.setuser_email(mPhone);
                            mSession.setuser_phone(response.body().getUserdetail().getPhone());
                            mSession.setuser_type(type);
                            mSession.setuserPaid(response.body().getUserdetail().getUsertype());
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }

                    } else {
                        Toast.makeText(EmailLoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

            }

            public void onFailure(Call<UserLoginModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(EmailLoginActivity.this, "Failed to login", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

}
