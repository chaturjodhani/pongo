package com.pongo.health.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.FileProvider;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.pongo.health.BuildConfig;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.EarningModel;
import com.pongo.health.ui.addpet.ActivityPetName;
import com.pongo.health.ui.vet.ActivityPatientDetail;
import com.pongo.health.ui.vet.ActivityPetCases;
import com.pongo.health.utils.ArchLifecycleApp;
import com.pongo.health.utils.Constants;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.SoftInputAssist;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchConfiguration;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2.NetworkType;
import com.tonyodev.fetch2.Priority;
import com.tonyodev.fetch2.Request;
import com.tonyodev.fetch2core.DownloadBlock;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class ChatViewActivity extends BaseActivity {
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.layout1)
    LinearLayout layout;
    @BindView(R.id.credit_layout)
    LinearLayout creditlayout;
    @BindView(R.id.credit)
    TextView creditText;
    @BindView(R.id.sendButton)
    RelativeLayout sendButton;
    @BindView(R.id.mediaButton)
    ImageView mediaButton;
    @BindView(R.id.loader)
    ImageView mTypingImg;
    @BindView(R.id.messageArea)
    EditText messageArea;
    @BindView(R.id.status)
    TextView mStatus;
    @BindView(R.id.name)
    TextView mChatWithName;
    @BindView(R.id.profile_image)
    CircleImageView mChatWithProfilePic;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.media_layout)
    LinearLayout medialayout;
    @BindView(R.id.send_image)
    LinearLayout sendImagelayout;
    @BindView(R.id.view_history)
    LinearLayout viewHistorylayout;
    @BindView(R.id.add_patient_note)
    LinearLayout addNotelayout;
    @BindView(R.id.note_txt)
    TextView noteText;
    @BindView(R.id.chat_layout)
    RelativeLayout chatBottomLayout;
    @BindView(R.id.complete_chat)
    LinearLayout completeChatlayout;
    Firebase reference1, reference2;
    Firebase refChatsender, refChatreceiver;
    Firebase myTypeStatus;
    Firebase online_status_all_users;
    Firebase otherTypeStatus;
    ChildEventListener childEventListener;
    ValueEventListener typeEventListener;
    ValueEventListener onlineEventListener;
    String chatWithName;
    String chatWithImage;
    String userId;
    String chatWithId;
    String chatid;
    String petId;
    Session mSession;
    CustomDialog mLoading;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private Uri filePath;
    private Fetch fetch;
    String mCurrentPhotoPath;
    private boolean isMediaVisible = false;
    SoftInputAssist softInputAssist;
    long delay = 3000; // 1 seconds after user stops typing
    long last_text_edit = 0;
    Handler handler = new Handler();
    ArrayList<String> chatWithToken = new ArrayList<>();
    private List<EarningModel.PethistoryBean> petHistory = new ArrayList<>();

    private Runnable input_finish_checker = new Runnable() {
        public void run() {
            if (System.currentTimeMillis() > (last_text_edit + delay - 500)) {
                // TODO: do what you need here
                // ............
                // ............
                myTypeStatus.child("status").setValue("false");

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        removeTaskBar();
        ButterKnife.bind(this);
        softInputAssist = new SoftInputAssist(this);
        mSession = new Session(this);
        mLoading = new CustomDialog(this);
        FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(this)
                .setDownloadConcurrentLimit(3)
                .build();

        fetch = Fetch.Impl.getInstance(fetchConfiguration);
        Firebase.setAndroidContext(this);
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        Intent intent = getIntent();
        if (null != intent) {
            chatWithId = intent.getStringExtra("chat_id");
            chatid = intent.getStringExtra("chatid");
            chatWithName = intent.getStringExtra("name");
            chatWithImage = intent.getStringExtra("profileimage");
            chatWithToken = getIntent().getStringArrayListExtra("token");
            String chatStatus = getIntent().getStringExtra("status");
            if (chatStatus != null && !TextUtils.isEmpty(chatStatus) && chatStatus.equalsIgnoreCase("complete")) {
                chatBottomLayout.setVisibility(View.GONE);
            }
            userId = mSession.getuser_id();
            mChatWithName.setText(chatWithName);
            String type = mSession.getuser_type();

            String userType = "";
            if (TextUtils.isEmpty(type)) {
                String paidType = mSession.getuserPaid();
                if (paidType.equalsIgnoreCase("free")) {
                    String credit = getIntent().getStringExtra("credit");
                    creditlayout.setVisibility(View.VISIBLE);
                    creditText.setText(credit);
                }

                userType = " | Technician";
            } else {
                petId = getIntent().getStringExtra("petId");

                if (!TextUtils.isEmpty(petId)) {
                    getPatientHistory();
                }
            }
            mStatus.setText("Offline" + userType);
            Constants.chatingWith = chatWithId;
            if (!TextUtils.isEmpty(chatWithImage)) {
                Glide.with(this)
                        .load(chatWithImage)
                        .into(mChatWithProfilePic);
            }
        }
        reference1 = new Firebase(Constants.firebaseUrl + "messages/" + userId + "_" + chatWithId);
        reference2 = new Firebase(Constants.firebaseUrl + "messages/" + chatWithId + "_" + userId);
        refChatsender = new Firebase(Constants.firebaseUrl + "recentchat/" + userId);
        refChatreceiver = new Firebase(Constants.firebaseUrl + "recentchat/" + chatWithId);
        setMediaLayout();
        statusListener();
        eventListener();
        setTypingStatus();
        typingListener();
        messageTypeListner();

        loadGif();
//        setToolbar();
    }

    private void getPatientHistory() {

        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).patientHistory(petId).enqueue(new Callback<EarningModel>() {
            public void onResponse(Call<EarningModel> call, retrofit2.Response<EarningModel> response) {
                if (response.body() != null) {
                    String status = ((EarningModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {

                        petHistory = response.body().getPethistory();


                    }
                }

            }

            public void onFailure(Call<EarningModel> call, Throwable th) {
                Log.e("cate", th.toString());
            }
        });


    }

    private void setMediaLayout() {
        String userType = mSession.getuser_type();
        if (TextUtils.isEmpty(userType)) {
            completeChatlayout.setVisibility(View.GONE);
            addNotelayout.setVisibility(View.GONE);
            viewHistorylayout.setVisibility(View.GONE);
            noteText.setText("View Patient Notes");
        } else {
            noteText.setText("Add Patient Notes");
            viewHistorylayout.setVisibility(View.VISIBLE);

        }
    }

    private void toggleMediaLayout() {
        if (isMediaVisible) {
            medialayout.setVisibility(View.GONE);
            isMediaVisible = false;
        } else {
            medialayout.setVisibility(View.VISIBLE);
            isMediaVisible = true;
        }
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        sendImagelayout.setOnClickListener(this);
        completeChatlayout.setOnClickListener(this);
        addNotelayout.setOnClickListener(this);
        viewHistorylayout.setOnClickListener(this);
        mediaButton.setOnClickListener(this);
        sendButton.setOnClickListener(this);
        //say your realtime database has the child `online_statuses`

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_chat_view;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {

    }

    @Override
    public void onBackPressed() {
        String type = mSession.getuserPaid();
        if (type.equalsIgnoreCase("free")) {
            confirDialog();
        } else {
            ChatViewActivity.super.onBackPressed();
        }
    }

    private void confirDialog() {
        new AlertDialog.Builder(this)
                .setMessage("You will lost chat. Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ChatViewActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                String type = mSession.getuserPaid();
                if (type.equalsIgnoreCase("free")) {
                    confirDialog();
                } else {
                    clearActivityStack();
                }

                break;
            case R.id.send_image:
                if (!checkPermission()) {
                    selectImage();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            102);
                }
                break;
            case R.id.mediaButton:
                toggleMediaLayout();
                break;
            case R.id.sendButton:
                sendMessage();
                break;
            case R.id.complete_chat:
                showConfirmation();
                break;
            case R.id.view_history:
                if (null != petHistory && petHistory.size() > 0) {
                    Gson gson = new Gson();
                    String myJson = gson.toJson(petHistory.get(0));
                    startActivity(new Intent(ChatViewActivity.this, ActivityPatientDetail.class)
                            .putExtra("model", myJson)
                            .putExtra("type", "hide"));
                } else {
                    Toast.makeText(this, "No pet history available", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.add_patient_note:
                String userType = mSession.getuser_type();
                if (TextUtils.isEmpty(userType)) {

                } else {
                    Intent intent = new Intent(ChatViewActivity.this, AddNoteActivity.class);
                    intent.putExtra("user_id", chatWithId);
                    intent.putExtra("chatid", chatid);
                    startActivity(intent);
                }
                break;
        }

    }

    private void showConfirmation() {
        new AlertDialog.Builder(this)
                .setTitle("Confirm!")
                .setMessage("Do you really want to mark complete chat?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (InternetConnection.checkConnection(ChatViewActivity.this)) {
                            chatCompleteMark();
                        } else {
                            Toast.makeText(ChatViewActivity.this, "No Internet available", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    private void loadGif() {
        Glide.with(this)
                .load(R.drawable.typing_indicator)
                .into(mTypingImg);
    }

    private boolean checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
        ) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 102) {
            if (grantResults.length > 0) {

                boolean recordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (recordAccepted) {
                    selectImage();
                }
            }
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (cameraIntent.resolveActivity(getPackageManager()) != null) {
// Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
// Error occurred while creating the File
                        }
// Continue only if the File was successfully created
                        if (photoFile != null) {
                            Uri outputFileUri = FileProvider.getUriForFile(ChatViewActivity.this, BuildConfig.APPLICATION_ID, photoFile);
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                            startActivityForResult(cameraIntent, 0);
                        }
                    }


                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        toggleMediaLayout();
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:


                    if (resultCode == Activity.RESULT_OK) {

//                        File file = new File(Objects.requireNonNull(mCurrentPhotoPath));
                        String fileName = Constants.compressImage(mCurrentPhotoPath, ChatViewActivity.this);
//                        Bitmap bitmap = BitmapFactory.decodeFile(fileName);
//                        bitmap = getResizedBitmap(bitmap, 150);
//                        String path = mCurrentPhotoPath;
                        filePath = Uri.fromFile(new File(fileName));
                        sendMedia("image", null);
                    }

                    break;

                case 1:

                    if (resultCode == RESULT_OK && data != null) {

                        Uri selectedImage = data.getData();
                        try {
//                            bitmap = MediaStore.Images.Media.getBitmap(ChatViewActivity.this.getContentResolver(), selectedImage);
//                            bitmap = getPath(selectedImage);
                            String img = null;
                            if (selectedImage != null) {
                                img = Constants.compressImage(selectedImage.toString(), this);
                            }
                            if (!TextUtils.isEmpty(img)) {
                                filePath = Uri.fromFile(new File(img));
                            } else {
                                filePath = selectedImage;
                            }
                            sendMedia("image", null);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    break;
            }
        }
    }

    private File createImageFile() throws IOException {
// Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName, // prefix
                ".jpg", // suffix
                storageDir // directory
        );

// Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();

        return image;
    }

    private void messageTypeListner() {
        messageArea.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s)) {
                    //Set the value of typing field to true.
                    myTypeStatus.child("status").setValue("true");

                } else {
                    // Set to false
                    myTypeStatus.child("status").setValue("false");

                }
                //You need to remove this to run only once
                handler.removeCallbacks(input_finish_checker);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                //avoid triggering event when text is empty
                if (s.length() > 0) {
                    last_text_edit = System.currentTimeMillis();
                    handler.postDelayed(input_finish_checker, delay);
                    myTypeStatus.child("status").setValue("true");

                } else {
                    myTypeStatus.child("status").setValue("false");
                }
            }
        });
    }

    private void typingListener() {
        otherTypeStatus = new Firebase(Constants.firebaseUrl + "typestatus/" + chatWithId + "_" + userId);

        typeEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String snooping_status = dataSnapshot.getValue(String.class);
                //mario should decide what to do with linker's snooping status here e.g.
                if (!TextUtils.isEmpty(snooping_status)) {
                    if (snooping_status.contentEquals("true")) {
                        mTypingImg.setVisibility(View.VISIBLE);
                    } else {
                        mTypingImg.setVisibility(View.INVISIBLE);
                    }
                } else {
                    mTypingImg.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }

        };
        otherTypeStatus.child("status").addValueEventListener(typeEventListener);

    }

    private void setTypingStatus() {
        myTypeStatus = new Firebase(Constants.firebaseUrl + "typestatus/" + userId + "_" + chatWithId);
        //on each user's device when connected they should indicate e.g. `linker` should tell everyone he's snooping around
        myTypeStatus.child("status").setValue("false");
        //also when he's not doing any snooping or if snooping goes bad he should also tell
    }

    private void statusListener() {
        online_status_all_users = new Firebase(Constants.firebaseUrl + "chatstatus/" + chatWithId);

        onlineEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String snooping_status = dataSnapshot.getValue(String.class);
                //mario should decide what to do with linker's snooping status here e.g.
                String type = mSession.getuser_type();
                String userType = "";
                if (TextUtils.isEmpty(type)) {
                    userType = " | Technician";
                }
                if (!TextUtils.isEmpty(snooping_status)) {
                    if (snooping_status.contentEquals("online")) {
                        mStatus.setText("Online" + userType);
//                        mStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.online_indicator, 0, 0, 0);
                    } else {
                        mStatus.setText("Offline" + userType);
//                        mStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ofline_indicator, 0, 0, 0);
                    }
                } else {
                    mStatus.setText("Offline" + userType);
//                    mStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ofline_indicator, 0, 0, 0);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }

        };
        online_status_all_users.child("status").addValueEventListener(onlineEventListener);

      /*  online_status_all_users.child("token").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String snooping_status = dataSnapshot.getValue(String.class);
                //mario should decide what to do with linker's snooping status here e.g.
               *//* if (!TextUtils.isEmpty(snooping_status)) {
                    chatWithToken = snooping_status;
                }*//*
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }

        });*/
    }

    private void eventListener() {

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                try {
                    Map map = dataSnapshot.getValue(Map.class);
                    String message = Objects.requireNonNull(map.get("message")).toString();
                    String userName = Objects.requireNonNull(map.get("user")).toString();
                    String timeSatamp = Objects.requireNonNull(map.get("timestamp")).toString();
                    String time = Constants.getTime(Long.parseLong(timeSatamp));
                    String type = Objects.requireNonNull(map.get("type")).toString();
                    String serverchatid = Objects.requireNonNull(map.get("chatid")).toString();
                    if (serverchatid.equalsIgnoreCase(chatid)) {
                        if (type.equals("complete")) {
                            String userType = mSession.getuser_type();
                            if (TextUtils.isEmpty(userType)) {
                                String pet_name = Objects.requireNonNull(map.get("pet_name")).toString();
                                String logo = Objects.requireNonNull(map.get("logo")).toString();
                                String user_id = Objects.requireNonNull(map.get("user_id")).toString();
                                String tech_id = Objects.requireNonNull(map.get("tech_id")).toString();
                                reference1.child(dataSnapshot.getKey()).removeValue();
                                reference2.child(dataSnapshot.getKey()).removeValue();
                                Intent intent = new Intent(ChatViewActivity.this, FeedbackActivity.class);
                                intent.putExtra("logo", logo);
                                intent.putExtra("user_id", user_id);
                                intent.putExtra("tech_id", tech_id);
                                intent.putExtra("pet_name", pet_name);
                                intent.putExtra("chatid", chatid);
                                startActivity(intent);
                                finish();
                            }
                        } else if (type.equals("message")) {
                            if (userName.equals(userId)) {
                                addMessageBox(message, 1, time);
                            } else {
                                addMessageBox(message, 2, time);
                            }
                        } else if (type.equals("image")) {
                            String fileName = Objects.requireNonNull(map.get("name")).toString();
                            String file = Objects.requireNonNull(map.get("file")).toString();
                            if (userName.equals(userId)) {
                                addImageBox(message, 2, fileName, file, time);
                            } else {
                                addImageBox(message, 1, fileName, file, time);
                            }
                        } else if (type.equals("video")) {
                            String file = Objects.requireNonNull(map.get("file")).toString();
                            String fileName = Objects.requireNonNull(map.get("name")).toString();
                            if (userName.equals(userId)) {
                                addVideoBox(message, 1, fileName, file);
                            } else {
                                addVideoBox(message, 2, fileName, file);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        };
        reference1.addChildEventListener(childEventListener);
    }

    private void sendMessage() {
        String messageText = messageArea.getText().toString();

        if (!TextUtils.isEmpty(messageText)) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("message", messageText);
            map.put("user", userId);
            map.put("type", "message");
            map.put("chatid", chatid);
            map.put("timestamp", Constants.getTimeStamp());
            reference1.push().setValue(map);
            reference2.push().setValue(map);
            messageArea.setText("");
//                    Constants.hideKeyboard(ChatViewActivity.this);
            setRecentChatData(messageText);
            if (null != chatWithToken && chatWithToken.size() > 0) {
                sendNotification(messageText);
            }
        } else {
            Toast.makeText(ChatViewActivity.this, "Please type your message", Toast.LENGTH_SHORT).show();
        }
    }


    private void sendNotification(String messageText) {
        String id = mSession.getuser_id();
        String name = mSession.getfirst_name();
        String pic = mSession.getuser_image();
        JSONObject json = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < chatWithToken.size(); i++) {
            jsonArray.put(chatWithToken.get(i));
        }
        try {
            JSONObject userData = new JSONObject();
            userData.put("name", name);
            userData.put("userid", id);
            userData.put("image", pic);
            userData.put("message", messageText);
            userData.put("type", "chat");
            JSONObject notiData = new JSONObject();
            notiData.put("title", name);
            notiData.put("text", messageText);
            notiData.put("body", messageText);
            notiData.put("image", pic);
            notiData.put("type", "chat");
            notiData.put("userid", id);

            json.put("data", userData);
            json.put("registration_ids", jsonArray);
            json.put("notification", notiData);
            json.put("priority", "high");
//            json.put("to", chatWithToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest("https://fcm.googleapis.com/fcm/send", json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.i("onResponse", "" + response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", "key=AAAANefSQHM:APA91bFQoD5t7afdlBFf-gEA4JBuSx7aeOrNk0cYRXdU8GarTP6i8RHpeYoEsGzyJ-mHDxJs2spckRCdHFbd4aViUYDsyCLuafP66iyVF8bDUyKBL3vA1mQoyjJ-gL9zoq3sS_B2Ujrw");
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        ArchLifecycleApp.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void setRecentChatData(String messageText) {
        Map<String, String> chatWithRece = new HashMap<String, String>();
        chatWithRece.put("message", messageText);
        chatWithRece.put("name", mSession.getfirst_name());
        chatWithRece.put("image", mSession.getuser_image());
        chatWithRece.put("timestamp", Constants.getTimeStamp());
        refChatreceiver.child(userId).setValue(chatWithRece);

        Map<String, String> chatWithSender = new HashMap<String, String>();
        chatWithSender.put("message", messageText);
        chatWithSender.put("name", chatWithName);
        chatWithSender.put("image", chatWithImage);
        chatWithSender.put("timestamp", Constants.getTimeStamp());
        refChatsender.child(chatWithId).setValue(chatWithSender);

    }

    public void addMessageBox(String message, int type, String time) {
        View message_lay;
//        TextView textView = new TextView(ChatViewActivity.this);
//        textView.setText(message);

//        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        lp2.weight = 1.0f;

        if (type == 1) {
//            lp2.gravity = Gravity.LEFT;
//            textView.setBackgroundResource(R.drawable.bubble_in);
            message_lay = LayoutInflater.from(this).inflate(R.layout.sender_message_layout, layout, false);
//            textView.setBackgroundResource(R.drawable.bubble_in);
            TextView msgTxt = message_lay.findViewById(R.id.message);
            TextView timeTxt = message_lay.findViewById(R.id.time);
            LinearLayout root = message_lay.findViewById(R.id.main_root);
            msgTxt.setText(message);
            timeTxt.setText(time);

        } else {
//            lp2.gravity = Gravity.RIGHT;
//            textView.setBackgroundResource(R.drawable.bubble_out);
            message_lay = LayoutInflater.from(this).inflate(R.layout.receiver_message_layout, layout, false);
//            textView.setBackgroundResource(R.drawable.bubble_in);
            TextView msgTxt = message_lay.findViewById(R.id.message);
            TextView timeTxt = message_lay.findViewById(R.id.time);
            TextView nameTxt = message_lay.findViewById(R.id.name);
            LinearLayout root = message_lay.findViewById(R.id.main_root);
            msgTxt.setText(message);
            timeTxt.setText(time);
            nameTxt.setText(chatWithName);
        }
//        textView.setLayoutParams(lp2);
        layout.addView(message_lay);
//        scrollView.fullScroll(View.FOCUS_DOWN);
        scrollView.post(new Runnable() {
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    public void addImageBox(String message, int type, String fileName, String fileLocal, String time) {
        View message_lay;
        if (type == 1) {
            message_lay = LayoutInflater.from(this).inflate(R.layout.left_image_inflater, layout, false);
//            textView.setBackgroundResource(R.drawable.bubble_in);
            ImageView image = message_lay.findViewById(R.id.selected_imageview);
            TextView timeText = message_lay.findViewById(R.id.time);
            Button download = message_lay.findViewById(R.id.download);
            RelativeLayout root = message_lay.findViewById(R.id.main_root);
            MaterialProgressBar progressBar = message_lay.findViewById(R.id.progress_bar);
            Glide.with(this)
                    .load(message)
                    .into(image);
            timeText.setText(time);
            layout.addView(message_lay);
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityOptionsCompat activityOptionsCompat =
                            ActivityOptionsCompat.makeSceneTransitionAnimation(ChatViewActivity.this, image, "TRANSITION_NAME");

                    Intent intent = new Intent(ChatViewActivity.this, ImageViewActivity.class);
                    intent.putExtra("name", chatWithName);
                    intent.putExtra("time", time);
                    intent.putExtra("image", message);
                    startActivity(intent, activityOptionsCompat.toBundle());
                }
            });
           /* fileLocal = fileLocal.replace("file://", "");
            final File file = new File(fileLocal);
            if (file.exists()) {
                progressBar.setVisibility(View.GONE);
                download.setVisibility(View.GONE);
                image.setClickable(true);

                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (file.exists()) {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.parse(file.toString()), "image/*");
                            startActivity(intent);
                        }
                    }
                });
                download.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        download.setVisibility(View.GONE);
                        progressBar.setIndeterminate(true);
                        downloadFile(message, file, image, progressBar);
                    }
                });

            }*/
        } else {
//            root.setGravity(Gravity.RIGHT);
            message_lay = LayoutInflater.from(this).inflate(R.layout.right_image_inflater, layout, false);
//            textView.setBackgroundResource(R.drawable.bubble_out);

            ImageView image = message_lay.findViewById(R.id.selected_imageview);
            TextView timeText = message_lay.findViewById(R.id.time);
            timeText.setText(time);
            Button download = message_lay.findViewById(R.id.download);
            RelativeLayout root = message_lay.findViewById(R.id.main_root);
            MaterialProgressBar progressBar = message_lay.findViewById(R.id.progress_bar);
           /* final File file = new File(Environment.getExternalStorageDirectory()
                    .getAbsolutePath()
                    + "/pongo/" + fileName);
            if (file.exists()) {
                progressBar.setVisibility(View.GONE);
                download.setVisibility(View.GONE);
                image.setClickable(true);
            } else {
                progressBar.setVisibility(View.VISIBLE);
                download.setVisibility(View.VISIBLE);
                image.setClickable(false);

            }*/
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityOptionsCompat activityOptionsCompat =
                            ActivityOptionsCompat.makeSceneTransitionAnimation(ChatViewActivity.this, image, "TRANSITION_NAME");
                    Intent intent = new Intent(ChatViewActivity.this, ImageViewActivity.class);
                    intent.putExtra("name", "You");
                    intent.putExtra("time", time);
                    intent.putExtra("image", message);
                    startActivity(intent, activityOptionsCompat.toBundle());
                }
            });
            /*download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    download.setVisibility(View.GONE);
                    progressBar.setIndeterminate(true);
                    downloadFile(message, file, image, progressBar);
                }
            });*/

            Glide.with(this)
                    .load(message)
                    .into(image);

            layout.addView(message_lay);
        }
        scrollView.fullScroll(View.FOCUS_DOWN);
        scrollView.post(new Runnable() {
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });

    }

    public void addVideoBox(final String message, int type, String fileName, String fileLocal) {
        View message_lay;
        if (type == 1) {
            message_lay = LayoutInflater.from(this).inflate(R.layout.left_image_inflater, layout, false);
//            textView.setBackgroundResource(R.drawable.bubble_in);
            ImageView image = message_lay.findViewById(R.id.selected_imageview);
            RelativeLayout root = message_lay.findViewById(R.id.main_root);
            Button download = message_lay.findViewById(R.id.download);
            MaterialProgressBar progressBar = message_lay.findViewById(R.id.progress_bar);
            fileLocal = fileLocal.replace("file://", "");
            final File file = new File(fileLocal);
            if (file.exists()) {
                progressBar.setVisibility(View.GONE);
                download.setVisibility(View.GONE);
                image.setClickable(true);

//          progressBar.setProgress(20);
//        textView.setText(message);
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (file.exists()) {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.parse(file.toString()), "video/*");
                            startActivity(intent);
                        }
                    }
                });
                download.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        progressBar.setIndeterminate(true);
                        download.setVisibility(View.GONE);
                        downloadFile(message, file, image, progressBar);
                    }
                });

                // pass video url into .load() method
                Glide.with(this)
                        .asBitmap()
                        .load(file)
                        .into(image);

                layout.addView(message_lay);
            }
        } else {
//            root.setGravity(Gravity.RIGHT);
            message_lay = LayoutInflater.from(this).inflate(R.layout.right_image_inflater, layout, false);
//            textView.setBackgroundResource(R.drawable.bubble_out);
            ImageView image = message_lay.findViewById(R.id.selected_imageview);
            RelativeLayout root = message_lay.findViewById(R.id.main_root);
            Button download = message_lay.findViewById(R.id.download);
            MaterialProgressBar progressBar = message_lay.findViewById(R.id.progress_bar);

            final File file = new File(Environment.getExternalStorageDirectory()
                    .getAbsolutePath()
                    + "/pongo/" + fileName);
            if (file.exists()) {
                progressBar.setVisibility(View.GONE);
                download.setVisibility(View.GONE);
                image.setClickable(true);
            } else {
                progressBar.setVisibility(View.VISIBLE);
                download.setVisibility(View.VISIBLE);
                image.setClickable(false);

            }
//          progressBar.setProgress(20);
//        textView.setText(message);
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (file.exists()) {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.parse(file.toString()), "video/*");
                        startActivity(intent);
                    }
                }
            });
            download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    progressBar.setIndeterminate(true);
                    download.setVisibility(View.GONE);
                    downloadFile(message, file, image, progressBar);
                }
            });

            // pass video url into .load() method
            Glide.with(this)
                    .asBitmap()
                    .load(message)
                    .into(image);

            layout.addView(message_lay);
        }
        scrollView.post(new Runnable() {
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
//        scrollView.fullScroll(View.FOCUS_DOWN);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
//        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        return true;
    }


    public static boolean isImageFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.startsWith("image");
    }


    public static boolean isVideoFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.startsWith("video");
    }

    public String getThumbnailPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media._ID};
        String result = null;
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media._ID);

        cursor.moveToFirst();
        long imageId = cursor.getLong(column_index);
        cursor.close();

        cursor = MediaStore.Images.Thumbnails.queryMiniThumbnail(
                getContentResolver(), imageId,
                MediaStore.Images.Thumbnails.MINI_KIND,
                null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            result = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails.DATA));
            cursor.close();
        }
        return result;
    }


    private void sendMedia(final String type, final Uri thumburi) {

        try {
            //bitmap = getPath(data.getData());
            if (filePath != null) {
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setTitle("Saving...");
                progressDialog.show();
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
//                    final StorageReference photoStorageReference = storageReference.child("ProfileImages").child(email);
                    final StorageReference photoStorageReference;
                    if (type.equals("video")) {
                        photoStorageReference = storageReference.child("Photos").child(Objects.requireNonNull(filePath.getLastPathSegment()));
                    } else {
                        photoStorageReference = storageReference.child("Videos").child(Objects.requireNonNull(filePath.getLastPathSegment()));

                    }
                    photoStorageReference.putFile(filePath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            photoStorageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
//                                    Toast.makeText(getBaseContext(), "Upload success! URL - " + uri.toString(), Toast.LENGTH_SHORT).show();
                                    if (null != uri) {
                                        Map<String, String> map = new HashMap<String, String>();
                                        map.put("message", uri.toString());
                                        map.put("type", type);
                                        map.put("name", filePath.getLastPathSegment());
                                        map.put("user", userId);
                                        map.put("chatid", chatid);
                                        map.put("file", filePath.toString());
//                                        map.put("thumbnail", thumburi.toString());
                                        map.put("timestamp", Constants.getTimeStamp());
                                        reference1.push().setValue(map);
                                        reference2.push().setValue(map);
                                        messageArea.setText("");
                                        Constants.hideKeyboard(ChatViewActivity.this);
//                                        setRecentChatData(messageText);
                                        progressDialog.dismiss();
                                    }
                                }
                            });


                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(ChatViewActivity.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded " + (int) progress + "%");
                        }

                    });
                }
            }

            Log.e("Set Selected Image", "");

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    private void downloadFile(String url, File file, ImageView image, MaterialProgressBar progressBar) {
//        String file = "/downloads/test.txt";

        final Request request = new Request(url, file.getAbsolutePath());
        request.setPriority(Priority.HIGH);
        request.setNetworkType(NetworkType.ALL);
        request.addHeader("clientKey", "SD78DF93_3947&MVNGHE1WONG");

        fetch.enqueue(request, updatedRequest -> {
            //Request was successfully enqueued for download.
        }, error -> {
            //An error occurred enqueuing the request.
        });

        fetch.addListener(new FetchListener() {
            @Override
            public void onError(@NotNull Download download, @NotNull Error error, @Nullable Throwable throwable) {

            }

            @Override
            public void onAdded(@NotNull Download download) {

            }

            @Override
            public void onQueued(@NotNull Download download, boolean b) {

            }

            @Override
            public void onWaitingNetwork(@NotNull Download download) {

            }

            @Override
            public void onCompleted(@NotNull Download download) {
                image.setClickable(true);
                progressBar.setVisibility(View.GONE);

            }


            @Override
            public void onDownloadBlockUpdated(@NotNull Download download, @NotNull DownloadBlock downloadBlock, int i) {

            }

            @Override
            public void onStarted(@NotNull Download download, @NotNull List<? extends DownloadBlock> list, int i) {
                progressBar.setIndeterminate(false);
            }

            @Override
            public void onProgress(@NotNull Download download, long l, long l1) {
                progressBar.setProgress(download.getProgress());
            }

            @Override
            public void onPaused(@NotNull Download download) {

            }

            @Override
            public void onResumed(@NotNull Download download) {

            }

            @Override
            public void onCancelled(@NotNull Download download) {

            }

            @Override
            public void onRemoved(@NotNull Download download) {

            }

            @Override
            public void onDeleted(@NotNull Download download) {

            }
        });

    }

    public void back(View view) {
        finish();
    }

    @Override
    public void onPause() {
        softInputAssist.onPause();
        super.onPause();
        Constants.isResumed = false;
        Constants.chatingWith = "";

    }

    @Override
    public void onDestroy() {
        softInputAssist.onDestroy();
        super.onDestroy();
        if (null != reference1) {
            reference1.removeEventListener(childEventListener);
        }
        if (null != otherTypeStatus) {
            otherTypeStatus.removeEventListener(typeEventListener);
        }
        if (null != online_status_all_users) {
            online_status_all_users.removeEventListener(onlineEventListener);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Constants.isResumed = true;
        Constants.chatingWith = chatWithId;
        softInputAssist.onResume();
    }

    private void chatCompleteMark() {
        mLoading.show();
        String userId = mSession.getuser_id();
        if (!TextUtils.isEmpty(userId)) {
            ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).chatCompleteMark(userId, chatWithId, chatid).enqueue(new Callback<ResponseBody>() {
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    mLoading.hideDialog();
                    if (response.body() != null) {
                        String responseData = "";
                        try {
                            responseData = response.body().string();
                            if (!TextUtils.isEmpty(responseData)) {
                                JSONObject jsonObject = new JSONObject(responseData);
                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("detail");
                                if (!TextUtils.isEmpty(status)) {
                                    if (status.equalsIgnoreCase("success")) {
                                        sendCompleteChatMessage();
                                        Toast.makeText(ChatViewActivity.this, message, Toast.LENGTH_SHORT).show();
                                        finish();
                                    } else {
                                        Toast.makeText(ChatViewActivity.this, message, Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(ChatViewActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                public void onFailure(Call<ResponseBody> call, Throwable th) {
                    Log.e("cate", th.toString());
                    mLoading.hideDialog();
                }
            });
        }
    }

    private void sendCompleteChatMessage() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("message", "chat complete");
        map.put("user", userId);
        map.put("type", "complete");
        map.put("chatid", chatid);
        map.put("timestamp", Constants.getTimeStamp());
        map.put("pet_name", chatWithName);
        map.put("logo", chatWithImage);
        map.put("user_id", chatWithId);
        map.put("tech_id", userId);
        reference1.push().setValue(map);
        reference2.push().setValue(map);

    }


}

