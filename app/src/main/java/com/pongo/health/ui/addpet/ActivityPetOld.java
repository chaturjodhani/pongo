package com.pongo.health.ui.addpet;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pongo.health.R;
import com.pongo.health.adapter.AddPetSizeAdapter;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.PetSizeModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityPetOld extends BaseActivity {
    @BindView(R.id.blue_btn_tv)
    TextView nextTv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.pet_name)
    TextView petnameTv;
    @BindView(R.id.pet_img)
    CircleImageView petImage;
    @BindView(R.id.pet_old_rv)
    RecyclerView petOldRv;
    private List<PetSizeModel> petSizeList = new ArrayList<>();
    private AddPetSizeAdapter mAdapter;
    private String petname;
    private String petimage;
    private String petType = "";
    private String petSize = "";
    private String petAge = "";
    private String breedSeleceted = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        if (null != intent) {
            petname = intent.getStringExtra("pet_name");
            petimage = intent.getStringExtra("pet_image");
            petType = intent.getStringExtra("pet_type");
            petSize = intent.getStringExtra("pet_size");
            breedSeleceted = intent.getStringExtra("breed");
        }
        petnameTv.setText("How old is " + petname + "?");
        Glide.with(this).load(petimage).apply(RequestOptions.circleCropTransform()).into(petImage);

        setPetOld();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                goBack();
                break;
            case R.id.blue_btn_tv:
                verify_data();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        Intent intent = new Intent(ActivityPetOld.this, ActivityPetSize.class);
        intent.putExtra("pet_name", petname);
        intent.putExtra("pet_image", petimage);
        intent.putExtra("pet_type", petType);
        intent.putExtra("breed", breedSeleceted);
        startActivity(intent);
        finish();
    }

    public static ActivityPetOld newInstance() {
        return new ActivityPetOld();
    }

    private void setPetOld() {
        petOldRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        petSizeList.add(new PetSizeModel(R.drawable.pet_small_size_img, "Kid", "Utp to 2 years old"));
        petSizeList.add(new PetSizeModel(R.drawable.adult, "Adult", "Between 2-8 years old"));
        petSizeList.add(new PetSizeModel(R.drawable.senior, "Senior", "More than 8 years old"));

        mAdapter = new AddPetSizeAdapter(this, petSizeList);
        petOldRv.setAdapter(mAdapter);

    }

    private void verify_data() {
        if (mAdapter.selectedPosition == -1) {
            Toast.makeText(this, "Please select pet age", Toast.LENGTH_SHORT).show();
        } else {
            petAge = petSizeList.get(mAdapter.selectedPosition).getSizeType();
            Intent intent = new Intent(ActivityPetOld.this, ActivityPetHealthyGoal.class);
            intent.putExtra("pet_name", petname);
            intent.putExtra("pet_image", petimage);
            intent.putExtra("pet_type", petType);
            intent.putExtra("pet_size", petSize);
            intent.putExtra("pet_age", petAge);
            intent.putExtra("breed", breedSeleceted);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void setText() {
        tbTv.setText(R.string.add_pet);
        nextTv.setText(R.string.next);
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        nextTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_add_pet_old;
    }
}
