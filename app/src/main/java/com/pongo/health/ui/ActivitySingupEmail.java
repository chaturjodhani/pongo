package com.pongo.health.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.pongo.health.R;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.OtpModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySingupEmail extends BaseActivity {

    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.blue_btn_tv)
    TextView submitTv;
    @BindView(R.id.phone_number_et)
    AppCompatEditText mPhoneEdit;
    @BindView(R.id.password_et)
    AppCompatEditText mPasswordEdit;
    private String mLogintype = "";
    private String mPhone = "";
    private String mPassword = "";
    private CustomDialog mLoadingView;
    private static final int REQUEST_ACCESS_FINE_LOCATIONMobile = 110;
    boolean hasPermissionLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        if (null != intent) {
            mLogintype = intent.getStringExtra("login_type");
        }
        mLoadingView = new CustomDialog(this);
        hasPermissionLocation = (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.blue_btn_tv:
                if (InternetConnection.checkConnection(this)) {
                    mPhone = mPhoneEdit.getText().toString().trim();
                    mPassword = mPasswordEdit.getText().toString().trim();

                    if (mPhone.isEmpty()) {
                        Toast.makeText(this, "Please enter your email", Toast.LENGTH_SHORT).show();
                    } else if (mPassword.isEmpty()) {
                        Toast.makeText(this, "Please enter your password", Toast.LENGTH_SHORT).show();
                    } else {
                        if (!hasPermissionLocation) {
                            ActivityCompat.requestPermissions(this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_ACCESS_FINE_LOCATIONMobile);
                        } else {
                            getOtp();
                        }
                    }
                } else {
                    Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
//                startActivity(new Intent(this, ActivityVetJoin.class));
                break;
        }
    }

    @Override
    protected void setText() {
        submitTv.setText(R.string.submit);
    }

    @Override
    protected void setOnClick() {
        vTbIv.setOnClickListener(this);
        submitTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login_email;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case REQUEST_ACCESS_FINE_LOCATIONMobile: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getOtp();
                } else {
                    Toast.makeText(ActivitySingupEmail.this, "The app was not allowed to get your location. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();

                }
                break;
            }

        }


    }


    private void getOtp() {
        this.mLoadingView.show();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getOtp("", mPhone).enqueue(new Callback<OtpModel>() {
            public void onResponse(Call<OtpModel> call, Response<OtpModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((OtpModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        Gson gson = new Gson();
                        String myJson = gson.toJson(response.body());
//                        Toast.makeText(ActivitySingupEmail.this, "" + response.body().getOtp(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ActivitySingupEmail.this, ActivityOtp.class);
                        intent.putExtra("otp", myJson);
                        intent.putExtra("login_type", mLogintype);
                        intent.putExtra("phone", "");
                        intent.putExtra("email", mPhone);
                        intent.putExtra("password", mPassword);
                        startActivity(intent);
                    } else {
                        Toast.makeText(ActivitySingupEmail.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }

            }

            public void onFailure(Call<OtpModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivitySingupEmail.this, "Failed to send OTP", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }
}
