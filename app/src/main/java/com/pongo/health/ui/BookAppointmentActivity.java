package com.pongo.health.ui;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.kinda.alert.KAlertDialog;
import com.pongo.health.BuildConfig;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.ImagesAdapter;
import com.pongo.health.adapter.PaymentCardAdapter;
import com.pongo.health.adapter.PetListAdapter;
import com.pongo.health.adapter.PetProblemAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.CardListResponseModel;
import com.pongo.health.model.CategoriesModel;
import com.pongo.health.model.PetModelResponse;
import com.pongo.health.utils.Constants;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.FilePath;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookAppointmentActivity extends BaseActivity {
    @BindView(R.id.tb_iv)
    ImageView backImage;
    @BindView(R.id.problem_pet_type_rv)
    RecyclerView problemPetTypeRv;
    @BindView(R.id.image_rv)
    RecyclerView imagesRv;
    @BindView(R.id.blue_btn_tv)
    TextView callVetcontinueTv;
    @BindView(R.id.attachment_layout)
    LinearLayout attachmentLayout;
    private List<CategoriesModel.ProblemlistBean> petProblemList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private CustomDialog mLoadingView;
    private Session mSession;
    private PetProblemAdapter mPetProblemAdapter;
    private List<PetModelResponse.DataBean> mPetList = new ArrayList<>();
    private int mSelectedPet = -1;
    private ArrayList<String> imageList = new ArrayList<>();
    private ImagesAdapter mImagesAdapter;
    String mCurrentPhotoPath;
    //Pdf request code
    private int PICK_PDF_REQUEST = 2;
    private List<CardListResponseModel.CardlistBean> cardList = new ArrayList<>();
    BottomSheetDialog mBottomSheetDialog;
    private String cardid = "";
    private String customer = "";
    RecyclerView cardRv;
    String mIssue;
    String mPetId;
    String mPetName;
    String vetId;
    String timeSlot;
    String bookingDate;
    String price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(BookAppointmentActivity.this);
        mSession = new Session(BookAppointmentActivity.this);
        Intent intent = getIntent();
        if (null != intent) {
            vetId = intent.getStringExtra("vet_id");
            timeSlot = intent.getStringExtra("time");
            price = intent.getStringExtra("price");
            bookingDate = intent.getStringExtra("booking_date");
            callVetcontinueTv.setText("Book Appointment ($" + price + ")");

        }
        if (InternetConnection.checkConnection(BookAppointmentActivity.this)) {
            getroblemList();
            getpetList();

        } else {
            Toast.makeText(BookAppointmentActivity.this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
        setAdapter();
        createBottomList();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getCardList();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.blue_btn_tv: {
                if (!checkPermission()) {
                    verifyData();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
                            102);
                }

                break;
            }
            case R.id.attachment_layout:
                if (!checkPermission()) {
                    selectImage();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            102);
                }

                break;
        }

    }


    @Override
    protected void setText() {
    }

    @Override
    protected void setOnClick() {
        backImage.setOnClickListener(this);
        callVetcontinueTv.setOnClickListener(this);
        attachmentLayout.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_book_appointment;
    }

    private void createBottomList() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.card_list_bottomsheet, null);
        cardRv = sheetView.findViewById(R.id.card_rv);
        TextView done = sheetView.findViewById(R.id.done);
        TextView addCard = sheetView.findViewById(R.id.blue_btn_tv);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        addCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BookAppointmentActivity.this, ActivityAddCard.class));
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(sheetView);
    }

    private void setCardist() {
        cardRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
      /*  for (int i = 0; i < 3; i++) {
            orderList.add("Lorem Ipsum");

        }*/
        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                cardid = cardList.get(position).getCard_id();
                customer = cardList.get(position).getCustomer();
                confimrDialog(cardList.get(position).getCardnumber());

            }
        };
        cardRv.setAdapter(new PaymentCardAdapter(this, cardList, itemClickListener));
    }

    private void setAdapter() {
        imagesRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                imageList.remove(position);
                mImagesAdapter.notifyDataSetChanged();
            }
        };
//        itemClickListener = (view, position) -> startActivity(new Intent(activity, ActivityYourPetSingleType.class));
        mImagesAdapter = new ImagesAdapter(this, imageList, itemClickListener);
        imagesRv.setAdapter(mImagesAdapter);
    }

    private void confimrDialog(String cardnumber) {
        new KAlertDialog(this, KAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("You want to make payment using " + cardnumber)
                .setCancelText("No")
                .setConfirmText("Yes")
                .showCancelButton(true)
                .setCancelClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                .setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog kAlertDialog) {
                        kAlertDialog.dismissWithAnimation();
                        mBottomSheetDialog.dismiss();
                        getNearestDoctor();
                    }
                })
                .show();
    }

    private void getroblemList() {
        this.mLoadingView.show();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).problemList().enqueue(new Callback<CategoriesModel>() {
            public void onResponse(Call<CategoriesModel> call, Response<CategoriesModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((CategoriesModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        petProblemList = response.body().getProblemlist();
                        if (null != petProblemList && petProblemList.size() > 0) {
                            setPetProblems();
                        }
                    }
                }

            }

            public void onFailure(Call<CategoriesModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(BookAppointmentActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void getpetList() {
        String userId = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getPets(userId).enqueue(new Callback<PetModelResponse>() {
            public void onResponse(Call<PetModelResponse> call, Response<PetModelResponse> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    try {
                        String status = response.body().getStatus();
                        if (!TextUtils.isEmpty(status)) {
                            if (status.equalsIgnoreCase("success")) {
                                mPetList = response.body().getData();

                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<PetModelResponse> call, Throwable th) {
                Log.e("cate", th.toString());

            }
        });
    }

    private void getCardList() {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", mSession.getuser_id());
        MultipartBody requestBody = builder.build();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CardListResponseModel> call = apiInterface.cardList(requestBody);

        call.enqueue(new Callback<CardListResponseModel>() {
            @Override
            public void onResponse(Call<CardListResponseModel> call, Response<CardListResponseModel> response) {
                if (response.body() != null) {
                    try {
                        String status = response.body().getStatus();
//                            String message = jsonObject.getString("message");
                        if (!TextUtils.isEmpty(status)) {
                            if (status.equalsIgnoreCase("success")) {
                                cardList = response.body().getCardlist();
                                setCardist();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<CardListResponseModel> call, Throwable t) {
                Log.e("cate", t.toString());
            }

        });
    }

    private void getNearestDoctor() {
        mLoadingView.show();
        String userId = mSession.getuser_id();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("problem", mIssue);
        builder.addFormDataPart("pet_id", mPetId);
        builder.addFormDataPart("user_id", userId);
        builder.addFormDataPart("doctor_id", vetId);
        builder.addFormDataPart("price", price);
        builder.addFormDataPart("booking_date", bookingDate);
        builder.addFormDataPart("booking_time", timeSlot);
        builder.addFormDataPart("customer", customer);
        builder.addFormDataPart("card_id", cardid);
//        File file = new File(res);
//        builder.addFormDataPart("img_identification", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
        if (imageList != null && imageList.size() > 0) {
            for (int i = 0; i < imageList.size(); i++) {
                if (imageList.get(i) != null) {
                    String model = imageList.get(i);
                    File file = new File(model);
                    builder.addFormDataPart("attachments[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
                }
            }
        } else {
            builder.addFormDataPart("attachments[]", "");
        }
//        builder.addFormDataPart("flag", filePath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), filePath));


        MultipartBody requestBody = builder.build();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.bookAppontment(requestBody);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(BookAppointmentActivity.this, message, Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(BookAppointmentActivity.this, ActivityHome.class);
                                    // set the new task and clear flags
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                    finish();
                                } else {
                                    Toast.makeText(BookAppointmentActivity.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(BookAppointmentActivity.this, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(BookAppointmentActivity.this, "Failed to book appointment", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }

        });

    }

    private boolean checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
        ) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 102) {
            if (grantResults.length > 0) {

                boolean recordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (recordAccepted) {
                    verifyData();
                }
            }
        }
    }

    private void verifyData() {
        if (null != petProblemList && petProblemList.size() > 0) {
            String issue = "";
            for (int i = 0; i < petProblemList.size(); i++) {
                CategoriesModel.ProblemlistBean problemlistBean = petProblemList.get(i);
                if (problemlistBean.isCheck()) {
                    issue = problemlistBean.getName() + "," + issue;
                }
            }
            if (!TextUtils.isEmpty(issue)) {
                if (mPetList.size() > 0) {
                    openPopup(issue);
                } else {
                    Toast.makeText(BookAppointmentActivity.this, "Add pet first", Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(this, "Please select issue first", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setPetProblems() {
        problemPetTypeRv.setLayoutManager(new GridLayoutManager(this,
                2));

        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                if (position >= 0) {
                    CategoriesModel.ProblemlistBean model = petProblemList.get(position);
                    if (model.isCheck()) {
                        model.setCheck(false);
                    } else {
                        model.setCheck(true);
                    }
                    mPetProblemAdapter.notifyDataSetChanged();
                }
            }
        };
        mPetProblemAdapter = new PetProblemAdapter(this, petProblemList, itemClickListener);
        problemPetTypeRv.setAdapter(mPetProblemAdapter);
    }

    private void openPopup(String issue) {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(BookAppointmentActivity.this);
        View promptsView = li.inflate(R.layout.pet_list_popup, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                BookAppointmentActivity.this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        alertDialogBuilder.setCancelable(true);

        RecyclerView petList = promptsView
                .findViewById(R.id.pet_list);
        TextView continueTv = promptsView
                .findViewById(R.id.blue_btn_tv);
        ItemClickListener itemClickListener;
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        itemClickListener = new ItemClickListener() {

            @Override
            public void onClicked(View view, int postion) {
//                alertDialog.dismiss();
                mSelectedPet = postion;
            }
        };
        petList.setLayoutManager(new LinearLayoutManager(BookAppointmentActivity.this, RecyclerView.VERTICAL, false));

        petList.setAdapter(new PetListAdapter(BookAppointmentActivity.this, mPetList, itemClickListener));


        continueTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelectedPet >= 0) {
                    alertDialog.dismiss();
                    mIssue = issue;
                    mPetId = mPetList.get(mSelectedPet).getId();
                    mPetName = mPetList.get(mSelectedPet).getPet_name();
                    mBottomSheetDialog.show();
                } else {
                    Toast.makeText(BookAppointmentActivity.this, "Please select pet first", Toast.LENGTH_SHORT).show();
                }
            }
        });
        // show it
        alertDialog.show();
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Select PDF", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (cameraIntent.resolveActivity(getPackageManager()) != null) {
// Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
// Error occurred while creating the File
                        }
// Continue only if the File was successfully created
                        if (photoFile != null) {
                            Uri outputFileUri = FileProvider.getUriForFile(BookAppointmentActivity.this, BuildConfig.APPLICATION_ID, photoFile);
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                            startActivityForResult(cameraIntent, 0);
                        }
                    }


                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);

                } else if (options[item].equals("Select PDF")) {
                    Intent intent = new Intent();
                    intent.setType("application/pdf");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PICK_PDF_REQUEST);


                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:


                    if (resultCode == Activity.RESULT_OK) {

                        File file = new File(Objects.requireNonNull(mCurrentPhotoPath));
                        String fileName = Constants.compressImage(mCurrentPhotoPath, BookAppointmentActivity.this);
                        if (!TextUtils.isEmpty(fileName)) {
                            imageList.add(fileName);
                            mImagesAdapter.notifyDataSetChanged();
                        }
                    }

                    break;

                case 1:

                    if (resultCode == RESULT_OK && data != null) {

                        Uri selectedImage = data.getData();
                        String fileName = null;
                        if (selectedImage != null) {
                            fileName = Constants.compressImage(selectedImage.toString(), BookAppointmentActivity.this);
                            if (!TextUtils.isEmpty(fileName)) {
                                imageList.add(fileName);
                                mImagesAdapter.notifyDataSetChanged();
                            }
                        }
                    }

                    break;
                case 2:
                    if (resultCode == RESULT_OK && data != null && data.getData() != null) {
                        Uri filePath = data.getData();
                        //getting the actual path of the image
                        String path = FilePath.getPath(this, filePath);
                        imageList.add(path);
                        mImagesAdapter.notifyDataSetChanged();

                    }
                    break;
            }
        }
    }

    private File createImageFile() throws IOException {
// Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName, // prefix
                ".jpg", // suffix
                storageDir // directory
        );

// Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();

        return image;
    }

}