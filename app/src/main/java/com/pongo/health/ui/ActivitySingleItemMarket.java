package com.pongo.health.ui;

import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.PrescribingPagerAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.DynamicPagerModel;
import com.pongo.health.model.SingleProductModel;
import com.pongo.health.ui.checkout.ActivityCheckOutPrescribed;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySingleItemMarket extends BaseActivity {

    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.item_description_tv)
    TextView itemDescriptionTv;
    @BindView(R.id.title)
    TextView titleTv;
    @BindView(R.id.powered_by)
    TextView poweredbyTv;
    @BindView(R.id.item_price_tv)
    TextView priceTv;
    @BindView(R.id.stock)
    TextView stockTv;
    @BindView(R.id.color)
    TextView colorTv;
    @BindView(R.id.add_to_cart_tv)
    TextView addToCartTv;
    @BindView(R.id.buy_now_tv)
    TextView buyNowTv;
    @BindView(R.id.item_viewPager)
    ViewPager itemViewPager;
    @BindView(R.id.item_strike_price_tv)
    TextView itemStrikePriceTv;
    @BindView(R.id.tabs_layout)
    TabLayout tabsLayout;
    private List<DynamicPagerModel> viewPagerModelList = new ArrayList<>();
    private CustomDialog mLoadingView;
    private Session mSession;
    SingleProductModel.SingleproductBean mSingleproductBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);
        if (InternetConnection.checkConnection(this)) {
            String productId = getIntent().getStringExtra("id");
            getProductData(productId);
        } else {
            Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buy_now_tv:
                if (InternetConnection.checkConnection(ActivitySingleItemMarket.this)) {
                    if (mSingleproductBean != null) {
                        addToCart("buy");
                    }
                } else {
                    Toast.makeText(ActivitySingleItemMarket.this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.add_to_cart_tv:
                if (InternetConnection.checkConnection(ActivitySingleItemMarket.this)) {
                    if (mSingleproductBean != null) {
                        addToCart("cart");
                    }
                } else {
                    Toast.makeText(ActivitySingleItemMarket.this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
        }
    }

    private void setItemViewPager(ViewPager viewPager) {
        tabsLayout.setupWithViewPager(viewPager);
//        medicationVp.setAdapter(new MedicationViewPagerAdapter(activity, viewPagerModelList));
        viewPager.setAdapter(new PrescribingPagerAdapter(this, viewPagerModelList));
//        viewPager.setAdapter(new MarketItemViewPagerAdapterNew(this,viewPagerModelList));
       /* viewPager.setClipToPadding(false);
        viewPager.setClipChildren(false);
        viewPager.setOffscreenPageLimit(3);
        viewPager.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(40));
        compositePageTransformer.addTransformer((page, position) -> {
            float f = 1-Math.abs(position);
            page.setScaleY(0.85f + f*0.15f);
        });
        viewPager.setPageTransformer(compositePageTransformer);*/

    }

    @Override
    protected void setOnClick() {
        addToCartTv.setOnClickListener(this);
        tbIv.setOnClickListener(this);
        buyNowTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_single_item_market;
    }

    @Override
    protected void setText() {
//        tbTv.setText("Lorem");
        itemStrikePriceTv.setPaintFlags(itemStrikePriceTv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
      /*  itemDescriptionTv.setText(Html.fromHtml("Chairs made from high-quality wood with" +
                "affordable prices ready to give your room more" +
                "attractive"));
*/

    }

    private void getProductData(String productId) {
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getSingleProductData(id, productId).enqueue(new Callback<SingleProductModel>() {
            public void onResponse(Call<SingleProductModel> call, Response<SingleProductModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((SingleProductModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        if (response.body().getSingleproduct().size() > 0) {
                            setViewPagerData(response.body().getSingleproduct().get(0).getProduct_image());
                            setTextData(response.body().getSingleproduct().get(0));
                            mSingleproductBean = response.body().getSingleproduct().get(0);
                        }
                    }
                }

            }

            public void onFailure(Call<SingleProductModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivitySingleItemMarket.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void setTextData(SingleProductModel.SingleproductBean singleproductBean) {
        String name = singleproductBean.getCategory();
        String subcate = singleproductBean.getSubcategory();
        if (!TextUtils.isEmpty(subcate)) {
            name = name + " > " + subcate;
        }
//        tbTv.setText(singleproductBean.getName());
        tbTv.setText(name);
        if (!TextUtils.isEmpty(singleproductBean.getDiscountprice())) {
            itemStrikePriceTv.setText("$" + singleproductBean.getDiscountprice());

        }
        itemDescriptionTv.setText(singleproductBean.getDescription());
        titleTv.setText(singleproductBean.getName());
        if (!TextUtils.isEmpty(singleproductBean.getBrand())) {
            poweredbyTv.setText("By: " + singleproductBean.getBrand());
        }
        stockTv.setText(singleproductBean.getQuantity() + " in stock");
        priceTv.setText("$" + singleproductBean.getOrignalprice());
        colorTv.setText(singleproductBean.getColor());
    }

    private void setViewPagerData(List<SingleProductModel.SingleproductBean.ProductImageBean> product_image) {
        for (int i = 0; i < product_image.size(); i++) {
            viewPagerModelList.add(new DynamicPagerModel("",
                    "",
                    product_image.get(i).getImage()));
        }

        setItemViewPager(itemViewPager);
    }

    private void addToCart(String openType) {
        this.mLoadingView.show();
        String prescription_from = "";
        String pharmacyName = "";
        String hospitalName = "";
        String have_allergy = "";
        String other_medication = "";
        String medical_condition = "";
        String specify = "";
        String prodId = mSingleproductBean.getId();
        String quantity = "1";
        String strength = "";
        String price = mSingleproductBean.getOrignalprice();
        ArrayList<String> imagelist = new ArrayList<>();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("userid", mSession.getuser_id());
        builder.addFormDataPart("existing_status", "no");
        builder.addFormDataPart("prescription_from", prescription_from);
        builder.addFormDataPart("transfer_pharmacy_name", pharmacyName);
        builder.addFormDataPart("primary_hospital_name", hospitalName);
        builder.addFormDataPart("have_allergy", have_allergy);
        builder.addFormDataPart("other_medication", other_medication);
        builder.addFormDataPart("medical_condition", medical_condition);
        builder.addFormDataPart("specify", specify);
        builder.addFormDataPart("product_id", prodId);
        builder.addFormDataPart("quantity", quantity);
        builder.addFormDataPart("strength", strength);
        builder.addFormDataPart("price", price);
        builder.addFormDataPart("click_image[]", "");

        MultipartBody requestBody = builder.build();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.addToCart(requestBody);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    if (openType.equalsIgnoreCase("buy")) {
                                        startActivity(new Intent(ActivitySingleItemMarket.this, ActivityAddress.class)
                                                .putExtra("existing_status", "no")
                                        );
                                    } else {
                                        startActivity(new Intent(ActivitySingleItemMarket.this, ActivityCart.class));
                                    }
                                } else {
                                    Toast.makeText(ActivitySingleItemMarket.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivitySingleItemMarket.this, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivitySingleItemMarket.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", t.toString());
            }

        });

    }

}
