package com.pongo.health.ui.checkout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.CheckoutResponseModel;
import com.pongo.health.ui.ActivityAddress;
import com.pongo.health.ui.ActivityCart;
import com.pongo.health.ui.addpet.ActivityHaveAnyOtherPet;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCheckoutAdditionalProfile extends BaseActivity {

    @BindView(R.id.patient_des_tv)
    TextView patientDesTv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.specify_view)
    LinearLayout specifyTv;
    @BindView(R.id.specify_et)
    EditText specifyEt;
    @BindView(R.id.toggle)
    RadioGroup radioGroup1;
    @BindView(R.id.toggle_two)
    RadioGroup radioGroup2;
    @BindView(R.id.toggle_three)
    RadioGroup radioGroup3;
    String group1 = "yes", group2 = "yes", group3 = "yes";
    private CustomDialog mLoadingView;
    private Session mSession;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setRadioListener();
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);

    }

    private void setRadioListener() {
        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                RadioButton radioSexButton = (RadioButton) findViewById(checkedId);
                group1 = radioSexButton.getText().toString();
                checkCondition();
            }

        });
        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                RadioButton radioSexButton = (RadioButton) findViewById(checkedId);
                group2 = radioSexButton.getText().toString();
                checkCondition();
            }

        });
        radioGroup3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                RadioButton radioSexButton = (RadioButton) findViewById(checkedId);
                group3 = radioSexButton.getText().toString();
                checkCondition();
            }

        });
    }

    private void checkCondition() {
        if (group1.equalsIgnoreCase("yes") || group2.equalsIgnoreCase("yes") || group3.equalsIgnoreCase("yes")) {
            specifyTv.setVisibility(View.VISIBLE);
        } else {
            specifyTv.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                Intent intent = getIntent();
                if (null != intent) {
                    String pet_name = getIntent().getStringExtra("pet_name");
                    if (!TextUtils.isEmpty(pet_name)) {
                        String pet_image = getIntent().getStringExtra("pet_image");
                        String pet_type = getIntent().getStringExtra("pet_type");
                        String pet_size = getIntent().getStringExtra("pet_size");
                        String pet_age = getIntent().getStringExtra("pet_age");
                        String pet_health = getIntent().getStringExtra("pet_health");
                        String recommendId = getIntent().getStringExtra("recommendId");
                        String breed = getIntent().getStringExtra("breed");
                        String prescription_from = intent.getStringExtra("prescription_from");
                        String address = intent.getStringExtra("address");
                        String hospital = intent.getStringExtra("hospital");
                        String specify = specifyEt.getText().toString();
                        ArrayList<String> imagelist = (ArrayList<String>) intent.getSerializableExtra("imagelist");
                        String pharmacyName = "";
                        if (prescription_from.equalsIgnoreCase("pharmacy")) {
                            pharmacyName = hospital + ", " + address;
                        }
                        String hospitalName = "";
                        if (prescription_from.equalsIgnoreCase("doctor")) {
                            hospitalName = hospital + ", " + address;
                        }
                        startActivity(new Intent(this, ActivityHaveAnyOtherPet.class)
                                .putExtra("pet_name", pet_name)
                                .putExtra("pet_type", pet_type)
                                .putExtra("pet_image", pet_image)
                                .putExtra("pet_size", pet_size)
                                .putExtra("pet_age", pet_age)
                                .putExtra("pet_health", pet_health)
                                .putExtra("recommendId", recommendId)
                                .putExtra("breed", breed)
                                .putExtra("prescription_from", prescription_from)
                                .putExtra("imagelist",imagelist)
                                .putExtra("existing_status","yes")
                                .putExtra("transfer_pharmacy_name",pharmacyName)
                                .putExtra("primary_hospital_name",hospitalName)
                                .putExtra("have_allergy",group1)
                                .putExtra("other_medication",group2)
                                .putExtra("medical_condition",group3)
                                .putExtra("specify",specify)

                        );
                    }
                    else {
                        addTocart();
                    }
                } else {
                    Toast.makeText(this, "no data found", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        patientDesTv.setText("We'll use this information to help you " +
                "double-check for drug interactions");
        continueTv.setText(R.string.continue_);
        tbTv.setText(R.string.checkout_small);

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        continueTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_checkout_additional_profile;
    }

    private void addTocart() {
        Intent intent = getIntent();
        if (null != intent) {
            String prescription_from = intent.getStringExtra("prescription_from");
            String address = intent.getStringExtra("address");
            String hospital = intent.getStringExtra("hospital");
            String prodId = getIntent().getStringExtra("prodId");
            String quantity = getIntent().getStringExtra("quantity");
            String strength = getIntent().getStringExtra("strength");
            String price = getIntent().getStringExtra("price");
            String specify = specifyEt.getText().toString();
            ArrayList<String> imagelist = (ArrayList<String>) intent.getSerializableExtra("imagelist");
            String pharmacyName = "";
            if (prescription_from.equalsIgnoreCase("pharmacy")) {
                pharmacyName = hospital + ", " + address;
            }
            String hospitalName = "";
            if (prescription_from.equalsIgnoreCase("doctor")) {
                hospitalName = hospital + ", " + address;
            }
            mLoadingView.show();

            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart("userid", mSession.getuser_id());
            builder.addFormDataPart("existing_status", "yes");
            builder.addFormDataPart("prescription_from", prescription_from);
            builder.addFormDataPart("transfer_pharmacy_name", pharmacyName);
            builder.addFormDataPart("primary_hospital_name", hospitalName);
            builder.addFormDataPart("have_allergy", group1);
            builder.addFormDataPart("other_medication", group2);
            builder.addFormDataPart("medical_condition", group3);
            builder.addFormDataPart("specify", specify);
            builder.addFormDataPart("product_id", prodId);
            builder.addFormDataPart("quantity", quantity);
            builder.addFormDataPart("strength", strength);
            builder.addFormDataPart("price", price);
            if (imagelist != null && imagelist.size() > 0) {
                for (int i = 0; i < imagelist.size(); i++) {
                    if (imagelist.get(i) != null) {
                        String model = imagelist.get(i);
                        File file = new File(model);
                        builder.addFormDataPart("click_image[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));

                    }
                }
            } else {
                builder.addFormDataPart("click_image[]", "");
            }

            MultipartBody requestBody = builder.build();
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ResponseBody> call = apiInterface.addToCart(requestBody);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    mLoadingView.hideDialog();
                    if (response.body() != null) {
                        String responseData = "";
                        try {
                            responseData = response.body().string();
                            if (!TextUtils.isEmpty(responseData)) {

                                JSONObject jsonObject = new JSONObject(responseData);
                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("detail");
                                if (!TextUtils.isEmpty(status)) {
                                    if (status.equalsIgnoreCase("success")) {
                                       /* startActivity(new Intent(ActivityCheckoutAdditionalProfile.this, ActivityAddress.class)
                                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                                .putExtra("existing_status", "no")
                                        );*/
                                        startActivity(new Intent(ActivityCheckoutAdditionalProfile.this, ActivityCart.class));

                                    } else {
                                        Toast.makeText(ActivityCheckoutAdditionalProfile.this, message, Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                    Toast.makeText(ActivityCheckoutAdditionalProfile.this, message, Toast.LENGTH_SHORT).show();

                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    mLoadingView.hideDialog();
                    Toast.makeText(ActivityCheckoutAdditionalProfile.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                    Log.e("cate", t.toString());
                }

            });
        }

    }

}
