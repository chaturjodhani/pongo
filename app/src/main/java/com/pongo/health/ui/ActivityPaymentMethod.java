package com.pongo.health.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.CardListAdapter;
import com.pongo.health.adapter.PaymentCardAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.CardListResponseModel;
import com.pongo.health.model.CheckoutResponseModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityPaymentMethod extends BaseActivity {
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.tb_tv)
    TextView tbTv;

    @BindView(R.id.add_new_card_ll)
    LinearLayout addNewCardLl;
    @BindView(R.id.apple_pay_ll)
    LinearLayout applePayLl;
    @BindView(R.id.card_checked_iv)
    ImageView cardCheckedIv;
    @BindView(R.id.card_list)
    RecyclerView cardRv;
    private CustomDialog mLoadingView;
    private Session mSession;
    private List<CardListResponseModel.CardlistBean> cardList = new ArrayList<>();
    private boolean checked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (InternetConnection.checkConnection(this)) {
            getCardList();
        } else {
            Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                clearActivityStack();
                break;
            case R.id.apple_pay_ll:
                setChecked();
                break;
            case R.id.add_new_card_ll:
                startActivity(new Intent(this, ActivityAddCard.class));
                break;
        }
    }

    @Override
    protected void setText() {
        tbTv.setText("Payment Method");

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        applePayLl.setOnClickListener(this);
        addNewCardLl.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_payment_method;
    }

    private void setChecked() {
        if (checked) {
            cardCheckedIv.setVisibility(View.VISIBLE);
            checked = false;
        } else {
            cardCheckedIv.setVisibility(View.GONE);
            checked = true;
        }
    }

    private void getCardList() {

        mLoadingView.show();
//            String status = oldIntent.getStringExtra("existing_status");
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", mSession.getuser_id());
        MultipartBody requestBody = builder.build();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CardListResponseModel> call = apiInterface.cardList(requestBody);

        call.enqueue(new Callback<CardListResponseModel>() {
            @Override
            public void onResponse(Call<CardListResponseModel> call, Response<CardListResponseModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    try {
                        String status = response.body().getStatus();
//                            String message = jsonObject.getString("message");
                        if (!TextUtils.isEmpty(status)) {
                            if (status.equalsIgnoreCase("success")) {
                                cardList = response.body().getCardlist();
                                setCardist();
                            } else {
                                Toast.makeText(ActivityPaymentMethod.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(ActivityPaymentMethod.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<CardListResponseModel> call, Throwable t) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityPaymentMethod.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", t.toString());
            }

        });
    }

    private void setCardist() {
        cardRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
      /*  for (int i = 0; i < 3; i++) {
            orderList.add("Lorem Ipsum");

        }*/
        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {

            }
        };
        cardRv.setAdapter(new PaymentCardAdapter(this, cardList, itemClickListener));
    }

}
