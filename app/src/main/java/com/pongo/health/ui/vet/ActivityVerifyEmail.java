package com.pongo.health.ui.vet;

import androidx.appcompat.app.AlertDialog;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityVerifyEmail extends BaseActivity {
    @BindView(R.id.v_tb_iv)ImageView vTbIv;
    @BindView(R.id.vet_heading_one_tv) TextView vetHeadingOneTv;
    @BindView(R.id.vet_heading_second_tv) TextView vetHeadingSecondTv;
    @BindView(R.id.blue_btn_tv) TextView continueTv;
    @BindView(R.id.resend_email_tv) TextView resendEmailTv;
    @BindView(R.id.verify_email_text) TextView verifyEmailText;
    @BindView(R.id.button_ll) LinearLayout buttonLl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.v_tb_iv:
               clearActivityStack();
                break;
            case R.id.resend_email_tv:
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setTitle("Email Sent!");
                dialog.setMessage("A verification email has been sent! Please check Your inbox");
                dialog.setPositiveButton("Ok", (dialogInterface, i) -> buttonLl.setVisibility(View.VISIBLE));
                dialog.show();
                break;
            case R.id.blue_btn_tv:
                startActivity(new Intent(this,ActivityEmailVerified.class));
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        vetHeadingOneTv.setText("Please verify Your Email");
        vetHeadingSecondTv.setText("We have sent a verification link " +
                "to the email bob@gmail.com");
        verifyEmailText.setText("Simply click the link in your email " +
                "and your account will automatically be verified");
        continueTv.setText(R.string.continue_);

    }

    @Override
    protected void setOnClick() {
        continueTv.setOnClickListener(this);
        vTbIv.setOnClickListener(this);
        resendEmailTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_verify_email;
    }
}
