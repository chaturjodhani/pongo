package com.pongo.health.ui;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.UserLoginModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.google.gson.Gson;
import com.pongo.health.utils.UsPhoneNumberFormatter;

import java.lang.ref.WeakReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhoneLoginActivity extends BaseActivity {
    @BindView(R.id.v_tb_iv)
    ImageView phoneBackIv;
    @BindView(R.id.blue_btn_tv)
    TextView phoneSubmitTv;
    @BindView(R.id.phone_number_et)
    AppCompatEditText mPhoneEdit;
    private String mLogintype = "";
    private String mPhone = "";
    private CustomDialog mLoadingView;
    private static final int REQUEST_ACCESS_FINE_LOCATIONMobile = 110;
    boolean hasPermissionLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        removeTaskBar();
        Intent intent = getIntent();
        if (null != intent) {
            mLogintype = intent.getStringExtra("login_type");
        }
        mLoadingView = new CustomDialog(this);
        UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(mPhoneEdit, "+1 (###) ###-####");

        mPhoneEdit.addTextChangedListener(addLineNumberFormatter);
        hasPermissionLocation = (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_tb_iv:
                clearActivityStack();
                break;
            case R.id.blue_btn_tv:
                if (InternetConnection.checkConnection(this)) {
                    mPhone = mPhoneEdit.getText().toString().trim();

                    if (mPhone.isEmpty()) {

                        Toast.makeText(this, "Please enter your mobile number", Toast.LENGTH_SHORT).show();

                    } else {
                        if (!hasPermissionLocation) {
                            ActivityCompat.requestPermissions(this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_ACCESS_FINE_LOCATIONMobile);
                        } else {
                            getOtp();
                        }
                    }
                } else {
                    Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
//                startActivity(new Intent(this,ActivityOtp.class));
                break;
        }
    }

    @Override
    protected void setText() {
        phoneSubmitTv.setText(R.string.continue_);
    }

    @Override
    protected void setOnClick() {
        phoneBackIv.setOnClickListener(this);
        phoneSubmitTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_phone_login;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case REQUEST_ACCESS_FINE_LOCATIONMobile: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getOtp();
                } else {
                    Toast.makeText(PhoneLoginActivity.this, "The app was not allowed to get your location. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();

                }
                break;
            }

        }
    }

    private void getOtp() {
        this.mLoadingView.show();
        String s = "";
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(mPhone); //editText.getText().toString()
        while (m.find()) {
            s = s + m.group(0);
        }
        s = s.substring(1);
        String finalS = s;
        Session session = new Session(this);
        String token = session.getToken();

        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).userLogin("", "", s, token, "android").enqueue(new Callback<UserLoginModel>() {
            public void onResponse(Call<UserLoginModel> call, Response<UserLoginModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((UserLoginModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        Gson gson = new Gson();
                        String myJson = gson.toJson(response.body());
                        String type = response.body().getUserdetail().getVet_type();
//                        Toast.makeText(PhoneLoginActivity.this, "" + response.body().getOtp(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(PhoneLoginActivity.this, OtpLoginActivity.class);
                        intent.putExtra("otp", myJson);
                        intent.putExtra("login_type", type);
                        intent.putExtra("phone", finalS);
                        intent.putExtra("format", mPhone);
                        intent.putExtra("email", "");
                        intent.putExtra("password", "");
                        startActivity(intent);
                    }
                    else
                    {
                        Toast.makeText(PhoneLoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

            }

            public void onFailure(Call<UserLoginModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(PhoneLoginActivity.this, "Failed to send OTP", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

}

