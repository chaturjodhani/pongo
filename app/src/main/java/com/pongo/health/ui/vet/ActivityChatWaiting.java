package com.pongo.health.ui.vet;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.WaitingListAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.ChatListModel;
import com.pongo.health.model.TechChatResponseModel;
import com.pongo.health.ui.ChatViewActivity;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityChatWaiting extends BaseActivity {

    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.blue_btn_tv)
    TextView submitTv;
    @BindView(R.id.waiting_txt)
    TextView waitingTv;
    @BindView(R.id.in_progress_txt)
    TextView inProgressTv;
    @BindView(R.id.chat_cmplt_txt)
    TextView completeTv;
    @BindView(R.id.waiting_selected_rv)
    RecyclerView waitingSelectedRv;
    @BindView(R.id.progress_chat_rv)
    RecyclerView progressChatRv;
    @BindView(R.id.chat_completed_rv)
    RecyclerView chatCompletedRv;

    private CustomDialog mLoadingView;
    private Session mSession;
    private List<ChatListModel> mWaitingSelectedList = new ArrayList<>();
    private List<ChatListModel> mProgressChatList = new ArrayList<>();
    private List<ChatListModel> mChatCompletedList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);


    }
    @Override
    protected void onResume() {
        super.onResume();
        if (InternetConnection.checkConnection(ActivityChatWaiting.this)) {
            getChatData();
        } else {
            Toast.makeText(ActivityChatWaiting.this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_tb_iv:
                finish();
                break;
            case R.id.blue_btn_tv:
                startActivity(new Intent(this, ActivityEmailVerified.class));
                break;
        }
    }

    @Override
    protected void setText() {
        submitTv.setText(R.string.submit);
    }

    @Override
    protected void setOnClick() {
        vTbIv.setOnClickListener(this);
        submitTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_chat_waiting;
    }

    private void setWaitingSelectedList() {
        waitingSelectedRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 2; i++) {
            waitingSelectedList.add(new CartModel(R.drawable.senior, "Tommy - 4yrs", "Robies",
                    "Dog | pug"));

        }*/
        ItemClickListener itemClickListener = (view, position) -> {
            ChatListModel detailBean = mWaitingSelectedList.get(position);
            String chatId = detailBean.getUser_id();
            String chat = detailBean.getChatid();
            String petId = detailBean.getId();
            String firstName = detailBean.getPet_name();
            String image = detailBean.getPetimage();
            List<String> token = detailBean.getToken();
            createChat(position, chatId, firstName, image, token, "waiting", petId, chat);
        };
        waitingSelectedRv.setAdapter(new WaitingListAdapter(this, mWaitingSelectedList, itemClickListener, "waiting"));
    }

    private void setProgressChatList() {
        progressChatRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 2; i++) {
            progressChatList.add(new CartModel(R.drawable.senior, "Tommy - yrs", "Robies",
                    "Dog | pug"));

        }*/
        ItemClickListener itemClickListener = (view, position) -> {
            ChatListModel detailBean = mProgressChatList.get(position);
            String chatId = detailBean.getUser_id();
            String firstName = detailBean.getPet_name();
            String image = detailBean.getPetimage();
            List<String> token = detailBean.getToken();
            String chat = detailBean.getChatid();
            String petId = detailBean.getId();
            createChat(position, chatId, firstName, image, token, "progress", petId, chat);
        };
        progressChatRv.setAdapter(new WaitingListAdapter(this, mProgressChatList, itemClickListener, "progress"));
    }

    private void setCompletedChatList() {
        chatCompletedRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 1; i++) {
            chatCompletedList.add(new CartModel(R.drawable.senior, "Tommy - 5yrs", "Robies",
                    "Dog | pug"));

        }*/
        ItemClickListener itemClickListener = (view, position) -> {
            ChatListModel detailBean = mChatCompletedList.get(position);
            String chatId = detailBean.getUser_id();
            String firstName = detailBean.getPet_name();
            String image = detailBean.getPetimage();
            List<String> token = detailBean.getToken();
            String chat = detailBean.getChatid();
            String petId = detailBean.getId();
            createChat(position, chatId, firstName, image, token, "complete", petId, chat);
        };
        chatCompletedRv.setAdapter(new WaitingListAdapter(this, mChatCompletedList, itemClickListener, "complete"));
    }


    private void getChatData() {
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).techChatList(id).enqueue(new Callback<TechChatResponseModel>() {
            public void onResponse(Call<TechChatResponseModel> call, Response<TechChatResponseModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((TechChatResponseModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        List<TechChatResponseModel.WatingBean> waitingSelectedList = response.body().getWating();
                        List<TechChatResponseModel.ProcessingBean> progressChatList = response.body().getProcessing();
                        List<TechChatResponseModel.CompleteBean> chatCompletedList = response.body().getComplete();
                        if (null != waitingSelectedList && waitingSelectedList.size() > 0) {
                            waitingTv.setVisibility(View.VISIBLE);
                            waitingSelectedRv.setVisibility(View.VISIBLE);
                            createWaitList(waitingSelectedList);

                        } else {
                            waitingTv.setVisibility(View.GONE);
                            waitingSelectedRv.setVisibility(View.GONE);
                        }
                        if (null != progressChatList && progressChatList.size() > 0) {
                            inProgressTv.setVisibility(View.VISIBLE);
                            progressChatRv.setVisibility(View.VISIBLE);
                            createProcessList(progressChatList);

                        } else {
                            inProgressTv.setVisibility(View.GONE);
                            progressChatRv.setVisibility(View.GONE);

                        }
                        if (null != chatCompletedList && chatCompletedList.size() > 0) {
                            completeTv.setVisibility(View.VISIBLE);
                            chatCompletedRv.setVisibility(View.VISIBLE);
                            createCompleteList(chatCompletedList);


                        } else {
                            completeTv.setVisibility(View.GONE);
                            chatCompletedRv.setVisibility(View.GONE);
                        }

                    }
                }

            }

            public void onFailure(Call<TechChatResponseModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityChatWaiting.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void createCompleteList(List<TechChatResponseModel.CompleteBean> chatCompletedList) {
        mChatCompletedList.clear();
        for (int i = 0; i < chatCompletedList.size(); i++) {
            ChatListModel chatmodel = new ChatListModel();
            chatmodel.setBreed(chatCompletedList.get(i).getBreed());
            chatmodel.setId(chatCompletedList.get(i).getId());
            chatmodel.setPet_age(chatCompletedList.get(i).getPet_age());
            chatmodel.setPet_name(chatCompletedList.get(i).getPet_name());
            chatmodel.setPet_type(chatCompletedList.get(i).getPet_type());
            chatmodel.setPetimage(chatCompletedList.get(i).getPetimage());
            chatmodel.setStatus(chatCompletedList.get(i).getStatus());
            chatmodel.setUser_id(chatCompletedList.get(i).getUser_id());
            chatmodel.setToken(chatCompletedList.get(i).getToken());
            chatmodel.setChatid(chatCompletedList.get(i).getChatid());
            mChatCompletedList.add(chatmodel);
        }
        setCompletedChatList();
    }

    private void createProcessList(List<TechChatResponseModel.ProcessingBean> progressChatList) {
        mProgressChatList.clear();
        for (int i = 0; i < progressChatList.size(); i++) {
            ChatListModel chatmodel = new ChatListModel();
            chatmodel.setBreed(progressChatList.get(i).getBreed());
            chatmodel.setId(progressChatList.get(i).getId());
            chatmodel.setPet_age(progressChatList.get(i).getPet_age());
            chatmodel.setPet_name(progressChatList.get(i).getPet_name());
            chatmodel.setPet_type(progressChatList.get(i).getPet_type());
            chatmodel.setPetimage(progressChatList.get(i).getPetimage());
            chatmodel.setStatus(progressChatList.get(i).getStatus());
            chatmodel.setUser_id(progressChatList.get(i).getUser_id());
            chatmodel.setToken(progressChatList.get(i).getToken());
            chatmodel.setChatid(progressChatList.get(i).getChatid());

            mProgressChatList.add(chatmodel);
        }
        setProgressChatList();
    }

    private void createWaitList(List<TechChatResponseModel.WatingBean> waitingSelectedList) {
        mWaitingSelectedList.clear();
        for (int i = 0; i < waitingSelectedList.size(); i++) {
            ChatListModel chatmodel = new ChatListModel();
            chatmodel.setBreed(waitingSelectedList.get(i).getBreed());
            chatmodel.setId(waitingSelectedList.get(i).getId());
            chatmodel.setPet_age(waitingSelectedList.get(i).getPet_age());
            chatmodel.setPet_name(waitingSelectedList.get(i).getPet_name());
            chatmodel.setPet_type(waitingSelectedList.get(i).getPet_type());
            chatmodel.setPetimage(waitingSelectedList.get(i).getPetimage());
            chatmodel.setStatus(waitingSelectedList.get(i).getStatus());
            chatmodel.setUser_id(waitingSelectedList.get(i).getUser_id());
            chatmodel.setToken(waitingSelectedList.get(i).getToken());
            chatmodel.setChatid(waitingSelectedList.get(i).getChatid());

            mWaitingSelectedList.add(chatmodel);
        }
        setWaitingSelectedList();
    }


    private void createChat(int position, String chatId, String firstName, String image, List<String> token, String mstatus, String petId, String chat) {
        this.mLoadingView.show();
        String userId = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).createChat(chatId, userId, petId, chat,userId).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    if (message.equalsIgnoreCase("chat start")) {
//                                        PastChatResponseModel.DetailBean detailBean = veterinaryList.get(position);
                                        String chatid = jsonObject.getString("chatid");
                                        Intent intent = new Intent(ActivityChatWaiting.this, ChatViewActivity.class);
                                        intent.putExtra("chat_id", chatId);
                                        intent.putExtra("name", firstName);
                                        intent.putExtra("profileimage", image);
                                        intent.putExtra("status", mstatus);
                                        intent.putExtra("chatid", chatid);
                                        intent.putExtra("petId", petId);
                                        intent.putStringArrayListExtra("token", (ArrayList<String>) token);
                                        startActivity(intent);

                                    } else {
                                        Toast.makeText(ActivityChatWaiting.this, message, Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                    Toast.makeText(ActivityChatWaiting.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivityChatWaiting.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityChatWaiting.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }


}

