package com.pongo.health.ui;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.kinda.alert.KAlertDialog;
import com.pongo.health.BuildConfig;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.CardListAdapter;
import com.pongo.health.adapter.ImagesAdapter;
import com.pongo.health.adapter.PaymentCardAdapter;
import com.pongo.health.adapter.PetListAdapter;
import com.pongo.health.adapter.PetProblemAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.CardListResponseModel;
import com.pongo.health.model.CategoriesModel;
import com.pongo.health.model.CheckoutResponseModel;
import com.pongo.health.model.NearestDoctorModel;
import com.pongo.health.model.PetModelResponse;
import com.pongo.health.ui.vet.ActivityIdentification;
import com.pongo.health.ui.vet.ActivityTechnician;
import com.pongo.health.ui.vet.ActivityVetDashboard;
import com.pongo.health.ui.vet.ActivityYourProfileSet;
import com.pongo.health.ui.vet.VideocallActivity;
import com.pongo.health.utils.Constants;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.FilePath;
import com.pongo.health.utils.GPSTracker;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityPetProblems extends BaseActivity {

    @BindView(R.id.problem_pet_type_rv)
    RecyclerView problemPetTypeRv;
    @BindView(R.id.image_rv)
    RecyclerView imagesRv;
    @BindView(R.id.blue_btn_tv)
    TextView callVetcontinueTv;
    @BindView(R.id.attachment_layout)
    LinearLayout attachmentLayout;
    private List<CategoriesModel.ProblemlistBean> petProblemList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private CustomDialog mLoadingView;
    private Session mSession;
    private PetProblemAdapter mPetProblemAdapter;
    private GPSTracker gps;
    private List<PetModelResponse.DataBean> mPetList = new ArrayList<>();
    private int mSelectedPet = -1;
    private ArrayList<String> imageList = new ArrayList<>();
    private ImagesAdapter mImagesAdapter;
    String mCurrentPhotoPath;
    //Pdf request code
    private int PICK_PDF_REQUEST = 2;
    private List<CardListResponseModel.CardlistBean> cardList = new ArrayList<>();
    BottomSheetDialog mBottomSheetDialog;
    private String cardid = "";
    private String customer = "";
    RecyclerView cardRv;
    String mIssue;
    String mPetId;
    String mPetName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(ActivityPetProblems.this);
        mSession = new Session(ActivityPetProblems.this);
        gps = new GPSTracker(ActivityPetProblems.this);

        if (InternetConnection.checkConnection(ActivityPetProblems.this)) {
            getroblemList();
            getpetList();

        } else {
            Toast.makeText(ActivityPetProblems.this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
        setAdapter();
        createBottomList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCardList();
    }

    private void createBottomList() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.card_list_bottomsheet, null);
        cardRv = sheetView.findViewById(R.id.card_rv);
        TextView done = sheetView.findViewById(R.id.done);
        TextView addCard = sheetView.findViewById(R.id.blue_btn_tv);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        addCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityPetProblems.this, ActivityAddCard.class));
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(sheetView);
    }

    private void setCardist() {
        cardRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
      /*  for (int i = 0; i < 3; i++) {
            orderList.add("Lorem Ipsum");

        }*/
        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                cardid = cardList.get(position).getCard_id();
                customer = cardList.get(position).getCustomer();
                confimrDialog(cardList.get(position).getCardnumber());

            }
        };
        cardRv.setAdapter(new PaymentCardAdapter(this, cardList, itemClickListener));
    }

    private void setAdapter() {
        imagesRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                imageList.remove(position);
                mImagesAdapter.notifyDataSetChanged();
            }
        };
//        itemClickListener = (view, position) -> startActivity(new Intent(activity, ActivityYourPetSingleType.class));
        mImagesAdapter = new ImagesAdapter(this, imageList, itemClickListener);
        imagesRv.setAdapter(mImagesAdapter);
    }

    private void confimrDialog(String cardnumber) {
        new KAlertDialog(this, KAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("You want to make payment using " + cardnumber)
                .setCancelText("No")
                .setConfirmText("Yes")
                .showCancelButton(true)
                .setCancelClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                .setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog kAlertDialog) {
                        kAlertDialog.dismissWithAnimation();
                        mBottomSheetDialog.dismiss();
                        getNearestDoctor();
                    }
                })
                .show();
    }

    private void getroblemList() {
        this.mLoadingView.show();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).problemList().enqueue(new Callback<CategoriesModel>() {
            public void onResponse(Call<CategoriesModel> call, Response<CategoriesModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((CategoriesModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        petProblemList = response.body().getProblemlist();
                        String price = response.body().getPayment();
                        callVetcontinueTv.setText("Start Video ($" + price + ")");
                        if (null != petProblemList && petProblemList.size() > 0) {
                            setPetProblems();
                        }
                    }
                }

            }

            public void onFailure(Call<CategoriesModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityPetProblems.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void getpetList() {
        String userId = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getPets(userId).enqueue(new Callback<PetModelResponse>() {
            public void onResponse(Call<PetModelResponse> call, Response<PetModelResponse> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    try {
                        String status = response.body().getStatus();
                        if (!TextUtils.isEmpty(status)) {
                            if (status.equalsIgnoreCase("success")) {
                                mPetList = response.body().getData();

                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<PetModelResponse> call, Throwable th) {
                Log.e("cate", th.toString());

            }
        });
    }

    private void getCardList() {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", mSession.getuser_id());
        MultipartBody requestBody = builder.build();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CardListResponseModel> call = apiInterface.cardList(requestBody);

        call.enqueue(new Callback<CardListResponseModel>() {
            @Override
            public void onResponse(Call<CardListResponseModel> call, Response<CardListResponseModel> response) {
                if (response.body() != null) {
                    try {
                        String status = response.body().getStatus();
//                            String message = jsonObject.getString("message");
                        if (!TextUtils.isEmpty(status)) {
                            if (status.equalsIgnoreCase("success")) {
                                cardList = response.body().getCardlist();
                                setCardist();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<CardListResponseModel> call, Throwable t) {
                Log.e("cate", t.toString());
            }

        });
    }

    private void getNearestDoctor() {
        mLoadingView.show();
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();
        String userId = mSession.getuser_id();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("problem", mIssue);
        builder.addFormDataPart("pet_id", mPetId);
        builder.addFormDataPart("user_id", userId);
        builder.addFormDataPart("lat", "" + latitude);
        builder.addFormDataPart("long", "" + longitude);
        builder.addFormDataPart("customer", customer);
        builder.addFormDataPart("card_id", cardid);
//        File file = new File(res);
//        builder.addFormDataPart("img_identification", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
        if (imageList != null && imageList.size() > 0) {
            for (int i = 0; i < imageList.size(); i++) {
                if (imageList.get(i) != null) {
                    String model = imageList.get(i);
                    File file = new File(model);
                    builder.addFormDataPart("attachments[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
                }
            }
        } else {
            builder.addFormDataPart("attachments[]", "");
        }
//        builder.addFormDataPart("flag", filePath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), filePath));


        MultipartBody requestBody = builder.build();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<NearestDoctorModel> call = apiInterface.searchNearestDoctor(requestBody);

        call.enqueue(new Callback<NearestDoctorModel>() {
            @Override
            public void onResponse(Call<NearestDoctorModel> call, Response<NearestDoctorModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((NearestDoctorModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        List<NearestDoctorModel.DoctorsBean> doctorsBeans = response.body().getDoctors();
                        if (null != doctorsBeans && doctorsBeans.size() > 0) {
                            String id = doctorsBeans.get(0).getId();
                            Intent intent = new Intent(ActivityPetProblems.this, VideocallActivity.class);
                            intent.putExtra("user_id", mPetId);
                            intent.putExtra("pet_name", mPetName);
                            startActivity(intent);
//                            Toast.makeText(ActivityPetProblems.this, "Doctor found", Toast.LENGTH_SHORT).show();
                        } else {
                            if (!TextUtils.isEmpty(response.body().getType()) && response.body().getType().equalsIgnoreCase("0")) {
                                appointmentconfimrDialog();

                            } else {
                                Toast.makeText(ActivityPetProblems.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (!TextUtils.isEmpty(response.body().getType()) && response.body().getType().equalsIgnoreCase("0")) {
                            appointmentconfimrDialog();

                        } else {
                            Toast.makeText(ActivityPetProblems.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<NearestDoctorModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityPetProblems.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }

        });

    }

    private void appointmentconfimrDialog() {
        new KAlertDialog(this, KAlertDialog.WARNING_TYPE)
                .setTitleText("Sorry")
                .setContentText("No Vets are available to speak immediately")
                .setCancelText("No")
                .setConfirmText("Book appointment")
                .showCancelButton(true)
                .setCancelClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        finish();
                    }
                })
                .setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog kAlertDialog) {
                        kAlertDialog.dismissWithAnimation();
                        startActivity(new Intent(ActivityPetProblems.this, ActivityAddPrimaryHospital.class)
                                .putExtra("screen", "video"));
                        finish();
                    }
                })
                .show();
    }

    /*  private void getNearestDoctor(String issue) {
          this.mLoadingView.show();
          double latitude = gps.getLatitude();
          double longitude = gps.getLongitude();
          ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).searchNearestDoctor("" + latitude, "" + longitude, issue).enqueue(new Callback<NearestDoctorModel>() {
              public void onResponse(Call<NearestDoctorModel> call, Response<NearestDoctorModel> response) {
                  mLoadingView.hideDialog();
                  if (response.body() != null) {
                      String status = ((NearestDoctorModel) response.body()).getStatus();
                      if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                          List<NearestDoctorModel.DoctorsBean> doctorsBeans = response.body().getDoctors();
                          if (null != doctorsBeans && doctorsBeans.size() > 0) {
                              String id = doctorsBeans.get(0).getId();
                              Intent intent = new Intent(ActivityPetProblems.this, VideocallActivity.class);
                              intent.putExtra("user_id", id);
                              startActivity(intent);
  //                            Toast.makeText(ActivityPetProblems.this, "Doctor found", Toast.LENGTH_SHORT).show();
                          } else {
                              Toast.makeText(ActivityPetProblems.this, "Not any vet available", Toast.LENGTH_SHORT).show();
                          }
                      } else {
                          Toast.makeText(ActivityPetProblems.this, "Not any vet available", Toast.LENGTH_SHORT).show();
                      }
                  }

              }

              public void onFailure(Call<NearestDoctorModel> call, Throwable th) {
                  mLoadingView.hideDialog();
                  Toast.makeText(ActivityPetProblems.this, "Not any vet available", Toast.LENGTH_SHORT).show();
                  Log.e("cate", th.toString());
              }
          });
      }
  */
    @Override
    protected void setText() {
        callVetcontinueTv.setText(R.string.continue_);

    }

    @Override
    protected void setOnClick() {
        callVetcontinueTv.setOnClickListener(this);
        attachmentLayout.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.blue_btn_tv: {
                if (!checkPermission()) {
                    verifyData();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
                            102);
                }

                break;
            }
            case R.id.attachment_layout:
                if (!checkPermission()) {
                    selectImage();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
                            103);
                }

                break;
        }
    }

    private boolean checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
        ) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 102) {
            if (grantResults.length > 0) {

                boolean recordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (recordAccepted) {
                    verifyData();
                }
            }
        } else if (requestCode == 103) {
            if (grantResults.length > 0) {

                boolean recordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (recordAccepted) {
                    selectImage();
                }
            }
        }
    }

    private void verifyData() {
        if (null != petProblemList && petProblemList.size() > 0) {
            String issue = "";
            for (int i = 0; i < petProblemList.size(); i++) {
                CategoriesModel.ProblemlistBean problemlistBean = petProblemList.get(i);
                if (problemlistBean.isCheck()) {
                    issue = problemlistBean.getName() + "," + issue;
                }
            }
            if (!TextUtils.isEmpty(issue)) {
                if (mPetList.size() > 0) {
                    openPopup(issue);
                } else {
                    Toast.makeText(ActivityPetProblems.this, "Add pet first", Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(this, "Please select issue first", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_pet_problems;
    }

    private void setPetProblems() {
        problemPetTypeRv.setLayoutManager(new GridLayoutManager(this,
                2));

       /* petProblemList.add(new CategoriesModel(R.drawable.general_check_img, "General Checkup"));
        petProblemList.add(new CategoriesModel(R.drawable.skin_rash_img, "Skin rash/allergy"));
        petProblemList.add(new CategoriesModel(R.drawable.ear_infection_img, "Ear Infection"));
        petProblemList.add(new CategoriesModel(R.drawable.tenderness_img, "Limps or tenderness"));
        petProblemList.add(new CategoriesModel(R.drawable.dog_eat_img, "Eating problems"));
        petProblemList.add(new CategoriesModel(R.drawable.diarreha, "Diarrhea"));
        petProblemList.add(new CategoriesModel(R.drawable.throwing_up_img, "Throwing Up"));
        petProblemList.add(new CategoriesModel(R.drawable.behavioural_img, "Behavioral"));
        petProblemList.add(new CategoriesModel(R.drawable.other_img, "Other"));
*/
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                CategoriesModel.ProblemlistBean model = petProblemList.get(position);
                if (model.isCheck()) {
                    model.setCheck(false);
                } else {
                    model.setCheck(true);
                }
                mPetProblemAdapter.notifyDataSetChanged();
            }
        };
        mPetProblemAdapter = new PetProblemAdapter(this, petProblemList, itemClickListener);
        problemPetTypeRv.setAdapter(mPetProblemAdapter);
    }

    private void openPopup(String issue) {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(ActivityPetProblems.this);
        View promptsView = li.inflate(R.layout.pet_list_popup, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ActivityPetProblems.this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        alertDialogBuilder.setCancelable(true);

        RecyclerView petList = promptsView
                .findViewById(R.id.pet_list);
        TextView continueTv = promptsView
                .findViewById(R.id.blue_btn_tv);
        ItemClickListener itemClickListener;
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        itemClickListener = new ItemClickListener() {

            @Override
            public void onClicked(View view, int postion) {
//                alertDialog.dismiss();
                mSelectedPet = postion;
            }
        };
        petList.setLayoutManager(new LinearLayoutManager(ActivityPetProblems.this, RecyclerView.VERTICAL, false));

        petList.setAdapter(new PetListAdapter(ActivityPetProblems.this, mPetList, itemClickListener));


        continueTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelectedPet >= 0) {
                    alertDialog.dismiss();
                    mIssue = issue;
                    mPetId = mPetList.get(mSelectedPet).getId();
                    mPetName = mPetList.get(mSelectedPet).getPet_name();
                    mBottomSheetDialog.show();
                } else {
                    Toast.makeText(ActivityPetProblems.this, "Please select pet first", Toast.LENGTH_SHORT).show();
                }
            }
        });
        // show it
        alertDialog.show();
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Select PDF", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (cameraIntent.resolveActivity(getPackageManager()) != null) {
// Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
// Error occurred while creating the File
                        }
// Continue only if the File was successfully created
                        if (photoFile != null) {
                            Uri outputFileUri = FileProvider.getUriForFile(ActivityPetProblems.this, BuildConfig.APPLICATION_ID, photoFile);
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                            startActivityForResult(cameraIntent, 0);
                        }
                    }


                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);

                } else if (options[item].equals("Select PDF")) {
                    Intent intent = new Intent();
                    intent.setType("application/pdf");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PICK_PDF_REQUEST);


                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:


                    if (resultCode == Activity.RESULT_OK) {

                        File file = new File(Objects.requireNonNull(mCurrentPhotoPath));
                        String fileName = Constants.compressImage(mCurrentPhotoPath, ActivityPetProblems.this);
                        if (!TextUtils.isEmpty(fileName)) {
                            imageList.add(fileName);
                            mImagesAdapter.notifyDataSetChanged();
                        }
                    }

                    break;

                case 1:

                    if (resultCode == RESULT_OK && data != null) {

                        Uri selectedImage = data.getData();

                        String fileName = null;
                        if (selectedImage != null) {
                            fileName = Constants.compressImage(selectedImage.toString(), ActivityPetProblems.this);
                            if (!TextUtils.isEmpty(fileName)) {
                                imageList.add(fileName);
                                mImagesAdapter.notifyDataSetChanged();
                            }
                        }
                    }

                    break;
                case 2:
                    if (resultCode == RESULT_OK && data != null && data.getData() != null) {
                        Uri filePath = data.getData();
                        //getting the actual path of the image
                        String path = FilePath.getPath(this, filePath);
                        imageList.add(path);
                        mImagesAdapter.notifyDataSetChanged();

                    }
                    break;
            }
        }
    }


    private File createImageFile() throws IOException {
// Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName, // prefix
                ".jpg", // suffix
                storageDir // directory
        );

// Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();

        return image;
    }


}
