package com.pongo.health.ui.pet_parent;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.HomeModel;
import com.pongo.health.utils.UsPhoneNumberFormatter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityVerifyInformation extends BaseActivity {


    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.phone_et)
    EditText phoneEt;
    @BindView(R.id.zip_et)
    EditText zipEt;
    @BindView(R.id.email_et)
    EditText emailEt;
    private HomeModel.PetlistBean mPetBean;
    private String mHospitalname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);
        Gson gson = new Gson();
        mPetBean = gson.fromJson(getIntent().getStringExtra("model"), HomeModel.PetlistBean.class);
        mHospitalname = getIntent().getStringExtra("hospital_name");
        UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(phoneEt, "+1 (###) ###-####");
        phoneEt.addTextChangedListener(addLineNumberFormatter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
               verify_data();
                break;
            case R.id.tb_iv:
                clearActivityStack();
                break;
        }
    }

    private void verify_data() {
        String phone = phoneEt.getText().toString();
        String zip = zipEt.getText().toString();
        String email = emailEt.getText().toString();
        if (TextUtils.isEmpty(phone)) {
            Toast.makeText(ActivityVerifyInformation.this, "Please enter phone number", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(zip)) {
            Toast.makeText(ActivityVerifyInformation.this, "Please enter zip code", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(email)) {
            Toast.makeText(ActivityVerifyInformation.this, "Please enter email", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(this, ActivityMedicalConfirmation.class);
            Gson gson = new Gson();
            String myJson = gson.toJson(mPetBean);
            intent.putExtra("model", myJson);
            intent.putExtra("hospital_name", mHospitalname);
            intent.putExtra("phone", phone);
            intent.putExtra("zip", zip);
            intent.putExtra("email", email);
            startActivity(intent);
        }

    }

    @Override
    protected void setText() {
        tbTv.setText(R.string.medical_records);
        continueTv.setText(R.string.continue_);

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        continueTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_verify_information;
    }
}
