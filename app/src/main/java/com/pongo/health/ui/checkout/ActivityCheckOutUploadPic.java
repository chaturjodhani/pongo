package com.pongo.health.ui.checkout;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pongo.health.BuildConfig;
import com.pongo.health.R;
import com.pongo.health.adapter.ImagesAdapter;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.ui.addpet.ActivityPetName;
import com.pongo.health.utils.Constants;
import com.pongo.health.utils.ItemClickListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityCheckOutUploadPic extends BaseActivity {

    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.upload_text)
    TextView uploadTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.image_rv)
    RecyclerView imagesRv;
    String mCurrentPhotoPath;
    private ArrayList imageList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private ImagesAdapter mImagesAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setAdapter();
    }

    private void setAdapter() {
        imagesRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                imageList.remove(position);
                mImagesAdapter.notifyDataSetChanged();
            }
        };
//        itemClickListener = (view, position) -> startActivity(new Intent(activity, ActivityYourPetSingleType.class));
        mImagesAdapter = new ImagesAdapter(this, imageList, itemClickListener);
        imagesRv.setAdapter(mImagesAdapter);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                if (imageList.size() > 0) {
                    String pet_name = getIntent().getStringExtra("pet_name");
                    if (!TextUtils.isEmpty(pet_name)) {
                        String pet_image = getIntent().getStringExtra("pet_image");
                        String pet_type = getIntent().getStringExtra("pet_type");
                        String pet_size = getIntent().getStringExtra("pet_size");
                        String pet_age = getIntent().getStringExtra("pet_age");
                        String pet_health = getIntent().getStringExtra("pet_health");
                        String recommendId = getIntent().getStringExtra("recommendId");
                        String breed = getIntent().getStringExtra("breed");
                        String address = "";
                        String hospital = "";
                        startActivity(new Intent(this, ActivityCheckoutAdditionalProfile.class)
                                .putExtra("pet_name", pet_name)
                                .putExtra("pet_type", pet_type)
                                .putExtra("pet_image", pet_image)
                                .putExtra("pet_size", pet_size)
                                .putExtra("pet_age", pet_age)
                                .putExtra("pet_health", pet_health)
                                .putExtra("recommendId", recommendId)
                                .putExtra("breed", breed)
                                .putExtra("address", address)
                                .putExtra("hospital", hospital)
                                .putExtra("prescription_from", "take picture")
                                .putExtra("imagelist", imageList)
                        );
                    }
                    else {
                        String name = getIntent().getStringExtra("name");
                        String prodId = getIntent().getStringExtra("prodId");
                        String quantity = getIntent().getStringExtra("quantity");
                        String strength = getIntent().getStringExtra("strength");
                        String price = getIntent().getStringExtra("price");

                        startActivity(new Intent(this, ActivityCheckoutAdditionalProfile.class)
                                .putExtra("name", name)
                                .putExtra("address", "")
                                .putExtra("hospital", "")
                                .putExtra("imagelist", imageList)
                                .putExtra("prescription_from", "take picture")
                                .putExtra("prodId", prodId)
                                .putExtra("quantity", quantity)
                                .putExtra("strength", strength)
                                .putExtra("price", price)
                        );
                    }
                } else {
                    Toast.makeText(ActivityCheckOutUploadPic.this, "Please upload image", Toast.LENGTH_SHORT).show();
                }
//                startActivity(new Intent(this, ActivityCheckOutYourPrescription.class));
                break;

            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.upload_text:
                if (!checkPermission()) {
                    selectImage();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            102);
                }
                break;
        }
    }

    private boolean checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
        ) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 102) {
            if (grantResults.length > 0) {

                boolean recordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (recordAccepted) {
                    selectImage();
                }
            }
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (cameraIntent.resolveActivity(getPackageManager()) != null) {
// Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
// Error occurred while creating the File
                        }
// Continue only if the File was successfully created
                        if (photoFile != null) {
                            Uri outputFileUri = FileProvider.getUriForFile(ActivityCheckOutUploadPic.this, BuildConfig.APPLICATION_ID, photoFile);
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                            startActivityForResult(cameraIntent, 0);
                        }
                    }


                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:


                    if (resultCode == Activity.RESULT_OK) {

                        File file = new File(Objects.requireNonNull(mCurrentPhotoPath));
                        String fileName = Constants.compressImage(mCurrentPhotoPath, ActivityCheckOutUploadPic.this);
                        Bitmap bitmap = BitmapFactory.decodeFile(fileName);
                        bitmap = getResizedBitmap(bitmap, 150);
                        String path = mCurrentPhotoPath;
//                        res = path;
//                        petImage.setImageBitmap(bitmap);
                        imageList.add(path);
                        mImagesAdapter.notifyDataSetChanged();

                    }

                    break;

                case 1:

                    if (resultCode == RESULT_OK && data != null) {

                        Uri selectedImage = data.getData();

                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(ActivityCheckOutUploadPic.this.getContentResolver(), selectedImage);
                            bitmap = getPath(selectedImage);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    break;
            }
        }
    }

    private Bitmap getPath(Uri selectedImage) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Log.e("Before", "Creating cursor");
        Cursor cursor = ActivityCheckOutUploadPic.this.managedQuery(selectedImage, projection, null, null, null);
        Log.e("After", "Creating cursor");
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String res = cursor.getString(column_index);
        Log.e("Selected Image Path", res);
        Log.e("After", "Closing cursor");
        imageList.add(res);
        mImagesAdapter.notifyDataSetChanged();
        Bitmap bitmap = BitmapFactory.decodeFile(res);
//        petImage.setImageBitmap(bitmap);
//        Glide.with(this).load(res).apply(RequestOptions.circleCropTransform()).into(petImage);

        return bitmap;
    }

    private File createImageFile() throws IOException {
// Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName, // prefix
                ".jpg", // suffix
                storageDir // directory
        );

// Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();

        return image;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void setText() {
        continueTv.setText(R.string.continue_);
        tbTv.setText(R.string.checkout);


    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        continueTv.setOnClickListener(this);
        uploadTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_checkout_uplaod_pic;
    }

}
