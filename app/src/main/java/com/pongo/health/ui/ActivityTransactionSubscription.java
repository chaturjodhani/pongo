package com.pongo.health.ui;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kinda.alert.KAlertDialog;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.SubscriptionAdapter;
import com.pongo.health.adapter.TransactionAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.CartModel;
import com.pongo.health.model.CartResponseModel;
import com.pongo.health.model.TransactionModel;
import com.pongo.health.ui.vet.ActivityPatientDetail;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;
import com.pongo.health.utils.TransactionListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityTransactionSubscription extends BaseActivity {

    @BindView(R.id.transaction_rv)
    RecyclerView transactionRv;
    @BindView(R.id.subscription_rv)
    RecyclerView subscriptionRv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.blue_btn_tv)
    TextView newSubscriptionTv;
    @BindView(R.id.see_all)
    TextView seeAllText;
    @BindView(R.id.subscription_txt)
    TextView subscriptionText;
    @BindView(R.id.transaction_layout)
    LinearLayout transactionLayout;
    private CustomDialog mLoadingView;
    private Session mSession;
    private SubscriptionAdapter subscriptionAdapter;
    List<TransactionModel.SubscribeproductBean> subscriptionList = new ArrayList<>();
    List<TransactionModel.YourtransactionBean> transactionList = new ArrayList<>();
    ItemClickListener itemClickListener;
    TransactionListener transactionClickListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (InternetConnection.checkConnection(ActivityTransactionSubscription.this)) {
            getTransactionData();
        } else {
            Toast.makeText(ActivityTransactionSubscription.this, "No Internet available", Toast.LENGTH_SHORT).show();
        }

    }

    private void getTransactionData() {
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getTransactionList(id).enqueue(new Callback<TransactionModel>() {
            public void onResponse(Call<TransactionModel> call, Response<TransactionModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((TransactionModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        transactionList = response.body().getYourtransaction();
                        subscriptionList = response.body().getSubscribeproduct();
                        if (null != transactionList && transactionList.size() > 0) {
                            transactionLayout.setVisibility(View.VISIBLE);
                            setTransaction();
                        }
                        if (null != subscriptionList && subscriptionList.size() > 0) {
                            subscriptionText.setVisibility(View.VISIBLE);
                            setSubscriptionRv();
                        }

                    }
                }

            }

            public void onFailure(Call<TransactionModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityTransactionSubscription.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void setTransaction() {
        transactionRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 2; i++) {
            transactionList.add(new CartModel(R.drawable.rx_two_img, "Lorem Ipsum", "15-04-2020",
                    "By Card,4565456545"));

        }*/
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                Gson gson = new Gson();
                String myJson = gson.toJson(transactionList.get(position));
                startActivity(new Intent(ActivityTransactionSubscription.this, OderDetailActivity.class)
                        .putExtra("model", myJson));
            }
        };

        transactionRv.setAdapter(new TransactionAdapter(this, transactionList, itemClickListener));
    }

    private void setSubscriptionRv() {
        subscriptionRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
      /*  for (int i = 0; i < 2; i++) {
            subscriptionList.add(new CartModel(R.drawable.rx_two_img, "Lorem Ipsum", "15-04-2020",
                    "By Card,4565456545"));

        }*/
        transactionClickListener = new TransactionListener() {
            @Override
            public void onClicked(View view, int position) {
                String type = subscriptionList.get(position).getType();
                if (!TextUtils.isEmpty(type) && !type.equalsIgnoreCase("plan")) {
                    Gson gson = new Gson();
                    String myJson = gson.toJson(subscriptionList.get(position));
                    startActivity(new Intent(ActivityTransactionSubscription.this, OderDetailActivity.class)
                            .putExtra("model", myJson));
                }

            }

            @Override
            public void onCancelClicked(View view, int position) {
                confimrDialog(position);
            }
        };
        subscriptionAdapter = new SubscriptionAdapter(this, subscriptionList, transactionClickListener);
        subscriptionRv.setAdapter(subscriptionAdapter);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                startActivity(new Intent(this, ActivitySubscription.class));
                break;
            case R.id.see_all:
                startActivity(new Intent(this, AlltransactionActivity.class));
                break;
            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;

        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("Transactions/Subscription");
        newSubscriptionTv.setText("Create new Subscription");

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        newSubscriptionTv.setOnClickListener(this);
        seeAllText.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_transaction_subscription;
    }

    private void confimrDialog(int position) {
        new KAlertDialog(this, KAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("You want to cancel Subscription!")
                .setCancelText("No")
                .setConfirmText("Yes")
                .showCancelButton(true)
                .setCancelClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                .setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog kAlertDialog) {
                        kAlertDialog.dismissWithAnimation();
                        cancelSubscription(position);
                    }
                })
                .show();
    }

    private void cancelSubscription(int position) {
        this.mLoadingView.show();
        String subscribeid = subscriptionList.get(position).getSubscribe_id();
        String userid = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).cancelSubscription(userid, subscribeid).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(ActivityTransactionSubscription.this, message, Toast.LENGTH_SHORT).show();
                                    mSession.setuserPaid("free");

                                    if (subscriptionList.size() == 1) {
                                        subscriptionText.setVisibility(View.GONE);
                                        subscriptionList.remove(position);
//                                    cartAdapter.notifyItemChanged(position);
                                        subscriptionAdapter.notifyDataSetChanged();
                                    } else {
                                        subscriptionList.remove(position);
//                                    cartAdapter.notifyItemChanged(position);
                                        subscriptionAdapter.notifyDataSetChanged();
                                    }
                                } else {
                                    Toast.makeText(ActivityTransactionSubscription.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivityTransactionSubscription.this, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityTransactionSubscription.this, "Failed to cancel subscription", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

}
