package com.pongo.health.ui;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.adapter.SearchHospitalAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.HospitalSearchModel;
import com.pongo.health.model.OtpModel;
import com.pongo.health.ui.checkout.ActivityCheckOutYourPrescriptionNew;
import com.pongo.health.ui.vet.ActivityVeterinaryLicense;
import com.google.gson.Gson;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.GPSTracker;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityAddPrimaryHospitalNew extends BaseActivity {


    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.vet_heading_one_tv)
    TextView vetHeadingOneTv;
    @BindView(R.id.vet_heading_second_tv)
    TextView vetHeadingSecondTv;
    @BindView(R.id.hospital_rv)
    RecyclerView searcheHospitalRv;
    @BindView(R.id.search_second_et)
    EditText searchEt;
    private List<HospitalSearchModel.ResultsBean> hospitalList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private GPSTracker gps;
    private int mPosition = -1;

    private ArrayList imageList = new ArrayList<>();
    private String vetType = "";
    private OtpModel otpModel;
    private String mLoginType;
    private String mMobile;
    private String str_firstname, str_lastname;
    private String mPassword;
    private String mEmail;
    private String mDesc;
    private String mExpertice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);
        searchEt.setHint("Search for hospital");
        gps = new GPSTracker(this);
        setSearchListener();
        Gson gson = new Gson();
        otpModel = gson.fromJson(getIntent().getStringExtra("otp"), OtpModel.class);
        mLoginType = getIntent().getStringExtra("login_type");
        mMobile = getIntent().getStringExtra("phone");
        mEmail = getIntent().getStringExtra("email");
        mPassword = getIntent().getStringExtra("password");
        str_firstname = getIntent().getStringExtra("first_name");
        str_lastname = getIntent().getStringExtra("last_name");
        vetType = getIntent().getStringExtra("vetType");
        mExpertice = getIntent().getStringExtra("desc");
        mDesc = getIntent().getStringExtra("expertise");
//        res = getIntent().getStringExtra("img_identification");
        imageList = (ArrayList<String>) getIntent().getSerializableExtra("img_identification");
    }


    private void setSearchListener() {
        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                if (gps.canGetLocation()) {
                    getSearch(editable.toString());

                } else {
                    gps.showSettingsAlert();
                }
            }
        });

        searchEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
              /*  if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String search = searchEt.getText().toString();
                    getSearch(search);
                    return true;
                }*/
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//Hide:
                if (imm != null) {
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }
                return false;
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                //startActivity(new Intent(this, ActivitySoundGoods.class));
                verify_data();
//                startActivity(new Intent(this, ActivityVeterinaryLicense.class));
                break;

            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
        }
    }

    private void verify_data() {
        if (mPosition >= 0) {
            String hospital = hospitalList.get(mPosition).getName();

            Intent intent = new Intent(ActivityAddPrimaryHospitalNew.this, ActivityVeterinaryLicense.class);
            Gson gson = new Gson();
            String myJson = gson.toJson(otpModel);
            intent.putExtra("otp", myJson);
            intent.putExtra("login_type", mLoginType);
            intent.putExtra("phone", mMobile);
            intent.putExtra("email", mEmail);
            intent.putExtra("password", mPassword);
            intent.putExtra("first_name", str_firstname);
            intent.putExtra("last_name", str_lastname);
            intent.putExtra("vetType", vetType);
            intent.putExtra("expertise", mExpertice);
            intent.putExtra("desc", mDesc);
            intent.putExtra("img_identification", imageList);
            intent.putExtra("primary_hospital_name", hospital);
            startActivity(intent);
        } else {
            Toast.makeText(ActivityAddPrimaryHospitalNew.this, "Please select primary hospital", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void setText() {
        vetHeadingOneTv.setText("Great, Now let's add your Primary hospital");
        vetHeadingSecondTv.setText("Please add your main hospital of employment. ");
//                + "If you don't have one, simply skip the step");
        continueTv.setText(R.string.continue_);

    }

    @Override
    protected void setOnClick() {
        continueTv.setOnClickListener(this);
        vTbIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_add_primary_hospital_new;
    }

    private void getSearch(String s) {
//        this.mLoadingView.show();
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();

//        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&radius=1500000&type=drugstore|hospital|veterinary_care|pharmacy|petstore&keyword=" + s + "&key=AIzaSyCIhWIx08t5ap5neBIX1B3npieSP9HQSdY";
        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&radius=1500000&keyword=" + s + "&key=AIzaSyCIhWIx08t5ap5neBIX1B3npieSP9HQSdY";
        ((ApiInterface) ApiClient.getDynamicClient().create(ApiInterface.class)).getHospitalSearchData(url).enqueue(new Callback<HospitalSearchModel>() {
            public void onResponse(Call<HospitalSearchModel> call, Response<HospitalSearchModel> response) {
//                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((HospitalSearchModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("OK")) {
                        hospitalList.clear();
                        hospitalList = response.body().getResults();
                        setMarketSearches();
                    }
                }

            }

            public void onFailure(Call<HospitalSearchModel> call, Throwable th) {
//                mLoadingView.hideDialog();
                Toast.makeText(ActivityAddPrimaryHospitalNew.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void setMarketSearches() {
        mPosition = -1;
        searcheHospitalRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 4; i++) {
            medicationList.add(new PetsModel("$45.99", "Ophtha Care",
                    "lorem ipsum is a simply dummy text of printing and type setting industry"));
        }*/
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                mPosition = position;
            }
        };

        searcheHospitalRv.setAdapter(new SearchHospitalAdapter(this, hospitalList, itemClickListener));
    }
}


