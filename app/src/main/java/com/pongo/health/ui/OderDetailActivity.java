package com.pongo.health.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kinda.alert.KAlertDialog;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.OrderProductAdapter;
import com.pongo.health.adapter.TransactionAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.HomeModel;
import com.pongo.health.model.TransactionModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.ItemClickListener;
import com.pongo.health.utils.TransactionListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OderDetailActivity extends BaseActivity {
    @BindView(R.id.transaction_rv)
    RecyclerView transactionRv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.order_id)
    TextView orderIdText;
    @BindView(R.id.order_date)
    TextView orderdateText;
    @BindView(R.id.order_price)
    TextView orderPriceText;
    TransactionListener itemClickListener;
    TransactionModel.SubscribeproductBean subscribeproductBean;
    private CustomDialog mLoadingView;
    private Session mSession;
    OrderProductAdapter orderProductAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);
        Gson gson = new Gson();
        subscribeproductBean = gson.fromJson(getIntent().getStringExtra("model"), TransactionModel.SubscribeproductBean.class);
        if (subscribeproductBean != null) {
            orderIdText.setText(subscribeproductBean.getOrderid());
            orderPriceText.setText("$" + subscribeproductBean.getTotal());
            orderdateText.setText(subscribeproductBean.getOrderdate());
            if (subscribeproductBean.getProducts() != null) {
                setTransaction();
            }
        }
    }

    private void setTransaction() {
        transactionRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 2; i++) {
            transactionList.add(new CartModel(R.drawable.rx_two_img, "Lorem Ipsum", "15-04-2020",
                    "By Card,4565456545"));

        }*/

        itemClickListener = new TransactionListener() {
            @Override
            public void onClicked(View view, int position) {
                // startActivity(new Intent(activity, ActivityMedicationSearch.class));
            }

            @Override
            public void onCancelClicked(View view, int position) {
                confimrDialog(position);
            }
        };
        orderProductAdapter = new OrderProductAdapter(this, subscribeproductBean.getProducts(), itemClickListener);
        transactionRv.setAdapter(orderProductAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;

        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("Order Detail");

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_oder_detail;
    }

    private void confimrDialog(int position) {
        new KAlertDialog(this, KAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("You want to cancel Subscription!")
                .setCancelText("No")
                .setConfirmText("Yes")
                .showCancelButton(true)
                .setCancelClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                .setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog kAlertDialog) {
                        kAlertDialog.dismissWithAnimation();
                        cancelSubscription(position);
                    }
                })
                .show();
    }

    private void cancelSubscription(int position) {
        this.mLoadingView.show();
        String subscribeid = subscribeproductBean.getProducts().get(position).getSubscribeid();
        String userid = mSession.getuser_id();
        String productId = subscribeproductBean.getProducts().get(position).getProduct_id();
        String orderId = subscribeproductBean.getOrderid();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).cancelProductSubscription(orderId,productId,userid, subscribeid).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(OderDetailActivity.this, message, Toast.LENGTH_SHORT).show();
                                    if (subscribeproductBean.getProducts().size() == 1) {
                                       finish();
                                    } else {
                                        subscribeproductBean.getProducts().remove(position);
//                                    cartAdapter.notifyItemChanged(position);
                                        orderProductAdapter.notifyDataSetChanged();
                                    }
                                } else {
                                    Toast.makeText(OderDetailActivity.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(OderDetailActivity.this, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(OderDetailActivity.this, "Failed to cancel subscription", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

}