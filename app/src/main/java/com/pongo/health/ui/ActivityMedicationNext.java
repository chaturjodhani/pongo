package com.pongo.health.ui;

import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.TabAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.SingleProductModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMedicationNext extends BaseActivity {

    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.tb_cart)
    ImageView tbCartIv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    private CustomDialog mLoadingView;
    private Session mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        tbCartIv.setVisibility(View.VISIBLE);

        //   setQuantity();
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);
        if (InternetConnection.checkConnection(this)) {
            String productId = getIntent().getStringExtra("id");
            getProductData(productId);
        } else {
            Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.tb_cart:
                startActivity(new Intent(ActivityMedicationNext.this, ActivityCart.class));
                break;
        }
    }

    @Override
    protected void setText() {
        String name = getIntent().getStringExtra("name");

        //addCartTv.setText("Add To Cart");
//        tbTv.setText("MEDICATION");
        tbTv.setText(name);
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        tbCartIv.setOnClickListener(this);

        //    addCartTv.setOnClickListener(this);
        //    strengthFl.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_medication_next;
    }

    private void setTabs(SingleProductModel singleProductModel) {

        tabLayout.addTab(tabLayout.newTab().setText("Highlights"));
        tabLayout.addTab(tabLayout.newTab().setText("Reviews"));
        tabLayout.addTab(tabLayout.newTab().setText("FAQ"));
        tabLayout.addTab(tabLayout.newTab().setText("Ingredients"));
        tabLayout.addTab(tabLayout.newTab().setText("Side Effects"));
        //  tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final TabAdapter adapter = new TabAdapter(this, getSupportFragmentManager(), tabLayout.getTabCount(), singleProductModel);
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }


    private void getProductData(String productId) {
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getSingleProductData(id, productId).enqueue(new Callback<SingleProductModel>() {
            public void onResponse(Call<SingleProductModel> call, Response<SingleProductModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((SingleProductModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        if (response.body().getSingleproduct().size() > 0) {
                            setTabs(response.body());
                        }
                    }
                }

            }

            public void onFailure(Call<SingleProductModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityMedicationNext.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

}
