package com.pongo.health.ui.addpet;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.adapter.AddPetRecommendationAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.HomeModel;
import com.pongo.health.model.PetRecommendationModel;
import com.pongo.health.model.PetsModel;
import com.pongo.health.ui.checkout.ActivityCheckOutPrescribed;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityAddPetRecommendation extends BaseActivity {
    @BindView(R.id.blue_btn_tv)
    TextView nextTv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;

    @BindView(R.id.recommendation_lol_tv)
    TextView recommendationLolTv;
    @BindView(R.id.recommendations_rv)
    RecyclerView recommendationsRv;
    private List<PetRecommendationModel.ProductdataBean> recommendationList = new ArrayList<>();
    @BindView(R.id.pet_name)
    TextView petnameTv;
    private AddPetRecommendationAdapter mAdapter;
    private String petname;
    private String petimage;
    private String petType = "";
    private String petSize = "";
    private String petAge = "";
    private String pethealthGoal = "";
    private String recommendId = "";
    private String product_type = "";
    private String breedSeleceted = "";
    private ItemClickListener itemClickListener;

    private String text;
    private CustomDialog mLoadingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        Intent intent = getIntent();
        if (null != intent) {
            petname = intent.getStringExtra("pet_name");
            petimage = intent.getStringExtra("pet_image");
            petType = intent.getStringExtra("pet_type");
            petSize = intent.getStringExtra("pet_size");
            petAge = intent.getStringExtra("pet_age");
            pethealthGoal = intent.getStringExtra("pet_health");
            breedSeleceted = intent.getStringExtra("breed");
        }
        petnameTv.setText("Recommendation for " + petname);
        if (InternetConnection.checkConnection(this)) {
            getRecommendationData();
        } else {
            Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
        }


    }

    private void getRecommendationData() {
        this.mLoadingView.show();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getPetRecommendationData(petType, pethealthGoal).enqueue(new Callback<PetRecommendationModel>() {
            public void onResponse(Call<PetRecommendationModel> call, Response<PetRecommendationModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((PetRecommendationModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        recommendationList = response.body().getProductdata();
                        setRecommendationList();
                    }
                }

            }

            public void onFailure(Call<PetRecommendationModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityAddPetRecommendation.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                goBack();
                break;

            case R.id.blue_btn_tv:
                verify_data();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {

        Intent intent = new Intent(ActivityAddPetRecommendation.this, ActivityPetHealthyGoal.class);
        intent.putExtra("pet_name", petname);
        intent.putExtra("pet_image", petimage);
        intent.putExtra("pet_type", petType);
        intent.putExtra("pet_size", petSize);
        intent.putExtra("pet_age", petAge);
        intent.putExtra("breed", breedSeleceted);
        startActivity(intent);
        finish();
    }

    private void setRecommendationList() {

        recommendationsRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 2; i++) {
            recommendationList.add(new PetsModel("FIRST MONTH FREE (Limit 2)", "EffiproPlus", text));
        }*/
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {

            }
        };
        mAdapter = new AddPetRecommendationAdapter(this, recommendationList, itemClickListener, petType);
        recommendationsRv.setAdapter(mAdapter);

    }


    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        recommendationLolTv.setText("Pongo recommends these items " +
                "for most pets with\nsimilar health goals.");
        text = "keep your best Friend safe from\n" +
                "parasitic pets with Virbac EFFIPRO\nPLUAS Tropical Solution";
        tbTv.setText(R.string.add_pet);
        nextTv.setText(R.string.next);


    }

    private void verify_data() {
        if (mAdapter.selectedPosition != -1) {
            recommendId = recommendationList.get(mAdapter.selectedPosition).getId();
            product_type = recommendationList.get(mAdapter.selectedPosition).getProduct_type();
        }
        if (!TextUtils.isEmpty(product_type) && product_type.equalsIgnoreCase("yes"))
        {
            Intent intent = new Intent(ActivityAddPetRecommendation.this, ActivityCheckOutPrescribed.class);
            intent.putExtra("pet_name", petname);
            intent.putExtra("pet_image", petimage);
            intent.putExtra("pet_type", petType);
            intent.putExtra("pet_size", petSize);
            intent.putExtra("pet_age", petAge);
            intent.putExtra("pet_health", pethealthGoal);
            intent.putExtra("recommendId", recommendId);
            intent.putExtra("breed", breedSeleceted);
            startActivity(intent);
//            finish();
        }
        else {
            Intent intent = new Intent(ActivityAddPetRecommendation.this, ActivityHaveAnyOtherPet.class);
            intent.putExtra("pet_name", petname);
            intent.putExtra("pet_image", petimage);
            intent.putExtra("pet_type", petType);
            intent.putExtra("pet_size", petSize);
            intent.putExtra("pet_age", petAge);
            intent.putExtra("pet_health", pethealthGoal);
            intent.putExtra("recommendId", recommendId);
            intent.putExtra("breed", breedSeleceted);
            intent.putExtra("existing_status", "No");
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        nextTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_add_pet_recommendation;
    }
}
