package com.pongo.health.ui.vet;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.PetCasesAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.CartModel;
import com.pongo.health.model.EarningModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityPetCases extends BaseActivity {

    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.tb_tv)
    TextView tbTv;

    @BindView(R.id.pet_cases_rv)
    RecyclerView petCasesRv;
    private List<EarningModel.PethistoryBean> petCasesList = new ArrayList<>();
    private CustomDialog mLoadingView;
    private Session mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);
        if (InternetConnection.checkConnection(ActivityPetCases.this)) {
            getPetCases();
        } else {
            Toast.makeText(ActivityPetCases.this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tb_iv) {
            onBackPressed();
            finish();
        }
    }

    private void setPetCasesRv() {
        petCasesRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
      /*  for (int i = 0; i < 2; i++) {
            petCasesList.add(new CartModel(R.drawable.senior, "Tommy - 5yrs", "Robies",
                    "Dog | pug"));

        }*/
        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
//                startActivity(new Intent(ActivityBillings.this, ActivityPatientDetail.class));
                Gson gson = new Gson();
                String myJson = gson.toJson(petCasesList.get(position));
                startActivity(new Intent(ActivityPetCases.this, ActivityPatientDetail.class)
                        .putExtra("model", myJson)
                        .putExtra("type", "hide"));
            }
        };

        petCasesRv.setAdapter(new PetCasesAdapter(this, petCasesList, itemClickListener));
    }

    private void getPetCases() {
        this.mLoadingView.show();
        String id = mSession.getuser_id();

        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).petCases(id).enqueue(new Callback<EarningModel>() {
            public void onResponse(Call<EarningModel> call, Response<EarningModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((EarningModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {

                        petCasesList = response.body().getPethistory();
                        if (null != petCasesList && petCasesList.size() > 0) {
                            setPetCasesRv();
                        }

                    }
                }

            }

            public void onFailure(Call<EarningModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityPetCases.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }


    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("Pet Cases");

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_pet_cases;
    }
}
