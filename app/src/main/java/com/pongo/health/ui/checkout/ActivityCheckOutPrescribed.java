package com.pongo.health.ui.checkout;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.ui.addpet.ActivityAddPetRecommendation;
import com.pongo.health.ui.addpet.ActivityHaveAnyOtherPet;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityCheckOutPrescribed extends BaseActivity {

    @BindView(R.id.prescribed_heading_tv)
    TextView prescribedHeadingTv;
    @BindView(R.id.description_tv)
    TextView descriptionTv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.p_no_btn_tv)
    TextView noTv;
    @BindView(R.id.p_yes_btn_tv)
    TextView yesTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    private String type = "";
    private String pet_name;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                if (type.equalsIgnoreCase("yes")) {

                    pet_name = getIntent().getStringExtra("pet_name");
                    if (!TextUtils.isEmpty(pet_name)) {
                        String pet_image = getIntent().getStringExtra("pet_image");
                        String pet_type = getIntent().getStringExtra("pet_type");
                        String pet_size = getIntent().getStringExtra("pet_size");
                        String pet_age = getIntent().getStringExtra("pet_age");
                        String pet_health = getIntent().getStringExtra("pet_health");
                        String recommendId = getIntent().getStringExtra("recommendId");
                        String breed = getIntent().getStringExtra("breed");
                        startActivity(new Intent(this, ActivityCheckOutYourPrescription.class)
                                .putExtra("pet_name", pet_name)
                                .putExtra("pet_type", pet_type)
                                .putExtra("pet_image", pet_image)
                                .putExtra("pet_size", pet_size)
                                .putExtra("pet_age", pet_age)
                                .putExtra("pet_health", pet_health)
                                .putExtra("recommendId", recommendId)
                                .putExtra("breed", breed)
                        );


                    } else {
                        String name = getIntent().getStringExtra("name");
                        String prodId = getIntent().getStringExtra("prodId");
                        String quantity = getIntent().getStringExtra("quantity");
                        String strength = getIntent().getStringExtra("strength");
                        String price = getIntent().getStringExtra("price");

                        startActivity(new Intent(this, ActivityCheckOutYourPrescription.class)
                                .putExtra("name", name)
                                .putExtra("prodId", prodId)
                                .putExtra("quantity", quantity)
                                .putExtra("strength", strength)
                                .putExtra("price", price)
                        );
                    }
                }
                break;

            case R.id.tb_iv:
                if (!TextUtils.isEmpty(pet_name)) {
                    goBack();
                } else {
                    finish();
                }
                break;
            case R.id.p_yes_btn_tv:
                type = "yes";
                setYesTvColor();
                descriptionTv.setText("");
                break;
            case R.id.p_no_btn_tv:
                type = "no";
                setNoTvColor();
                descriptionTv.setText("To use Pongo, you need a valid prescription from your doctor. Pongo does not currently prescribe this medication.");
                break;
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_checkout_prescribed;
    }

    @Override
    protected void setText() {
        String name = getIntent().getStringExtra("name");
        prescribedHeadingTv.setText("Do you have an existing " + name + " prescription ?");
        continueTv.setText(R.string.continue_);
        tbTv.setText(R.string.checkout);
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        continueTv.setOnClickListener(this);
        yesTv.setOnClickListener(this);
        noTv.setOnClickListener(this);
    }

    private void setYesTvColor() {

        noTv.setBackground(getResources().getDrawable(R.drawable.button_shaper_second));
        yesTv.setBackground(getResources().getDrawable(R.drawable.button_shaper_selected));
        noTv.setTextColor(getResources().getColor(R.color.text_color_blue));
        yesTv.setTextColor(getResources().getColor(R.color.white_color));

    }

    private void setNoTvColor() {

        noTv.setBackground(getResources().getDrawable(R.drawable.button_shaper_selected));
        yesTv.setBackground(getResources().getDrawable(R.drawable.button_shaper_second));
        noTv.setTextColor(getResources().getColor(R.color.white_color));
        yesTv.setTextColor(getResources().getColor(R.color.text_color_blue));
    }

    @Override
    public void onBackPressed() {
        if (!TextUtils.isEmpty(pet_name)) {
            goBack();
        } else {
            finish();
        }
    }


    private void goBack() {

        if (!TextUtils.isEmpty(pet_name)) {
            String pet_image = getIntent().getStringExtra("pet_image");
            String pet_type = getIntent().getStringExtra("pet_type");
            String pet_size = getIntent().getStringExtra("pet_size");
            String pet_age = getIntent().getStringExtra("pet_age");
            String pet_health = getIntent().getStringExtra("pet_health");
            String recommendId = getIntent().getStringExtra("recommendId");
            String breed = getIntent().getStringExtra("breed");

            Intent intent = new Intent(ActivityCheckOutPrescribed.this, ActivityAddPetRecommendation.class);
            intent.putExtra("pet_name", pet_name);
            intent.putExtra("pet_image", pet_image);
            intent.putExtra("pet_type", pet_type);
            intent.putExtra("pet_size", pet_size);
            intent.putExtra("pet_age", pet_age);
            intent.putExtra("pet_health", pet_health);
            intent.putExtra("breed", breed);
            startActivity(intent);
            finish();
        }
    }
}
