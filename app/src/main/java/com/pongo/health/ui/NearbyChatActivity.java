package com.pongo.health.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.PetListAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.PetModelResponse;
import com.pongo.health.utils.Constants;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.GPSTracker;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NearbyChatActivity extends BaseActivity {

    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.searching)
    AppCompatImageView searchingImage;
    @BindView(R.id.no_data)
    RelativeLayout noDataFound;
    @BindView(R.id.searchLayout)
    RelativeLayout searchLayout;
    @BindView(R.id.retry_btn_tv)
    TextView retryBtn;
    @BindView(R.id.blue_btn_tv)
    TextView noDataretryBtn;
    private CustomDialog mLoadingView;
    private Session mSession;
    private GPSTracker gps;
    Firebase reference1;
    ChildEventListener childEventListener;
    private CountDownTimer mCountDownTimer;
    private List<PetModelResponse.DataBean> mPetList = new ArrayList<>();
    private int mSelectedPet = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        removeTaskBar();
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
        gps = new GPSTracker(this);
        Glide.with(this).load(R.drawable.searching).into(searchingImage);
        searchingImage.setVisibility(View.GONE);
        if (InternetConnection.checkConnection(this)) {
            getpetList();
        } else {
            Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
        eventListener();
    }

    private void searchTech() {
        String userId = mSession.getuser_id();
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();
        if (!TextUtils.isEmpty(userId)) {
            ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).searchTech(String.valueOf(latitude), String.valueOf(longitude), userId, mPetList.get(mSelectedPet).getId()).enqueue(new Callback<ResponseBody>() {
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.body() != null) {
                        String responseData = "";
                        try {
                            responseData = response.body().string();
                            if (!TextUtils.isEmpty(responseData)) {
                                JSONObject jsonObject = new JSONObject(responseData);
                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (!TextUtils.isEmpty(status)) {
                                    if (status.equalsIgnoreCase("success")) {
                                        InsertFirebaseData();
                                        startTimer();
                                    } else {
                                        Toast.makeText(NearbyChatActivity.this, message, Toast.LENGTH_SHORT).show();
                                        String type = jsonObject.getString("type");
                                        if (type.equalsIgnoreCase("2")) {
                                            Intent intent = new Intent(NearbyChatActivity.this, ActivitySubscription.class);
                                            startActivity(intent);
                                        }
                                        searchingImage.setVisibility(View.GONE);
                                        retryBtn.setVisibility(View.VISIBLE);
                                        searchLayout.setVisibility(View.VISIBLE);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                public void onFailure(Call<ResponseBody> call, Throwable th) {
                    Log.e("cate", th.toString());
                    searchingImage.setVisibility(View.GONE);
                    retryBtn.setVisibility(View.VISIBLE);
                    searchLayout.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void getpetList() {
        mLoadingView.show();
        String userId = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getPets(userId).enqueue(new Callback<PetModelResponse>() {
            public void onResponse(Call<PetModelResponse> call, Response<PetModelResponse> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    try {
                        String status = response.body().getStatus();
                        if (!TextUtils.isEmpty(status)) {
                            if (status.equalsIgnoreCase("success")) {
                                mPetList = response.body().getData();
                                if (mPetList.size() > 0) {
                                    openPopup();
                                } else {
                                    Toast.makeText(NearbyChatActivity.this, "Add pet first", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<PetModelResponse> call, Throwable th) {
                Log.e("cate", th.toString());

            }
        });
    }

    private void InsertFirebaseData() {
        String userId = mSession.getuser_id();
        reference1 = new Firebase(Constants.firebaseUrl + "techchat/" + userId);
        Map<String, String> map = new HashMap<String, String>();
        map.put("devicetoken", "");
        map.put("userid", userId);
        map.put("usertype", "free");
        map.put("first_name", mSession.getfirst_name());
        map.put("last_name", mSession.getlast_name());
        map.put("image", mSession.getuser_image());
        map.put("chatid", "");
        reference1.push().setValue(map);
        reference1.addChildEventListener(childEventListener);
    }

    private void eventListener() {
        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                try {
                    Map map = dataSnapshot.getValue(Map.class);
                    String userid = Objects.requireNonNull(map.get("userid")).toString();
                    String usertype = Objects.requireNonNull(map.get("usertype")).toString();
                    String first_name = Objects.requireNonNull(map.get("first_name")).toString();
                    String last_name = Objects.requireNonNull(map.get("last_name")).toString();
                    String image = Objects.requireNonNull(map.get("image")).toString();
                    if (usertype.equalsIgnoreCase("technician")) {
                        ArrayList<String> chatWithToken = (ArrayList<String>) map.get("devicetoken");
                        String chatid = Objects.requireNonNull(map.get("chatid")).toString();
                        String credit = Objects.requireNonNull(map.get("credit")).toString();
                        Intent intent = new Intent(NearbyChatActivity.this, ChatViewActivity.class);
                        intent.putExtra("chat_id", userid);
                        intent.putExtra("name", first_name + " " + last_name);
                        intent.putExtra("profileimage", image);
                        intent.putExtra("status", "waiting");
                        intent.putExtra("chatid", chatid);
                        intent.putExtra("credit", credit);
                        intent.putStringArrayListExtra("token", chatWithToken);
                        startActivity(intent);
                        finish();
                        deleteTable();
                        searchingImage.setVisibility(View.GONE);
                        retryBtn.setVisibility(View.VISIBLE);
                        searchLayout.setVisibility(View.VISIBLE);
                        noDataFound.setVisibility(View.GONE);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        };

    }

    private void deleteTable() {
        if (null != reference1) {
            reference1.removeValue();
        }
    }

    private void startTimer() {
        mCountDownTimer = new CountDownTimer(300000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                searchingImage.setVisibility(View.GONE);
                retryBtn.setVisibility(View.GONE);
                noDataFound.setVisibility(View.VISIBLE);
                searchLayout.setVisibility(View.GONE);
                deleteTable();
            }
        }.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        deleteTable();
        if (null != mCountDownTimer) {
            mCountDownTimer.cancel();
        }
        if (null != reference1) {
            reference1.removeEventListener(childEventListener);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                clearActivityStack();
                break;
            case R.id.retry_btn_tv: {
                if (InternetConnection.checkConnection(NearbyChatActivity.this)) {
                    searchingImage.setVisibility(View.VISIBLE);
                    retryBtn.setVisibility(View.GONE);
                    searchLayout.setVisibility(View.VISIBLE);
                    searchTech();
                } else {
                    searchingImage.setVisibility(View.GONE);
                    retryBtn.setVisibility(View.VISIBLE);
                    searchLayout.setVisibility(View.VISIBLE);
                    Toast.makeText(NearbyChatActivity.this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case R.id.blue_btn_tv: {
                if (InternetConnection.checkConnection(NearbyChatActivity.this)) {
                    searchingImage.setVisibility(View.VISIBLE);
                    retryBtn.setVisibility(View.GONE);
                    searchLayout.setVisibility(View.VISIBLE);
                    noDataFound.setVisibility(View.GONE);
                    searchTech();
                } else {
                    searchingImage.setVisibility(View.GONE);
                    retryBtn.setVisibility(View.GONE);
                    noDataFound.setVisibility(View.VISIBLE);
                    searchLayout.setVisibility(View.GONE);

                    Toast.makeText(NearbyChatActivity.this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        noDataretryBtn.setOnClickListener(this);
        retryBtn.setOnClickListener(this);
    }


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_nearby_chat;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("First available Veterinary professional");
    }

    private void openPopup() {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.pet_list_popup, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        alertDialogBuilder.setCancelable(true);

        RecyclerView petList = promptsView
                .findViewById(R.id.pet_list);
        TextView continueTv = promptsView
                .findViewById(R.id.blue_btn_tv);
        ItemClickListener itemClickListener;
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        itemClickListener = new ItemClickListener() {

            @Override
            public void onClicked(View view, int postion) {
//                alertDialog.dismiss();
                mSelectedPet = postion;
            }
        };
        petList.setLayoutManager(new LinearLayoutManager(NearbyChatActivity.this, RecyclerView.VERTICAL, false));

        petList.setAdapter(new PetListAdapter(NearbyChatActivity.this, mPetList, itemClickListener));


        continueTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelectedPet >= 0) {
                    alertDialog.dismiss();
                    searchingImage.setVisibility(View.VISIBLE);
                    searchTech();
                } else {
                    Toast.makeText(NearbyChatActivity.this, "Please select pet first", Toast.LENGTH_SHORT).show();
                }
            }
        });
        // show it
        alertDialog.show();
    }
}