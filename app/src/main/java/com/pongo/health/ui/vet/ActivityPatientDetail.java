package com.pongo.health.ui.vet;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityOptionsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.kinda.alert.KAlertDialog;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.AttachmentAdapter;
import com.pongo.health.adapter.ImagesAdapter;
import com.pongo.health.adapter.IssueAdapter;
import com.pongo.health.adapter.PastDoctorAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.DoctorWaitingListModel;
import com.pongo.health.model.UserLoginModel;
import com.pongo.health.ui.ActivityAddPrimaryHospital;
import com.pongo.health.ui.ActivityPdfView;
import com.pongo.health.ui.ImageViewActivity;
import com.pongo.health.ui.pet_parent.ActivityMedicalConfirmation;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.GPSTracker;
import com.pongo.health.utils.ImageUtil;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityPatientDetail extends BaseActivity {

    @BindView(R.id.v_tb_iv)
    ImageView VTbIv;
    @BindView(R.id.blue_btn_tv)
    TextView videoCallBtnTv;
    @BindView(R.id.pet_name)
    TextView petNameTv;
    @BindView(R.id.pet_type)
    TextView petTypeTv;
    @BindView(R.id.breed)
    TextView breedTv;
    @BindView(R.id.pet_age)
    TextView petAgeTv;
    @BindView(R.id.issue_rv)
    RecyclerView issueRv;
    @BindView(R.id.pdf_rv)
    RecyclerView pdfRv;
    @BindView(R.id.history_rv)
    RecyclerView historyRv;
    @BindView(R.id.pastdoctor_rv)
    RecyclerView doctorRv;
    @BindView(R.id.pet_image)
    CircleImageView petImage;
    DoctorWaitingListModel.WatingBean mBean;
    private CustomDialog mLoadingView;
    private Session mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
        Intent intent = getIntent();
        if (null != intent) {
            Gson gson = new Gson();
            mBean = gson.fromJson(getIntent().getStringExtra("model"), DoctorWaitingListModel.WatingBean.class);
            if (null != mBean) {
                setData();
            }
            String type = getIntent().getStringExtra("type");
            if (type != null && !TextUtils.isEmpty(type) && type.equalsIgnoreCase("hide")) {
                videoCallBtnTv.setVisibility(View.GONE);
            }
        }
    }

    private void setData() {
        petNameTv.setText(mBean.getPet_name());
        petTypeTv.setText(mBean.getPet_type());
        breedTv.setText(mBean.getBreed());
        petAgeTv.setText(mBean.getPet_age());
        Glide.with(ActivityPatientDetail.this).load(mBean.getPetimage()).into(petImage);
        setIssueAdapter();
        setAttachmentAdapter();
        setMedicalAdapter();
        setPastDoctorAdapter();
    }

    private void setMedicalAdapter() {
        historyRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                if (mBean.getMedicalrecords().get(position).contains(".pdf")) {
                    Intent intent2 = new Intent(ActivityPatientDetail.this, ActivityPdfView.class);
                    String url2 = mBean.getMedicalrecords().get(position);
                    intent2.putExtra("name", "Attachments by patient");
                    intent2.putExtra("url", url2);
                    if (!TextUtils.isEmpty(url2)) {
                        startActivity(intent2);
                    } else {
                        Toast.makeText(ActivityPatientDetail.this, "No pdf available", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    String url2 = mBean.getMedicalrecords().get(position);
                    Intent intent = new Intent(ActivityPatientDetail.this, ImageViewActivity.class);
                    intent.putExtra("name", mBean.getPet_name());
                    intent.putExtra("time", "");
                    intent.putExtra("image", url2);
                    startActivity(intent);
                }
            }
        };
//        itemClickListener = (view, position) -> startActivity(new Intent(activity, ActivityYourPetSingleType.class));
        AttachmentAdapter mImagesAdapter = new AttachmentAdapter(this, mBean.getMedicalrecords(), itemClickListener);
        historyRv.setAdapter(mImagesAdapter);
    }

    private void setIssueAdapter() {
        issueRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {

            }
        };
//        itemClickListener = (view, position) -> startActivity(new Intent(activity, ActivityYourPetSingleType.class));
        IssueAdapter mImagesAdapter = new IssueAdapter(this, mBean.getCurrentissue(), itemClickListener);
        issueRv.setAdapter(mImagesAdapter);
    }

    private void setAttachmentAdapter() {
        pdfRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                if (mBean.getAttachments().get(position).contains(".pdf")) {
                    Intent intent2 = new Intent(ActivityPatientDetail.this, ActivityPdfView.class);
                    String url2 = mBean.getAttachments().get(position);
                    intent2.putExtra("name", "Attachments by patient");
                    intent2.putExtra("url", url2);
                    if (!TextUtils.isEmpty(url2)) {
                        startActivity(intent2);
                    } else {
                        Toast.makeText(ActivityPatientDetail.this, "No pdf available", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    String url2 = mBean.getAttachments().get(position);
                    Intent intent = new Intent(ActivityPatientDetail.this, ImageViewActivity.class);
                    intent.putExtra("name", mBean.getPet_name());
                    intent.putExtra("time", "");
                    intent.putExtra("image", url2);
                    startActivity(intent);
                }
            }
        };
//        itemClickListener = (view, position) -> startActivity(new Intent(activity, ActivityYourPetSingleType.class));
        AttachmentAdapter mImagesAdapter = new AttachmentAdapter(this, mBean.getAttachments(), itemClickListener);
        pdfRv.setAdapter(mImagesAdapter);
    }

    private void setPastDoctorAdapter() {
        doctorRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                if (!TextUtils.isEmpty(mBean.getPastdoctor().get(position).getDoctor_note())) {
                    doctorNoteDialog(mBean.getPastdoctor().get(position).getDoctor_note());
                } else {
                    Toast.makeText(ActivityPatientDetail.this, "No doctor note available", Toast.LENGTH_SHORT).show();
                }
            }
        };
//        itemClickListener = (view, position) -> startActivity(new Intent(activity, ActivityYourPetSingleType.class));
        PastDoctorAdapter mImagesAdapter = new PastDoctorAdapter(this, mBean.getPastdoctor(), itemClickListener);
        doctorRv.setAdapter(mImagesAdapter);
    }

    private void doctorNoteDialog(String description) {
        KAlertDialog kAlertDialog = new KAlertDialog(this, KAlertDialog.CUSTOM_IMAGE_TYPE);
        kAlertDialog.setTitleText("Doctor Note");
        kAlertDialog.setCancelable(true);
        kAlertDialog.setContentText(description);

        kAlertDialog.setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(KAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
            }
        })
                .show();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_tb_iv:
                clearActivityStack();
                break;
            case R.id.blue_btn_tv:
                if (null != mBean) {
                    StartVideoCall();
                }
                break;
        }
    }

    private void StartVideoCall() {
        this.mLoadingView.show();
        String petid = mBean.getPet_id();
        String userId = mBean.getUser_id();
        String doctorId = mSession.getuser_id();

        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).startVideoCall(petid, doctorId, userId).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Intent intent = new Intent(getApplicationContext(), VideocallActivity.class);
                                    intent.putExtra("chatroom", mBean.getChatroomname());
                                    intent.putExtra("user_id", mBean.getUser_id());
                                    intent.putExtra("chatid", mBean.getChatid());
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(ActivityPatientDetail.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivityPatientDetail.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityPatientDetail.this, "Failed to start call", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });

    }

    @Override
    protected void setText() {
        videoCallBtnTv.setText(R.string.begin_vedio_call);
    }

    @Override
    protected void setOnClick() {
        videoCallBtnTv.setOnClickListener(this);
        VTbIv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_patient_detail;
    }

}
