package com.pongo.health.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.AddressModel;
import com.pongo.health.model.StateListModel;
import com.pongo.health.utils.Constants;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestAnothervetActivity extends BaseActivity {
    @BindView(R.id.v_tb_iv)
    ImageView tbIv;
    @BindView(R.id.blue_btn_tv)
    TextView submitTv;
    @BindView(R.id.hospital_name)
    TextView hospitalTv;
    @BindView(R.id.name_et)
    EditText nameEt;
    @BindView(R.id.lastname_et)
    EditText lastnameEt;
    private Session mSession;
    private String hospital = "";
    private CustomDialog mLoadingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);
        submitTv.setText("Request another Vet");
        Intent intent = getIntent();
        if (null != intent) {
            hospital = intent.getStringExtra("hospital");
            hospitalTv.setText(hospital);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.blue_btn_tv:
                if (InternetConnection.checkConnection(this)) {
                    verify_data();
                } else {
                    Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {


    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        submitTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_request_anothervet;
    }

    private void verify_data() {
//        String team = spin_team.getSelectedItem().toString();
        String name = nameEt.getText().toString().trim();
        String lastName = lastnameEt.getText().toString().trim();
        hospital = hospitalTv.getText().toString().trim();

        if (hospital.isEmpty() && name.isEmpty() && lastName.isEmpty()) {
            Toast.makeText(this, "Please enter detail in all fields", Toast.LENGTH_SHORT).show();

        } else if (name.isEmpty()) {
            nameEt.requestFocus();
            nameEt.setError("Please enter Vet first name");
        } else if (lastName.isEmpty()) {
            lastnameEt.requestFocus();
            lastnameEt.setError("Please enter Vet last name");

        } else {
            if (InternetConnection.checkConnection(this)) {
                sendRequest();
            } else {
                Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void sendRequest() {
        this.mLoadingView.show();
        String userid = mSession.getuser_id();
        String name = nameEt.getText().toString().trim();
        String lastName = lastnameEt.getText().toString().trim();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).requestAnotherVet(userid, hospital, name, lastName).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
//                            String message = jsonObject.getString("message");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    String message = jsonObject.getString("detail");
                            startActivity(new Intent(RequestAnothervetActivity.this, RequestDoneActivity.class)
                                    .putExtra("hospital", hospital)
                                    .putExtra("name", name)
                            );

                                } else {
                                    Toast.makeText(RequestAnothervetActivity.this, "Failed to send request", Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(RequestAnothervetActivity.this, "Failed to send request", Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(RequestAnothervetActivity.this, "Failed to send request", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }
}