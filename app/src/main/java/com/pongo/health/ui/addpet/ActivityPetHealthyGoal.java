package com.pongo.health.ui.addpet;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.adapter.AddPetHealthGoalAdapter;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.PetSizeModels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityPetHealthyGoal extends BaseActivity {
    @BindView(R.id.blue_btn_tv)
    TextView nextTv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;

    @BindView(R.id.health_goal_rv)
    RecyclerView healthGoalRv;
    private List<PetSizeModels> petHealthGoalList = new ArrayList<>();
    @BindView(R.id.pet_name)
    TextView petnameTv;
    private String petname;
    private String petimage;
    private String petType = "";
    private String petSize = "";
    private String petAge = "";
    private String breedSeleceted = "";
    private String pethealthGoal = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        if (null != intent) {
            petname = intent.getStringExtra("pet_name");
            petimage = intent.getStringExtra("pet_image");
            petType = intent.getStringExtra("pet_type");
            petSize = intent.getStringExtra("pet_size");
            petAge = intent.getStringExtra("pet_age");
            breedSeleceted = intent.getStringExtra("breed");
        }
        petnameTv.setText("What's your immediate health goal for " + petname + "?");
        setPetHealthyGoal();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                goBack();
                break;

            case R.id.blue_btn_tv:
                verify_data();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        if (petType.equalsIgnoreCase("dog")) {
            Intent intent = new Intent(ActivityPetHealthyGoal.this, ActivityPetOld.class);
            intent.putExtra("pet_name", petname);
            intent.putExtra("pet_image", petimage);
            intent.putExtra("pet_type", petType);
            intent.putExtra("pet_size", petSize);
            intent.putExtra("breed", breedSeleceted);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(ActivityPetHealthyGoal.this, ActivityCatOld.class);
            intent.putExtra("pet_name", petname);
            intent.putExtra("pet_image", petimage);
            intent.putExtra("pet_type", petType);
            intent.putExtra("pet_size", petSize);
            intent.putExtra("breed", breedSeleceted);
            startActivity(intent);
            finish();
        }
    }


    private void setPetHealthyGoal() {
        healthGoalRv.setLayoutManager(new GridLayoutManager(this, 2));
        petHealthGoalList.add(new PetSizeModels(R.drawable.h1, "Manage Anxiety", ""));
        petHealthGoalList.add(new PetSizeModels(R.drawable.h2, "Prevent Fleas & Tricks", ""));
        petHealthGoalList.add(new PetSizeModels(R.drawable.h3, "Improve Mobility", ""));
        petHealthGoalList.add(new PetSizeModels(R.drawable.h4, "Boost Oral Health", ""));
        petHealthGoalList.add(new PetSizeModels(R.drawable.h5, "Protect skin & coat", ""));
        petHealthGoalList.add(new PetSizeModels(R.drawable.h6, "Behavioural Improvement", ""));
        petHealthGoalList.add(new PetSizeModels(R.mipmap.nutrition, "Improve Nutrition", ""));
        healthGoalRv.setAdapter(new AddPetHealthGoalAdapter(this, petHealthGoalList));
    }

    private void verify_data() {
        String healthGoal = "";
        for (int i = 0; i < petHealthGoalList.size(); i++) {
            if (petHealthGoalList.get(i).isSelected()) {
                healthGoal = healthGoal + petHealthGoalList.get(i).getSizeType() + ",";
            }
        }
        if (TextUtils.isEmpty(healthGoal)) {
            Toast.makeText(this, "Please select pet health goal", Toast.LENGTH_SHORT).show();
        } else {
            if (healthGoal.endsWith(",")) {
                healthGoal = healthGoal.substring(0, healthGoal.length() - 1);
            }
            Intent intent = new Intent(ActivityPetHealthyGoal.this, ActivityAddPetRecommendation.class);
            intent.putExtra("pet_name", petname);
            intent.putExtra("pet_image", petimage);
            intent.putExtra("pet_type", petType);
            intent.putExtra("pet_size", petSize);
            intent.putExtra("pet_age", petAge);
            intent.putExtra("pet_health", healthGoal);
            intent.putExtra("breed", breedSeleceted);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void setText() {
        tbTv.setText(R.string.add_pet);
        nextTv.setText(R.string.next);
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        nextTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_add_pet_healthy_goal;
    }
}
