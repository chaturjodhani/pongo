package com.pongo.health.ui;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.CartAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.CartModel;
import com.pongo.health.model.CartResponseModel;
import com.pongo.health.ui.addpet.ActivityPetName;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;
import com.pongo.health.utils.ItemUpdateListener;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCart extends BaseActivity {

    @BindView(R.id.cart_rv)
    RecyclerView cartRv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.blue_btn_tv)
    TextView checkoutTv;
    @BindView(R.id.continue_shopping_tv)
    TextView continueShopTv;
    private CustomDialog mLoadingView;
    private Session mSession;
    private List<CartResponseModel.CartlistBean> cartList = new ArrayList<>();
    private CartAdapter cartAdapter;
    private String screen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);
        continueShopTv.setVisibility(View.VISIBLE);
        checkoutTv.setVisibility(View.GONE);
        if (null != getIntent()) {
            screen = getIntent().getStringExtra("screen");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (InternetConnection.checkConnection(ActivityCart.this)) {
            getCartData();
        } else {
            Toast.makeText(ActivityCart.this, "No Internet available", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                goBack();
                break;

            case R.id.blue_btn_tv:
                /*startActivity(new Intent(this, ActivityPayment.class)
                        .putExtra("existing_status", "no"));*/
                startActivity(new Intent(this, ActivityAddress.class)
                        .putExtra("existing_status", "no")
                );
                break;
            case R.id.continue_shopping_tv:
                Intent i = new Intent(ActivityCart.this, ActivityHome.class);
                // set the new task and clear flags
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.putExtra("market", "market");
                startActivity(i);
                finish();
                break;
        }
    }

    private void goBack() {
        if (!TextUtils.isEmpty(screen)) {
            Intent i = new Intent(ActivityCart.this, ActivityHome.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            finish();
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    @Override
    protected void setText() {
        checkoutTv.setText("Buy Now");
        tbTv.setText("Cart");
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        checkoutTv.setOnClickListener(this);
        continueShopTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_cart;
    }

    private void setCartItems() {
        cartRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

      /*  for (int i = 0; i < 3; i++) {

            cartList.add(new CartModel(R.drawable.medicine_care, "Ophtha Care ",
                    "Lorem lapsum is simply dummy text of thr printing and typesting " +
                            "industry", "$25.99 x "));
        }*/

        ItemUpdateListener itemUpdateListener = new ItemUpdateListener() {
            @Override
            public void onClicked(View view, int position) {
                deleteCartItem(position);
            }

            @Override
            public void onUpdateClicked(View view, int position, int quantity) {
                updateCartItemQuantity(position, quantity);
            }
        };
        cartAdapter = new CartAdapter(this, cartList, itemUpdateListener);
        cartRv.setAdapter(cartAdapter);
    }

    private void deleteCartItem(int position) {
        this.mLoadingView.show();
        String id = cartList.get(position).getCartid();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).deleteCartItem(id).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(ActivityCart.this, message, Toast.LENGTH_SHORT).show();
                                    if (cartList.size() == 1) {
                                        finish();
                                    } else {
                                        cartList.remove(position);
//                                    cartAdapter.notifyItemChanged(position);
                                        cartAdapter.notifyDataSetChanged();
                                    }
                                } else {
                                    Toast.makeText(ActivityCart.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivityCart.this, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityCart.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void updateCartItemQuantity(int position, int quantity) {
        this.mLoadingView.show();
        String userid = mSession.getuser_id();
        String cartid = cartList.get(position).getCartid();
        String prodId = cartList.get(position).getProduct_id();
        float originalPrice = Float.parseFloat(cartList.get(position).getOrignalprice());
        float totalPrice = quantity * originalPrice;
        DecimalFormat df = new DecimalFormat("0.00");
        totalPrice = Float.parseFloat(df.format(totalPrice));
//        String price = cartList.get(position).getPrice();
        float finalTotalPrice = totalPrice;
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).updateCartItem(cartid, prodId, String.valueOf(quantity), userid, String.valueOf(totalPrice)).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(ActivityCart.this, message, Toast.LENGTH_SHORT).show();
                                    CartResponseModel.CartlistBean cartlistBean = cartList.get(position);
                                    cartlistBean.setPrice(String.valueOf(finalTotalPrice));
                                    cartlistBean.setQuantity(String.valueOf(quantity));
                                    cartAdapter.apiCalled = true;
                                    cartAdapter.notifyDataSetChanged();

                                } else {
                                    Toast.makeText(ActivityCart.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivityCart.this, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityCart.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }


    private void getCartData() {
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getCartList(id).enqueue(new Callback<CartResponseModel>() {
            public void onResponse(Call<CartResponseModel> call, Response<CartResponseModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((CartResponseModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        cartList = response.body().getCartlist();
                        if (null != cartList && cartList.size() > 0) {
                            checkoutTv.setVisibility(View.VISIBLE);
                        }
                        setCartItems();
                    }
                }

            }

            public void onFailure(Call<CartResponseModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityCart.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }
}

