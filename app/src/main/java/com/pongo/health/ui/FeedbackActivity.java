package com.pongo.health.ui;

import androidx.appcompat.widget.AppCompatSeekBar;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.ui.pet_parent.ActivityGeneralInformationPet;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackActivity extends BaseActivity {
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.pet_image)
    ImageView petIv;
    @BindView(R.id.pet_name)
    TextView petNameTv;
    @BindView(R.id.call_progress)
    AppCompatSeekBar callProgress;
    @BindView(R.id.satisfy_progress)
    AppCompatSeekBar satisfyProgress;
    @BindView(R.id.note_et)
    EditText noteEt;
    private CustomDialog mLoadingView;
    private Session mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
        setData();

    }

    private void setData() {
        String pet_name = getIntent().getStringExtra("pet_name");
        String pet_image = getIntent().getStringExtra("logo");
        petNameTv.setText("We hope " + pet_name + " feels\n better soon!");
        Glide.with(FeedbackActivity.this).load(pet_image).into(petIv);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                if (InternetConnection.checkConnection(FeedbackActivity.this)) {
                    String id = getIntent().getStringExtra("user_id");
                    if (!TextUtils.isEmpty(id)) {
                        addFeedbackApi();
                    }
                } else {
                    Toast.makeText(FeedbackActivity.this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.v_tb_iv:
                Intent i = new Intent(FeedbackActivity.this, ActivityHome.class);
                // set the new task and clear flags
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
                break;
        }
    }

    @Override
    protected void setText() {
        continueTv.setText(R.string.submit);
    }

    @Override
    protected void setOnClick() {
        continueTv.setOnClickListener(this);
        vTbIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_feedback;
    }

    private void addFeedbackApi() {
        this.mLoadingView.show();
        String note = noteEt.getText().toString();
        String userId = getIntent().getStringExtra("user_id");
        String techId = getIntent().getStringExtra("tech_id");
        String chatid = getIntent().getStringExtra("chatid");
        String callQuality = String.valueOf(callProgress.getProgress());
        String consultRating = String.valueOf(satisfyProgress.getProgress());

        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).feedback(userId, techId, callQuality, consultRating, note,chatid).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(FeedbackActivity.this, message, Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(FeedbackActivity.this, ActivityHome.class);
                                    // set the new task and clear flags
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                    finish();
                                } else {
                                    Toast.makeText(FeedbackActivity.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(FeedbackActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(FeedbackActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(FeedbackActivity.this, ActivityHome.class);
        // set the new task and clear flags
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }
}