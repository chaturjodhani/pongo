package com.pongo.health.ui.vet;
import android.annotation.SuppressLint;
import android.os.Bundle;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.fragment.waiting.FragmentAppointment;
import com.pongo.health.fragment.waiting.FragmentWaiting;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityWaitingList extends BaseActivity {

    @BindView( R.id.tab_second_tv) TextView waitingRoomTv;
    @BindView( R.id.tab_one_tv) TextView appointmentTv;
    @BindView( R.id.tb_tv) TextView tbTv;
    @BindView( R.id.tb_iv) ImageView tbIv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);
        setWaitingRoomTvColor();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tab_one_tv:
                waitingRoomTv.setBackground(getResources().getDrawable(R.drawable.button_shaper));
                appointmentTv.setBackground(getResources().getDrawable(R.drawable.rectangle_blue_shape));
                waitingRoomTv.setTextColor(getResources().getColor(R.color.text_color_blue));
                appointmentTv.setTextColor(getResources().getColor(R.color.white_color));

                getSupportFragmentManager().beginTransaction().
                        replace(R.id.waiting_fragment_container,
                                FragmentAppointment.newInstance()).commitNow();
                break;
            case R.id.tab_second_tv:
                setWaitingRoomTvColor();
                break;
            case R.id.tb_iv:
              clearActivityStack();
                break;
        }
    }

    private  void setWaitingRoomTvColor(){

        appointmentTv.setBackground(getResources().getDrawable(R.drawable.button_shaper));
        waitingRoomTv.setBackground(getResources().getDrawable(R.drawable.rectangle_blue_shape));
        appointmentTv.setTextColor(getResources().getColor(R.color.text_color_blue));
        waitingRoomTv.setTextColor(getResources().getColor(R.color.white_color));
        getSupportFragmentManager().beginTransaction().
                replace(R.id.waiting_fragment_container,
                        FragmentWaiting.newInstance()).commitNow();
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("Waiting Room");
        appointmentTv.setText("Patient with booked Appointments");
        waitingRoomTv.setText("Patient in Waiting Room");
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        appointmentTv.setOnClickListener(this);
        waitingRoomTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_waiting_list;
    }
}
