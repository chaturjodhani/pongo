package com.pongo.health.ui;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.IntroductionViewPagerAdapter;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.ViewPagerModel;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityIntroduction extends BaseActivity {


    @BindView(R.id.blue_btn_tv)
    TextView introSignupTv;
    @BindView(R.id.intro_login_tv)
    TextView introLoginTv;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.introduction_viewPager)
    ViewPager introductionViewPager;

    private List<ViewPagerModel> viewPagerModelList = new ArrayList<>();
    private String txtOne;
    private String txtSecond;
    private String txtthree;
    boolean hasPermissionLocation;
    private static final int REQUEST_CODE_PERMISSION = 2;
    private static final int REQUEST_ACCESS_FINE_LOCATIONSignup = 110;
    private static final int REQUEST_ACCESS_FINE_LOCATIONLogin = 111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        removeTaskBar();
        ButterKnife.bind(this);
        setAdapter();
        hasPermissionLocation = (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.intro_login_tv:

                startActivity(new Intent(this, ActivityLogin.class));

                break;
            case R.id.blue_btn_tv:
                startActivity(new Intent(this, ActivitySignup.class));

                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case REQUEST_ACCESS_FINE_LOCATIONLogin: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    startActivity(new Intent(this, ActivityLogin.class));

                } else {
                    Toast.makeText(ActivityIntroduction.this, "The app was not allowed to get your location. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();

                }
                break;
            }
            case REQUEST_ACCESS_FINE_LOCATIONSignup: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    startActivity(new Intent(this, ActivitySignup.class));

                } else {
                    Toast.makeText(ActivityIntroduction.this, "The app was not allowed to get your location. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();

                }
                break;
            }

        }


    }


    @Override
    protected void setText() {
        introSignupTv.setText(R.string.signup);
        txtOne = "Get Pet related questions\nanswered by our Veterinary\nTeam";
        txtSecond = "Get the care your Pets needs-\nWhenever & Whenever";
        txtthree = "Get medicine and supplies\ndelivered to your door";
    }

    private void addData() {
        viewPagerModelList.add(new ViewPagerModel("", txtOne, R.drawable.i1));
        viewPagerModelList.add(new ViewPagerModel("", txtSecond, R.drawable.i2));
        viewPagerModelList.add(new ViewPagerModel("", txtthree, R.drawable.i3));

    }

    private void setAdapter() {
        addData();
        tabLayout.setupWithViewPager(introductionViewPager);
        introductionViewPager.setAdapter(new IntroductionViewPagerAdapter(this, viewPagerModelList));

    }

    @Override
    protected void setOnClick() {
        introSignupTv.setOnClickListener(this);
        introLoginTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_introduction;
    }

}
