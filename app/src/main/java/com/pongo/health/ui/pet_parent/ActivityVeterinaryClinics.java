package com.pongo.health.ui.pet_parent;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.pongo.health.R;
import com.pongo.health.adapter.SearchHospitalAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.HomeModel;
import com.pongo.health.model.HospitalSearchModel;
import com.pongo.health.ui.ActivityAddPrimaryHospitalNew;
import com.pongo.health.ui.vet.ActivityVeterinaryLicense;
import com.pongo.health.utils.GPSTracker;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityVeterinaryClinics extends BaseActivity {

    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.blue_btn_tv)
    TextView ContinueTv;
    @BindView(R.id.heading_vc_tv)
    TextView headingVcTv;
    @BindView(R.id.hospital_rv)
    RecyclerView searcheHospitalRv;
    @BindView(R.id.search_et)
    EditText searchEt;
    private List<HospitalSearchModel.ResultsBean> hospitalList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private HomeModel.PetlistBean mPetBean;
    private GPSTracker gps;
    private int mPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Gson gson = new Gson();
        mPetBean = gson.fromJson(getIntent().getStringExtra("model"), HomeModel.PetlistBean.class);
        if (null != mPetBean) {
            setData();
        }
        gps = new GPSTracker(this);
        searchEt.setHint("Search for Veterinary clinics");
        setSearchListener();
    }

    private void setData() {
        headingVcTv.setText(Html.fromHtml("Which Veterinary clinics has " + mPetBean.getPet_name() + " visited?"));

    }

    private void setSearchListener() {
        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                if (gps.canGetLocation()) {
                    getSearch(editable.toString());

                } else {
                    gps.showSettingsAlert();
                }
            }
        });

        searchEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
              /*  if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String search = searchEt.getText().toString();
                    getSearch(search);
                    return true;
                }*/
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//Hide:
                if (imm != null) {
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                verify_data();
                break;
            case R.id.tb_iv:
                clearActivityStack();
                break;
        }
    }

    private void verify_data() {
        if (mPosition >= 0) {
            String hospital = hospitalList.get(mPosition).getName();
            Intent intent = new Intent(this, ActivityVerifyInformation.class);
            Gson gson = new Gson();
            String myJson = gson.toJson(mPetBean);
            intent.putExtra("model", myJson);
            intent.putExtra("hospital_name", hospital);
            startActivity(intent);
        } else {
            Toast.makeText(ActivityVeterinaryClinics.this, "Please select clinic", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    protected void setText() {
        tbTv.setText(R.string.medical_records);
        ContinueTv.setText(R.string.continue_);

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        ContinueTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_veterinary_clinics;
    }

    private void getSearch(String s) {
//        this.mLoadingView.show();
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();

//        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&radius=1500000&type=drugstore|hospital|veterinary_care|pharmacy|petstore&keyword=" + s + "&key=AIzaSyCIhWIx08t5ap5neBIX1B3npieSP9HQSdY";
        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&radius=1500000&keyword=" + s + "&key=AIzaSyCIhWIx08t5ap5neBIX1B3npieSP9HQSdY";
        ((ApiInterface) ApiClient.getDynamicClient().create(ApiInterface.class)).getHospitalSearchData(url).enqueue(new Callback<HospitalSearchModel>() {
            public void onResponse(Call<HospitalSearchModel> call, Response<HospitalSearchModel> response) {
//                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((HospitalSearchModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("OK")) {
                        hospitalList.clear();
                        hospitalList = response.body().getResults();
                        setMarketSearches();
                    }
                }

            }

            public void onFailure(Call<HospitalSearchModel> call, Throwable th) {
//                mLoadingView.hideDialog();
                Toast.makeText(ActivityVeterinaryClinics.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void setMarketSearches() {
        mPosition = -1;
        searcheHospitalRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 4; i++) {
            medicationList.add(new PetsModel("$45.99", "Ophtha Care",
                    "lorem ipsum is a simply dummy text of printing and type setting industry"));
        }*/
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                mPosition = position;
            }
        };

        searcheHospitalRv.setAdapter(new SearchHospitalAdapter(this, hospitalList, itemClickListener));
    }
}
