package com.pongo.health.ui;


import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.NotificationAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.NotificationResponseModel;
import com.pongo.health.model.PetSizeModel;
import com.pongo.health.model.ProductResponseModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityNotification extends BaseActivity {

    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.tb_tv)
    TextView tbTv;

    @BindView(R.id.notification_rv)
    RecyclerView notificationRv;
    private List<NotificationResponseModel.NotifyBean> notificationList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private CustomDialog mLoadingView;
    private Session mSession;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
        if (InternetConnection.checkConnection(this)) {
            getNotification();
        } else {
            Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
    }

    private void getNotification() {
        this.mLoadingView.show();
        String userId = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getNotifications(userId).enqueue(new Callback<NotificationResponseModel>() {
            public void onResponse(Call<NotificationResponseModel> call, Response<NotificationResponseModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = response.body().getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        notificationList = response.body().getNotify();
                        setNotificationRv();

                    }
                }

            }

            public void onFailure(Call<NotificationResponseModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityNotification.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
        }
    }

    @Override
    protected void setText() {
        tbTv.setText("Notification");

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_notification;
    }


    private void setNotificationRv() {
        notificationRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 4; i++) {
            notificationList.add(new PetSizeModel(R.drawable.rx_img,
                    "Lorem Ipsum dummy text generator\nLorem Ipsum dummy text generator",
                    "Lorem Ipsum"));
        }*/
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                openPopup(notificationList.get(position).getDescription(), notificationList.get(position).getId());
            }
        };
        notificationRv.setAdapter(new NotificationAdapter(this, notificationList, itemClickListener));
    }

    private void openPopup(String header2, String title) {


        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.review_desc_popup, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final TextView closeText = promptsView
                .findViewById(R.id.close);
        TextView descText = promptsView
                .findViewById(R.id.desc);
        TextView titleText = promptsView
                .findViewById(R.id.title);
        descText.setText(header2);
        titleText.setText(title);
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();


        closeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        // show it
        alertDialog.show();
    }

}
