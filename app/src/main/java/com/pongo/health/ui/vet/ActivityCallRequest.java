package com.pongo.health.ui.vet;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.fragment.calling_request.FragmentActionRequired;
import com.pongo.health.fragment.calling_request.FragmentUpcomingAppointments;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityCallRequest extends BaseActivity {

    @BindView(R.id.tab_second_tv)
    TextView upcomingAppointmentsTv;
    @BindView(R.id.tab_one_tv)
    TextView ActionRequiredTv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setWaitingRoomTvColor();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tab_one_tv:
                upcomingAppointmentsTv.setBackground(getResources().getDrawable(R.drawable.button_shaper));
                ActionRequiredTv.setBackground(getResources().getDrawable(R.drawable.rectangle_blue_shape));
                upcomingAppointmentsTv.setTextColor(getResources().getColor(R.color.text_color_blue));
                ActionRequiredTv.setTextColor(getResources().getColor(R.color.white_color));

                getSupportFragmentManager().beginTransaction().
                        replace(R.id.call_request_fl, FragmentActionRequired
                                .newInstance()).commitNow();
                break;
            case R.id.tab_second_tv:
                setWaitingRoomTvColor();
                break;
            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
        }
    }

    private void setWaitingRoomTvColor() {

        ActionRequiredTv.setBackground(getResources().getDrawable(R.drawable.button_shaper));
        upcomingAppointmentsTv.setBackground(getResources().getDrawable(R.drawable.rectangle_blue_shape));
        ActionRequiredTv.setTextColor(getResources().getColor(R.color.text_color_blue));
        upcomingAppointmentsTv.setTextColor(getResources().getColor(R.color.white_color));
        getSupportFragmentManager().beginTransaction().
                replace(R.id.call_request_fl,
                        FragmentUpcomingAppointments.newInstance()).commitNow();
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("Call Request");
        upcomingAppointmentsTv.setText("Upcoming\nAppointments");
        ActionRequiredTv.setText(" Action\nRequired");
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        upcomingAppointmentsTv.setOnClickListener(this);
        ActionRequiredTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_call_request;
    }

}
