package com.pongo.health.ui;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.ExpiryDateTextWatcher;
import com.pongo.health.utils.FourDigitCardFormatWatcher;
import com.pongo.health.utils.InternetConnection;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityAddCard extends BaseActivity {
    boolean isSlash = false; //class level initialization
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.tb_tv)
    TextView tbTv;

    @BindView(R.id.card_number_et)
    EditText cardNumberEt;
    @BindView(R.id.card_holder_name_et)
    EditText cardHolderNameEt;
    @BindView(R.id.card_expiry_date_et)
    EditText cardExpiryDateEt;
    @BindView(R.id.card_cvv_et)
    EditText cardCvvEt;
    @BindView(R.id.blue_btn_tv)
    TextView addCardTv;
    int count = 0;
    private boolean isDelete;
    private CustomDialog mLoadingView;
    private Session mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
//        cardNumberEt.addTextChangedListener(new FourDigitCardFormatWatcher(cardNumberEt));
//        cardExpiryDateEt.addTextChangedListener(new ExpiryDateTextWatcher());
        cardNumberFormat();
        dateFormat();
    }

    private void cardNumberFormat() {
        cardNumberEt.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    int inputlength = cardNumberEt.getText().toString().length();
                    if ((inputlength == 5 ||
                            inputlength == 10 || inputlength == 15)) {
                        cardNumberEt.setText(cardNumberEt.getText().toString()
                                .substring(0, cardNumberEt.getText()
                                        .toString().length() - 1));

                        int pos = cardNumberEt.getText().length();
                        cardNumberEt.setSelection(pos);
                    }
                    isDelete = true;
                }
                return false;
            }
        });
        cardNumberEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) { /*Empty*/}

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) { /*Empty*/ }

            @Override
            public void afterTextChanged(Editable s) {
                int inputlength = cardNumberEt.getText().toString().length();

                if (isDelete) {
                    isDelete = false;
                   /* if (inputlength == 5 ||
                            inputlength == 10 || inputlength == 15) {
                        cardNumberEt.setText(cardNumberEt.getText().toString()
                                .substring(0, cardNumberEt.getText()
                                        .toString().length() - 1));

                        int pos = cardNumberEt.getText().length();
                        cardNumberEt.setSelection(pos);
                    }*/
                    return;
                }

                if (inputlength == 4 ||
                        inputlength == 9 || inputlength == 14) {

                    cardNumberEt.setText(cardNumberEt.getText().toString() + " ");

                    int pos = cardNumberEt.getText().length();
                    cardNumberEt.setSelection(pos);

                } else if (count >= inputlength && (inputlength == 4 ||
                        inputlength == 9 || inputlength == 14)) {
                    cardNumberEt.setText(cardNumberEt.getText().toString()
                            .substring(0, cardNumberEt.getText()
                                    .toString().length() - 2));

                    int pos = cardNumberEt.getText().length();
                    cardNumberEt.setSelection(pos);
                }
//                count = cardNumberEt.getText().toString().length();
            }
        });
    }

    private void dateFormat() {
        cardExpiryDateEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            /*    int len = s.toString().length();

                if (before == 0 && len == 2)
                    cardExpiryDateEt.append("/");*/
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    formatCardExpiringDate(s);
                } catch (NumberFormatException e) {
                    s.clear();
                    //Toast message here.. Wrong date formate

                }
            }
        });
    }

    private void formatCardExpiringDate(Editable s) {
        String input = s.toString();
        String mLastInput = "";

        SimpleDateFormat formatter = new SimpleDateFormat("MM/yy", Locale.ENGLISH);
        Calendar expiryDateDate = Calendar.getInstance();

        try {
            expiryDateDate.setTime(formatter.parse(input));
        } catch (java.text.ParseException e) {
            if (s.length() == 2 && !mLastInput.endsWith("/") && isSlash) {
                isSlash = false;
                int month = Integer.parseInt(input);
                if (month <= 12) {
                    cardExpiryDateEt.setText(cardExpiryDateEt.getText().toString().substring(0, 1));
                    cardExpiryDateEt.setSelection(cardExpiryDateEt.getText().toString().length());
                } else {
                    s.clear();
                    cardExpiryDateEt.setText("");
                    cardExpiryDateEt.setSelection(cardExpiryDateEt.getText().toString().length());
                    Toast.makeText(getApplicationContext(), "Enter a valid month", Toast.LENGTH_LONG).show();
                }
            } else if (s.length() == 2 && !mLastInput.endsWith("/") && !isSlash) {
                isSlash = true;
                int month = Integer.parseInt(input);
                if (month <= 12) {
                    cardExpiryDateEt.setText(cardExpiryDateEt.getText().toString() + "/");
                    cardExpiryDateEt.setSelection(cardExpiryDateEt.getText().toString().length());
                } else if (month > 12) {
//                    edCardDate.setText("");
                    cardExpiryDateEt.setSelection(cardExpiryDateEt.getText().toString().length());
                    s.clear();
//                    _toastMessage("invalid month", context);
                    Toast.makeText(this, "invalid month", Toast.LENGTH_SHORT).show();
                }


            } else if (s.length() == 1) {

                int month = Integer.parseInt(input);
                if (month > 1 && month < 12) {
                    isSlash = true;
                    cardExpiryDateEt.setText("0" + cardExpiryDateEt.getText().toString() + "/");
                    cardExpiryDateEt.setSelection(cardExpiryDateEt.getText().toString().length());
                }
            }

            mLastInput = cardExpiryDateEt.getText().toString();
            return;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                if (InternetConnection.checkConnection(ActivityAddCard.this)) {
                    varifyData();
                } else {
                    Toast.makeText(ActivityAddCard.this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
//                Toast.makeText(this, "Card Added Successfully", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
        }
    }

    private void varifyData() {
        String name = cardHolderNameEt.getText().toString();
        String number = cardNumberEt.getText().toString();
        String cvv = cardCvvEt.getText().toString();
        String cardDate = cardExpiryDateEt.getText().toString();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(this, "Please enter card holder name", Toast.LENGTH_SHORT).show();

        } else if (TextUtils.isEmpty(number)) {
            Toast.makeText(this, "Please enter card number", Toast.LENGTH_SHORT).show();

        } else if (number.length() < 17) {
            Toast.makeText(this, "Please enter valid card number", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(cvv)) {
            Toast.makeText(this, "Please enter cvv number", Toast.LENGTH_SHORT).show();
        } else if (cvv.length() < 3) {
            Toast.makeText(this, "Please enter valid cvv number", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(cardDate)) {
            Toast.makeText(this, "Please enter expiry", Toast.LENGTH_SHORT).show();
        } else if (cardDate.length() < 7) {
            Toast.makeText(this, "Please enter valid expiry date", Toast.LENGTH_SHORT).show();
        } else {
//            Toast.makeText(this, "Card Added Successfully", Toast.LENGTH_SHORT).show();
            addCardApi();
        }
    }

    private void addCardApi() {
        this.mLoadingView.show();
        String userId = mSession.getuser_id();
        String userEmail = mSession.getuser_email();
        String name = cardHolderNameEt.getText().toString();
        String number = cardNumberEt.getText().toString();
        String cvv = cardCvvEt.getText().toString();
        String cardDate = cardExpiryDateEt.getText().toString();
        String splitted[] = cardDate.split("/");
        String month = splitted[0];
        String year = splitted[1];
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).saveCard(userId, userEmail, number, month, year, cvv, name).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(ActivityAddCard.this, message, Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(ActivityAddCard.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivityAddCard.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityAddCard.this, "Failed to add card", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }


    @Override
    protected void setText() {

        addCardTv.setText("Add Card");
        tbTv.setText("Add a Card");

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        addCardTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_add_card;
    }
}
