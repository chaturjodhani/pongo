package com.pongo.health.ui.pet_parent;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.BreedAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.BreedDynamicModel;
import com.pongo.health.model.BreedModel;
import com.pongo.health.model.HomeModel;
import com.pongo.health.ui.ActivityAddCard;
import com.pongo.health.ui.ActivityHome;
import com.pongo.health.ui.ActivityJoinUs;
import com.pongo.health.ui.addpet.ActivityPetSize;
import com.pongo.health.ui.addpet.ActivityPetType;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityGeneralInformationPet extends BaseActivity {

    @BindView(R.id.tb_iv)
    ImageView sTbIv;
    @BindView(R.id.tb_tv)
    TextView sTbTvv;
    @BindView(R.id.firts_name_et)
    EditText firstNameEt;
    @BindView(R.id.birday_et)
    EditText birthdayEt;
    @BindView(R.id.microchip_et)
    EditText microEt;
    @BindView(R.id.animal_type_spin)
    Spinner animalTypeSpin;
    @BindView(R.id.breed_sp)
    TextView breedSpin;
    @BindView(R.id.breed_layout)
    FrameLayout breedFrame;
    @BindView(R.id.sex_sp)
    Spinner genderSpin;
    @BindView(R.id.neutered_sp)
    Spinner neutralSpin;
    @BindView(R.id.save_ll)
    LinearLayout saveLayout;
    private BreedModel modelList;
    private List<BreedDynamicModel> selectionList = new ArrayList<>();
    private String petType = "";
    private String breedSeleceted = "";

    private HomeModel.PetlistBean mPetBean;
    private CustomDialog mLoadingView;
    private Session mSession;
    private boolean isFirstTime = true;
    private DatePickerDialog picker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Gson gson = new Gson();
        mPetBean = gson.fromJson(getIntent().getStringExtra("model"), HomeModel.PetlistBean.class);
        setData();
        birthdayEt.setInputType(InputType.TYPE_NULL);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
        if (InternetConnection.checkConnection(this)) {
            getBreedData();
        } else {
            Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
        birthdayListener();
    }

    private void birthdayListener() {
        birthdayEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(ActivityGeneralInformationPet.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                birthdayEt.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });
    }

    private void setData() {
        if (null != mPetBean) {
            String name = mPetBean.getPet_name();
            String dob = mPetBean.getBirthday();
            String micro = mPetBean.getMicrochipnumber();
            breedSeleceted = mPetBean.getBreed();
            petType = mPetBean.getPet_type();
            if (!TextUtils.isEmpty(name)) {
                firstNameEt.setText(name);
            }
            if (!TextUtils.isEmpty(dob)) {
                birthdayEt.setText(dob);
            }
            if (!TextUtils.isEmpty(breedSeleceted)) {
                breedSpin.setText(breedSeleceted);
            }
            if (!TextUtils.isEmpty(micro)) {
                microEt.setText(micro);
            }
            setAnimalSpin();
            setGenderSpin();
            setNeutredSpin();
        }
    }

    private void setAnimalSpin() {
        ArrayAdapter userAdapter = new ArrayAdapter(this, R.layout.spinner, getResources().getStringArray(R.array.spinner1_enteries));
        userAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        animalTypeSpin.setAdapter(userAdapter);

        if (!TextUtils.isEmpty(mPetBean.getPet_type())) {
            String upperString = mPetBean.getPet_type().substring(0, 1).toUpperCase() + mPetBean.getPet_type().substring(1).toLowerCase();
            int pos = userAdapter.getPosition(upperString);
            animalTypeSpin.setSelection(pos, true);

        }
//            if (!apiCalled) {

        animalTypeSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int sposition, long id) {
//                if (!isFirstTime) {
                breedSeleceted = "";
                breedSpin.setText("Select Breed");
                petType = animalTypeSpin.getSelectedItem().toString().toLowerCase();
//                } else {
//                    isFirstTime = false;
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setGenderSpin() {
        ArrayAdapter userAdapter = new ArrayAdapter(this, R.layout.spinner, getResources().getStringArray(R.array.sex_enterise));
        userAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpin.setAdapter(userAdapter);

        if (!TextUtils.isEmpty(mPetBean.getGender())) {
            int pos = userAdapter.getPosition(mPetBean.getGender());
            genderSpin.setSelection(pos, true);

        }
//            if (!apiCalled) {

    }

    private void setNeutredSpin() {
        ArrayAdapter userAdapter = new ArrayAdapter(this, R.layout.spinner, getResources().getStringArray(R.array.spinner4_enteries));
        userAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        neutralSpin.setAdapter(userAdapter);

        if (!TextUtils.isEmpty(mPetBean.getNeutered())) {
            int pos = userAdapter.getPosition(mPetBean.getNeutered());
            neutralSpin.setSelection(pos, true);

        }
//            if (!apiCalled) {

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                clearActivityStack();
                break;
            case R.id.save_ll:
                verify_data();
                break;
            case R.id.breed_layout:
                if (null != modelList) {
                    if (!TextUtils.isEmpty(petType)) {
                        openPopup();
                    } else {
                        Toast.makeText(this, "please select pet type first", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, "no breed data available", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void setText() {
        sTbTvv.setText("General Information");
    }

    @Override
    protected void setOnClick() {
        sTbIv.setOnClickListener(this);
        breedFrame.setOnClickListener(this);
        saveLayout.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_general_information_pet;
    }

    private void getBreedData() {
        this.mLoadingView.show();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getBreedData().enqueue(new Callback<BreedModel>() {
            public void onResponse(Call<BreedModel> call, Response<BreedModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((BreedModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        modelList = response.body();
                    }
                }

            }

            public void onFailure(Call<BreedModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityGeneralInformationPet.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void openPopup() {
        selectionList.clear();
        if (petType.equalsIgnoreCase("dog")) {
            for (int i = 0; i < modelList.getDogbreed().size(); i++) {
                String type = modelList.getDogbreed().get(i).getType();
                String id = modelList.getDogbreed().get(i).getId();
                String breed = modelList.getDogbreed().get(i).getBreed();
                selectionList.add(new BreedDynamicModel(type, id, breed));
            }
        } else {
            for (int i = 0; i < modelList.getCatbreed().size(); i++) {
                String type = modelList.getCatbreed().get(i).getType();
                String id = modelList.getCatbreed().get(i).getId();
                String breed = modelList.getCatbreed().get(i).getBreed();
                selectionList.add(new BreedDynamicModel(type, id, breed));
            }
        }

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.breed_popup, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final TextView closeText = promptsView
                .findViewById(R.id.close);

        RecyclerView BreedList = promptsView
                .findViewById(R.id.breed_list);
        ItemClickListener itemClickListener;
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        itemClickListener = new ItemClickListener() {

            @Override
            public void onClicked(View view, int postion) {
                breedSeleceted = selectionList
                        .get(postion).getBreed();
                breedSpin.setText(selectionList
                        .get(postion).getBreed());
                alertDialog.dismiss();
            }
        };
        BreedList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        BreedList.setAdapter(new BreedAdapter(this, selectionList, itemClickListener));


        closeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        // show it
        alertDialog.show();
    }

    private void verify_data() {
        String name = firstNameEt.getText().toString();
        String dob = birthdayEt.getText().toString();
        String gender = genderSpin.getSelectedItem().toString();
        if (petType.isEmpty()) {
            Toast.makeText(this, "Please select pet type", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(breedSeleceted)) {
            Toast.makeText(this, "Please select breed", Toast.LENGTH_SHORT).show();

        } else if (TextUtils.isEmpty(dob)) {
            Toast.makeText(this, "Please enter birthday", Toast.LENGTH_SHORT).show();

        } else if (gender.equalsIgnoreCase("Select Gender")) {
            Toast.makeText(this, "Please select gender", Toast.LENGTH_SHORT).show();

        } else {
            updateInformation();
        }
    }

    private void updateInformation() {

        this.mLoadingView.show();
        String userId = mSession.getuser_id();
        String name = firstNameEt.getText().toString();
        String gender = genderSpin.getSelectedItem().toString();
        String dob = birthdayEt.getText().toString();
        String neutred = neutralSpin.getSelectedItem().toString();
        String microchip = microEt.getText().toString();
        if (TextUtils.isEmpty(microchip)) {
            microchip = "";
        }

        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).updatePetInfo(mPetBean.getId(), userId, name, petType, breedSeleceted, gender, dob, neutred, microchip).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
//                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(ActivityGeneralInformationPet.this, "Information updated successfully", Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(ActivityGeneralInformationPet.this, ActivityHome.class);
                                    // set the new task and clear flags
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                    finish();
                                } else {
                                    Toast.makeText(ActivityGeneralInformationPet.this, "something went wrong!", Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivityGeneralInformationPet.this, "something went wrong!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityGeneralInformationPet.this, "Failed to update", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });

    }

}
