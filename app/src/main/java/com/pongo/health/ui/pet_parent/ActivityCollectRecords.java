package com.pongo.health.ui.pet_parent;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.ui.ActivityHome;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityCollectRecords extends BaseActivity {

    @BindView( R.id.no_problem_tv) TextView noProblemTv;
    @BindView(R.id.tb_tv) TextView tbTv;
    @BindView(R.id.tb_iv) ImageView tbIv;
    @BindView(R.id.blue_btn_tv) TextView finishTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.blue_btn_tv:
                startActivity(new Intent(this, ActivityHome.class));
                finish();
                break;
            case R.id.tb_iv:
               clearActivityStack();
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("Medical Records");
        finishTv.setText("Finish");
        noProblemTv.setText(Html.fromHtml("<p>We'll work together to start tracking Robert's" +
                "<br/> health from now on</p>"));
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        finishTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_collect_records;
    }
}
