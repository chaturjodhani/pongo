package com.pongo.health.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.PetListAdapter;
import com.pongo.health.adapter.VeterinaryProfessionalAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.PastChatResponseModel;
import com.pongo.health.model.PetModelResponse;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetDoctorsListActivity extends BaseActivity {

    @BindView(R.id.users_rv)
    RecyclerView userRv;
    @BindView(R.id.search_et)
    EditText searchEt;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    private List<PastChatResponseModel.DetailBean> veterinaryList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private CustomDialog mLoadingView;
    private Session mSession;
    private List<PetModelResponse.DataBean> mPetList = new ArrayList<>();
    private int mSelectedPet = -1;
    VeterinaryProfessionalAdapter veterinaryProfessionalAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        removeTaskBar();
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
        if (InternetConnection.checkConnection(this)) {
            getpetList();
            getUserList("");
        } else {
            Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
        }

        setSearchListener();
    }

    private void setSearchListener() {

        searchEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String search = searchEt.getText().toString();
                    getUserList(search);
                    return true;
                }
                return false;
            }
        });
    }

    private void setVeterinaryList() {
        userRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                if (InternetConnection.checkConnection(GetDoctorsListActivity.this)) {
                    if (mPetList.size() > 0) {
                        openPopup(position);
                    } else {
                        Toast.makeText(GetDoctorsListActivity.this, "Add pet first", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(GetDoctorsListActivity.this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
            }
        };
        veterinaryProfessionalAdapter = new VeterinaryProfessionalAdapter(this, veterinaryList, itemClickListener);
        userRv.setAdapter(veterinaryProfessionalAdapter);
    }

    private void createChat(int position) {
        this.mLoadingView.show();
        PastChatResponseModel.DetailBean detailBean = veterinaryList.get(position);
        String userId = mSession.getuser_id();
        String chatId = detailBean.getChat_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).createChat(userId, chatId, mPetList.get(mSelectedPet).getId(), "", userId).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    if (message.equalsIgnoreCase("chat start")) {
                                        String chatid = jsonObject.getString("chatid");
                                        PastChatResponseModel.DetailBean detailBean = veterinaryList.get(position);
                                        Intent intent = new Intent(GetDoctorsListActivity.this, ChatViewActivity.class);
                                        intent.putExtra("chat_id", detailBean.getChat_id());
                                        intent.putExtra("name", detailBean.getFirst_name() + " " + detailBean.getLast_name());
                                        intent.putExtra("profileimage", detailBean.getUserimage());
                                        intent.putExtra("status", detailBean.getChatstatus());
                                        intent.putExtra("chatid", chatid);
                                        intent.putStringArrayListExtra("token", (ArrayList<String>) detailBean.getToken());
                                        startActivity(intent);

                                    } else {
                                        Toast.makeText(GetDoctorsListActivity.this, message, Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                    Toast.makeText(GetDoctorsListActivity.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(GetDoctorsListActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(GetDoctorsListActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }


    private void getUserList(String key) {
        this.mLoadingView.show();
        if (null != veterinaryProfessionalAdapter) {
            veterinaryList.clear();
            veterinaryProfessionalAdapter.notifyDataSetChanged();
        }
        String type = getIntent().getStringExtra("type");
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getUserList(type, key).enqueue(new Callback<PastChatResponseModel>() {
            public void onResponse(Call<PastChatResponseModel> call, Response<PastChatResponseModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((PastChatResponseModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        veterinaryList = response.body().getDetail();
                        setVeterinaryList();
                    }
                }

            }

            public void onFailure(Call<PastChatResponseModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(GetDoctorsListActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                clearActivityStack();
                break;
        }

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_get_doctors_list;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        String name = getIntent().getStringExtra("type");
        if (!TextUtils.isEmpty(name) && name.equalsIgnoreCase("online")) {
            tbTv.setText("First available Veterinary professional");
        } else {
            tbTv.setText("Veterinary professional by Name or Expertise");

        }
//        searchEt.setHint("Type your vet name");
        searchEt.setHint("Search by Name");

    }

    private void getpetList() {
        String userId = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getPets(userId).enqueue(new Callback<PetModelResponse>() {
            public void onResponse(Call<PetModelResponse> call, Response<PetModelResponse> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    try {
                        String status = response.body().getStatus();
                        if (!TextUtils.isEmpty(status)) {
                            if (status.equalsIgnoreCase("success")) {
                                mPetList = response.body().getData();
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<PetModelResponse> call, Throwable th) {
                Log.e("cate", th.toString());

            }
        });
    }

    private void openPopup(int position) {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(GetDoctorsListActivity.this);
        View promptsView = li.inflate(R.layout.pet_list_popup, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                GetDoctorsListActivity.this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        alertDialogBuilder.setCancelable(true);

        RecyclerView petList = promptsView
                .findViewById(R.id.pet_list);
        TextView continueTv = promptsView
                .findViewById(R.id.blue_btn_tv);
        ItemClickListener itemClickListener;
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        itemClickListener = new ItemClickListener() {

            @Override
            public void onClicked(View view, int postion) {
//                alertDialog.dismiss();
                mSelectedPet = postion;
            }
        };
        petList.setLayoutManager(new LinearLayoutManager(GetDoctorsListActivity.this, RecyclerView.VERTICAL, false));

        petList.setAdapter(new PetListAdapter(GetDoctorsListActivity.this, mPetList, itemClickListener));


        continueTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelectedPet >= 0) {
                    alertDialog.dismiss();
                    createChat(position);
                } else {
                    Toast.makeText(GetDoctorsListActivity.this, "Please select pet first", Toast.LENGTH_SHORT).show();
                }
            }
        });
        // show it
        alertDialog.show();
    }
}
