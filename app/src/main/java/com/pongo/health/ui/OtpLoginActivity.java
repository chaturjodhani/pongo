package com.pongo.health.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mukesh.OtpView;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.UserLoginModel;
import com.pongo.health.ui.vet.ActivityTechnician;
import com.pongo.health.ui.vet.ActivityVetDashboard;
import com.google.gson.Gson;
import com.pongo.health.utils.GPSTracker;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpLoginActivity extends BaseActivity {
    @BindView(R.id.v_tb_iv)
    ImageView otpBackIv;
    @BindView(R.id.blue_btn_tv)
    TextView otpSubmitTv;
    @BindView(R.id.phone_txt)
    TextView phoneTv;
    @BindView(R.id.otp_one_et)
    EditText otpEd1;
    @BindView(R.id.otp_two_et)
    EditText otpEd2;
    @BindView(R.id.otp_three_et)
    EditText otpEd3;
    @BindView(R.id.otp_four_et)
    EditText otpEd4;
    @BindView(R.id.otp_view)
    OtpView otpView;
    private UserLoginModel otpModel;
    private String mLoginType;
    private String mMobile;
    private String mPassword;
    private String mEmail;
    private Session mSession;
    private GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        removeTaskBar();
        ButterKnife.bind(this);
        mSession = new Session(this);
        Gson gson = new Gson();
        gps = new GPSTracker(this);
        otpModel = gson.fromJson(getIntent().getStringExtra("otp"), UserLoginModel.class);
        mLoginType = getIntent().getStringExtra("login_type");
        mMobile = getIntent().getStringExtra("phone");
        String mFormat = getIntent().getStringExtra("format");
        mEmail = getIntent().getStringExtra("email");
        mPassword = getIntent().getStringExtra("password");
        if (!TextUtils.isEmpty(mMobile)) {
            phoneTv.setText(mFormat);
        } else {
            phoneTv.setText(" " + mEmail);

        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_tb_iv:
                clearActivityStack();
                break;
            case R.id.blue_btn_tv:
                checkOtp();
                break;
        }
    }

    private void checkOtp() {
       /* String otp1 = otpEd1.getText().toString();
        String otp2 = otpEd2.getText().toString();
        String otp3 = otpEd3.getText().toString();
        String otp4 = otpEd4.getText().toString();
        String otpText = otp1 + otp2 + otp3 + otp4;*/
        String otpText = otpView.getText().toString();

       /* if (TextUtils.isEmpty(otp1) || TextUtils.isEmpty(otp2) || TextUtils.isEmpty(otp3) || TextUtils.isEmpty(otp4)) {
            Toast.makeText(this, "Please enter otp", Toast.LENGTH_SHORT).show();
        } else {*/
        String type = otpModel.getUserdetail().getVet_type();
        String userId = otpModel.getUserdetail().getId();
        if (otpText.equalsIgnoreCase(String.valueOf(otpModel.getOtp()))) {
            Intent intent;
//            if (mLoginType.equalsIgnoreCase("petparent")) {
            if (TextUtils.isEmpty(type)) {
                intent = new Intent(OtpLoginActivity.this, ActivityHome.class);
                mSession.setuser_id(userId);
                mSession.setuser_login(true);
                mSession.setfirt_name(otpModel.getUserdetail().getFirst_name());
                mSession.setlast_name(otpModel.getUserdetail().getLast_name());
                mSession.setuser_email(otpModel.getUserdetail().getEmail());
                mSession.setuser_type(otpModel.getUserdetail().getVet_type());
                mSession.setuserPaid(otpModel.getUserdetail().getUsertype());
                mSession.setuser_phone(mMobile);
                changeStatus("1");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
               /* else {
                    Toast.makeText(OtpLoginActivity.this, "Failed to login", Toast.LENGTH_SHORT).show();
                }
            } */
            else {
//                if (!TextUtils.isEmpty(type)) {
                if ("vet".equals(type)) {
                    intent = new Intent(OtpLoginActivity.this, ActivityVetDashboard.class);
                } else {
                    intent = new Intent(OtpLoginActivity.this, ActivityTechnician.class);
                }
                mSession.setuser_id(userId);
                mSession.setuser_login(true);
                mSession.setfirt_name(otpModel.getUserdetail().getFirst_name());
                mSession.setlast_name(otpModel.getUserdetail().getLast_name());
                mSession.setuser_email(otpModel.getUserdetail().getEmail());
                mSession.setuser_type(otpModel.getUserdetail().getVet_type());
                mSession.setuserPaid(otpModel.getUserdetail().getUsertype());
                mSession.setuser_phone(mMobile);
                changeStatus("1");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
               /* } else {
                    Toast.makeText(OtpLoginActivity.this, "Failed to login", Toast.LENGTH_SHORT).show();
                }*/

            }


        } else {
            Toast.makeText(this, "otp not match", Toast.LENGTH_SHORT).show();

        }
//        }

    }

    @Override
    protected void setText() {
        otpSubmitTv.setText(R.string.submit);
    }

    @Override
    protected void setOnClick() {
        otpBackIv.setOnClickListener(this);
        otpSubmitTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_otp_login;
    }

    private void changeStatus(String status) {
        String userId = mSession.getuser_id();
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();
        if (!TextUtils.isEmpty(userId)) {
            ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).onlineStatus(userId, status, "" + latitude, "" + longitude).enqueue(new Callback<ResponseBody>() {
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.body() != null) {
                        String responseData = "";
                        try {
                            responseData = response.body().string();
                            if (!TextUtils.isEmpty(responseData)) {
                                JSONObject jsonObject = new JSONObject(responseData);
                               /* String status = jsonObject.getString("status");
                                String message = jsonObject.getString("detail");
                                if (!TextUtils.isEmpty(status)) {
                                    if (status.equalsIgnoreCase("success")) {

                                    }
                                }*/
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                public void onFailure(Call<ResponseBody> call, Throwable th) {
                    Log.e("cate", th.toString());
                }
            });
        }
    }

}
