package com.pongo.health.ui;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.TimeSlotAdapter;
import com.pongo.health.adapter.VetTimeSlotAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.github.jhonnyx2012.horizontalpicker.DatePickerListener;
import com.github.jhonnyx2012.horizontalpicker.HorizontalPicker;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.pongo.health.model.TimeslotModel;
import com.pongo.health.model.VetTimeSlotModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import org.joda.time.DateTime;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AvailableActivity extends BaseActivity implements DatePickerListener {
    @BindView(R.id.blue_btn_tv)
    TextView updateTv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.toggle)
    RadioGroup radioGroup1;
    @BindView(R.id.datePicker)
    HorizontalPicker datePicker;
    @BindView(R.id.barchart)
    BarChart chart;
    @BindView(R.id.time_rv)
    RecyclerView timeRv;
    String bookDate = "";
    private List<VetTimeSlotModel.TimeslotsBean> timeList = new ArrayList<>();
    private CustomDialog mLoadingView;
    private Session mSession;
    private String timeSlot = "";
    private final String[] mDates = new String[]{
            "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"
    };
    VetTimeSlotAdapter vetTimeSlotAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
        datePicker.showTodayButton(false);
        datePicker.setTodayButtonTextColor(Color.TRANSPARENT);
//        datePicker.setDate(new DateTime().plusDays(1));
        datePicker.setDate(new DateTime());
        datePicker.setBackgroundColor(Color.TRANSPARENT);
        graphInit();
    }

    private void graphInit() {
        chart.getAxisRight().setDrawGridLines(true);
        chart.getAxisRight().setEnabled(false);
        chart.getAxisLeft().setDrawGridLines(true);
        chart.getXAxis().setDrawGridLines(true);
        chart.setTouchEnabled(false);
        chart.getAxisLeft().setTextColor(Color.BLACK); // left y-axis
        chart.getXAxis().setTextColor(Color.WHITE);
        chart.getLegend().setTextColor(Color.WHITE);
        chart.getDescription().setEnabled(false);
        chart.getLegend().setEnabled(false);   // Hide the legend
        chart.setExtraOffsets(0, 0, 0, 15);
//        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
//        ValueFormatter xAxisFormatter = new DayAxisValueFormatter(chart);

//        xAxis.setTypeface(tfLight);
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setDrawGridLines(false);
//        xAxis.setGranularity(-1f); // only intervals of 1 day
        xAxis.setLabelCount(5);
//        xAxis.setValueFormatter(xAxisFormatter);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(getDate()));
      /*  XYMarkerView mv = new XYMarkerView(this, xAxisFormatter);
        mv.setChartView(chart); // For bounds control
        chart.setMarker(mv); // Set the marker to the chart*/
        setData();
    }

    public ArrayList<String> getDate() {

        return new ArrayList<>(Arrays.asList(mDates));
    }

    private void setData() {
        ArrayList<BarEntry> bargroup1 = new ArrayList<>();
        bargroup1.add(new BarEntry(5f, 1));
        bargroup1.add(new BarEntry(3f, 2));
        bargroup1.add(new BarEntry(2f, 3));
        bargroup1.add(new BarEntry(2f, 4));
        bargroup1.add(new BarEntry(1f, 5));
        bargroup1.add(new BarEntry(3f, 6));
        bargroup1.add(new BarEntry(4f, 7));
        bargroup1.add(new BarEntry(3f, 8));
        bargroup1.add(new BarEntry(2f, 9));
        bargroup1.add(new BarEntry(0f, 10));
        bargroup1.add(new BarEntry(1f, 11));


// creating dataset for Bar Group1
        BarDataSet barDataSet1 = new BarDataSet(bargroup1, "");

//barDataSet1.setColor(Color.rgb(0, 155, 0));
//        barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);
        barDataSet1.setColors(getResources().getColor(R.color.grey_text_color));

        ArrayList<BarDataSet> dataSets = new ArrayList<>();  // combined all dataset into an arraylist
        dataSets.add(barDataSet1);

// initialize the Bardata with argument labels and dataSet
        BarData data = new BarData(barDataSet1);
//        BarData data = new BarData( bardataset);
        chart.setData(data); // set the data and list of labels into chart
        chart.animateY(1000);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("Availability");
        updateTv.setText("Update");

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        datePicker.setListener(this).init();

    }

    @Override
    public void onDateSelected(DateTime dateSelected) {
        //  Toast.makeText(this, "Selected date is " + dateSelected.toString(), Toast.LENGTH_SHORT).show();
        try {
            SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
            SimpleDateFormat dateFormatter = new SimpleDateFormat("YYYY-MM-dd");
            Date date = dateParser.parse(dateSelected.toString());
            String dt = dateFormatter.format(date);
            if (InternetConnection.checkConnection(this)) {
                bookDate = dt;
                getTimeSlot(dt);
            } else {
                Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_available;
    }

    private void getTimeSlot(String date) {
        timeSlot = "";
        this.mLoadingView.show();
        if (null!=vetTimeSlotAdapter)
        {
            timeList.clear();
            vetTimeSlotAdapter.notifyDataSetChanged();
        }
        String vetid = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getVetTimeSlot(vetid, date).enqueue(new Callback<VetTimeSlotModel>() {
            public void onResponse(Call<VetTimeSlotModel> call, Response<VetTimeSlotModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((VetTimeSlotModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        timeList = response.body().getTimeslots();
                        setTime();
                    }
                }

            }

            public void onFailure(Call<VetTimeSlotModel> call, Throwable th) {
                timeList = new ArrayList<>();
                setTime();
                mLoadingView.hideDialog();
                Toast.makeText(AvailableActivity.this, "No time available", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }


    private void setTime() {
        timeRv.setLayoutManager(new GridLayoutManager(this, 5));
//        timeRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
      /*  timeList.add("05:00pm");
        timeList.add("06:00pm");
        timeList.add("07:00pm");
        timeList.add("08:00pm");*/


        // strengthTv.setText(strengthList.get(postion));
        ItemClickListener itemClickListener = new ItemClickListener() {

            @Override
            public void onClicked(View view, int postion) {
                // strengthTv.setText(strengthList.get(postion));
                timeSlot = timeList.get(postion).getStart();
                updateTimeSlot(postion);
            }
        };
        vetTimeSlotAdapter = new VetTimeSlotAdapter(this, timeList, itemClickListener);
        timeRv.setAdapter(vetTimeSlotAdapter);

    }

    private void updateTimeSlot(int postion) {
        this.mLoadingView.show();
        String userid = mSession.getuser_id();
        String status = "";
        if (timeList.get(postion).isStatus()) {
            status = "false";
        } else {
            status = "true";
        }
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).updateVetTimeSlot(userid, timeSlot, status, bookDate).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(AvailableActivity.this, message, Toast.LENGTH_SHORT).show();
                                    if (timeList.get(postion).isStatus()) {
                                        timeList.get(postion).setStatus(false);
                                        vetTimeSlotAdapter.notifyDataSetChanged();
                                    } else {
                                        timeList.get(postion).setStatus(true);
                                        vetTimeSlotAdapter.notifyDataSetChanged();
                                    }
                                } else {
                                    Toast.makeText(AvailableActivity.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(AvailableActivity.this, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(AvailableActivity.this, "Failed to update", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

}

