package com.pongo.health.ui.addpet;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.ui.ActivityCart;
import com.pongo.health.utils.CustomDialog;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityHaveAnyOtherPet extends BaseActivity {

    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.no_btn_tv)
    TextView noBtnTv;
    @BindView(R.id.yes_btn_tv)
    TextView yesBtnTv;
    private CustomDialog mLoadingView;
    private Session mSession;
    String otherPet = "";
    private String petname;
    private String petimage;
    private String petType = "";
    private String petSize = "";
    private String petAge = "";
    private String pethealthGoal = "";
    private String recommendId = "";
    private String breedSeleceted = "";
    private String existing_status = "";
    private String prescription_from = "";
    private String transfer_pharmacy_name = "";
    private String primary_hospital_name = "";
    private String have_allergy = "";
    private String other_medication = "";
    private String medical_condition = "";
    private String specify = "";
    private ArrayList<String> imagelist = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
        Intent intent = getIntent();
        if (null != intent) {
            petname = intent.getStringExtra("pet_name");
            petimage = intent.getStringExtra("pet_image");
            petType = intent.getStringExtra("pet_type");
            petSize = intent.getStringExtra("pet_size");
            petAge = intent.getStringExtra("pet_age");
            pethealthGoal = intent.getStringExtra("pet_health");
            recommendId = intent.getStringExtra("recommendId");
            breedSeleceted = intent.getStringExtra("breed");
            existing_status = getIntent().getStringExtra("existing_status");
            if (!TextUtils.isEmpty(existing_status) && existing_status.equalsIgnoreCase("yes")) {
                prescription_from = intent.getStringExtra("prescription_from");
                imagelist = (ArrayList<String>) intent.getSerializableExtra("imagelist");
                transfer_pharmacy_name = intent.getStringExtra("transfer_pharmacy_name");
                primary_hospital_name = intent.getStringExtra("primary_hospital_name");
                have_allergy = intent.getStringExtra("have_allergy");
                other_medication = intent.getStringExtra("other_medication");
                medical_condition = intent.getStringExtra("medical_condition");
                specify = intent.getStringExtra("specify");
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                goBack();
                break;

            case R.id.yes_btn_tv:
                otherPet = "yes";
                yesBtnTv.setBackground(getResources().getDrawable(R.drawable.button_shaper_selected));
                yesBtnTv.setTextColor(getResources().getColor(R.color.white_color));
                noBtnTv.setBackground(getResources().getDrawable(R.drawable.button_shaper));
                noBtnTv.setTextColor(getResources().getColor(R.color.signup_color_blue));
                addPet();
                break;
            case R.id.no_btn_tv:
                otherPet = "no";
                yesBtnTv.setBackground(getResources().getDrawable(R.drawable.button_shaper));
                yesBtnTv.setTextColor(getResources().getColor(R.color.signup_color_blue));
                noBtnTv.setBackground(getResources().getDrawable(R.drawable.button_shaper_selected));
                noBtnTv.setTextColor(getResources().getColor(R.color.white_color));
                addPet();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        Intent intent = new Intent(ActivityHaveAnyOtherPet.this, ActivityAddPetRecommendation.class);
        intent.putExtra("pet_name", petname);
        intent.putExtra("pet_image", petimage);
        intent.putExtra("pet_type", petType);
        intent.putExtra("pet_size", petSize);
        intent.putExtra("pet_age", petAge);
        intent.putExtra("pet_health", pethealthGoal);
        intent.putExtra("breed", breedSeleceted);
        startActivity(intent);
        finish();
    }

    @Override
    protected void setText() {
        tbTv.setText(R.string.add_pet);

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        yesBtnTv.setOnClickListener(this);
        noBtnTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_add_pet_any_other;
    }

    private void addPet() {
        mLoadingView.show();

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", mSession.getuser_id());
        builder.addFormDataPart("pet_name", petname);
        builder.addFormDataPart("pet_type", petType.toLowerCase());
        builder.addFormDataPart("pet_size", petSize.toLowerCase());
        builder.addFormDataPart("pet_old", petAge.toLowerCase());
        builder.addFormDataPart("helath_goal", pethealthGoal.toLowerCase());
        builder.addFormDataPart("recommended_iem_id", recommendId);
        builder.addFormDataPart("another_pet", otherPet);
        builder.addFormDataPart("breed", breedSeleceted);
        builder.addFormDataPart("existing_status", existing_status);
        builder.addFormDataPart("prescription_from", prescription_from);
        builder.addFormDataPart("transfer_pharmacy_name", transfer_pharmacy_name);
        builder.addFormDataPart("primary_hospital_name", primary_hospital_name);
        builder.addFormDataPart("have_allergy",have_allergy);
        builder.addFormDataPart("other_medication",other_medication);
        builder.addFormDataPart("medical_condition",medical_condition);
        builder.addFormDataPart("specify",specify);
        File file = new File(petimage);
        builder.addFormDataPart("pet_image", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
        if (imagelist != null && imagelist.size() > 0) {
            for (int i = 0; i < imagelist.size(); i++) {
                if (imagelist.get(i) != null) {
                    String model = imagelist.get(i);
                    File files = new File(model);
                    builder.addFormDataPart("click_image[]", files.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), files));
                }
            }
        } else {
            builder.addFormDataPart("click_image[]", "");
        }

        MultipartBody requestBody = builder.build();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.addNewPet(requestBody);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
//                            String message = jsonObject.getString("message");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    if (otherPet.equalsIgnoreCase("yes")) {
                                        Intent a = new Intent(ActivityHaveAnyOtherPet.this, ActivityPetName.class);
//                                        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(a);
                                        finish();
                                    } else {
                                        if (!TextUtils.isEmpty(recommendId)) {
                                            Intent i = new Intent(ActivityHaveAnyOtherPet.this, ActivityCart.class);
                                            i.putExtra("pet_name", petname);
                                            i.putExtra("pet_image", petimage);
                                            i.putExtra("screen", "addpet");
                                            // set the new task and clear flags
//                                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(i);
                                            finish();
                                        } else {
                                            Intent i = new Intent(ActivityHaveAnyOtherPet.this, ActivityAddingPet.class);
                                            i.putExtra("pet_name", petname);
                                            i.putExtra("pet_image", petimage);
                                            // set the new task and clear flags
//                                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(i);
                                            finish();
                                        }
                                    }


                                } else {
                                    Toast.makeText(ActivityHaveAnyOtherPet.this, "Failed to add pet", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(ActivityHaveAnyOtherPet.this, "Failed to add pet", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityHaveAnyOtherPet.this, "Failed to add pet", Toast.LENGTH_SHORT).show();
                Log.e("cate", t.toString());
            }

        });

    }

}
