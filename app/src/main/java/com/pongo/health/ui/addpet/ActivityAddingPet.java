package com.pongo.health.ui.addpet;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.ui.ActivityCart;
import com.pongo.health.ui.ActivityHome;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityAddingPet extends BaseActivity {
    @BindView(R.id.blue_btn_tv)
    TextView nextTv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.adding_pet_text)
    TextView petnameTv;
    @BindView(R.id.pet_img)
    CircleImageView petImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        if (null != intent) {
            String petname = intent.getStringExtra("pet_name");
            String petimage = intent.getStringExtra("pet_image");
            petnameTv.setText("Adding " + petname + " to our Pongo\nHealth Family!");
            Glide.with(this).load(petimage).apply(RequestOptions.circleCropTransform()).into(petImage);

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                goBack();
                break;

            case R.id.blue_btn_tv:
                goBack();
                break;
        }
    }

    @Override
    protected void setText() {
        tbTv.setText(R.string.add_pet);
        nextTv.setText("Ok");
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        nextTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_adding_pet;
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        Intent i = new Intent(ActivityAddingPet.this, ActivityHome.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }
}
