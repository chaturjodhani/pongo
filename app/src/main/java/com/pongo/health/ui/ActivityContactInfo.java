package com.pongo.health.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityContactInfo extends BaseActivity {

    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.blue_btn_tv)
    TextView submitTv;
    @BindView(R.id.firts_name_et)
    EditText firstNameEt;
    @BindView(R.id.last_name_et)
    EditText lastNameEt;
    @BindView(R.id.email_et)
    EditText emailEt;
    @BindView(R.id.contact_et)
    EditText phoneEt;
    private Session mSession;
    private String str_firstname, str_lastname, str_email, str_phone;
    private CustomDialog mLoadingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);
        setData();
    }

    private void setData() {
        firstNameEt.setText(mSession.getfirst_name());
        lastNameEt.setText(mSession.getlast_name());
        emailEt.setText(mSession.getuser_email());
        phoneEt.setText(mSession.getuser_phone());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.blue_btn_tv:
                if (InternetConnection.checkConnection(this)) {
                    verify_data();
                } else {
                    Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
//                Toast.makeText(this, "Submit", Toast.LENGTH_SHORT).show();
                break;
        }

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("Contact Info");
        submitTv.setText(R.string.submit);


    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        submitTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_contact_info;
    }


    private void verify_data() {
//        String team = spin_team.getSelectedItem().toString();
        str_firstname = firstNameEt.getText().toString().trim();
        str_lastname = lastNameEt.getText().toString().trim();
        str_email = emailEt.getText().toString().trim();
        str_phone = phoneEt.getText().toString().trim();


        if (str_firstname.isEmpty() && str_email.isEmpty() && str_lastname.isEmpty() && str_phone.isEmpty()) {
            Toast.makeText(this, "Please enter detail in all fields", Toast.LENGTH_SHORT).show();

        } else if (str_firstname.isEmpty()) {
            firstNameEt.requestFocus();
            firstNameEt.setError("Please enter your first name");

        } else if (str_lastname.isEmpty()) {
            lastNameEt.requestFocus();
            lastNameEt.setError("Please enter your last name");

        } else if (str_email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(str_email).matches()) {
            emailEt.requestFocus();
            emailEt.setError("Please enter your valid email");

        } else if (str_phone.isEmpty()) {
            phoneEt.requestFocus();
            phoneEt.setError("Please enter your mobile number");

        } else {
            if (InternetConnection.checkConnection(this)) {
                updateProfile();
            } else {
                Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void updateProfile() {
        this.mLoadingView.show();
        String userid = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).updateContact(userid, str_firstname, str_lastname, str_email, str_phone).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(ActivityContactInfo.this, message, Toast.LENGTH_SHORT).show();
                                    mSession.setfirt_name(str_firstname);
                                    mSession.setlast_name(str_lastname);
                                    mSession.setuser_email(str_email);
                                    mSession.setuser_phone(str_phone);
                                } else {
                                    Toast.makeText(ActivityContactInfo.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivityContactInfo.this, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityContactInfo.this, "Failed to connect", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }
}
