package com.pongo.health.ui.vet;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pongo.health.BuildConfig;
import com.pongo.health.R;
import com.pongo.health.adapter.ImagesAdapter;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.OtpModel;
import com.pongo.health.ui.ActivityAddPrimaryHospitalNew;
import com.pongo.health.utils.Constants;
import com.google.gson.Gson;
import com.pongo.health.utils.ItemClickListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityIdentification extends BaseActivity {

    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.vet_heading_one_tv)
    TextView vetHeadingOneTv;
    @BindView(R.id.vet_heading_second_tv)
    TextView vetHeadingSecondTv;
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.pic_upload_iv)
    ImageView picUploadIv;
    String mCurrentPhotoPath;
    @BindView(R.id.image_rv)
    RecyclerView imagesRv;
    private ArrayList imageList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private ImagesAdapter mImagesAdapter;
    private String vetType = "";
    private OtpModel otpModel;
    private String mLoginType;
    private String mMobile;
    private String str_firstname, str_lastname;
    private String mPassword;
    private String mEmail;
    private String mDesc;
    private String mExpertice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        Gson gson = new Gson();
        otpModel = gson.fromJson(getIntent().getStringExtra("otp"), OtpModel.class);
        mLoginType = getIntent().getStringExtra("login_type");
        mMobile = getIntent().getStringExtra("phone");
        mEmail = getIntent().getStringExtra("email");
        mPassword = getIntent().getStringExtra("password");
        str_firstname = getIntent().getStringExtra("first_name");
        str_lastname = getIntent().getStringExtra("last_name");
        vetType = getIntent().getStringExtra("vetType");
        mExpertice = getIntent().getStringExtra("desc");
        mDesc = getIntent().getStringExtra("expertise");
        vetHeadingOneTv.setText("Hi " + str_firstname + " " + str_lastname + ", \nPlease add a valid " +
                "form of identification");

        setAdapter();
    }

    private void setAdapter() {
        imagesRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                imageList.remove(position);
                mImagesAdapter.notifyDataSetChanged();
            }
        };
//        itemClickListener = (view, position) -> startActivity(new Intent(activity, ActivityYourPetSingleType.class));
        mImagesAdapter = new ImagesAdapter(this, imageList, itemClickListener);
        imagesRv.setAdapter(mImagesAdapter);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.blue_btn_tv:
                verify_data();
//                startActivity(new Intent(this, ActivityAddPrimaryHospitalNew.class));
                break;
            case R.id.pic_upload_iv:
                if (!checkPermission()) {
                    selectImage();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            102);
                }

                break;
        }
    }

    private boolean checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
        ) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 102) {
            if (grantResults.length > 0) {

                boolean recordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (recordAccepted) {
                    selectImage();
                }
            }
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (cameraIntent.resolveActivity(getPackageManager()) != null) {
// Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
// Error occurred while creating the File
                        }
// Continue only if the File was successfully created
                        if (photoFile != null) {
                            Uri outputFileUri = FileProvider.getUriForFile(ActivityIdentification.this, BuildConfig.APPLICATION_ID, photoFile);
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                            startActivityForResult(cameraIntent, 0);
                        }
                    }


                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:


                    if (resultCode == Activity.RESULT_OK) {

                        File file = new File(Objects.requireNonNull(mCurrentPhotoPath));
                        String fileName = Constants.compressImage(mCurrentPhotoPath, ActivityIdentification.this);
                        Bitmap bitmap = BitmapFactory.decodeFile(fileName);
                        bitmap = getResizedBitmap(bitmap, 150);
                        String path = mCurrentPhotoPath;
//                        res = path;
//                        picUploadIv.setImageBitmap(bitmap);
                        imageList.add(path);
                        mImagesAdapter.notifyDataSetChanged();

                    }

                    break;

                case 1:

                    if (resultCode == RESULT_OK && data != null) {

                        Uri selectedImage = data.getData();

                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(ActivityIdentification.this.getContentResolver(), selectedImage);
                            bitmap = getPath(selectedImage);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    break;
            }
        }
    }

    private Bitmap getPath(Uri selectedImage) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Log.e("Before", "Creating cursor");
        Cursor cursor = ActivityIdentification.this.managedQuery(selectedImage, projection, null, null, null);
        Log.e("After", "Creating cursor");
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        if (cursor.getString(column_index) != null) {
            String res = cursor.getString(column_index);
            Log.e("Selected Image Path", res);
            Log.e("After", "Closing cursor");
            imageList.add(res);
            mImagesAdapter.notifyDataSetChanged();
            Bitmap bitmap = BitmapFactory.decodeFile(res);
//        picUploadIv.setImageBitmap(bitmap);
//        Glide.with(this).load(res).apply(RequestOptions.circleCropTransform()).into(picUploadIv);

            return bitmap;
        } else {
            return null;
        }

    }

    private File createImageFile() throws IOException {
// Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName, // prefix
                ".jpg", // suffix
                storageDir // directory
        );

// Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();

        return image;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void verify_data() {
        if (imageList.size() > 0) {
            Intent intent = new Intent(ActivityIdentification.this, ActivityAddPrimaryHospitalNew.class);
            Gson gson = new Gson();
            String myJson = gson.toJson(otpModel);
            intent.putExtra("otp", myJson);
            intent.putExtra("login_type", mLoginType);
            intent.putExtra("phone", mMobile);
            intent.putExtra("email", mEmail);
            intent.putExtra("password", mPassword);
            intent.putExtra("first_name", str_firstname);
            intent.putExtra("last_name", str_lastname);
            intent.putExtra("vetType", vetType);
            intent.putExtra("expertise", mExpertice);
            intent.putExtra("desc", mDesc);
            intent.putExtra("img_identification", imageList);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Please add a valid identification", Toast.LENGTH_SHORT).show();
        }

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {

        vetHeadingSecondTv.setText("We need to verify your identity." +
                " Valid forms of identification include: a passport, State ID, or a drivers license");
        continueTv.setText(R.string.continue_);

    }

    @Override
    protected void setOnClick() {
        continueTv.setOnClickListener(this);
        picUploadIv.setOnClickListener(this);
        vTbIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_identification;
    }
}
