package com.pongo.health.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RequestDoneActivity extends BaseActivity {

    @BindView(R.id.blue_btn_tv)
    TextView submitTv;
    @BindView(R.id.hospital)
    TextView hospitalTv;
    @BindView(R.id.name)
    TextView nameTv;
    private String hospital = "";
    private String name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        submitTv.setText("Sounds good");
        Intent intent = getIntent();
        if (null != intent) {
            hospital = intent.getStringExtra("hospital");
            name = intent.getStringExtra("name");
            hospitalTv.setText(hospital);
//            nameTv.setText("We will contact " + name + "'s Vet at");
            nameTv.setText("We will contact your pet's Vet at");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                Intent i = new Intent(RequestDoneActivity.this, ActivityHome.class);
                // set the new task and clear flags
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
                break;
        }

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {


    }

    @Override
    protected void setOnClick() {
        submitTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_request_done;
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}