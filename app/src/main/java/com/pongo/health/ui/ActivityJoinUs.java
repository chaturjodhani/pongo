package com.pongo.health.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.LocationsAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.OtpModel;
import com.pongo.health.utils.Constants;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.GPSTracker;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityJoinUs extends BaseActivity {

    @BindView(R.id.blue_btn_tv)
    TextView addContinueTv;
    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.location_tv)
    TextView locationTv;
    @BindView(R.id.firts_name_et)
    EditText firstNameEt;
    @BindView(R.id.last_name_et)
    EditText lastNameEt;
    @BindView(R.id.email_et)
    EditText emailEt;
    @BindView(R.id.location_rv)
    RecyclerView locationRv;

    private List<OtpModel.StatelistBean> locationList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private OtpModel otpModel;
    private String mLoginType;
    private String mMobile;
    private String str_firstname, str_lastname, str_email, state;
    private CustomDialog mLoadingView;
    private Session mSession;
    private String mPassword;
    private String mEmail;
    private GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        removeTaskBar();
        ButterKnife.bind(this);
        mSession = new Session(this);
        Gson gson = new Gson();
        gps = new GPSTracker(this);
        otpModel = gson.fromJson(getIntent().getStringExtra("otp"), OtpModel.class);
        mLoginType = getIntent().getStringExtra("login_type");
        mMobile = getIntent().getStringExtra("phone");
        mEmail = getIntent().getStringExtra("email");
        mPassword = getIntent().getStringExtra("password");
        mLoadingView = new CustomDialog(this);
        if (!TextUtils.isEmpty(mEmail)) {
            emailEt.setFocusable(false);
            emailEt.setText(mEmail);
        }

        setLocationRv();
        firstNameEt.setInputType(
                InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
        );
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                if (InternetConnection.checkConnection(this)) {
                    verify_signup();
                } else {
                    Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
//                startActivity(new Intent(this, ActivityHome.class));
                break;
            case R.id.v_tb_iv:
                clearActivityStack();
                break;
            case R.id.location_tv:
                if (locationRv.getVisibility() == View.VISIBLE) {
                    locationRv.setVisibility(View.GONE);

                } else {
                    Constants.hideKeyboard(this);
                    locationRv.setVisibility(View.VISIBLE);

                }
                break;
        }
    }

    @Override
    protected void setOnClick() {
        addContinueTv.setOnClickListener(this);
        vTbIv.setOnClickListener(this);
        locationTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_join_us;
    }

    @Override
    protected void setText() {
        addContinueTv.setText(R.string.continue_);
    }

    private void addData() {
        locationList = otpModel.getStatelist();
       /* locationList.add("A");
        locationList.add("B");
        locationList.add("C");
        locationList.add("D");*/


    }

    private void setLocationRv() {
        locationRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        addData();
        itemClickListener = (view, position) -> {
            locationTv.setText(locationList.get(position).getState());
            locationRv.setVisibility(View.GONE);
        };
        locationRv.setAdapter(new LocationsAdapter(this, locationList, itemClickListener));


    }


    private void verify_signup() {
//        String team = spin_team.getSelectedItem().toString();
        str_firstname = firstNameEt.getText().toString().trim();
        str_lastname = lastNameEt.getText().toString().trim();
        str_email = emailEt.getText().toString().trim();
        state = locationTv.getText().toString().trim();


        if (str_firstname.isEmpty() && str_email.isEmpty() && str_lastname.isEmpty() && state.isEmpty()) {
            Toast.makeText(this, "Please enter your details for signup", Toast.LENGTH_SHORT).show();

        } else if (str_firstname.isEmpty()) {
            firstNameEt.requestFocus();
            firstNameEt.setError("Please enter your first name");

        } else if (str_lastname.isEmpty()) {
            lastNameEt.requestFocus();
            lastNameEt.setError("Please enter your last name");

        } else if (str_email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(str_email).matches()) {
            emailEt.requestFocus();
            emailEt.setError("Please enter your valid email");

        } else if (state.isEmpty()) {
            Toast.makeText(this, "Please select location", Toast.LENGTH_SHORT).show();

        } else {
            if (InternetConnection.checkConnection(this)) {
                signupUser();
            } else {
                Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void signupUser() {
        mLoadingView.show();
        String token = mSession.getToken();
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("login_type", mLoginType);
        builder.addFormDataPart("phone_no", mMobile);
        builder.addFormDataPart("email_id", str_email);
        builder.addFormDataPart("password", mPassword);
        builder.addFormDataPart("first_name", str_firstname);
        builder.addFormDataPart("last_name", str_lastname);
        builder.addFormDataPart("location", state);
        builder.addFormDataPart("vet_type", "");
        builder.addFormDataPart("expertise", "");
        builder.addFormDataPart("area_description", "");
        builder.addFormDataPart("img_identification", "");
        builder.addFormDataPart("primary_hospital_name", "");
        builder.addFormDataPart("state_issued", "");
        builder.addFormDataPart("license_number", "");
        builder.addFormDataPart("email_verification_status", "");
        builder.addFormDataPart("notification_status", "");
        builder.addFormDataPart("devicetoken", token);
        builder.addFormDataPart("devicename", "android");
        builder.addFormDataPart("lat", "" + latitude);
        builder.addFormDataPart("long", "" + longitude);
//        builder.addFormDataPart("flag", filePath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), filePath));


        MultipartBody requestBody = builder.build();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.usersignup(requestBody);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
//                            String message = jsonObject.getString("message");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    JSONObject obj = jsonObject.getJSONObject("userinfo");
                                    String userId = obj.getString("userId");
                                    mSession.setuser_id(userId);
                                    mSession.setuser_login(true);
                                    mSession.setfirt_name(str_firstname);
                                    mSession.setlast_name(str_lastname);
                                    mSession.setuser_email(mEmail);
                                    mSession.setuser_phone(mMobile);
                                    mSession.setuser_type("");
                                    mSession.setuserPaid("free");
//                                    Toast.makeText(ActivityJoinUs.this, message, Toast.LENGTH_SHORT).show();
//                                    startActivity(new Intent(Signup.this, Login.class));
//                                    finishAffinity();
                                    Intent i = new Intent(ActivityJoinUs.this, ActivityHome.class);
                                    // set the new task and clear flags
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                    finish();
//                                    Toast.makeText(Signup.this, message, Toast.LENGTH_SHORT).show();
//
//                                    startBillingSubscription();

                                } else {
                                    Toast.makeText(ActivityJoinUs.this, "Some thing went wrong", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(ActivityJoinUs.this, "Some thing went wrong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityJoinUs.this, "Failed to signup", Toast.LENGTH_SHORT).show();
                Log.e("cate", t.toString());
            }

        });

    }

}
