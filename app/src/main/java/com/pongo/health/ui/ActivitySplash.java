package com.pongo.health.ui;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.Toast;


import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.ui.vet.ActivityTechnician;
import com.pongo.health.ui.vet.ActivityVetDashboard;


import butterknife.ButterKnife;

public class ActivitySplash extends BaseActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        removeTaskBar();
        ButterKnife.bind(this);


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Session session = new Session(ActivitySplash.this);
                    if (session.getuser_login()) {
                        String type = session.getuser_type();
                        if (TextUtils.isEmpty(type)) {
                            startActivity(new Intent(ActivitySplash.this, ActivityHome.class));
                            finish();
                        } else {
                            if ("vet".equals(type)) {
                                startActivity(new Intent(ActivitySplash.this, ActivityVetDashboard.class));
                            } else {
                                startActivity(new Intent(ActivitySplash.this, ActivityTechnician.class));
                            }
                            finish();
                        }
                    } else {
                        startActivity(new Intent(ActivitySplash.this, ActivityIntroduction.class));
                        finish();
                    }
                }
            }, 3 * 1000);



    }



    @Override
    protected void setText() {
    }

    @Override
    protected void setOnClick() {
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_splash;
    }
}

