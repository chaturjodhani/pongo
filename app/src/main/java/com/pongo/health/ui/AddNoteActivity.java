package com.pongo.health.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.ImagesAdapter;
import com.pongo.health.adapter.PetProductRecommendAdapter;
import com.pongo.health.adapter.SearchHospitalAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.HospitalSearchModel;
import com.pongo.health.model.MarketResponseModel;
import com.pongo.health.model.SearchProductModel;
import com.pongo.health.ui.checkout.ActivityCheckoutAdditionalProfile;
import com.pongo.health.ui.vet.ActivityProfile;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.GPSTracker;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNoteActivity extends BaseActivity {
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.search_icon)
    ImageView searchIv;
    @BindView(R.id.pet_image)
    CircleImageView petIv;
    @BindView(R.id.pet_name)
    TextView petNameTv;
    @BindView(R.id.pet_type)
    TextView petTypeTv;
    @BindView(R.id.breed_name)
    TextView breednameTv;
    @BindView(R.id.pet_age)
    TextView petAgeTv;
    @BindView(R.id.note_et)
    EditText noteEt;
    @BindView(R.id.product_rv)
    RecyclerView searchListRv;
    @BindView(R.id.search_second_et)
    EditText searchEt;
    @BindView(R.id.image_rv)
    RecyclerView imagesRv;
    private ArrayList imageList = new ArrayList<>();
    private ArrayList<String> idsList = new ArrayList<String>();
    private ImagesAdapter mImagesAdapter;
    private List<SearchProductModel.ProductdataBean> medicationList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private CustomDialog mLoadingView;
    private Session mSession;
    private PetProductRecommendAdapter petProductRecommendAdapter;
//    private int mPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        vTbIv.setVisibility(View.GONE);
        searchEt.setHint("Search for product");
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
        setSearchListener();
        if (InternetConnection.checkConnection(AddNoteActivity.this)) {
            String userId = getIntent().getStringExtra("user_id");
            if (!TextUtils.isEmpty(userId)) {
                getInfo();
            }
        } else {
            Toast.makeText(AddNoteActivity.this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
        setAdapter();
    }

    private void setAdapter() {
        imagesRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                imageList.remove(position);
                idsList.remove(position);
                petProductRecommendAdapter.notifyDataSetChanged();
                mImagesAdapter.notifyDataSetChanged();
            }
        };
//        itemClickListener = (view, position) -> startActivity(new Intent(activity, ActivityYourPetSingleType.class));
        mImagesAdapter = new ImagesAdapter(this, imageList, itemClickListener);
        imagesRv.setAdapter(mImagesAdapter);
    }

    private void getInfo() {
        this.mLoadingView.show();
//        String userId = getIntent().getStringExtra("user_id");
        String chatid = getIntent().getStringExtra("chatid");
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getSinglePet(chatid).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    JSONObject detaill_object = jsonObject.getJSONObject("petdetail");
                                    String pet_name = detaill_object.getString("pet_name");
                                    String pet_type = detaill_object.getString("pet_type");
                                    String breed = detaill_object.getString("breed");
                                    String petimage = detaill_object.getString("petimage");
                                    String pet_age = detaill_object.getString("pet_age");
                                    String docNote = detaill_object.getString("doctor_note");
                                    JSONArray prodArray = jsonObject.getJSONArray("recommended_products");
                                    if (null != prodArray && prodArray.length() > 0) {
                                        setProductList(prodArray);
                                    }

                                    petNameTv.setText(pet_name);
                                    petTypeTv.setText(pet_type);
                                    breednameTv.setText(breed);
                                    petAgeTv.setText(pet_age);
                                    Glide.with(AddNoteActivity.this).load(petimage).into(petIv);
                                    if (!TextUtils.isEmpty(docNote)) {
                                        noteEt.setText(docNote);
                                        noteEt.setSelection(docNote.length());
                                    }

                                } else {
                                    Toast.makeText(AddNoteActivity.this, "Opps .. Something went wrong", Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(AddNoteActivity.this, "Opps .. Something went wrong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(AddNoteActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void setProductList(JSONArray prodArray) {
        medicationList.clear();
        for (int i = 0; i < prodArray.length(); i++) {
            try {
                JSONObject detaill_object = prodArray.getJSONObject(i);
                String id = detaill_object.getString("id");
                String name = detaill_object.getString("name");
                String description = detaill_object.getString("description");
                String product_image = detaill_object.getString("product_image");
                String orignalprice = detaill_object.getString("orignalprice");
                String discountprice = detaill_object.getString("discountprice");
                SearchProductModel.ProductdataBean productdataBean = new SearchProductModel.ProductdataBean();
                productdataBean.setId(id);
                productdataBean.setName(name);
                productdataBean.setDescription(description);
                productdataBean.setProduct_image(product_image);
                productdataBean.setOrignalprice(orignalprice);
                productdataBean.setDiscountprice(discountprice);
                medicationList.add(productdataBean);
                idsList.add(id);
                imageList.add(product_image);
            } catch (JSONException e) {
                e.printStackTrace();
            }

//            petProductRecommendAdapter.notifyDataSetChanged();

        }
        mImagesAdapter.notifyDataSetChanged();
        setMarketSearches();
    }

    private void addNoteApi() {
        this.mLoadingView.show();
        String note = noteEt.getText().toString();
        String userId = getIntent().getStringExtra("user_id");
        String chatid = getIntent().getStringExtra("chatid");
        String techId = mSession.getuser_id();
        String productId = "";
        for (int i = 0; i < idsList.size(); i++) {
            productId = idsList.get(i) + "," + productId;
        }
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).addNote(userId, techId, note, productId, chatid).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(AddNoteActivity.this, "successfully added", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(AddNoteActivity.this, "Opps .. Something went wrong", Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(AddNoteActivity.this, "Opps .. Something went wrong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(AddNoteActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void setSearchListener() {
        searchEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String search = searchEt.getText().toString();
                    getSearch(search);
                    return true;
                }
                return false;
            }
        });
    }

    private void getSearch(String s) {
        this.mLoadingView.show();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).searchSuggested(s).enqueue(new Callback<SearchProductModel>() {
            public void onResponse(Call<SearchProductModel> call, Response<SearchProductModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((SearchProductModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        medicationList.clear();
                        medicationList = response.body().getProductdata();
                        setMarketSearches();
                    }
                }

            }

            public void onFailure(Call<SearchProductModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(AddNoteActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void setMarketSearches() {
//        mPosition = -1;
        searchListRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 4; i++) {
            medicationList.add(new PetsModel("$45.99", "Ophtha Care",
                    "lorem ipsum is a simply dummy text of printing and type setting industry"));
        }*/
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                if (imageList.size() > 2) {
                    Toast.makeText(AddNoteActivity.this, "You exceed maximum limit", Toast.LENGTH_SHORT).show();
                } else {
                    String id = medicationList.get(position).getId();
                    String image = medicationList.get(position).getProduct_image();
                    if (idsList.contains(id)) {
                        int pos = idsList.indexOf(id);
                        idsList.remove(pos);
                        imageList.remove(pos);
                    } else {
                        idsList.add(id);
                        imageList.add(image);
                    }
                    petProductRecommendAdapter.notifyDataSetChanged();
                    mImagesAdapter.notifyDataSetChanged();
                }

            }
        };
        petProductRecommendAdapter = new PetProductRecommendAdapter(this, medicationList, itemClickListener, idsList);
        searchListRv.setAdapter(petProductRecommendAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                String note = noteEt.getText().toString();
                if (TextUtils.isEmpty(note)) {
                    Toast.makeText(AddNoteActivity.this, "Please enter note", Toast.LENGTH_SHORT).show();
                } else if (imageList.size() < 2) {
                    Toast.makeText(AddNoteActivity.this, "Please select atleast 2 recommended product", Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(this, "posted", Toast.LENGTH_SHORT).show();
                    if (InternetConnection.checkConnection(AddNoteActivity.this)) {
                        addNoteApi();
                    } else {
                        Toast.makeText(AddNoteActivity.this, "No Internet available", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case R.id.v_tb_iv:
                finish();
                break;
            case R.id.search_icon:
                String search = searchEt.getText().toString();
                getSearch(search);
                break;
        }
    }

    @Override
    protected void setText() {
        continueTv.setText(R.string.submit);
    }

    @Override
    protected void setOnClick() {
        continueTv.setOnClickListener(this);
        vTbIv.setOnClickListener(this);
        searchIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_add_note;
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}