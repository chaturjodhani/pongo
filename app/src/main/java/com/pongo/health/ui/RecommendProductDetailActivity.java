package com.pongo.health.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.adapter.SearchMarketAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.MarketResponseModel;
import com.pongo.health.model.SearchProductModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecommendProductDetailActivity extends BaseActivity {
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.doc_image)
    ImageView doctorIv;
    @BindView(R.id.byTv)
    TextView byTv;
    @BindView(R.id.nameTv)
    TextView nameTv;
    @BindView(R.id.dateTv)
    TextView dateTv;
    @BindView(R.id.product_list)
    RecyclerView productRv;
    private List<MarketResponseModel.ProductlistBean> medicationList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private CustomDialog mLoadingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        if (InternetConnection.checkConnection(this)) {
            Intent intent = getIntent();
            if (null != intent) {
                String consultId = intent.getStringExtra("id");
                if (!TextUtils.isEmpty(consultId)) {
                    getProduct(consultId);
                }
            }
        } else {
            Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                clearActivityStack();
                break;

        }
    }

    @Override
    protected void setText() {
      /*  blogDecTv.setText(Html.fromHtml("<p>Lorem Ipsum is simply dummy text of the printing and" +
                "typesetting industry. LoremIpsum has been the industry's standard dummy" +
                " text ever since the 1500s, when an unknown printer took a gallery of type " +
                "and scrambled it to make a type spicimen book. it has survived not only fivr centuries, but also" +
                "the leap into electronic typesetting, remaining essential unchanged. It ws popularised in the" +
                "1960s with the release of Letraset sheets containing Lorem Ipsum passages</p>"));
   */
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_recommend_product_detail;
    }

    private void getProduct(String consultId) {
        this.mLoadingView.show();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getSingleRcommend(consultId).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    String doctorname = jsonObject.getString("doctorname");
                                    String date = jsonObject.getString("date");
                                    String petname = jsonObject.getString("petname");
                                    String techimg = jsonObject.getString("techimg");

                                    JSONArray prodArray = jsonObject.getJSONArray("recommended_products");
                                    if (null != prodArray && prodArray.length() > 0) {
                                        setProductList(prodArray);
                                    }

                                    nameTv.setText(doctorname);
                                    dateTv.setText(date);
                                    tbTv.setText("Product Recommendations for " + petname);

                                    Glide.with(RecommendProductDetailActivity.this).load(techimg).into(doctorIv);

                                } else {
                                    Toast.makeText(RecommendProductDetailActivity.this, "Opps .. Something went wrong", Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(RecommendProductDetailActivity.this, "Opps .. Something went wrong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(RecommendProductDetailActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void setProductList(JSONArray prodArray) {
        medicationList.clear();
        for (int i = 0; i < prodArray.length(); i++) {
            try {
                JSONObject detaill_object = prodArray.getJSONObject(i);
                String id = detaill_object.getString("id");
                String name = detaill_object.getString("name");
                String description = detaill_object.getString("description");
                String product_image = detaill_object.getString("product_image");
                String orignalprice = detaill_object.getString("orignalprice");
                String discountprice = detaill_object.getString("discountprice");
                String category = detaill_object.getString("category");
                String subcategory = detaill_object.getString("subcategory");
                boolean product_type = detaill_object.getBoolean("product_type");
                MarketResponseModel.ProductlistBean productdataBean = new MarketResponseModel.ProductlistBean();
                productdataBean.setId(id);
                productdataBean.setName(name);
                productdataBean.setDescription(description);
                productdataBean.setProduct_image(product_image);
                productdataBean.setOrignalprice(orignalprice);
                productdataBean.setDiscountprice(discountprice);
                productdataBean.setProduct_type(product_type);
                productdataBean.setCategory(category);
                productdataBean.setSubcategory(subcategory);
                medicationList.add(productdataBean);
            } catch (JSONException e) {
                e.printStackTrace();
            }

//            petProductRecommendAdapter.notifyDataSetChanged();

        }
        setMarketSearches();
    }

    private void setMarketSearches() {
        productRv.setLayoutManager(new LinearLayoutManager(RecommendProductDetailActivity.this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 4; i++) {
            medicationList.add(new PetsModel("$45.99", "Ophtha Care",
                    "lorem ipsum is a simply dummy text of printing and type setting industry"));
        }*/

        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                String id = medicationList.get(position).getId();
                String name = medicationList.get(position).getName();
                if (medicationList.get(position).isProduct_type()) {
                    startActivity(new Intent(RecommendProductDetailActivity.this, ActivityMedicationNext.class).putExtra("id", id).putExtra("name", name));
                } else {
                    name = medicationList.get(position).getCategory();
                    String subcate = medicationList.get(position).getSubcategory();
                    if (!TextUtils.isEmpty(subcate)) {
                        name = name + " > " + subcate;
                    }
                    startActivity(new Intent(RecommendProductDetailActivity.this, ActivitySingleItemMarket.class).putExtra("id", id).putExtra("name", name));
                }
            }
        };

        productRv.setAdapter(new SearchMarketAdapter(RecommendProductDetailActivity.this, medicationList, itemClickListener));
    }
}
