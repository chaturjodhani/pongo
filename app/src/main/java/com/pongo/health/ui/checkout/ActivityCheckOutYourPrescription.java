package com.pongo.health.ui.checkout;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.ui.ActivityAddPrimaryHospitalNew;
import com.pongo.health.ui.AddDoctorActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityCheckOutYourPrescription extends BaseActivity {

    @BindView(R.id.prescription_description_tv)
    TextView prescriptionDescriptionTv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.title_transfer)
    TextView transferTv;
    private int position;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:

                String pet_name = getIntent().getStringExtra("pet_name");
                if (!TextUtils.isEmpty(pet_name)) {
                    String pet_image = getIntent().getStringExtra("pet_image");
                    String pet_type = getIntent().getStringExtra("pet_type");
                    String pet_size = getIntent().getStringExtra("pet_size");
                    String pet_age = getIntent().getStringExtra("pet_age");
                    String pet_health = getIntent().getStringExtra("pet_health");
                    String recommendId = getIntent().getStringExtra("recommendId");
                    String breed = getIntent().getStringExtra("breed");

                    if (position == 1) {
                        startActivity(new Intent(this, ActivityCheckOutYourPrescriptionNew.class)
                                .putExtra("pet_name", pet_name)
                                .putExtra("pet_type", pet_type)
                                .putExtra("pet_image", pet_image)
                                .putExtra("pet_size", pet_size)
                                .putExtra("pet_age", pet_age)
                                .putExtra("pet_health", pet_health)
                                .putExtra("recommendId", recommendId)
                                .putExtra("breed", breed)
                        );

                    } else if (position == 2) {
                        startActivity(new Intent(this, AddDoctorActivity.class)
                                .putExtra("pet_name", pet_name)
                                .putExtra("pet_type", pet_type)
                                .putExtra("pet_image", pet_image)
                                .putExtra("pet_size", pet_size)
                                .putExtra("pet_age", pet_age)
                                .putExtra("pet_health", pet_health)
                                .putExtra("recommendId", recommendId)
                                .putExtra("breed", breed)
                        );

                    } else if (position == 4) {
                        startActivity(new Intent(this, ActivityCheckOutUploadPic.class)
                                .putExtra("pet_name", pet_name)
                                .putExtra("pet_type", pet_type)
                                .putExtra("pet_image", pet_image)
                                .putExtra("pet_size", pet_size)
                                .putExtra("pet_age", pet_age)
                                .putExtra("pet_health", pet_health)
                                .putExtra("recommendId", recommendId)
                                .putExtra("breed", breed)
                        );

                    } else {
                        Toast.makeText(this, "Please Select", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    String name = getIntent().getStringExtra("name");
                    String prodId = getIntent().getStringExtra("prodId");
                    String quantity = getIntent().getStringExtra("quantity");
                    String strength = getIntent().getStringExtra("strength");
                    String price = getIntent().getStringExtra("price");

                    if (position == 1) {
                        startActivity(new Intent(this, ActivityCheckOutYourPrescriptionNew.class)
                                .putExtra("name", name)
                                .putExtra("prodId", prodId)
                                .putExtra("quantity", quantity)
                                .putExtra("strength", strength)
                                .putExtra("price", price)
                        );
                    } else if (position == 2) {
                        startActivity(new Intent(this, AddDoctorActivity.class)
                                .putExtra("name", name)
                                .putExtra("prodId", prodId)
                                .putExtra("quantity", quantity)
                                .putExtra("strength", strength)
                                .putExtra("price", price)
                        );
                    } else if (position == 4) {
                        startActivity(new Intent(this, ActivityCheckOutUploadPic.class)
                                .putExtra("name", name)
                                .putExtra("prodId", prodId)
                                .putExtra("quantity", quantity)
                                .putExtra("strength", strength)
                                .putExtra("price", price)
                        );
                    } else {
                        Toast.makeText(this, "Please Select", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case R.id.tb_iv:
                clearActivityStack();
                break;
        }
    }

    @Override
    protected void setText() {
        String name = getIntent().getStringExtra("name");

        prescriptionDescriptionTv.setText(Html.fromHtml("<p>Pongo will get your Pet's prescription and save it on File. We will notify you if we are unable to get your Pet's prescription.</P>"));
        transferTv.setText("Transfer " + name + " from");
        continueTv.setText(R.string.continue_);
        tbTv.setText(R.string.checkout_small);

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        continueTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_checkout_your_prescription;
    }


    public void pharmacyClicked(View view) {
        position = 1;

    }

    public void doctorClicked(View view) {
        position = 2;
    }

    public void transferClicked(View view) {
        position = 3;
    }

    public void uploadPicClicked(View view) {
        position = 4;
    }
}
