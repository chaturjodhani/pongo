package com.pongo.health.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.HomeModel;
import com.pongo.health.utils.PicassoImageGetter;
import com.squareup.picasso.Picasso;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityBlog extends BaseActivity {

    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.byTv)
    TextView byTv;
    @BindView(R.id.titleTv)
    TextView titleTv;
    @BindView(R.id.desc)
    HtmlTextView descTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.blog_pic)
    ImageView blogPic;
    @BindView(R.id.web)
    WebView webview;

    private HomeModel.BlogBean mBlogBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Gson gson = new Gson();
        mBlogBean = gson.fromJson(getIntent().getStringExtra("model"), HomeModel.BlogBean.class);
        setTextData();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setTextData() {
        if (null != mBlogBean) {
            String title = mBlogBean.getTitle();
            String desc = mBlogBean.getDescription();
            String image = mBlogBean.getBlogimage().get(0);
            String by = mBlogBean.getBy();
            titleTv.setText(title);
//            descTv.setText(desc);
           /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                descTv.setText(Html.fromHtml(desc, Html.FROM_HTML_MODE_COMPACT));
            } else {
                descTv.setText(Html.fromHtml(desc));
            }*/
            if (!TextUtils.isEmpty(desc)) {
//                CharSequence charSequence;
//                PicassoImageGetter picassoImageGetter = new PicassoImageGetter(descTv, this);
//                if (Build.VERSION.SDK_INT >= 24) {
//                    charSequence = (Spannable) Html.fromHtml(desc, 0, picassoImageGetter, null);
//                } else {
//                    charSequence = (Spannable) Html.fromHtml(desc, picassoImageGetter, null);
//                }
//                descTv.setText(charSequence);
                webview.getSettings().setJavaScriptEnabled(true);

//                webview.loadData(desc, "text/html; charset=utf-8", "UTF-8");
//                webview.loadData(desc, "text/html", "UTF-8");
//                webview.loadData(String.valueOf(Html.fromHtml(desc)),"text/html","utf-8");
                String replaced2 = desc.replace("&gt;", ">");
                String newBody = "<html><body>" + replaced2 + "</body></html>";
                webview.loadDataWithBaseURL(null, newBody, "text/html", "utf-8", null);

            }
            byTv.setText("By : " + by);
            if (!TextUtils.isEmpty(image)) {
//                Picasso.get().load(image).into(blogPic);
                Glide.with(this).load(image).into(blogPic);

            }

              }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                clearActivityStack();
                break;

            case R.id.blue_btn_tv:
                startActivity(new Intent(this, ActivityHome.class));
                finish();
                break;
        }
    }

    @Override
    protected void setText() {
        tbTv.setText("Blog");
      /*  blogDecTv.setText(Html.fromHtml("<p>Lorem Ipsum is simply dummy text of the printing and" +
                "typesetting industry. LoremIpsum has been the industry's standard dummy" +
                " text ever since the 1500s, when an unknown printer took a gallery of type " +
                "and scrambled it to make a type spicimen book. it has survived not only fivr centuries, but also" +
                "the leap into electronic typesetting, remaining essential unchanged. It ws popularised in the" +
                "1960s with the release of Letraset sheets containing Lorem Ipsum passages</p>"));
   */
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_blog;
    }
}
