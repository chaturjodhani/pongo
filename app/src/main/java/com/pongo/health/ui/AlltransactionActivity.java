package com.pongo.health.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.SubscriptionAdapter;
import com.pongo.health.adapter.TransactionAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.TransactionModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;
import com.pongo.health.utils.TransactionListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlltransactionActivity extends BaseActivity {

    @BindView(R.id.transaction_rv)
    RecyclerView transactionRv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;

    private CustomDialog mLoadingView;
    private Session mSession;

    List<TransactionModel.YourtransactionBean> transactionList = new ArrayList<>();
    ItemClickListener itemClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);
        if (InternetConnection.checkConnection(AlltransactionActivity.this)) {
            getTransactionData();
        } else {
            Toast.makeText(AlltransactionActivity.this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
    }

    private void getTransactionData() {
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getAllTransactionList(id).enqueue(new Callback<TransactionModel>() {
            public void onResponse(Call<TransactionModel> call, Response<TransactionModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((TransactionModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        transactionList = response.body().getYourtransaction();
                       if (null!= transactionList) {
                           setTransaction();
                       }
                    }
                }

            }

            public void onFailure(Call<TransactionModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(AlltransactionActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void setTransaction() {
        transactionRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 2; i++) {
            transactionList.add(new CartModel(R.drawable.rx_two_img, "Lorem Ipsum", "15-04-2020",
                    "By Card,4565456545"));

        }*/
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                Gson gson = new Gson();
                String myJson = gson.toJson(transactionList.get(position));
                startActivity(new Intent(AlltransactionActivity.this, OderDetailActivity.class)
                        .putExtra("model", myJson));
            }
        };

        transactionRv.setAdapter(new TransactionAdapter(this, transactionList, itemClickListener));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;

        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("All Transactions");

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_alltransaction;
    }
}