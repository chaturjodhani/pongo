package com.pongo.health.ui;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.kinda.alert.KAlertDialog;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.CardListAdapter;
import com.pongo.health.adapter.OrdersAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.CheckoutResponseModel;
import com.pongo.health.ui.checkout.ActivityCheckoutAdditionalProfile;
import com.pongo.health.ui.pet_parent.ActivityGeneralInformationPet;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityPayment extends BaseActivity {
    //  intent.putExtra("address",addressType);
//                    intent.putExtra(/**/"address_id",addressType);
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.continue_shopping_tv)
    TextView continueShopTv;
    @BindView(R.id.blue_btn_tv)
    TextView payTv;
    @BindView(R.id.exit_btn_tv)
    TextView exitTv;
    @BindView(R.id.address)
    TextView addressTv;
    @BindView(R.id.price_tv)
    TextView priceTv;
    @BindView(R.id.discount_price)
    TextView discountpriceTv;
    @BindView(R.id.card_text)
    TextView cardTv;
    @BindView(R.id.shipping_price)
    TextView shipPriceTv;
    @BindView(R.id.shipping_layout)
    LinearLayout shipLayout;
    @BindView(R.id.discount_layout)
    LinearLayout discountLayout;
    @BindView(R.id.card_layout)
    LinearLayout cardLayout;
    @BindView(R.id.total_price)
    TextView totalPriceTv;
    @BindView(R.id.apply_coupon)
    ImageButton applyCoupon;
    @BindView(R.id.delete_coupon)
    ImageButton deleteCoupon;
    @BindView(R.id.coupon_edittext)
    EditText couponEdit;
    @BindView(R.id.check_first)
    AppCompatCheckBox checkFirst;
    @BindView(R.id.check_second)
    AppCompatCheckBox checkSecond;
    @BindView(R.id.orders_rv)
    RecyclerView ordersRv;
    RecyclerView cardRv;
    private CustomDialog mLoadingView;
    private Session mSession;
    private List<CheckoutResponseModel.CheckoutBean> orderList = new ArrayList<>();
    private List<CheckoutResponseModel.CardlistBean> cardList = new ArrayList<>();
    BottomSheetDialog mBottomSheetDialog;
    private String cardid = "";
    private String customer = "";
    private CheckoutResponseModel checkoutResponseModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
        continueShopTv.setVisibility(View.VISIBLE);
        setAddress();
        createBottomList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCheckout();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        setAddress();
    }

    private void createBottomList() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.card_list_bottomsheet, null);
        cardRv = sheetView.findViewById(R.id.card_rv);
        TextView done = sheetView.findViewById(R.id.done);
        TextView addCard = sheetView.findViewById(R.id.blue_btn_tv);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        addCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityPayment.this, ActivityAddCard.class));
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(sheetView);
    }

    private void setAddress() {
        Intent intent = getIntent();
        if (null != getIntent().getExtras()) {
            String address = intent.getStringExtra("address");
            if (!TextUtils.isEmpty(address)) {
                if (address.equalsIgnoreCase("home")) {
                    addressTv.setText("TO BILLING");
                } else {
                    addressTv.setText("TO SHIPPING");
                }
            } else {
                addressTv.setVisibility(View.GONE);
            }
        } else {
            addressTv.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                if (!checkFirst.isChecked()) {
                    Toast.makeText(this, "Select conditions first", Toast.LENGTH_SHORT).show();
                } else if (!checkSecond.isChecked()) {
                    Toast.makeText(this, "Select conditions first", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(cardid)) {
                    Toast.makeText(this, "Please select payment option first", Toast.LENGTH_SHORT).show();
                } else {
                    confimrDialog();
                }
                break;
            case R.id.tb_iv:
                clearActivityStack();
                break;
            case R.id.continue_shopping_tv:
                Intent i = new Intent(ActivityPayment.this, ActivityHome.class);
                // set the new task and clear flags
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.putExtra("market", "market");
                startActivity(i);
                finish();
                break;
            case R.id.exit_btn_tv:
                Intent in = new Intent(ActivityPayment.this, ActivityHome.class);
                // set the new task and clear flags
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                in.putExtra("market", "market");
                startActivity(in);
                finish();
                break;
            case R.id.address:
                startActivity(new Intent(ActivityPayment.this, ActivityAddress.class)
                        .putExtra("existing_status", "no")
                );
                break;
            case R.id.card_layout:
                showCradList();
                break;
            case R.id.apply_coupon:
                String coupon = couponEdit.getText().toString();
                if (!TextUtils.isEmpty(coupon)) {
                    applyCoupon(coupon);
                } else {
                    Toast.makeText(this, "Enter coupon first", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.delete_coupon:
                deleteCoupon();
                break;

//            case R.id.add_shipping_address_tv:
//                startActivity(new Intent(this,ActivityAddShippingAddress.class));
//                break;

        }
    }

    private void confimrDialog() {
        new KAlertDialog(this, KAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("You want to place order!")
                .setCancelText("No")
                .setConfirmText("Yes")
                .showCancelButton(true)
                .setCancelClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                .setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog kAlertDialog) {
                        kAlertDialog.dismissWithAnimation();
                        placeOrder();
                    }
                })
                .show();
    }

    private void showCradList() {
        mBottomSheetDialog.show();
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        payTv.setText("Buy Now");
        exitTv.setText("Exit");
        tbTv.setText("Your Order");

    }

    @Override
    protected void setOnClick() {
//        addShippingAddressTv.setOnClickListener(this);
        tbIv.setOnClickListener(this);
        addressTv.setOnClickListener(this);
        payTv.setOnClickListener(this);
        exitTv.setOnClickListener(this);
        continueShopTv.setOnClickListener(this);
        cardLayout.setOnClickListener(this);
        applyCoupon.setOnClickListener(this);
        deleteCoupon.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_payment;
    }

    private void setOrdersList(List<CheckoutResponseModel.CheckoutBean> checkout) {
        orderList = checkout;
        ordersRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
      /*  for (int i = 0; i < 3; i++) {
            orderList.add("Lorem Ipsum");

        }*/
        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                deleteCartItem(position);
            }
        };
        ordersRv.setAdapter(new OrdersAdapter(this, orderList, itemClickListener));
    }

    private void deleteCartItem(int position) {
        this.mLoadingView.show();
        String id = orderList.get(position).getCartid();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).deleteCartItem(id).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(ActivityPayment.this, message, Toast.LENGTH_SHORT).show();
                                    if (orderList.size() == 1) {
                                        Intent i = new Intent(ActivityPayment.this, ActivityHome.class);
                                        // set the new task and clear flags
                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(i);
                                        finish();
                                    } else {
                                        getCheckout();
                                    }

                                } else {
                                    Toast.makeText(ActivityPayment.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivityPayment.this, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityPayment.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void applyCoupon(String coupon) {
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).applyCoupon(coupon, id).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    String finalprice = jsonObject.getString("finalprice");
                                    String couponprice = jsonObject.getString("couponprice");
                                    Toast.makeText(ActivityPayment.this, message, Toast.LENGTH_SHORT).show();
                                    checkoutResponseModel.setFinalprice(Double.parseDouble(finalprice));
                                    totalPriceTv.setText("$" + finalprice);
                                    deleteCoupon.setVisibility(View.VISIBLE);
                                    applyCoupon.setVisibility(View.GONE);
                                    discountpriceTv.setText("$" + couponprice);
                                    discountLayout.setVisibility(View.VISIBLE);
                                } else {
                                    Toast.makeText(ActivityPayment.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivityPayment.this, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityPayment.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void deleteCoupon() {
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).deleteCoupon(id).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    String finalprice = jsonObject.getString("finalprice");
                                    String couponprice = jsonObject.getString("couponprice");
                                    Toast.makeText(ActivityPayment.this, message, Toast.LENGTH_SHORT).show();
                                    checkoutResponseModel.setFinalprice(Double.parseDouble(finalprice));
                                    totalPriceTv.setText("$" + finalprice);
                                    couponEdit.setText("");
                                    deleteCoupon.setVisibility(View.GONE);
                                    applyCoupon.setVisibility(View.VISIBLE);
                                    discountLayout.setVisibility(View.GONE);
                                } else {
                                    Toast.makeText(ActivityPayment.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivityPayment.this, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityPayment.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void getCheckout() {
        Intent oldIntent = getIntent();
        if (null != oldIntent.getExtras()) {
            mLoadingView.show();
//            String status = oldIntent.getStringExtra("existing_status");
            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart("user_id", mSession.getuser_id());
            MultipartBody requestBody = builder.build();
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<CheckoutResponseModel> call = apiInterface.checkout(requestBody);

            call.enqueue(new Callback<CheckoutResponseModel>() {
                @Override
                public void onResponse(Call<CheckoutResponseModel> call, Response<CheckoutResponseModel> response) {
                    mLoadingView.hideDialog();
                    if (response.body() != null) {
                        try {
                            String status = response.body().getStatus();
//                            String message = jsonObject.getString("message");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    cardList = response.body().getCardlist();
                                    setOrdersList(response.body().getCheckout());
                                    setCardist();
                                    setPriceData(response.body());
                                } else {
                                    Toast.makeText(ActivityPayment.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(ActivityPayment.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(Call<CheckoutResponseModel> call, Throwable t) {
                    mLoadingView.hideDialog();
                    Toast.makeText(ActivityPayment.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                    Log.e("cate", t.toString());
                }

            });
        }
    }

    private void setCardist() {
        cardRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
      /*  for (int i = 0; i < 3; i++) {
            orderList.add("Lorem Ipsum");

        }*/
        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                cardTv.setText(cardList.get(position).getCardnumber());
                cardid = cardList.get(position).getCard_id();
                customer = cardList.get(position).getCustomer();
                mBottomSheetDialog.dismiss();
            }
        };
        cardRv.setAdapter(new CardListAdapter(this, cardList, itemClickListener));
    }

    private void setPriceData(CheckoutResponseModel body) {
        checkoutResponseModel = body;
        priceTv.setText("$" + body.getTotalprice());
        String shipPrice = body.getShipping();
        String coupon = body.getCouponcode();
        String discount = body.getDiscount();
        if (!TextUtils.isEmpty(shipPrice)) {
            shipPriceTv.setText("$" + shipPrice);
        } else {
            shipPriceTv.setText("FREE");
        }

        if (!TextUtils.isEmpty(coupon)) {
            couponEdit.setText(coupon);
            applyCoupon.setVisibility(View.GONE);
            deleteCoupon.setVisibility(View.VISIBLE);
            discountpriceTv.setText("$" + discount);
            discountLayout.setVisibility(View.VISIBLE);
        } else {
            discountLayout.setVisibility(View.GONE);
        }
        totalPriceTv.setText("$" + body.getFinalprice());
        if (cardList.size() > 0) {
            cardTv.setText(cardList.get(0).getCardnumber());
            cardid = cardList.get(0).getCard_id();
            customer = cardList.get(0).getCustomer();
        }
    }

    private void successDialog(String message) {
        KAlertDialog kAlertDialog = new KAlertDialog(this, KAlertDialog.SUCCESS_TYPE);
        kAlertDialog.setTitleText("Success!");
        kAlertDialog.setContentText(message);
        kAlertDialog.setCancelable(false);
        kAlertDialog.setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(KAlertDialog sDialog) {
//                Toast.makeText(ActivityPayment.this, "Success", Toast.LENGTH_SHORT).show();

                Intent i = new Intent(ActivityPayment.this, ActivityHome.class);
                // set the new task and clear flags
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
                sDialog.dismissWithAnimation();
            }
        })
                .show();
    }

    private void failureDialog(String message) {
        KAlertDialog kAlertDialog = new KAlertDialog(this, KAlertDialog.ERROR_TYPE);
        kAlertDialog.setTitleText("Oops...");
        kAlertDialog.setContentText(message);
        kAlertDialog.setCancelable(true);
        kAlertDialog.setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(KAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
            }
        })
                .show();
    }

    private void placeOrder() {
        this.mLoadingView.show();
        String userid = mSession.getuser_id();
        String addressType = getIntent().getStringExtra("address");
        String totalPrice = "";
        String ship = "";
        String finalPrice = "";
        if (checkoutResponseModel != null) {
            totalPrice = checkoutResponseModel.getTotalprice();
            ship = checkoutResponseModel.getShipping();
            finalPrice = String.valueOf(checkoutResponseModel.getFinalprice());
        }
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).placeOrder(userid, addressType, totalPrice, ship, cardid, finalPrice, customer).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
//                                    Toast.makeText(ActivityPayment.this, message, Toast.LENGTH_SHORT).show();
                                    successDialog(message);
                                } else {
                                    failureDialog(message);
                                }
                            } else {
                                failureDialog(message);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityPayment.this, "Failed to place order", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

}

