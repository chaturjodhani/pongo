package com.pongo.health.ui.vet;

import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.ui.ActivityIntroduction;
import com.pongo.health.ui.ActivityLogin;
import com.pongo.health.ui.ActivityNotification;
import com.pongo.health.utils.GPSTracker;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityLogout extends BaseActivity {

    @BindView(R.id.v_tb_iv)
    ImageView vTbIV;
    @BindView(R.id.blue_btn_tv)
    TextView logoutTv;
    @BindView(R.id.billing_cv)
    CardView billingCv;
    @BindView(R.id.personal_profile_cv)
    CardView personalProfileCv;
    @BindView(R.id.contact_pongo_cv)
    CardView contactPongoCv;
    @BindView(R.id.notification_pongo_cv)
    CardView notificationPongoCv;
    @BindView(R.id.payout_pongo_cv)
    CardView payoutPongoCv;
    private Session mSession;
    private GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        gps = new GPSTracker(this);
        mSession = new Session(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_tb_iv:
                clearActivityStack();
                break;
            case R.id.blue_btn_tv:
                changeStatus("0");
                mSession.clear();
                Intent intent = new Intent(this, ActivityIntroduction.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
            case R.id.billing_cv:
                startActivity(new Intent(this, ActivityBillings.class));
                break;
            case R.id.personal_profile_cv:
                startActivity(new Intent(this, ActivityProfile.class));
                break;
            case R.id.payout_pongo_cv:
                startActivity(new Intent(this, VetPaymentActivity.class));
                break;
            case R.id.contact_pongo_cv:
                sendEmail();
                break;
            case R.id.notification_pongo_cv:
                startActivity(new Intent(this, ActivityNotification.class));
                break;
        }
    }

    private void sendEmail() {
       /* Intent email = new Intent(Intent.ACTION_SENDTO);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{ "support@pongohealth.com"});
        email.putExtra(Intent.EXTRA_SUBJECT, "");
        email.putExtra(Intent.EXTRA_TEXT, "");

//need this to prompts email client only
        email.setType("message/rfc822");

        startActivity(Intent.createChooser(email, "Contact Pongo"));*/

        Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        intent.putExtra(Intent.EXTRA_TEXT, "");
        intent.setData(Uri.parse("mailto:support@pongohealth.com")); // or just "mailto:" for blank
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        startActivity(intent);
    }

    @Override
    protected void setText() {
        logoutTv.setText(R.string.logout);
    }

    @Override
    protected void setOnClick() {
        vTbIV.setOnClickListener(this);
        logoutTv.setOnClickListener(this);
        billingCv.setOnClickListener(this);
        personalProfileCv.setOnClickListener(this);
        contactPongoCv.setOnClickListener(this);
        notificationPongoCv.setOnClickListener(this);
        payoutPongoCv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_logout;
    }

    private void changeStatus(String status) {
        String userId = mSession.getuser_id();
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();
        if (!TextUtils.isEmpty(userId)) {
            ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).onlineStatus(userId, status, "" + latitude, "" + longitude).enqueue(new Callback<ResponseBody>() {
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.body() != null) {
                        String responseData = "";
                        try {
                            responseData = response.body().string();
                            if (!TextUtils.isEmpty(responseData)) {
                                JSONObject jsonObject = new JSONObject(responseData);
                               /* String status = jsonObject.getString("status");
                                String message = jsonObject.getString("detail");
                                if (!TextUtils.isEmpty(status)) {
                                    if (status.equalsIgnoreCase("success")) {

                                    }
                                }*/
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                public void onFailure(Call<ResponseBody> call, Throwable th) {
                    Log.e("cate", th.toString());
                }
            });
        }
    }

}
