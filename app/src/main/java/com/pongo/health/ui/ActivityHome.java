package com.pongo.health.ui;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.widget.ImageViewCompat;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.fragment.FragmentHome;
import com.pongo.health.fragment.FragmentMarket;
import com.pongo.health.fragment.FragmentProfile;
import com.pongo.health.fragment.FragmentTalkToUs;
import com.pongo.health.fragment.FragmentChat;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pongo.health.fragment.FreeUserFragment;
import com.pongo.health.utils.ArchLifecycleApp;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityHome extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.customBottomBar)
    CurvedBottomNavigationView customBottomNavigationView;
    @BindView(R.id.fragment_container)
    FrameLayout fragmentContainer;
    @BindView(R.id.fab)
    AppCompatImageView videoFab;
    private Session mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);
        mSession = new Session(this);
        customBottomNavigationView.inflateMenu(R.menu.menus);
        customBottomNavigationView.setOnNavigationItemSelectedListener(ActivityHome.this);

        videoFab.setOnClickListener(view -> getSupportFragmentManager().beginTransaction().
                replace(R.id.fragment_container,
                        FragmentTalkToUs.newInstance()).commitNow());
        Intent intent = getIntent();
        if (null != intent) {
                                                                                          String shoping = intent.getStringExtra("market");
            if (!TextUtils.isEmpty(shoping)) {
                customBottomNavigationView.setSelectedItemId(R.id.market_tv);
               /* getSupportFragmentManager().beginTransaction().
                        replace(R.id.fragment_container,
                                FragmentMarket.newInstance()).commitNow();*/

            } else {
                launchFragment();
            }
        } else {
            launchFragment();
        }
        ArchLifecycleApp.getInstance().changeStatus("1");

    }

    @Override
    protected void setText() {

    }

    @Override
    protected void setOnClick() {

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_home;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.home_iv:
                launchFragment();
                return true;
            case R.id.chat_tv:
                if (mSession.getuserPaid().equalsIgnoreCase("free")) {
                    getSupportFragmentManager().beginTransaction().
                            replace(R.id.fragment_container,
                                    FreeUserFragment.newInstance()).commitNow();
                } else {
                    getSupportFragmentManager().beginTransaction().
                            replace(R.id.fragment_container,
                                    FragmentChat.newInstance()).commitNow();
                }

                return true;
            case R.id.market_tv:
                getSupportFragmentManager().beginTransaction().
                        replace(R.id.fragment_container,
                                FragmentMarket.newInstance()).commitNow();
                return true;
            case R.id.profile_tv:

                getSupportFragmentManager().beginTransaction().
                        replace(R.id.fragment_container,
                                FragmentProfile.newInstance()).commitNow();
                return true;

            case R.id.pongo_tv:

                getSupportFragmentManager().beginTransaction().
                        replace(R.id.fragment_container,
                                FragmentTalkToUs.newInstance()).commitNow();
                return true;
        }
        return false;
    }

    private void launchFragment() {
        getSupportFragmentManager().beginTransaction().
                replace(R.id.fragment_container,
                        FragmentHome.newInstance()).commitNow();
    }


}
