package com.pongo.health.ui.vet;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.OtpModel;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityVetJoin extends BaseActivity {

    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.first_name)
    EditText firstNameTv;
    @BindView(R.id.last_name)
    EditText lastNameTv;
    @BindView(R.id.technicain_ll)
    LinearLayout technicainLl;
    @BindView(R.id.veterian_ll)
    LinearLayout veterianLl;
    private String vetType = "";
    private OtpModel otpModel;
    private String mLoginType;
    private String mMobile;
    private String str_firstname, str_lastname;
    private String mPassword;
    private String mEmail;
    public static String checkType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Gson gson = new Gson();
        otpModel = gson.fromJson(getIntent().getStringExtra("otp"), OtpModel.class);
        mLoginType = getIntent().getStringExtra("login_type");
        mMobile = getIntent().getStringExtra("phone");
        mEmail = getIntent().getStringExtra("email");
        mPassword = getIntent().getStringExtra("password");

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.technicain_ll:
                checkType = "technician";
                technicainLl.setBackground(getResources().getDrawable(R.drawable.rectangle_shaped_two));
                veterianLl.setBackground(getResources().getDrawable(R.drawable.rectangle_shaped));
                vetType = "technician";
                break;
            case R.id.veterian_ll:
                veterianLl.setBackground(getResources().getDrawable(R.drawable.rectangle_shaped_two));
                technicainLl.setBackground(getResources().getDrawable(R.drawable.rectangle_shaped));
                vetType = "vet";
                break;
            case R.id.blue_btn_tv:
                verifyData();

                break;
        }
    }

    private void verifyData() {
        str_firstname = firstNameTv.getText().toString().trim();
        str_lastname = lastNameTv.getText().toString().trim();

        if (str_firstname.isEmpty() && str_lastname.isEmpty()) {
            Toast.makeText(this, "Please enter your details for signup", Toast.LENGTH_SHORT).show();

        } else if (str_firstname.isEmpty()) {
            firstNameTv.requestFocus();
            firstNameTv.setError("Please enter your first name");

        } else if (str_lastname.isEmpty()) {
            lastNameTv.requestFocus();
            lastNameTv.setError("Please enter your last name");

        } else if (vetType.isEmpty()) {
            Toast.makeText(this, "Please select Vet or Technician", Toast.LENGTH_SHORT).show();

        } else {
            Intent intent = new Intent(this, ActivityAboutYourself.class);
            Gson gson = new Gson();
            String myJson = gson.toJson(otpModel);
            intent.putExtra("otp", myJson);
            intent.putExtra("login_type", mLoginType);
            intent.putExtra("phone", mMobile);
            intent.putExtra("email", mEmail);
            intent.putExtra("password", mPassword);
            intent.putExtra("first_name", str_firstname);
            intent.putExtra("last_name", str_lastname);
            intent.putExtra("vetType", vetType);
            startActivity(intent);
           /* if ("technician".equals(vetType)) {
                startActivity(intent);
            } else {
                startActivity(new Intent(this, ActivityAboutYourself.class));
            }*/
        }
    }

    @Override
    protected void setText() {
        continueTv.setText(R.string.continue_);

    }

    @Override
    protected void setOnClick() {
        vTbIv.setOnClickListener(this);
        continueTv.setOnClickListener(this);
        technicainLl.setOnClickListener(this);
        veterianLl.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_vet_join;
    }
}
