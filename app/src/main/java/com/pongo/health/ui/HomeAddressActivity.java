package com.pongo.health.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.StateListAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.AddressModel;
import com.pongo.health.model.StateListModel;
import com.pongo.health.utils.Constants;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.google.gson.Gson;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeAddressActivity extends BaseActivity {
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.blue_btn_tv)
    TextView submitTv;
    @BindView(R.id.street_et)
    EditText streetEt;
    @BindView(R.id.apt_et)
    EditText aptEt;
    @BindView(R.id.city_et)
    EditText cityEt;
    @BindView(R.id.state_et)
    TextView stateEt;
    @BindView(R.id.zip_et)
    EditText zipEt;
    @BindView(R.id.location_rv)
    RecyclerView locationRv;
    private Session mSession;
    private AddressModel mAddressModel;
    private AddressModel.HomeaddressBean mHomeAddress;
    private String state, apt, city, street, zip;
    private String addressId = "";
    private CustomDialog mLoadingView;
    private List<StateListModel.StatelistBean> locationList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);
        Gson gson = new Gson();
        mAddressModel = gson.fromJson(getIntent().getStringExtra("model"), AddressModel.class);
        if (mAddressModel.getHomeaddress().size() > 0) {
            mHomeAddress = mAddressModel.getHomeaddress().get(0);
            addressId = mHomeAddress.getId();
            setData();
            tbTv.setText("Update Billing Address");
            submitTv.setText("Update");

        } else {
            tbTv.setText("Add Billing Address");
            submitTv.setText("Add");

        }
        getState();
    }

    private void setData() {
        streetEt.setText(mHomeAddress.getStreet_address());
        aptEt.setText(mHomeAddress.getApartment_detail());
        cityEt.setText(mHomeAddress.getCity());
        stateEt.setText(mHomeAddress.getState());
        zipEt.setText(mHomeAddress.getZip_code());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.blue_btn_tv:
                if (InternetConnection.checkConnection(this)) {
                    verify_data();
                } else {
                    Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.state_et:
                if (locationRv.getVisibility() == View.VISIBLE) {
                    locationRv.setVisibility(View.GONE);

                } else {
                    Constants.hideKeyboard(this);
                    locationRv.setVisibility(View.VISIBLE);

                }
                break;
        }

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {


    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        submitTv.setOnClickListener(this);
        stateEt.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_home_address;
    }

    private void setLocationRv() {
        locationRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        itemClickListener = (view, position) -> {
            stateEt.setText(locationList.get(position).getState());
            locationRv.setVisibility(View.GONE);

        };
        locationRv.setAdapter(new StateListAdapter(this, locationList, itemClickListener));


    }

    private void getState() {
//        mLoadingView.show();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getStateList().enqueue(new Callback<StateListModel>() {
            public void onResponse(Call<StateListModel> call, Response<StateListModel> response) {
//                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((StateListModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        locationList = response.body().getStatelist();
                        setLocationRv();
                    }
                }

            }

            public void onFailure(Call<StateListModel> call, Throwable th) {
//                mLoadingView.hideDialog();
//                Toast.makeText(ShipAddressActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });

    }

    private void verify_data() {
//        String team = spin_team.getSelectedItem().toString();
        street = streetEt.getText().toString().trim();
        apt = aptEt.getText().toString().trim();
        city = cityEt.getText().toString().trim();
        state = stateEt.getText().toString().trim();
        zip = zipEt.getText().toString().trim();


        if (street.isEmpty() && apt.isEmpty() && city.isEmpty() && state.isEmpty() && zip.isEmpty()) {
            Toast.makeText(this, "Please enter detail in all fields", Toast.LENGTH_SHORT).show();

        } else if (street.isEmpty()) {
            streetEt.requestFocus();
            streetEt.setError("Please enter your street address");
        } else if (city.isEmpty()) {
            cityEt.requestFocus();
            cityEt.setError("Please enter your city");

        } else if (state.isEmpty()) {
            stateEt.requestFocus();
            stateEt.setError("Please enter your state");

        } else if (zip.isEmpty()) {
            zipEt.requestFocus();
            zipEt.setError("Please enter your zip code");

        } else {
            if (InternetConnection.checkConnection(this)) {
                addAddress();
            } else {
                Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void addAddress() {
        this.mLoadingView.show();
        String userid = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).addHomeAddress(userid, street, apt, addressId, city, state, zip).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
//                            String message = jsonObject.getString("message");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    String message = jsonObject.getString("detail");
                                    Toast.makeText(HomeAddressActivity.this, message, Toast.LENGTH_SHORT).show();

                                } else {
                                    if (TextUtils.isEmpty(addressId)) {
                                        Toast.makeText(HomeAddressActivity.this, "Failed to add address", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(HomeAddressActivity.this, "Failed to update address", Toast.LENGTH_SHORT).show();

                                    }
                                }
                            } else {
                                if (TextUtils.isEmpty(addressId)) {
                                    Toast.makeText(HomeAddressActivity.this, "Failed to add address", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(HomeAddressActivity.this, "Failed to update address", Toast.LENGTH_SHORT).show();

                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(HomeAddressActivity.this, "Failed to connect", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

}

