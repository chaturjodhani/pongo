package com.pongo.health.ui.vet;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agik.AGIKSwipeButton.Controller.OnSwipeCompleteListener;
import com.agik.AGIKSwipeButton.View.Swipe_Button_View;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kinda.alert.KAlertDialog;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.TechDashboradResponseModel;
import com.pongo.health.model.VetDashBoardResponseModel;
import com.pongo.health.utils.ArchLifecycleApp;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.SquareLayout;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityTechnician extends BaseActivity {

    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.w_pet_iv)
    ImageView vTechIv;
    @BindView(R.id.my_profile_tv)
    TextView myProfileTv;
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.w_pet_name_tv)
    TextView techNameTv;
    @BindView(R.id.experience)
    TextView experienceTv;
    @BindView(R.id.type)
    TextView expertType;
    @BindView(R.id.walk_count)
    TextView walkCount;
    @BindView(R.id.request_count)
    TextView requestCount;
    @BindView(R.id.profile_layout)
    LinearLayout profileLl;
    @BindView(R.id.pw_room_ll)
    SquareLayout pwRoomLl;
    @BindView(R.id.request_room_ll)
    SquareLayout requestRoom;
    @BindView(R.id.swipe_button)
    Swipe_Button_View activeButton;
    private Session mSession;
    private CustomDialog mLoadingView;
    String adminStatus = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);
        activeListener();
        ArchLifecycleApp.getInstance().changeStatus("1");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (InternetConnection.checkConnection(ActivityTechnician.this)) {
            getDashBoard();
        } else {
            Toast.makeText(ActivityTechnician.this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
    }

    private void activeListener() {
        activeButton.setOnSwipeCompleteListener_forward_reverse(new OnSwipeCompleteListener() {
            @Override
            public void onSwipe_Forward(Swipe_Button_View swipeView) {
                if (adminStatus.equalsIgnoreCase("0")) {
                    setDoctorStatus("0");
                    popupDialog("Your Information is being reviewed by the Pongo Team. Please check back later");
                } else {
                    updateVetStatus("1");
                    activeButton.setText("Go Offline");
                    activeButton.setSwipeBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    activeButton.setTextColor(getResources().getColor(R.color.white_color));
//                activeButton.set(getResources().getColor(R.color.white_color));
                }
            }

            @Override
            public void onSwipe_Reverse(Swipe_Button_View swipeView) {
                if (adminStatus.equalsIgnoreCase("0")) {
                    setDoctorStatus("1");
                    popupDialog("Your Information is being reviewed by the Pongo Team. Please check back later");
                } else {
                    updateVetStatus("0");
                    activeButton.setText(getResources().getString(R.string.receive_patients));
                    activeButton.setSwipeBackgroundColor(getResources().getColor(R.color.white_color));
                    activeButton.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            }
        });
    }

    private void popupDialog(String message) {
        KAlertDialog kAlertDialog = new KAlertDialog(this, KAlertDialog.WARNING_TYPE);
        kAlertDialog.setTitleText("Alert!");
        kAlertDialog.setContentText(message);
        kAlertDialog.setCancelable(true);
        kAlertDialog.setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(KAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
            }
        })
                .show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.my_profile_tv:
                startActivity(new Intent(this, ActivityLogout.class));
                break;
            case R.id.pw_room_ll:
                startActivity(new Intent(this, ActivityChatWaiting.class));
                break;
            case R.id.request_room_ll:
                startActivity(new Intent(this, ActivityRequestorChatRoom.class));
                break;
            case R.id.profile_layout:
                startActivity(new Intent(this, ActivityProfile.class));
                break;
            case R.id.blue_btn_tv:
                // startActivity(new Intent(this,ActivityLogout.class));
                break;
            case R.id.v_tb_iv:
                onBackPressed();
                finish();
                break;
        }
    }

    @Override
    protected void setText() {
        continueTv.setText(R.string.receive_patients);
    }


    @Override
    protected void setOnClick() {
        myProfileTv.setOnClickListener(this);
        continueTv.setOnClickListener(this);
        vTbIv.setOnClickListener(this);
        pwRoomLl.setOnClickListener(this);
        profileLl.setOnClickListener(this);
        requestRoom.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_technician;
    }

    private void updateVetStatus(String status) {
        mLoadingView.show();
        String userId = mSession.getuser_id();
        if (!TextUtils.isEmpty(userId)) {
            ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).updateVetAvailability(status, userId).enqueue(new Callback<ResponseBody>() {
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    mLoadingView.hideDialog();
                    if (response.body() != null) {
                        String responseData = "";
                        try {
                            responseData = response.body().string();
                            if (!TextUtils.isEmpty(responseData)) {
                                JSONObject jsonObject = new JSONObject(responseData);
                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("detail");
                                if (!TextUtils.isEmpty(status)) {
                                    if (status.equalsIgnoreCase("success")) {
                                        Toast.makeText(ActivityTechnician.this, message, Toast.LENGTH_SHORT).show();
                                    }
                                    else {
                                        Toast.makeText(ActivityTechnician.this, message, Toast.LENGTH_SHORT).show();
                                        if (status.equalsIgnoreCase("0")) {
                                            activeButton.setreverse_180();
                                            activeButton.swipe_reverse = true;
                                            activeButton.setText("Go Offline");
                                            activeButton.setSwipeBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                            activeButton.setTextColor(getResources().getColor(R.color.white_color));
                                        } else {
                                            activeButton.setreverse_0();
                                            activeButton.swipe_reverse = false;
                                            activeButton.setText(getResources().getString(R.string.receive_patients));
                                            activeButton.setSwipeBackgroundColor(getResources().getColor(R.color.white_color));
                                            activeButton.setTextColor(getResources().getColor(R.color.colorPrimary));
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                public void onFailure(Call<ResponseBody> call, Throwable th) {
                    mLoadingView.hideDialog();
                    if (status.equalsIgnoreCase("0")) {
                        activeButton.setreverse_180();
                        activeButton.swipe_reverse = true;
                        activeButton.setText("Go Offline");
                        activeButton.setSwipeBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        activeButton.setTextColor(getResources().getColor(R.color.white_color));
                    } else {
                        activeButton.setreverse_0();
                        activeButton.swipe_reverse = false;
                        activeButton.setText(getResources().getString(R.string.receive_patients));
                        activeButton.setSwipeBackgroundColor(getResources().getColor(R.color.white_color));
                        activeButton.setTextColor(getResources().getColor(R.color.colorPrimary));
                    }
                    Log.e("cate", th.toString());
                }
            });
        }
    }

    private void getDashBoard() {
        mLoadingView.show();
        String userId = mSession.getuser_id();
        if (!TextUtils.isEmpty(userId)) {
            ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).techDashBoard(userId).enqueue(new Callback<TechDashboradResponseModel>() {
                public void onResponse(Call<TechDashboradResponseModel> call, Response<TechDashboradResponseModel> response) {
                    mLoadingView.hideDialog();
                    if (response.body() != null) {
                        try {
                            TechDashboradResponseModel responseData = response.body();
                            String status = responseData.getStatus();
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    String doctorStatus = responseData.getDoctors().getRecieve_patient();
                                    adminStatus = responseData.getDoctors().getAdminstatus();
                                    setData(responseData);
                                    if (!TextUtils.isEmpty(doctorStatus)) {
                                        setDoctorStatus(doctorStatus);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                public void onFailure(Call<TechDashboradResponseModel> call, Throwable th) {
                    mLoadingView.hideDialog();
                    Log.e("cate", th.toString());
                }
            });
        }
    }

    private void setData(TechDashboradResponseModel responseData) {
        TechDashboradResponseModel.DoctorsBean doctorsBean = responseData.getDoctors();
        Glide.with(this).load(doctorsBean.getUserimage()).apply(RequestOptions.circleCropTransform()).into(vTechIv);
        techNameTv.setText("DR, " + doctorsBean.getFirst_name());
        experienceTv.setText(doctorsBean.getExpertise());
        expertType.setText(doctorsBean.getVet_type());
        walkCount.setText(String.valueOf(responseData.getWalking()));
        requestCount.setText(String.valueOf(responseData.getRequestor()));
    }

    private void setDoctorStatus(String doctorStatus) {
        if (doctorStatus.equalsIgnoreCase("0")) {  //0 for offline
            activeButton.setreverse_0();
            activeButton.swipe_reverse = false;
            activeButton.setText(getResources().getString(R.string.receive_patients));
            activeButton.setSwipeBackgroundColor(getResources().getColor(R.color.white_color));
            activeButton.setTextColor(getResources().getColor(R.color.colorPrimary));
        } else {
            activeButton.setreverse_180();
            activeButton.swipe_reverse = true;
            activeButton.setText("Go Offline");
            activeButton.setSwipeBackgroundColor(getResources().getColor(R.color.colorPrimary));
            activeButton.setTextColor(getResources().getColor(R.color.white_color));
        }
    }
}
