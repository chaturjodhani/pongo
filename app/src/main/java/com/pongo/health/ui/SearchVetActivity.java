package com.pongo.health.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.PastPongoVetsAdapter;
import com.pongo.health.adapter.RecentSearchesAdapter;
import com.pongo.health.adapter.SearchHospitalAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.HospitalSearchModel;
import com.pongo.health.model.MarketResponseModel;
import com.pongo.health.model.PastVetSearchModel;
import com.pongo.health.model.SearchProductModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.GPSTracker;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchVetActivity extends BaseActivity {
    @BindView(R.id.tb_iv)
    ImageView backImage;
    @BindView(R.id.past_pongo_vents_rv)
    RecyclerView pastPongoVentsRv;
    @BindView(R.id.search_et)
    EditText searchEt;
    @BindView(R.id.search_icon)
    ImageView searchIcon;
    @BindView(R.id.blue_btn_tv)
    TextView submitTv;
    private ItemClickListener itemClickListener;
    private CustomDialog mLoadingView;
    private Session mSession;
    private List<PastVetSearchModel.PastdoctorBean> pastPongoVentsList = new ArrayList<>();
    @BindView(R.id.hospital_rv)
    RecyclerView searcheHospitalRv;
    private List<HospitalSearchModel.ResultsBean> hospitalList = new ArrayList<>();
    private GPSTracker gps;
    private int mPosition = -1;
    String hospital = "";
    String screen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        gps = new GPSTracker(this);
        pastPongoVentsRv.setNestedScrollingEnabled(false);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
        submitTv.setText("Request another Vet");
        submitTv.setVisibility(View.GONE);
        searchEt.setHint("Enter your Vets Hospital");
        if (InternetConnection.checkConnection(this)) {
            String key = getIntent().getStringExtra("key");
            if (!TextUtils.isEmpty(key)) {
                searchEt.setText(key);
                getSearch(key);
            }
        } else {
            Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
        setSearchListener();
        Intent intent = getIntent();
        if (null != intent) {
            screen = intent.getStringExtra("screen");
        }
    }


    private void setSearchListener() {
        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String search = searchEt.getText().toString();
                if (!TextUtils.isEmpty(search)) {
                    getSearch(search);
                }
            }
        });
        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                if (gps.canGetLocation()) {
                    getSearch(editable.toString());

                } else {
                    gps.showSettingsAlert();
                }
            }
        });
        searchEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
              /*  if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String search = searchEt.getText().toString();
                    getSearch(search);
                    return true;
                }*/

                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//Hide:
                if (imm != null) {
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }
                return false;
            }
        });
    }

    private void getDoctorSearch(String s) {
        this.mLoadingView.show();
        submitTv.setVisibility(View.VISIBLE);

        String userId = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getSearchVet(s, userId).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    JSONArray doctorArray = jsonObject.getJSONArray("doctors");
                                    pastPongoVentsList.clear();
                                    for (int i = 0; i < doctorArray.length(); i++) {
                                        PastVetSearchModel.PastdoctorBean productlistBean = new PastVetSearchModel.PastdoctorBean();
                                        JSONObject obj = doctorArray.getJSONObject(i);
                                        productlistBean.setUserId(obj.getString("userId"));
                                        productlistBean.setName(obj.getString("name"));
                                        productlistBean.setExpertise(obj.getString("expertise"));
                                        productlistBean.setDate(obj.getString("date"));
                                        productlistBean.setCallprice(obj.getString("callprice"));
//                                        productlistBean.setD(obj.getString("description"));
                                        productlistBean.setUserimage(obj.getString("userimage"));
                                        pastPongoVentsList.add(productlistBean);
                                    }
                                    setPastPongoVents();
                                } else {
                                    Toast.makeText(SearchVetActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(SearchVetActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(SearchVetActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.blue_btn_tv:
                startActivity(new Intent(this, RequestAnothervetActivity.class).putExtra("hospital", hospital));
                break;
        }
    }


    @Override
    protected void setText() {
    }

    @Override
    protected void setOnClick() {
        backImage.setOnClickListener(this);
        submitTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_search_vet;
    }

    private void setPastPongoVents() {
        pastPongoVentsRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 2; i++) {
            pastPongoVentsList.add(new PetsModel("Dog Specialist (3 yrs experiences) ",
                    "Dr Andrew Fleming",
                    "lorem deus simply during" +
                            "lorem deus simply during"));
        }*/
        pastPongoVentsRv.setAdapter(new PastPongoVetsAdapter(this, pastPongoVentsList,screen));
    }

    private void getSearch(String s) {
//        this.mLoadingView.show();
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();
        submitTv.setVisibility(View.GONE);

//        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&radius=1500000&type=drugstore|hospital|veterinary_care|pharmacy|petstore&keyword=" + s + "&key=AIzaSyCIhWIx08t5ap5neBIX1B3npieSP9HQSdY";
        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&radius=1500000&keyword=" + s + "&key=AIzaSyCIhWIx08t5ap5neBIX1B3npieSP9HQSdY";
        ((ApiInterface) ApiClient.getDynamicClient().create(ApiInterface.class)).getHospitalSearchData(url).enqueue(new Callback<HospitalSearchModel>() {
            public void onResponse(Call<HospitalSearchModel> call, Response<HospitalSearchModel> response) {
//                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((HospitalSearchModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("OK")) {
                        hospitalList.clear();
                        hospitalList = response.body().getResults();
                        setMarketSearches();
                    }
                }

            }

            public void onFailure(Call<HospitalSearchModel> call, Throwable th) {
//                mLoadingView.hideDialog();
                Toast.makeText(SearchVetActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void setMarketSearches() {
        mPosition = -1;
        searcheHospitalRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 4; i++) {
            medicationList.add(new PetsModel("$45.99", "Ophtha Care",
                    "lorem ipsum is a simply dummy text of printing and type setting industry"));
        }*/
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                mPosition = position;
                searcheHospitalRv.setVisibility(View.GONE);
                pastPongoVentsRv.setVisibility(View.VISIBLE);
                hospital = hospitalList.get(mPosition).getName();
                getDoctorSearch(hospital);
//                getSearch(hospital);
            }
        };
        searcheHospitalRv.setVisibility(View.VISIBLE);
        pastPongoVentsRv.setVisibility(View.GONE);

        searcheHospitalRv.setAdapter(new SearchHospitalAdapter(this, hospitalList, itemClickListener));
    }

}
