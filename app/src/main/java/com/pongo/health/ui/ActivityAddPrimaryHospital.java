package com.pongo.health.ui;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.PastPongoVetsAdapter;
import com.pongo.health.adapter.RecentSearchesAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.MarketResponseModel;
import com.pongo.health.model.PastVetSearchModel;
import com.pongo.health.model.PetsModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.DeleteListener;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityAddPrimaryHospital extends BaseActivity {

    @BindView(R.id.tb_iv)
    ImageView backImage;
    @BindView(R.id.clear_all_tv)
    TextView clearAll;
    @BindView(R.id.past_pongotv)
    TextView pastPongoTv;
    @BindView(R.id.recent_layout)
    LinearLayout recentLayout;
    @BindView(R.id.past_pongo_vents_rv)
    RecyclerView pastPongoVentsRv;
    @BindView(R.id.recent_searches_rv)
    RecyclerView recentSearchesRv;
    @BindView(R.id.search_et)
    EditText searchEt;
    @BindView(R.id.search_icon)
    ImageView searchIcon;
    private CustomDialog mLoadingView;
    private Session mSession;
    private List<PastVetSearchModel.PastdoctorBean> pastPongoVentsList = new ArrayList<>();
    private List<PastVetSearchModel.RecentBean> recentSearchesList = new ArrayList<>();
    private RecentSearchesAdapter recentSearchesAdapter;
    String screen = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);
        searchEt.setHint("Enter your Vets Hospital");
//        searchEt.setFocusable(false);
        searchEt.setInputType(InputType.TYPE_NULL);
        pastPongoVentsRv.setNestedScrollingEnabled(false);
        recentSearchesRv.setNestedScrollingEnabled(false);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);

        setSearchListener();
        Intent intent = getIntent();
        if (null != intent) {
            screen = intent.getStringExtra("screen");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (InternetConnection.checkConnection(this)) {
            getPastVet();
        } else {
            Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
    }

    private void setSearchListener() {
        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String search = searchEt.getText().toString();
//                if (!TextUtils.isEmpty(search)) {
                startActivity(new Intent(ActivityAddPrimaryHospital.this, SearchVetActivity.class)
                        .putExtra("key", search)
                        .putExtra("screen", screen));
//                }
            }
        });
        searchEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityAddPrimaryHospital.this, SearchVetActivity.class)
                        .putExtra("key", "")
                        .putExtra("screen", screen));

            }
        });
        searchEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String search = searchEt.getText().toString();
                    if (!TextUtils.isEmpty(search)) {
                        startActivity(new Intent(ActivityAddPrimaryHospital.this, SearchVetActivity.class)
                                .putExtra("key", search)
                                .putExtra("screen", screen));
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void getPastVet() {
        this.mLoadingView.show();
        String userId = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getPastVet(userId).enqueue(new Callback<PastVetSearchModel>() {
            public void onResponse(Call<PastVetSearchModel> call, Response<PastVetSearchModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((PastVetSearchModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        pastPongoVentsList = response.body().getPastdoctor();
                        recentSearchesList = response.body().getRecent();
                        if (recentSearchesList == null || recentSearchesList.size() == 0) {
                            recentLayout.setVisibility(View.GONE);
                        }
                        if (pastPongoVentsList == null || pastPongoVentsList.size() == 0) {
                            pastPongoTv.setVisibility(View.GONE);
                        }
                        setRecentSearches();
                        setPastPongoVents();
                    }
                }

            }

            public void onFailure(Call<PastVetSearchModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityAddPrimaryHospital.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.clear_all_tv:
                deleteRecent("all", 0);
                break;
        }
    }


    @Override
    protected void setText() {
    }

    @Override
    protected void setOnClick() {
        backImage.setOnClickListener(this);
        clearAll.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_add_primary_hospital;
    }

    private void setPastPongoVents() {
        pastPongoVentsRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 2; i++) {
            pastPongoVentsList.add(new PetsModel("Dog Specialist (3 yrs experiences) ",
                    "Dr Andrew Fleming",
                    "lorem deus simply during" +
                            "lorem deus simply during"));
        } */
        pastPongoVentsRv.setAdapter(new PastPongoVetsAdapter(this, pastPongoVentsList,screen));
    }

    private void setRecentSearches() {
        recentSearchesRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
//        recentSearchesList.add("Dr. Andrew Fleming");
//        recentSearchesList.add("Dr. Anny Marry");
        DeleteListener deleteListener = new DeleteListener() {
            @Override
            public void onDeleteClicked(View view, int position) {
                deleteRecent(recentSearchesList.get(position).getId(), position);
            }

            @Override
            public void onkeyClicked(View view, int position) {
                String search = recentSearchesList.get(position).getSearch();
                if (!TextUtils.isEmpty(search)) {
                    startActivity(new Intent(ActivityAddPrimaryHospital.this, SearchVetActivity.class)
                            .putExtra("key", search)
                            .putExtra("screen", screen));
                }
            }
        };
        recentSearchesAdapter = new RecentSearchesAdapter(this, recentSearchesList, deleteListener);
        recentSearchesRv.setAdapter(recentSearchesAdapter);
    }

    private void deleteRecent(String id, int position) {
        this.mLoadingView.show();
        String userid = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).deleteRecent(userid, id).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(ActivityAddPrimaryHospital.this, message, Toast.LENGTH_SHORT).show();
                                    if (id.equalsIgnoreCase("all")) {
                                        recentLayout.setVisibility(View.GONE);
                                        recentSearchesList.clear();
                                        recentSearchesAdapter.notifyDataSetChanged();
                                    } else {
                                        if (recentSearchesList.size() == 1) {
                                            recentSearchesList.remove(position);
                                            recentSearchesAdapter.notifyDataSetChanged();
                                            recentLayout.setVisibility(View.GONE);
                                        } else {
                                            recentSearchesList.remove(position);
                                            recentSearchesAdapter.notifyDataSetChanged();
                                        }
                                    }
                                } else {
                                    Toast.makeText(ActivityAddPrimaryHospital.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivityAddPrimaryHospital.this, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityAddPrimaryHospital.this, "Failed to delete", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

}
