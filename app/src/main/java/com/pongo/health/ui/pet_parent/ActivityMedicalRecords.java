package com.pongo.health.ui.pet_parent;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.pongo.health.R;
import com.pongo.health.adapter.AttachmentAdapter;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.HomeModel;
import com.pongo.health.ui.ActivityPdfView;
import com.pongo.health.ui.ImageViewActivity;
import com.pongo.health.ui.vet.ActivityPatientDetail;
import com.pongo.health.utils.ItemClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityMedicalRecords extends BaseActivity {

    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.medical_heading_tv)
    TextView medicalHeadingTv;
    @BindView(R.id.collect_records_tv)
    TextView collectRecordsTv;
    @BindView(R.id.blue_btn_tv)
    TextView letsDOItTv;
    @BindView(R.id.history_rv)
    RecyclerView historyRv;
    private HomeModel.PetlistBean mPetBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Gson gson = new Gson();
        mPetBean = gson.fromJson(getIntent().getStringExtra("model"), HomeModel.PetlistBean.class);
        if (null != mPetBean) {
            setData();
        }
    }

    private void setData() {
        if (null != mPetBean.getMedicalrecords() && mPetBean.getMedicalrecords().size() > 0) {
            setMedicalAdapter();
        }
        medicalHeadingTv.setText(Html.fromHtml("<p>Pongo will collect " + mPetBean.getPet_name() + "'s official " +
                "medical records for you. Just tell us which Vets you've seen </p>"));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                Gson gson = new Gson();
                String myJson = gson.toJson(mPetBean);
                startActivity(new Intent(this, ActivityVeterinaryClinics.class).putExtra("model", myJson));
                break;
            case R.id.collect_records_tv:
                startActivity(new Intent(this, ActivityCollectRecords.class));
                break;
            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("Medical Records");
        letsDOItTv.setText("Let's Do It");

    }

    @Override
    protected void setOnClick() {

        tbIv.setOnClickListener(this);
        letsDOItTv.setOnClickListener(this);
        collectRecordsTv.setOnClickListener(this);


    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_medical_records;
    }

    private void setMedicalAdapter() {
        historyRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                if (mPetBean.getMedicalrecords().get(position).contains(".pdf")) {
                    Intent intent2 = new Intent(ActivityMedicalRecords.this, ActivityPdfView.class);
                    String url2 = mPetBean.getMedicalrecords().get(position);
                    intent2.putExtra("name", "Attachments by patient");
                    intent2.putExtra("url", url2);
                    if (!TextUtils.isEmpty(url2)) {
                        startActivity(intent2);
                    } else {
                        Toast.makeText(ActivityMedicalRecords.this, "No pdf available", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    String url2 = mPetBean.getMedicalrecords().get(position);
                    Intent intent = new Intent(ActivityMedicalRecords.this, ImageViewActivity.class);
                    intent.putExtra("name", mPetBean.getPet_name());
                    intent.putExtra("time", "");
                    intent.putExtra("image", url2);
                    startActivity(intent);
                }
            }
        };
//        itemClickListener = (view, position) -> startActivity(new Intent(activity, ActivityYourPetSingleType.class));
        AttachmentAdapter mImagesAdapter = new AttachmentAdapter(this, mPetBean.getMedicalrecords(), itemClickListener);
        historyRv.setAdapter(mImagesAdapter);
    }

}
