package com.pongo.health.ui.vet;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.BillingAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.EarningModel;
import com.pongo.health.ui.AlltransactionActivity;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllPatientHistoryActivity extends BaseActivity {
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.billings_rv)
    RecyclerView billingsRv;
    private List<EarningModel.PethistoryBean> billingList = new ArrayList<>();
    BillingAdapter billingAdapter;
    private CustomDialog mLoadingView;
    private Session mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);
        Intent intent = getIntent();
        if (null != intent) {
            String month = intent.getStringExtra("month");
            if (!TextUtils.isEmpty(month)) {
                if (InternetConnection.checkConnection(AllPatientHistoryActivity.this)) {
                    getHistory(month);
                } else {
                    Toast.makeText(AllPatientHistoryActivity.this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                clearActivityStack();
                break;
        }

    }


    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("Patient History");
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_all_patient_history;
    }

    private void getHistory(String month) {
        this.mLoadingView.show();
        String id = mSession.getuser_id();

        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).seeAllPatientHistory(id, month).enqueue(new Callback<EarningModel>() {
            public void onResponse(Call<EarningModel> call, Response<EarningModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((EarningModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {

                        billingList = response.body().getPethistory();
                        if (null != billingList && billingList.size() > 0) {
                            setBillingRv();
                        }

                    }
                }

            }

            public void onFailure(Call<EarningModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(AllPatientHistoryActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void setBillingRv() {
        billingsRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 3; i++) {
            billingList.add(new CartModel(R.drawable.senior, "Tommy - yrs", "Robies",
                    "Dog | pug"));

        }*/
        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
//                startActivity(new Intent(ActivityBillings.this, ActivityPatientDetail.class));
                Gson gson = new Gson();
                String myJson = gson.toJson(billingList.get(position));
                startActivity(new Intent(AllPatientHistoryActivity.this, ActivityPatientDetail.class)
                        .putExtra("model", myJson)
                        .putExtra("type", "hide"));
            }
        };
        billingAdapter = new BillingAdapter(this, billingList, itemClickListener);
        billingsRv.setAdapter(billingAdapter);
    }

}