package com.pongo.health.ui.vet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.StateListAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.StateListModel;
import com.pongo.health.utils.Constants;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;
import com.pongo.health.utils.UsPhoneNumberFormatter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VetPaymentActivity extends BaseActivity {
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    private CustomDialog mLoadingView;
    private Session mSession;
    EditText Account_name, Bank_name, Isbn_number, bank_address;
    @BindView(R.id.street_et)
    EditText streetEt;
    @BindView(R.id.apt_et)
    EditText aptEt;
    @BindView(R.id.city_et)
    EditText cityEt;
    @BindView(R.id.state_et)
    TextView stateEt;
    @BindView(R.id.zip_et)
    EditText zipEt;
    @BindView(R.id.location_rv)
    RecyclerView locationRv;
    private List<StateListModel.StatelistBean> locationList = new ArrayList<>();
    private ItemClickListener itemClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(VetPaymentActivity.this);
        mSession = new Session(VetPaymentActivity.this);
        init_();
        if (InternetConnection.checkConnection(VetPaymentActivity.this)) {
            getInfo();
            getState();
        } else {
            Toast.makeText(VetPaymentActivity.this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
    }

    private void init_() {
        Account_name = findViewById(R.id.acnt_txt_name);
        Bank_name = findViewById(R.id.bank_txt_name);
        Isbn_number = findViewById(R.id.txt_isbn);
        bank_address = findViewById(R.id.txt_bank_address);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_tb_iv:
                clearActivityStack();
                break;
            case R.id.blue_btn_tv:
                verify_data();
                break;
            case R.id.state_et:
                if (locationRv.getVisibility() == View.VISIBLE) {
                    locationRv.setVisibility(View.GONE);

                } else {
                    Constants.hideKeyboard(this);
                    locationRv.setVisibility(View.VISIBLE);

                }
                break;
        }

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("Payout Method");
        continueTv.setText(R.string.continue_);
        stateEt.setOnClickListener(this);

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        continueTv.setOnClickListener(this);
        stateEt.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_vet_payment;
    }

    private void setLocationRv() {
        locationRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        itemClickListener = (view, position) -> {
            stateEt.setText(locationList.get(position).getState());
            locationRv.setVisibility(View.GONE);

        };
        locationRv.setAdapter(new StateListAdapter(this, locationList, itemClickListener));


    }

    private void getState() {
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getStateList().enqueue(new Callback<StateListModel>() {
            public void onResponse(Call<StateListModel> call, Response<StateListModel> response) {
                if (response.body() != null) {
                    String status = ((StateListModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        locationList = response.body().getStatelist();
                        setLocationRv();
                    }
                }

            }

            public void onFailure(Call<StateListModel> call, Throwable th) {
//                Toast.makeText(ShipAddressActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });

    }

    private void getInfo() {
        this.mLoadingView.show();
        String userId = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getPayoutInformation(userId).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    JSONObject detaill_object = jsonObject.getJSONObject("detail");
                                    String accountNumber = detaill_object.getString("account_number");
                                    String bankname = detaill_object.getString("bank_name");
                                    String isbn = detaill_object.getString("isbn_number");
                                    String bankaddress = detaill_object.getString("bank_address");
                                    String address = detaill_object.getString("address");
                                    String apartment = detaill_object.getString("apartment");
                                    String city = detaill_object.getString("city");
                                    String state = detaill_object.getString("state");
                                    String zipcode = detaill_object.getString("zipcode");

                                    Account_name.setText(accountNumber);
                                    Bank_name.setText(bankname);
                                    Isbn_number.setText(isbn);
                                    bank_address.setText(bankaddress);
                                    streetEt.setText(address);
                                    aptEt.setText(apartment);
                                    cityEt.setText(city);
                                    stateEt.setText(state);
                                    zipEt.setText(zipcode);


                                } else {
                                    Toast.makeText(VetPaymentActivity.this, "Opps .. Something went wrong", Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(VetPaymentActivity.this, "Opps .. Something went wrong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(VetPaymentActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void verify_data() {
//        String team = spin_team.getSelectedItem().toString();
      /*  String banknumber = Account_name.getText().toString();
        String isbn = Isbn_number.getText().toString();
        String bankname = Bank_name.getText().toString();
        String address = bank_address.getText().toString();*/

        String street = streetEt.getText().toString().trim();
        String apt = aptEt.getText().toString().trim();
        String city = cityEt.getText().toString().trim();
        String state = stateEt.getText().toString().trim();
        String zip = zipEt.getText().toString().trim();

     /*   if (banknumber.isEmpty() && isbn.isEmpty() && bankname.isEmpty() && address.isEmpty()) {
            Toast.makeText(this, "Please enter detail in all fields", Toast.LENGTH_SHORT).show();

        } else if (banknumber.isEmpty()) {
            Account_name.requestFocus();
            Account_name.setError("Please enter your account number");

        } else if (bankname.isEmpty()) {
            Bank_name.requestFocus();
            Bank_name.setError("Please enter bank name");

        } else if (isbn.isEmpty()) {
            Isbn_number.requestFocus();
            Isbn_number.setError("Please enter ISBN");

        } else if (address.isEmpty()) {
            bank_address.requestFocus();
            bank_address.setError("Please enter bank address");

        } else*/
            if (street.isEmpty()) {
            streetEt.requestFocus();
            streetEt.setError("Please enter your address");
        } else if (city.isEmpty()) {
            cityEt.requestFocus();
            cityEt.setError("Please enter your city");

        } else if (state.isEmpty()) {
//            stateEt.requestFocus();
//            stateEt.setError("Please enter your state");
            Toast.makeText(this, "Please select your state", Toast.LENGTH_SHORT).show();
        } else if (zip.isEmpty()) {
            zipEt.requestFocus();
            zipEt.setError("Please enter your zip code");

        } else {
            if (InternetConnection.checkConnection(this)) {
                UpdatePayout();
            } else {
                Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void UpdatePayout() {
        mLoadingView.show();
        String userId = mSession.getuser_id();
//        String banknumber = Account_name.getText().toString();
//        String isbn = Isbn_number.getText().toString();
//        String bankname = Bank_name.getText().toString();
//        String address = bank_address.getText().toString();
        String useraddress = streetEt.getText().toString();
        String apt = aptEt.getText().toString().trim();
        String city = cityEt.getText().toString().trim();
        String state = stateEt.getText().toString().trim();
        String zip = zipEt.getText().toString().trim();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("doctor_id", userId);
        builder.addFormDataPart("account_number", "");
        builder.addFormDataPart("bank_name", "");
        builder.addFormDataPart("isbn_number", "");
        builder.addFormDataPart("bank_address", "");
        builder.addFormDataPart("address", useraddress);
        builder.addFormDataPart("apartment", apt);
        builder.addFormDataPart("city", city);
        builder.addFormDataPart("state", state);
        builder.addFormDataPart("zipcode", zip);

        MultipartBody requestBody = builder.build();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.addPayoutMethod(requestBody);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("detail");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(VetPaymentActivity.this, message, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(VetPaymentActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(VetPaymentActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mLoadingView.hideDialog();
                Toast.makeText(VetPaymentActivity.this, "Failed to update payout method", Toast.LENGTH_SHORT).show();
                Log.e("cate", t.toString());
            }

        });

    }

}