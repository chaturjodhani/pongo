package com.pongo.health.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.adapter.SearchHospitalAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.HospitalSearchModel;
import com.pongo.health.ui.checkout.ActivityCheckOutYourPrescriptionNew;
import com.pongo.health.ui.checkout.ActivityCheckoutAdditionalProfile;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.GPSTracker;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddDoctorActivity extends BaseActivity {
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.skip)
    TextView skipTv;
    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.vet_heading_one_tv)
    TextView vetHeadingOneTv;
    @BindView(R.id.vet_heading_second_tv)
    TextView vetHeadingSecondTv;
    @BindView(R.id.hospital_rv)
    RecyclerView searcheHospitalRv;
    @BindView(R.id.search_second_et)
    EditText searchEt;
    private List<HospitalSearchModel.ResultsBean> hospitalList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private CustomDialog mLoadingView;
    private GPSTracker gps;
    private int mPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_add_doctor);
        ButterKnife.bind(this);
        searchEt.setHint("Search for doctor");
        mLoadingView = new CustomDialog(this);
        setSearchListener();
        gps = new GPSTracker(this);
    }

    private void setSearchListener() {
        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                if (gps.canGetLocation()) {
                    getSearch(editable.toString());

                } else {
                    gps.showSettingsAlert();
                }
            }
        });

        searchEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
              /*  if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String search = searchEt.getText().toString();
                    getSearch(search);
                    return true;
                }*/
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//Hide:
                if (imm != null) {
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }
                return false;
            }
        });
    }

    private void getSearch(String s) {
//        this.mLoadingView.show();
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();

//        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&radius=1500000&type=doctor|hospital|zoo|veterinary_care&keyword=" + s + "&key=AIzaSyCIhWIx08t5ap5neBIX1B3npieSP9HQSdY";
        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&radius=1500000&keyword=" + s + "&key=AIzaSyCIhWIx08t5ap5neBIX1B3npieSP9HQSdY";
        ((ApiInterface) ApiClient.getDynamicClient().create(ApiInterface.class)).getHospitalSearchData(url).enqueue(new Callback<HospitalSearchModel>() {
            public void onResponse(Call<HospitalSearchModel> call, Response<HospitalSearchModel> response) {
//                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((HospitalSearchModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("OK")) {
                        hospitalList.clear();
                        hospitalList = response.body().getResults();
                        /*List<MarketResponseModel.ProductlistBean> productlist = new ArrayList<>();
                        for (int i = 0; i < response.body().getProductdata().size(); i++) {
                            MarketResponseModel.ProductlistBean productlistBean = new MarketResponseModel.ProductlistBean();
                            productlistBean.setId(response.body().getProductdata().get(i).getId());
                            productlistBean.setId(response.body().getProductdata().get(i).getId());
                            productlistBean.setName(response.body().getProductdata().get(i).getName());
                            productlistBean.setDescription(response.body().getProductdata().get(i).getDescription());
                            productlistBean.setOrignalprice(response.body().getProductdata().get(i).getOrignalprice());
                            productlistBean.setDiscountprice(response.body().getProductdata().get(i).getDiscountprice());
                            productlistBean.setProduct_type(response.body().getProductdata().get(i).isProduct_type());
                            productlistBean.setProduct_image(response.body().getProductdata().get(i).getProduct_image());
                            productlist.add(productlistBean);
                        }*/
                        setMarketSearches();
                    }
                }

            }

            public void onFailure(Call<HospitalSearchModel> call, Throwable th) {
//                mLoadingView.hideDialog();
                Toast.makeText(AddDoctorActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void setMarketSearches() {
        mPosition = -1;
        searcheHospitalRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 4; i++) {
            medicationList.add(new PetsModel("$45.99", "Ophtha Care",
                    "lorem ipsum is a simply dummy text of printing and type setting industry"));
        }*/
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                mPosition = position;
            }
        };

        searcheHospitalRv.setAdapter(new SearchHospitalAdapter(this, hospitalList, itemClickListener));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                if (mPosition >= 0) {
                    String pet_name = getIntent().getStringExtra("pet_name");
                    if (!TextUtils.isEmpty(pet_name)) {
                        String pet_image = getIntent().getStringExtra("pet_image");
                        String pet_type = getIntent().getStringExtra("pet_type");
                        String pet_size = getIntent().getStringExtra("pet_size");
                        String pet_age = getIntent().getStringExtra("pet_age");
                        String pet_health = getIntent().getStringExtra("pet_health");
                        String recommendId = getIntent().getStringExtra("recommendId");
                        String breed = getIntent().getStringExtra("breed");
                        String address = hospitalList.get(mPosition).getVicinity();
                        String hospital = hospitalList.get(mPosition).getName();
                        startActivity(new Intent(this, ActivityCheckoutAdditionalProfile.class)
                                .putExtra("pet_name", pet_name)
                                .putExtra("pet_type", pet_type)
                                .putExtra("pet_image", pet_image)
                                .putExtra("pet_size", pet_size)
                                .putExtra("pet_age", pet_age)
                                .putExtra("pet_health", pet_health)
                                .putExtra("recommendId", recommendId)
                                .putExtra("breed", breed)
                                .putExtra("address", address)
                                .putExtra("hospital", hospital)
                                .putExtra("prescription_from", "doctor")
                                .putExtra("imagelist", new ArrayList<String>())
                        );
                    } else {
                        String name = getIntent().getStringExtra("name");
                        String address = hospitalList.get(mPosition).getVicinity();
                        String hospital = hospitalList.get(mPosition).getName();
                        String prodId = getIntent().getStringExtra("prodId");
                        String quantity = getIntent().getStringExtra("quantity");
                        String strength = getIntent().getStringExtra("strength");
                        String price = getIntent().getStringExtra("price");

                        startActivity(new Intent(this, ActivityCheckoutAdditionalProfile.class)
                                .putExtra("name", name)
                                .putExtra("address", address)
                                .putExtra("hospital", hospital)
                                .putExtra("prescription_from", "doctor")
                                .putExtra("imagelist", new ArrayList<String>())
                                .putExtra("prodId", prodId)
                                .putExtra("quantity", quantity)
                                .putExtra("strength", strength)
                                .putExtra("price", price)
                        );
                    }
                } else {
                    Toast.makeText(AddDoctorActivity.this, "Please select primary hospital", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.skip:
                String pet_name = getIntent().getStringExtra("pet_name");
                if (!TextUtils.isEmpty(pet_name)) {
                    String pet_image = getIntent().getStringExtra("pet_image");
                    String pet_type = getIntent().getStringExtra("pet_type");
                    String pet_size = getIntent().getStringExtra("pet_size");
                    String pet_age = getIntent().getStringExtra("pet_age");
                    String pet_health = getIntent().getStringExtra("pet_health");
                    String recommendId = getIntent().getStringExtra("recommendId");
                    String breed = getIntent().getStringExtra("breed");
                    String address = "";
                    String hospital = "";
                    startActivity(new Intent(this, ActivityCheckoutAdditionalProfile.class)
                            .putExtra("pet_name", pet_name)
                            .putExtra("pet_type", pet_type)
                            .putExtra("pet_image", pet_image)
                            .putExtra("pet_size", pet_size)
                            .putExtra("pet_age", pet_age)
                            .putExtra("pet_health", pet_health)
                            .putExtra("recommendId", recommendId)
                            .putExtra("breed", breed)
                            .putExtra("address", address)
                            .putExtra("hospital", hospital)
                            .putExtra("prescription_from", "doctor")
                            .putExtra("imagelist", new ArrayList<String>())
                    );
                }
                else {
                    String name = getIntent().getStringExtra("name");
                    String address = "";
                    String hospital = "";
                    String prodId = getIntent().getStringExtra("prodId");
                    String quantity = getIntent().getStringExtra("quantity");
                    String strength = getIntent().getStringExtra("strength");
                    String price = getIntent().getStringExtra("price");

                    startActivity(new Intent(this, ActivityCheckoutAdditionalProfile.class)
                            .putExtra("name", name)
                            .putExtra("address", address)
                            .putExtra("hospital", hospital)
                            .putExtra("prescription_from", "doctor")
                            .putExtra("imagelist", new ArrayList<String>())
                            .putExtra("prodId", prodId)
                            .putExtra("quantity", quantity)
                            .putExtra("strength", strength)
                            .putExtra("price", price)
                    );
                }
                break;

            case R.id.v_tb_iv:
                onBackPressed();
                finish();
                break;
        }
    }

    @Override
    protected void setText() {
        vetHeadingOneTv.setText("Great, Now let's add your Primary hospital");
        vetHeadingSecondTv.setText("Please add your main hospital of employment. ");
//                "If you don't have one, simply skip the step");
        continueTv.setText(R.string.continue_);

    }

    @Override
    protected void setOnClick() {
        continueTv.setOnClickListener(this);
        vTbIv.setOnClickListener(this);
        skipTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_add_doctor;
    }
}
