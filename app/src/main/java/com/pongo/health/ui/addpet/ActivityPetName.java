package com.pongo.health.ui.addpet;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pongo.health.BuildConfig;
import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.ui.ActivityHome;
import com.pongo.health.ui.ActivityPayment;
import com.pongo.health.utils.CapitalizeFirstLetter;
import com.pongo.health.utils.Constants;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityPetName extends BaseActivity {

    @BindView(R.id.add_pet_name_et)
    EditText addPetNameEt;
    @BindView(R.id.pet_img)
    CircleImageView petImage;
    @BindView(R.id.blue_btn_tv)
    TextView nextTv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    String str_petname;
    String mCurrentPhotoPath;
    String res;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        petnameLisetner();

    }

    private void petnameLisetner() {
        addPetNameEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(addPetNameEt.getText().toString())) {
                    addPetNameEt.removeTextChangedListener(this);

                    // The trick to update text smoothly.
                    addPetNameEt.setText(CapitalizeFirstLetter.capitaliseOnlyFirstLetter(addPetNameEt.getText().toString()));
                    addPetNameEt.setSelection(addPetNameEt.getText().toString().length());
                    // Re-register self after update
                    addPetNameEt.addTextChangedListener(this);

                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                goBack();
                break;

            case R.id.blue_btn_tv:
                verify_data();
                break;
            case R.id.pet_img:
                if (!checkPermission()) {
                    selectImage();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            102);
                }
                break;
        }
    }

    private void goBack() {
        Intent i = new Intent(ActivityPetName.this, ActivityHome.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private boolean checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
        ) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 102) {
            if (grantResults.length > 0) {

                boolean recordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (recordAccepted) {
                    selectImage();
                }
            }
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (cameraIntent.resolveActivity(getPackageManager()) != null) {
// Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
// Error occurred while creating the File
                        }
// Continue only if the File was successfully created
                        if (photoFile != null) {
                            Uri outputFileUri = FileProvider.getUriForFile(ActivityPetName.this, BuildConfig.APPLICATION_ID, photoFile);
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                            startActivityForResult(cameraIntent, 0);
                        }
                    }


                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:


                    if (resultCode == Activity.RESULT_OK) {

                        File file = new File(Objects.requireNonNull(mCurrentPhotoPath));
                        String fileName = Constants.compressImage(mCurrentPhotoPath, ActivityPetName.this);
                        Bitmap bitmap = BitmapFactory.decodeFile(fileName);
                        bitmap = getResizedBitmap(bitmap, 150);
                        String path = mCurrentPhotoPath;
                        if (!TextUtils.isEmpty(fileName)) {
                            res = fileName;
                        }
                        petImage.setImageBitmap(bitmap);


                    }

                    break;

                case 1:

                    if (resultCode == RESULT_OK && data != null) {

                        Uri selectedImage = data.getData();
                        String fileName = null;
                        if (selectedImage != null) {
                            fileName = Constants.compressImage(selectedImage.toString(), ActivityPetName.this);
                            Bitmap bitmap = BitmapFactory.decodeFile(fileName);
                            bitmap = getResizedBitmap(bitmap, 150);
                            String path = mCurrentPhotoPath;
                            if (!TextUtils.isEmpty(fileName)) {
                                res = fileName;
                            }
                        }

                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(ActivityPetName.this.getContentResolver(), selectedImage);
                            bitmap = getPath(selectedImage);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    break;
            }
        }
    }

    private Bitmap getPath(Uri selectedImage) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Log.e("Before", "Creating cursor");
        Cursor cursor = ActivityPetName.this.managedQuery(selectedImage, projection, null, null, null);
        Log.e("After", "Creating cursor");
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
//        res = cursor.getString(column_index);
        Log.e("Selected Image Path", res);
        Log.e("After", "Closing cursor");
        bitmap = BitmapFactory.decodeFile(res);
        petImage.setImageBitmap(bitmap);
        Glide.with(this).load(res).apply(RequestOptions.circleCropTransform()).into(petImage);

        return bitmap;
    }

    private File createImageFile() throws IOException {
// Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName, // prefix
                ".jpg", // suffix
                storageDir // directory
        );

// Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();

        return image;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void verify_data() {
//        String team = spin_team.getSelectedItem().toString();
        str_petname = addPetNameEt.getText().toString().trim();

        if (str_petname.isEmpty()) {
            Toast.makeText(this, "Please enter your pet name", Toast.LENGTH_SHORT).show();

        } else if (res == null) {
            Toast.makeText(this, "Please select pet image", Toast.LENGTH_SHORT).show();

        } else {
            Intent intent = new Intent(ActivityPetName.this, ActivityPetType.class);
            intent.putExtra("pet_name", str_petname);
            intent.putExtra("pet_image", res);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void setText() {
        tbTv.setText(R.string.add_pet);
        nextTv.setText(R.string.next);
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        nextTv.setOnClickListener(this);
        petImage.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_add_pet_name;
    }
}
