package com.pongo.health.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.kinda.alert.KAlertDialog;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.PaymentCardAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.CardListResponseModel;
import com.pongo.health.model.CategoriesModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySubscription extends BaseActivity {

    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.free_card)
    CardView freeCard;
    @BindView(R.id.title)
    TextView freeTitle;
    @BindView(R.id.pet_first_description_tv)
    TextView petFirstDescriptionTv;
    @BindView(R.id.free)
    TextView freePlan;
    @BindView(R.id.paid_card)
    CardView paidCard;
    @BindView(R.id.paid_title)
    TextView paidTitle;
    @BindView(R.id.paid_two)
    TextView paidTwoTv;
    @BindView(R.id.paid_three)
    TextView paidThreeTv;
    @BindView(R.id.paid_four)
    TextView paidFourTv;
    @BindView(R.id.blue_btn_tv)
    TextView blueBtnTv;
    private CustomDialog mLoadingView;
    private Session mSession;
    private String currentPlan = "";
    private String planPrice = "";
    private List<CardListResponseModel.CardlistBean> cardList = new ArrayList<>();
    BottomSheetDialog mBottomSheetDialog;
    private String cardid = "";
    private String customer = "";
    RecyclerView cardRv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);

        if (InternetConnection.checkConnection(ActivitySubscription.this)) {
            getSubscription();
        } else {
            Toast.makeText(ActivitySubscription.this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
        createBottomList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCardList();
    }

    private void createBottomList() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.card_list_bottomsheet, null);
        cardRv = sheetView.findViewById(R.id.card_rv);
        TextView done = sheetView.findViewById(R.id.done);
        TextView addCard = sheetView.findViewById(R.id.blue_btn_tv);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        addCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivitySubscription.this, ActivityAddCard.class));
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(sheetView);
    }

    private void setCardist() {
        cardRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
      /*  for (int i = 0; i < 3; i++) {
            orderList.add("Lorem Ipsum");

        }*/
        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                cardid = cardList.get(position).getCard_id();
                customer = cardList.get(position).getCustomer();
                confimrDialog(cardList.get(position).getCardnumber());

            }
        };
        cardRv.setAdapter(new PaymentCardAdapter(this, cardList, itemClickListener));
    }

    private void confimrDialog(String cardnumber) {
        new KAlertDialog(this, KAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("You want to make payment using " + cardnumber)
                .setCancelText("No")
                .setConfirmText("Yes")
                .showCancelButton(true)
                .setCancelClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                .setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog kAlertDialog) {
                        kAlertDialog.dismissWithAnimation();
                        mBottomSheetDialog.dismiss();
                        buySubscription();
                    }
                })
                .show();
    }

    private void getCardList() {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("user_id", mSession.getuser_id());
        MultipartBody requestBody = builder.build();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<CardListResponseModel> call = apiInterface.cardList(requestBody);

        call.enqueue(new Callback<CardListResponseModel>() {
            @Override
            public void onResponse(Call<CardListResponseModel> call, Response<CardListResponseModel> response) {
                if (response.body() != null) {
                    try {
                        String status = response.body().getStatus();
//                            String message = jsonObject.getString("message");
                        if (!TextUtils.isEmpty(status)) {
                            if (status.equalsIgnoreCase("success")) {
                                cardList = response.body().getCardlist();
                                setCardist();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<CardListResponseModel> call, Throwable t) {
                Log.e("cate", t.toString());
            }

        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                clearActivityStack();
                break;
            case R.id.blue_btn_tv: {
                if (currentPlan.equalsIgnoreCase("free")) {
                    mBottomSheetDialog.show();
                } else {
                    warningDialog();
                }

                break;
            }
        }

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("Subscription");
        petFirstDescriptionTv.setText(Html.fromHtml("<p>3 free Chat based<br>" +
                "conversation with<br>" +
                "veterinary professionals"));

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        blueBtnTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_subscription;
    }

    private void getSubscription() {
        this.mLoadingView.show();
        String userid = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getCurrentSubscription(userid).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    String plan = jsonObject.getString("plan");
                                    planPrice = jsonObject.getString("price");
                                    currentPlan = jsonObject.getString("userplanstatus");
                                    blueBtnTv.setText("$" + planPrice + "/month");
                                    setPlan();
                                } else {
                                    Toast.makeText(ActivitySubscription.this, "Failed to get subscription", Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivitySubscription.this, "Failed to get subscription", Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivitySubscription.this, "Failed to get subscription", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void buySubscription() {
        this.mLoadingView.show();
        String userid = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).buySubscription(userid, customer).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(ActivitySubscription.this, message, Toast.LENGTH_SHORT).show();
                                    currentPlan = "paid";
                                    mSession.setuserPaid("paid");
                                    setPlan();
                                } else {
                                    Toast.makeText(ActivitySubscription.this, message, Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivitySubscription.this, message, Toast.LENGTH_SHORT).show();

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivitySubscription.this, "Failed to get subscription", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void setPlan() {
        if (currentPlan.equalsIgnoreCase("free")) {
            freeCard.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
            freeTitle.setTextColor(getResources().getColor(R.color.white_color));
            petFirstDescriptionTv.setTextColor(getResources().getColor(R.color.white_color));
            freePlan.setTextColor(getResources().getColor(R.color.white_color));
            freePlan.setBackground(getResources().getDrawable(R.drawable.white_border));

            paidCard.setCardBackgroundColor(getResources().getColor(R.color.white_color));
            paidTitle.setTextColor(getResources().getColor(R.color.sub_text_color));
            paidTwoTv.setTextColor(getResources().getColor(R.color.sub_text_color));
            paidThreeTv.setTextColor(getResources().getColor(R.color.sub_text_color));
            paidFourTv.setTextColor(getResources().getColor(R.color.sub_text_color));
            blueBtnTv.setTextColor(getResources().getColor(R.color.colorPrimary));
            blueBtnTv.setBackground(getResources().getDrawable(R.drawable.blue_border));
        } else {
            freeCard.setCardBackgroundColor(getResources().getColor(R.color.white_color));
            freeTitle.setTextColor(getResources().getColor(R.color.sub_text_color));
            petFirstDescriptionTv.setTextColor(getResources().getColor(R.color.sub_text_color));
            freePlan.setTextColor(getResources().getColor(R.color.colorPrimary));
            freePlan.setBackground(getResources().getDrawable(R.drawable.blue_border));

            paidCard.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
            paidTitle.setTextColor(getResources().getColor(R.color.white_color));
            paidTwoTv.setTextColor(getResources().getColor(R.color.white_color));
            paidThreeTv.setTextColor(getResources().getColor(R.color.white_color));
            paidFourTv.setTextColor(getResources().getColor(R.color.white_color));
            blueBtnTv.setTextColor(getResources().getColor(R.color.white_color));
            blueBtnTv.setBackground(getResources().getDrawable(R.drawable.white_border));
        }

    }

    private void warningDialog() {
        KAlertDialog kAlertDialog = new KAlertDialog(this, KAlertDialog.WARNING_TYPE);
        kAlertDialog.setTitleText("Warning!");
        kAlertDialog.setContentText("You are already paid user");
        kAlertDialog.setCancelable(false);
        kAlertDialog.setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(KAlertDialog sDialog) {
//                Toast.makeText(ActivityPayment.this, "Success", Toast.LENGTH_SHORT).show();
                sDialog.dismissWithAnimation();
            }
        })
                .show();
    }
}
