package com.pongo.health.ui;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mukesh.OtpView;
import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.OtpModel;
import com.pongo.health.ui.vet.ActivityVetJoin;
import com.google.gson.Gson;
//import com.pongo.health.utils.GenericTextWatcher;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityOtp extends BaseActivity {

    @BindView(R.id.v_tb_iv)
    ImageView otpBackIv;
    @BindView(R.id.blue_btn_tv)
    TextView otpSubmitTv;
    @BindView(R.id.phone_txt)
    TextView phoneTv;
    @BindView(R.id.otp_one_et)
    EditText otpEd1;
    @BindView(R.id.otp_two_et)
    EditText otpEd2;
    @BindView(R.id.otp_three_et)
    EditText otpEd3;
    @BindView(R.id.otp_four_et)
    EditText otpEd4;
    @BindView(R.id.otp_view)
    OtpView otpView;
    private OtpModel otpModel;
    private String mLoginType;
    private String mMobile;
    private String mPassword;
    private String mEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        removeTaskBar();
        ButterKnife.bind(this);
        Gson gson = new Gson();
        otpModel = gson.fromJson(getIntent().getStringExtra("otp"), OtpModel.class);
        mLoginType = getIntent().getStringExtra("login_type");
        mMobile = getIntent().getStringExtra("phone");
        String mFomat = getIntent().getStringExtra("format");
        mEmail = getIntent().getStringExtra("email");
        mPassword = getIntent().getStringExtra("password");
        if (!TextUtils.isEmpty(mMobile)) {
            phoneTv.setText(mFomat);
        } else {
            phoneTv.setText(" " + mEmail);

        }
     /*   otpEd1.addTextChangedListener(new GenericTextWatcher(otpEd1));
        otpEd2.addTextChangedListener(new GenericTextWatcher(otpEd2));
        otpEd3.addTextChangedListener(new GenericTextWatcher(otpEd3));
        otpEd4.addTextChangedListener(new GenericTextWatcher(otpEd4));*/
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_tb_iv:
                clearActivityStack();
                break;
            case R.id.blue_btn_tv:
                checkOtp();
                break;
        }
    }

    private void checkOtp() {
        String otp1 = otpEd1.getText().toString();
        String otp2 = otpEd2.getText().toString();
        String otp3 = otpEd3.getText().toString();
        String otp4 = otpEd4.getText().toString();
//        String otpText = otp1 + otp2 + otp3 + otp4;
        String otpText = otpView.getText().toString();
       /* if (TextUtils.isEmpty(otp1) || TextUtils.isEmpty(otp2) || TextUtils.isEmpty(otp3) || TextUtils.isEmpty(otp4)) {
            Toast.makeText(this, "Please enter otp", Toast.LENGTH_SHORT).show();
        } else {*/
        if (otpText.equalsIgnoreCase(String.valueOf(otpModel.getOtp()))) {
            Intent intent;
            if (mLoginType.equalsIgnoreCase("petparent")) {
                intent = new Intent(ActivityOtp.this, ActivityJoinUs.class);
            } else {
                intent = new Intent(ActivityOtp.this, ActivityVetJoin.class);
            }
            Gson gson = new Gson();
            String myJson = gson.toJson(otpModel);
            intent.putExtra("otp", myJson);
            intent.putExtra("login_type", mLoginType);
            intent.putExtra("phone", mMobile);
            intent.putExtra("email", mEmail);
            intent.putExtra("password", mPassword);
            startActivity(intent);

        } else {
            Toast.makeText(this, "otp not match", Toast.LENGTH_SHORT).show();

        }
//        }

    }

    @Override
    protected void setText() {
        otpSubmitTv.setText(R.string.submit);
    }

    @Override
    protected void setOnClick() {
        otpBackIv.setOnClickListener(this);
        otpSubmitTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_otp;
    }
}
