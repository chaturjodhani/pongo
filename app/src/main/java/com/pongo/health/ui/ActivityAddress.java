package com.pongo.health.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.AddressModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityAddress extends BaseActivity {


    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.home_txt)
    TextView homeTv;
    @BindView(R.id.shipping_txt)
    TextView shippingTv;
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.home_address_ll)
    LinearLayout homeAddressLl;
    @BindView(R.id.shipping_address_ll)
    LinearLayout shippingAddressLl;
    @BindView(R.id.ship_radio)
    RadioButton mShipRadio;
    @BindView(R.id.home_radio)
    RadioButton mHomeRadio;
    private CustomDialog mLoadingView;
    private Session mSession;
    private AddressModel mAddressModel;
    private String addressType = "";
    private String addressId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);
        Intent intent = getIntent();
        if (null == intent.getExtras()) {
            mHomeRadio.setVisibility(View.GONE);
            mShipRadio.setVisibility(View.GONE);
            continueTv.setVisibility(View.GONE);
        } else {
            radioListener();
        }

    }

    private void radioListener() {
        mHomeRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && !addressType.equalsIgnoreCase("home")) {
                    addressType = "home";
                    mShipRadio.setChecked(false);
                    if (null != mAddressModel && mAddressModel.getHomeaddress().size() > 0) {
                        addressId = mAddressModel.getHomeaddress().get(0).getId();
                    } else {
                        addressId = "";
                    }
                }
            }
        });
        mShipRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && !addressType.equalsIgnoreCase("ship")) {
                    addressType = "ship";
                    mHomeRadio.setChecked(false);
                    if (null != mAddressModel && mAddressModel.getShipaddress().size() > 0) {
                        addressId = mAddressModel.getShipaddress().get(0).getId();
                    } else {
                        addressId = "";
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (InternetConnection.checkConnection(this)) {
            getAddressData();
        } else {
            Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
        }

    }

    private void getAddressData() {
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getAddresses(id).enqueue(new Callback<AddressModel>() {
            public void onResponse(Call<AddressModel> call, Response<AddressModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((AddressModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        mAddressModel = response.body();
                        if (response.body().getHomeaddress().size() > 0) {
                            homeTv.setText("Update Billing Address");
                        } else {
                            homeTv.setText("Add Billing Address");
                        }
                        if (response.body().getShipaddress().size() > 0) {
                            shippingTv.setText("Update Shipping Address");
                        } else {
                            shippingTv.setText("Add Shipping Address");
                        }
                    }
                }

            }

            public void onFailure(Call<AddressModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityAddress.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.home_address_ll:
                if (null != mAddressModel) {
                    mHomeRadio.setChecked(false);
                    mShipRadio.setChecked(false);
                    addressType="";
                    Intent intent = new Intent(ActivityAddress.this, HomeAddressActivity.class);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(mAddressModel);
                    intent.putExtra("model", myJson);
                    startActivity(intent);
                }
                break;
            case R.id.shipping_address_ll:
                if (null != mAddressModel) {
                    mHomeRadio.setChecked(false);
                    mShipRadio.setChecked(false);
                    addressType="";
                    Intent intent = new Intent(ActivityAddress.this, ShipAddressActivity.class);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(mAddressModel);
                    intent.putExtra("model", myJson);
                    startActivity(intent);
                }
                break;
            case R.id.blue_btn_tv:
                if (TextUtils.isEmpty(addressType)) {
                    Toast.makeText(this, "Please select address", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(addressId)) {
                    Toast.makeText(this, "Please add address first", Toast.LENGTH_SHORT).show();
                } else {
                    Intent oldIntent = getIntent();
                    String status = oldIntent.getStringExtra("existing_status");
                    Intent intent = new Intent(ActivityAddress.this, ActivityPayment.class);
                    intent.putExtra("existing_status", status);
                    intent.putExtra("address", addressType);
                    intent.putExtra("address_id", addressId);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent);
                    finish();
                }
                break;
        }
    }


    @Override
    protected void setText() {
        continueTv.setText(R.string.continue_);
        tbTv.setText("Addresses");
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        continueTv.setOnClickListener(this);
        homeAddressLl.setOnClickListener(this);
        shippingAddressLl.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_address;
    }
}
