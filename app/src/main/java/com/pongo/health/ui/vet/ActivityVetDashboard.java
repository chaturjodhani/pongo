package com.pongo.health.ui.vet;

import androidx.cardview.widget.CardView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agik.AGIKSwipeButton.Controller.OnSwipeCompleteListener;
import com.agik.AGIKSwipeButton.View.Swipe_Button_View;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kinda.alert.KAlertDialog;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.TechDashboradResponseModel;
import com.pongo.health.model.VetDashBoardResponseModel;
import com.pongo.health.ui.ActivityPetProblems;
import com.pongo.health.utils.ArchLifecycleApp;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.audioListener.AppRTCAudioManager;

import org.json.JSONObject;

import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityVetDashboard extends BaseActivity {

    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.vp_dr_iv)
    ImageView doctorIv;
    @BindView(R.id.arrow_right)
    ImageView imageIv;
    @BindView(R.id.vp_dr_name_tv)
    TextView doctorNameTv;
    @BindView(R.id.expertise)
    TextView expertTv;
    @BindView(R.id.specialist)
    TextView specialistTv;
    @BindView(R.id.walk_count)
    TextView walkCount;
    @BindView(R.id.requestor_count)
    TextView requestorCount;
    @BindView(R.id.waiting_patient_cv)
    CardView waitingPatientCv;
    @BindView(R.id.appointments_request_cv)
    CardView appointmentsRequestCv;
    @BindView(R.id.past_cases_cv)
    CardView pastCasesCv;
    @BindView(R.id.availability_cv)
    CardView availabilityCv;
    @BindView(R.id.swipe_button)
    Swipe_Button_View activeButton;
    private Session mSession;
    private CustomDialog mLoadingView;
    String adminStatus = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);
        activeListener();
        ArchLifecycleApp.getInstance().changeStatus("1");
//        audioListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (InternetConnection.checkConnection(ActivityVetDashboard.this)) {
            getDashBoard();
        } else {
            Toast.makeText(ActivityVetDashboard.this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
    }

    private void activeListener() {
        activeButton.setOnSwipeCompleteListener_forward_reverse(new OnSwipeCompleteListener() {
            @Override
            public void onSwipe_Forward(Swipe_Button_View swipeView) {
                if (adminStatus.equalsIgnoreCase("0")) {
                    setDoctorStatus("0");
                    popupDialog("Your Information is being reviewed by the Pongo Team. Please check back later");
                } else {
                    updateVetStatus("1");
                    activeButton.setText("Go Offline");
                    activeButton.setSwipeBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    activeButton.setTextColor(getResources().getColor(R.color.white_color));
                }
//                activeButton.set(getResources().getColor(R.color.white_color));
            }

            @Override
            public void onSwipe_Reverse(Swipe_Button_View swipeView) {
                if (adminStatus.equalsIgnoreCase("0")) {
                    setDoctorStatus("1");
                    popupDialog("Your Information is being reviewed by the Pongo Team. Please check back later");
                } else {
                    updateVetStatus("0");
                    activeButton.setText(getResources().getString(R.string.receive_patients));
                    activeButton.setSwipeBackgroundColor(getResources().getColor(R.color.white_color));
                    activeButton.setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            }
        });
    }

    private void popupDialog(String message) {
        KAlertDialog kAlertDialog = new KAlertDialog(this, KAlertDialog.WARNING_TYPE);
        kAlertDialog.setTitleText("Alert!");
        kAlertDialog.setContentText(message);
        kAlertDialog.setCancelable(true);
        kAlertDialog.setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(KAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
            }
        })
                .show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                clearActivityStack();
                break;
            case R.id.waiting_patient_cv:
                startActivity(new Intent(this, ActivityWaitingList.class));
                break;
            case R.id.appointments_request_cv:
                startActivity(new Intent(this, ActivityCallRequest.class));
                break;
            case R.id.past_cases_cv:
                startActivity(new Intent(this, ActivityPetCases.class));
                break;
            case R.id.availability_cv:
                startActivity(new Intent(this, ActivityVetProfile.class));
                break;
            case R.id.arrow_right:
                startActivity(new Intent(this, ActivityProfile.class));
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("Pongo Health Dashboard");

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        waitingPatientCv.setOnClickListener(this);
        appointmentsRequestCv.setOnClickListener(this);
        pastCasesCv.setOnClickListener(this);
        availabilityCv.setOnClickListener(this);
        imageIv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_vet_dashboard;
    }

    private void updateVetStatus(String status) {
        mLoadingView.show();
        String userId = mSession.getuser_id();
        if (!TextUtils.isEmpty(userId)) {
            ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).updateVetAvailability(status, userId).enqueue(new Callback<ResponseBody>() {
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    mLoadingView.hideDialog();
                    if (response.body() != null) {
                        String responseData = "";
                        try {
                            responseData = response.body().string();
                            if (!TextUtils.isEmpty(responseData)) {
                                JSONObject jsonObject = new JSONObject(responseData);
                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("detail");
                                if (!TextUtils.isEmpty(status)) {
                                    if (status.equalsIgnoreCase("success")) {
                                        Toast.makeText(ActivityVetDashboard.this, message, Toast.LENGTH_SHORT).show();

                                    } else {
                                        if (status.equalsIgnoreCase("0")) {
                                            Toast.makeText(ActivityVetDashboard.this, message, Toast.LENGTH_SHORT).show();
                                            activeButton.setreverse_180();
                                            activeButton.swipe_reverse = true;
                                            activeButton.setText("Go Offline");
                                            activeButton.setSwipeBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                            activeButton.setTextColor(getResources().getColor(R.color.white_color));
                                        } else {
                                            activeButton.setreverse_0();
                                            activeButton.swipe_reverse = false;
                                            activeButton.setText(getResources().getString(R.string.receive_calls));
                                            activeButton.setSwipeBackgroundColor(getResources().getColor(R.color.white_color));
                                            activeButton.setTextColor(getResources().getColor(R.color.colorPrimary));
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                public void onFailure(Call<ResponseBody> call, Throwable th) {
                    mLoadingView.hideDialog();
                    if (status.equalsIgnoreCase("0")) {
                        activeButton.setreverse_180();
                        activeButton.swipe_reverse = true;
                        activeButton.setText("Go Offline");
                        activeButton.setSwipeBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        activeButton.setTextColor(getResources().getColor(R.color.white_color));
                    } else {
                        activeButton.setreverse_0();
                        activeButton.swipe_reverse = false;
                        activeButton.setText(getResources().getString(R.string.receive_calls));
                        activeButton.setSwipeBackgroundColor(getResources().getColor(R.color.white_color));
                        activeButton.setTextColor(getResources().getColor(R.color.colorPrimary));
                    }
                    Log.e("cate", th.toString());
                }
            });
        }
    }

    private void getDashBoard() {
        mLoadingView.show();
        String userId = mSession.getuser_id();
        if (!TextUtils.isEmpty(userId)) {
            ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).vetDashBoard(userId).enqueue(new Callback<VetDashBoardResponseModel>() {
                public void onResponse(Call<VetDashBoardResponseModel> call, Response<VetDashBoardResponseModel> response) {
                    mLoadingView.hideDialog();
                    if (response.body() != null) {
                        try {
                            VetDashBoardResponseModel responseData = response.body();
                            String status = responseData.getStatus();
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    String doctorStatus = responseData.getDoctors().getRecieve_patient();
                                    adminStatus = responseData.getDoctors().getAdminstatus();
                                   /* if (adminStatus.equalsIgnoreCase("0"))
                                    {
                                        activeButton.setEnabled(false);
                                    }*/
                                    setDoctorStatus(doctorStatus);
                                    setDoctorData(responseData.getDoctors());

                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                public void onFailure(Call<VetDashBoardResponseModel> call, Throwable th) {
                    mLoadingView.hideDialog();
                    Log.e("cate", th.toString());
                }
            });
        }
    }

    private void setDoctorData(VetDashBoardResponseModel.DoctorsBean doctors) {

        Glide.with(this).load(doctors.getUserimage()).apply(RequestOptions.circleCropTransform()).into(doctorIv);
        doctorNameTv.setText("Hi, " + doctors.getFirst_name() + " " + doctors.getLast_name());
        expertTv.setText(doctors.getExpertise());
        specialistTv.setText(doctors.getVet_type());
        walkCount.setText(doctors.getWalking());
        requestorCount.setText(doctors.getRequestor());

    }

    private void setDoctorStatus(String doctorStatus) {
        if (doctorStatus.equalsIgnoreCase("0")) {  //0 for offline
            activeButton.setreverse_0();
            activeButton.swipe_reverse = false;
            activeButton.setText(getResources().getString(R.string.receive_calls));
            activeButton.setSwipeBackgroundColor(getResources().getColor(R.color.white_color));
            activeButton.setTextColor(getResources().getColor(R.color.colorPrimary));
        } else {
            activeButton.setreverse_180();
            activeButton.swipe_reverse = true;
            activeButton.setText("Go Offline");
            activeButton.setSwipeBackgroundColor(getResources().getColor(R.color.colorPrimary));
            activeButton.setTextColor(getResources().getColor(R.color.white_color));
        }
    }

    private void audioListener() {
        AppRTCAudioManager audioManager = AppRTCAudioManager.create(this);
        audioManager.start(new AppRTCAudioManager.AudioManagerEvents() {
            @Override
            public void onAudioDeviceChanged(AppRTCAudioManager.AudioDevice audioDevice, Set<AppRTCAudioManager.AudioDevice> availableAudioDevices) {
//                onAudioManagerDevicesChanged(audioDevice, availableAudioDevices);
                Toast.makeText(ActivityVetDashboard.this, "device", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
