package com.pongo.health.ui;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.Constraints;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.UserLoginModel;
import com.pongo.health.utils.CustomDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySignup extends BaseActivity {

    @BindView(R.id.continue_with_google_ll)
    LinearLayout continueWithGoogleLl;
    @BindView(R.id.continue_with_phone_ll)
    LinearLayout continueWithPhoneLl;
    @BindView(R.id.continue_with_email_ll)
    LinearLayout continueWithEmailLl;
    @BindView(R.id.veternarains_tv)
    TextView veterinariansTv;
    @BindView(R.id.pet_parent_tv)
    TextView petParentTv;
    @BindView(R.id.login_description_tv)
    TextView loginDescriptionTv;

    @BindView(R.id.terms_tv)
    TextView termsTv;
    @BindView(R.id.condition_tv)
    TextView conditionTv;
    @BindView(R.id.privacy_policy)
    TextView privacyPolicy;
    @BindView(R.id.intro_login_tv)
    TextView introLoginTv;
    private String mLoginType = "petparent";
    private static final int REQUEST_ACCESS_FINE_LOCATIONMobile = 110;
    boolean hasPermissionLocation;
    private GoogleSignInClient mGoogleSignInClient;
    private int GOOGLE_SIGN_IN = 101;
    private CustomDialog mLoadingView;
    private Session mSession;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        removeTaskBar();
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
        googleIniTialize();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setPetParentTvColor();
        }

        setOnClick();
        hasPermissionLocation = (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.veternarains_tv:
//                loginDescriptionTv.setText(Html.fromHtml("<P>Join thousand of \n" +
//                        " Veterinarians from across the country</p>"));
                loginDescriptionTv.setText(Html.fromHtml("<P>Join thousand of \n" +
                        " Professionals from across the country</p>"));
                petParentTv.setBackground(getResources().getDrawable(R.drawable.button_shaper));
                veterinariansTv.setBackground(getResources().getDrawable(R.drawable.button_shaper_selected));
                petParentTv.setTextColor(getResources().getColor(R.color.text_color_blue));
                veterinariansTv.setTextColor(getResources().getColor(R.color.white_color));
                mLoginType = "veternarians";
                continueWithGoogleLl.setVisibility(View.GONE);
                break;
            case R.id.pet_parent_tv:
                setPetParentTvColor();
                break;
            case R.id.continue_with_google_ll:
                if (!hasPermissionLocation) {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                            REQUEST_ACCESS_FINE_LOCATIONMobile);
                } else {
                    googleSignIn();
                }
                break;
            case R.id.continue_with_email_ll:
                Intent intent = new Intent(this, ActivitySingupEmail.class);
                intent.putExtra("login_type", mLoginType);
                startActivity(intent);
                break;
            case R.id.continue_with_phone_ll:
                Intent eintent = new Intent(this, ActivityLoginPhoneNumber.class);
                eintent.putExtra("login_type", mLoginType);
                startActivity(eintent);
                break;
            case R.id.terms_tv:
                Intent termintent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://pongohealth.com/terms-conditions/"));
                startActivity(termintent);
                break;
            case R.id.condition_tv:
                Intent con = new Intent(Intent.ACTION_VIEW, Uri.parse("https://pongohealth.com/terms-conditions/"));
                startActivity(con);
                break;
            case R.id.privacy_policy:
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://pongohealth.com/privacy_policy"));
                startActivity(i);
                break;
            case R.id.intro_login_tv:
                startActivity(new Intent(this, ActivityLogin.class));
                break;
        }
    }

    @Override
    protected void setText() {

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setPetParentTvColor() {
        continueWithGoogleLl.setVisibility(View.VISIBLE);
        loginDescriptionTv.setText(Html.fromHtml("<P>Join thousand of \n" +
                " Pet parents from across the country</p>"));
        veterinariansTv.setBackground(getResources().getDrawable(R.drawable.button_shaper));
        petParentTv.setBackground(getResources().getDrawable(R.drawable.button_shaper_selected));
        veterinariansTv.setTextColor(getResources().getColor(R.color.text_color_blue));
        petParentTv.setTextColor(getResources().getColor(R.color.white_color));
        mLoginType = "petparent";

    }

    @Override
    protected void setOnClick() {
        continueWithEmailLl.setOnClickListener(this);
        continueWithPhoneLl.setOnClickListener(this);
        continueWithGoogleLl.setOnClickListener(this);
        veterinariansTv.setOnClickListener(this);
        petParentTv.setOnClickListener(this);
        termsTv.setOnClickListener(this);
        conditionTv.setOnClickListener(this);
        privacyPolicy.setOnClickListener(this);
        introLoginTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }


    private void googleLogout() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                    }
                });
    }

    private void googleIniTialize() {
        this.mGoogleSignInClient = GoogleSignIn.getClient(this, new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build());
    }

    private void googleSignIn() {
        startActivityForResult(this.mGoogleSignInClient.getSignInIntent(), this.GOOGLE_SIGN_IN);
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == this.GOOGLE_SIGN_IN) {
            handleSignInResult(GoogleSignIn.getSignedInAccountFromIntent(intent));
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> task) {
        try {
            GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) task.getResult(ApiException.class);
            if (googleSignInAccount != null) {
                socialLogIn(googleSignInAccount.getGivenName(), googleSignInAccount.getFamilyName(), googleSignInAccount.getEmail(), googleSignInAccount.getId(), "Google");
            }
        } catch (ApiException e) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("signInResult:failed code=");
            stringBuilder.append(e.getStatusCode());
            Log.w(Constraints.TAG, stringBuilder.toString());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case REQUEST_ACCESS_FINE_LOCATIONMobile: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    googleSignIn();
                } else {
                    Toast.makeText(ActivitySignup.this, "The app was not allowed to get your location. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();

                }
                break;
            }

        }
    }

    private void socialLogIn(String fName, String lName, String email, String socialId, final String type) {
        mLoadingView.show();
        if (TextUtils.isEmpty(fName)) {
            fName = "";
        }
        if (TextUtils.isEmpty(lName)) {
            lName = "";
        }
        String token = this.mSession.getToken();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).socialLogin(fName, lName, email, socialId, token, "android").enqueue(new Callback<UserLoginModel>() {
            public void onResponse(Call<UserLoginModel> call, Response<UserLoginModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((UserLoginModel) response.body()).getStatus();
                    String detail = ((UserLoginModel) response.body()).getMessage();
                    if (TextUtils.isEmpty(status)) {
                        Toast.makeText(ActivitySignup.this, "Signup failed", Toast.LENGTH_SHORT).show();
                    } else if (status.equalsIgnoreCase("success")) {

                        String type = response.body().getUserdetail().getVet_type();
                        String userId = response.body().getUserdetail().getId();
                        Intent intent;
                        if (TextUtils.isEmpty(type)) {
                            intent = new Intent(ActivitySignup.this, ActivityHome.class);
                            mSession.setuser_id(userId);
                            mSession.setuser_login(true);
                            mSession.setfirt_name(response.body().getUserdetail().getFirst_name());
                            mSession.setlast_name(response.body().getUserdetail().getLast_name());
                            mSession.setuser_email(email);
                            mSession.setuser_phone(response.body().getUserdetail().getPhone());
                            mSession.setuser_type(type);
                            mSession.setuserPaid(response.body().getUserdetail().getUsertype());
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    } else {
                        Toast.makeText(ActivitySignup.this, detail, Toast.LENGTH_SHORT).show();
                        if (type.equalsIgnoreCase("Google")) {
                            googleLogout();
                        }
                    }
                }
            }

            public void onFailure(Call<UserLoginModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivitySignup.this, "Failed to signup", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
                if (type.equalsIgnoreCase("Google")) {
                    googleLogout();
                }
            }
        });
    }
}
