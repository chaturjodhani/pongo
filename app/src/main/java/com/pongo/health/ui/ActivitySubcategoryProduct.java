package com.pongo.health.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.adapter.SearchMedicationAdapter;
import com.pongo.health.adapter.SubCategoriesAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.ProductResponseModel;
import com.pongo.health.model.SearchProductModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySubcategoryProduct extends BaseActivity {
    @BindView(R.id.popular_medication_rv)
    RecyclerView popularMedicationRv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.search_second_et)
    EditText searchEt;
    @BindView(R.id.progressBar)
    ProgressBar mBottomProgress;
    @BindView(R.id.nested)
    NestedScrollView mScrollView;

    private List<ProductResponseModel.ProductlistBean> popularMedicationList = new ArrayList<>();
    private ItemClickListener itemClickListener;
    private CustomDialog mLoadingView;
    private LinearLayoutManager mLayoutManager;
    private boolean loading = true;
    private int totalItems;
    private int totalPages;
    private int mPageNo = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        removeTaskBar();
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);

        if (InternetConnection.checkConnection(this)) {
            this.mLoadingView.show();
            String id = getIntent().getStringExtra("id");
            getProduct(id);
        } else {
            Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
        popularMedicationRv.setNestedScrollingEnabled(true);
        setSearchListener();
        paginationListener();
    }

    private void paginationListener() {
        mLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        popularMedicationRv.setLayoutManager(mLayoutManager);

        this.popularMedicationRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrolled(@NonNull RecyclerView recyclerView, int i, int i2) {
                super.onScrolled(recyclerView, i, i2);
                int itemCount = mLayoutManager.getItemCount();
//                Toast.makeText(ActivityMedicationSearch.this, "scrollde", Toast.LENGTH_SHORT).show();
                i = mLayoutManager.findLastVisibleItemPosition();
                if (itemCount > 0 && i == itemCount - 1 && loading) {
                    loading = false;
                    mBottomProgress.setVisibility(View.VISIBLE);
                    String id = getIntent().getStringExtra("id");
                    getProduct(id);
                }
            }
        });
    }

    private void setSearchListener() {

        searchEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String search = searchEt.getText().toString();
                    getSearch(search);
                    return true;
                }
                return false;
            }
        });
    }

    private void getSearch(String s) {
        this.mLoadingView.show();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getSearchProducts(s).enqueue(new Callback<SearchProductModel>() {
            public void onResponse(Call<SearchProductModel> call, Response<SearchProductModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((SearchProductModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        popularMedicationList.clear();
                        List<ProductResponseModel.ProductlistBean> productlist = new ArrayList<>();
                        for (int i = 0; i < response.body().getProductdata().size(); i++) {
                            ProductResponseModel.ProductlistBean productlistBean = new ProductResponseModel.ProductlistBean();
                            productlistBean.setId(response.body().getProductdata().get(i).getId());
                            productlistBean.setId(response.body().getProductdata().get(i).getId());
                            productlistBean.setName(response.body().getProductdata().get(i).getName());
                            productlistBean.setDescription(response.body().getProductdata().get(i).getDescription());
                            productlistBean.setOrignalprice(response.body().getProductdata().get(i).getOrignalprice());
                            productlistBean.setDiscountprice(response.body().getProductdata().get(i).getDiscountprice());
                            productlistBean.setProduct_type(response.body().getProductdata().get(i).isProduct_type());
                            productlistBean.setProduct_image(response.body().getProductdata().get(i).getProduct_image());
                            productlist.add(productlistBean);
                        }
                        popularMedicationList = productlist;
                        setTopProducts();
                    }
                }

            }

            public void onFailure(Call<SearchProductModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivitySubcategoryProduct.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void getProduct(String id) {
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getCateProducts(id, 1).enqueue(new Callback<ProductResponseModel>() {
            public void onResponse(Call<ProductResponseModel> call, Response<ProductResponseModel> response) {
                mLoadingView.hideDialog();
                mBottomProgress.setVisibility(View.GONE);

                if (response.body() != null) {
                    String status = ((ProductResponseModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        totalPages = response.body().getTotalpages();
//                        setTopProducts(response.body().getProductlist());
                        if (mPageNo > 1) {
                            popularMedicationList.addAll(response.body().getProductlist());
                            setTopProducts();
                        } else {
                            popularMedicationList = response.body().getProductlist();
                            setTopProducts();
                        }
                        if (mPageNo == totalPages) {
                            loading = false;
                            return;
                        }
                        loading = true;
                        mPageNo = mPageNo + 1;

                    }
                }

            }

            public void onFailure(Call<ProductResponseModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivitySubcategoryProduct.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }


    private void setTopProducts() {
//        popularMedicationRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
      /*  for (int i=0;i<4;i++){
            popularMedicationList.add(new PetsModel("$5.00","Lorem Ipsum Rx","Lorem Ipsum"));
        }*/
        itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                String id = popularMedicationList.get(position).getId();
                String name = popularMedicationList.get(position).getName();

                if (popularMedicationList.get(position).isProduct_type()) {
                    startActivity(new Intent(ActivitySubcategoryProduct.this, ActivityMedicationNext.class).putExtra("id", id).putExtra("name", name));
                } else {
                    name = popularMedicationList.get(position).getCategory();
                    String subcate = popularMedicationList.get(position).getSubcategory();
                    if (!TextUtils.isEmpty(subcate)) {
                        name = name + " > " + subcate;
                    }
                    startActivity(new Intent(ActivitySubcategoryProduct.this, ActivitySingleItemMarket.class).putExtra("id", id).putExtra("name", name));
                }
            }
        };
        /*itemClickListener = (view, position) ->

                startActivity(new Intent(this, ActivityMedicationNext.class));*/
        popularMedicationRv.setAdapter(new SearchMedicationAdapter(this, popularMedicationList, itemClickListener));
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                clearActivityStack();
                break;
        }

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_subcategory_product;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        String name = getIntent().getStringExtra("name");
        searchEt.setHint("Type your " + name + " name");
        tbTv.setText(name);
    }
}
