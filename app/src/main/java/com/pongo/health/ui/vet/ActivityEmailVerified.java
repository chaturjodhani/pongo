package com.pongo.health.ui.vet;

import androidx.appcompat.app.AlertDialog;


import android.annotation.SuppressLint;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityEmailVerified extends BaseActivity {
    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.vet_heading_one_tv)
    TextView vetHeadingOneTv;
    @BindView(R.id.vet_heading_second_tv)
    TextView vetHeadingSecondTv;
    @BindView(R.id.blue_btn_tv)
    TextView allowTv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.blue_btn_tv:
                String type = getIntent().getStringExtra("type");
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setTitle("Pongo would like to send notifications");
                dialog.setMessage("Notifications may include alert,sounds and icon badges," +
                        "this can be configured settings");
                dialog.setPositiveButton("Ok", (dialogInterface, i) ->
                        startActivity(new Intent(ActivityEmailVerified.
                                this, ActivityCameraMic.class).putExtra("type", type)));
                dialog.setNegativeButton("cancel", (dialogInterface, i) -> finish());
                dialog.show();
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        vetHeadingOneTv.setText("Please verify Your Email");
        vetHeadingSecondTv.setText("Please provide Pongo permission to send " +
                "you Patient/Vet notifications");
        allowTv.setText(R.string.allow);

    }

    @Override
    protected void setOnClick() {
        allowTv.setOnClickListener(this);
        vTbIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_email_verified;
    }
}
