package com.pongo.health.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.ui.checkout.ActivityCheckoutAdditionalProfile;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivitySoundGoods extends BaseActivity {
    @BindView(R.id.tb_tv)TextView tbTv;
    @BindView(R.id.tb_iv)ImageView tbIv;

    @BindView(R.id.sound_goods_description_tv) TextView soundGoodsDescriptionTv;
    @BindView(R.id.blue_btn_tv) TextView soundGoodTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);



    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.blue_btn_tv:
                startActivity(new Intent(this, ActivityCheckoutAdditionalProfile.class));
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        soundGoodsDescriptionTv.setText(Html.fromHtml("<p>You will get a " +
                "message on Your Phone as <br/>soon as prescription is received</P>"));
        tbTv.setText("CHECKOUT");
        soundGoodTv.setText("continue");

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        soundGoodTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_sound_goods;
    }
}
