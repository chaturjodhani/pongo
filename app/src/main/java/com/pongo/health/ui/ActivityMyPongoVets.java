package com.pongo.health.ui;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.PongoVetsAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.PetsModel;
import com.pongo.health.model.PongoVetsModel;
import com.pongo.health.model.TechChatResponseModel;
import com.pongo.health.ui.vet.ActivityChatWaiting;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMyPongoVets extends BaseActivity {

    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.tb_tv)
    TextView tbTv;

    @BindView(R.id.pongo_vets_rv)
    RecyclerView pongoVetsRv;

    private List<PongoVetsModel.DetailBean> vetsList = new ArrayList<>();
    private CustomDialog mLoadingView;
    private Session mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mSession = new Session(this);
        mLoadingView = new CustomDialog(this);
        if (InternetConnection.checkConnection(ActivityMyPongoVets.this)) {
            getPongVetsData();
        } else {
            Toast.makeText(ActivityMyPongoVets.this, "No Internet available", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                clearActivityStack();
                break;
        }
    }

    @Override
    protected void setText() {
        tbTv.setText("My Pongo Vets");

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_my_pongo_vets;
    }

    private void getPongVetsData() {
        this.mLoadingView.show();
        String id = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).pongoVets(id).enqueue(new Callback<PongoVetsModel>() {
            public void onResponse(Call<PongoVetsModel> call, Response<PongoVetsModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((PongoVetsModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        vetsList = response.body().getDetail();
                        if (null != vetsList && vetsList.size() > 0) {
                            setPongoVetsList();
                        }

                    }
                }

            }

            public void onFailure(Call<PongoVetsModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityMyPongoVets.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }


    private void setPongoVetsList() {

        pongoVetsRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
       /* for (int i = 0; i < 4; i++) {
            vetsList.add(new PetsModel("seen Date : 21-02-2020", "Pet Name : abcd", "Dr Naveen Kumar"));
        }*/
        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                String vetId = vetsList.get(position).getId();
                startActivity(new Intent(ActivityMyPongoVets.this, ActivityVetAvailableTime.class).putExtra("vet_id", vetId));
            }
        };
        pongoVetsRv.setAdapter(new PongoVetsAdapter(this, vetsList, itemClickListener));
    }
}

