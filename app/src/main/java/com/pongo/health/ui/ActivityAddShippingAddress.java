package com.pongo.health.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityAddShippingAddress extends BaseActivity {

    @BindView(R.id.tb_tv) TextView tbTv;
    @BindView(R.id.tb_iv) ImageView tbIv;
    @BindView(R.id.blue_btn_tv) TextView addTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tb_iv:
                clearActivityStack();
                break;
            case R.id.blue_btn_tv:
                Toast.makeText(this, "Added", Toast.LENGTH_SHORT).show();
                break;

        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        tbTv.setText("Shipping Address");
        addTv.setText("Add");

    }

    @Override
    protected void setOnClick() {

        tbIv.setOnClickListener(this);
        addTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_add_shipping_address;
    }
}
