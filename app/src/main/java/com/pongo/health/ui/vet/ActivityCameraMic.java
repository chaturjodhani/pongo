package com.pongo.health.ui.vet;

import androidx.appcompat.app.AlertDialog;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityCameraMic extends BaseActivity {

    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.camera_dis_tv)
    TextView cameraDisTv;
    @BindView(R.id.blue_btn_tv)
    TextView allowBtnTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setTitle("Pongo would like to access Microphone");
                dialog.setMessage("Send audio between pet-parents & veterinarians");
//                dialog.setPositiveButton("Ok", (dialogInterface, i) -> startActivity(new Intent(ActivityCameraMic.this, ActivityVetDashboard.class)));
                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String type = getIntent().getStringExtra("type");
                        if ("vet".equals(type)) {
                            startActivity(new Intent(ActivityCameraMic.this, ActivityVetDashboard.class));
                        } else {
                            startActivity(new Intent(ActivityCameraMic.this, ActivityTechnician.class));
                        }
                    }
                });
                dialog.setNegativeButton("cancel", (dialogInterface, i) -> finish());
                dialog.show();
                break;
            case R.id.v_tb_iv:
                clearActivityStack();
                break;

        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        cameraDisTv.setText("Please provide us permission to access your camera and mic so you can " +
                "make or receive calls, and upload photos to the app ");
        allowBtnTv.setText(R.string.allow);

    }

    @Override
    protected void setOnClick() {
        vTbIv.setOnClickListener(this);
        allowBtnTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_camera_mic;
    }

}
