package com.pongo.health.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityInviteFriend extends BaseActivity {

    @BindView(R.id.tb_tv) TextView tbTv;
    @BindView(R.id.tb_iv) ImageView tbIv;

    @BindView(R.id.mail_iv) ImageView mailIv;
    @BindView(R.id.we_chat_iv) ImageView weChatIv;
    @BindView(R.id.fb_iv) ImageView fbIv;
    @BindView(R.id.twitter_iv) ImageView twitterIv;
    @BindView(R.id.whats_app_iv) ImageView whatsAppIv;
    @BindView(R.id.msg_iv) ImageView msgIv;
    @BindView(R.id.copy_link_iv) ImageView copyLinkIv;
    @BindView(R.id.more_iv) ImageView moreIv;

    @BindView(R.id.refer_term_tv) TextView referTermTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.mail_iv:
                break;
            case R.id.we_chat_iv:
                break;
            case R.id.fb_iv:
                break;
            case R.id.twitter_iv:
                break;
            case R.id.whats_app_iv:
                break;
            case R.id.msg_iv:
                break;
            case R.id.copy_link_iv:
                break;
            case R.id.more_iv:
                break;
            case R.id.refer_term_tv:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://pongohealth.com/terms-conditions/"));
                startActivity(browserIntent);
                break;

        }
    }

    @Override
    protected void setText() {
        tbTv.setText("Invite Friends");
    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        mailIv.setOnClickListener(this);
        weChatIv.setOnClickListener(this);
        fbIv.setOnClickListener(this);
        twitterIv.setOnClickListener(this);
        whatsAppIv.setOnClickListener(this);
        msgIv.setOnClickListener(this);
        copyLinkIv.setOnClickListener(this);
        moreIv.setOnClickListener(this);
        referTermTv.setOnClickListener(this);


    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_invite_friend;
    }
}
