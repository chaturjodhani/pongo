package com.pongo.health.ui.vet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.ImagesAdapter;
import com.pongo.health.adapter.PetProductRecommendAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.SearchProductModel;
import com.pongo.health.ui.AddNoteActivity;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PongoContactActivity extends BaseActivity {
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.subject_et)
    EditText subjectEt;
    @BindView(R.id.description_et)
    EditText descriptionEt;
    @BindView(R.id.image_rv)
    RecyclerView imagesRv;
    private CustomDialog mLoadingView;
    private Session mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
    }

  /*  private void addNoteApi() {
        this.mLoadingView.show();
        String subject = subjectEt.getText().toString();
        String description = descriptionEt.getText().toString();
        String userId = getIntent().getStringExtra("user_id");
        String techId = mSession.getuser_id();

        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).addNote(userId, techId, note, productId).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    Toast.makeText(PongoContactActivity.this, "successfully added", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(PongoContactActivity.this, "Opps .. Something went wrong", Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(PongoContactActivity.this, "Opps .. Something went wrong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(PongoContactActivity.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }*/

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                String subject = subjectEt.getText().toString();
                String description = descriptionEt.getText().toString();
                if (TextUtils.isEmpty(subject)) {
                    Toast.makeText(PongoContactActivity.this, "Please enter subject", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(description)) {
                    Toast.makeText(PongoContactActivity.this, "Please enter description", Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(this, "posted", Toast.LENGTH_SHORT).show();
                    if (InternetConnection.checkConnection(PongoContactActivity.this)) {
//                        addNoteApi();
                    } else {
                        Toast.makeText(PongoContactActivity.this, "No Internet available", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case R.id.v_tb_iv:
                onBackPressed();
                finish();
                break;
        }
    }

    @Override
    protected void setText() {
        continueTv.setText(R.string.submit);
    }

    @Override
    protected void setOnClick() {
        continueTv.setOnClickListener(this);
        vTbIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_pongo_contact;
    }
}