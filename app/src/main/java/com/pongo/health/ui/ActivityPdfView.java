package com.pongo.health.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.utils.CustomDialog;
import com.shockwave.pdfium.PdfDocument;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityPdfView extends BaseActivity implements OnLoadCompleteListener, OnPageChangeListener {
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.pdfView)
    PDFView pdfView;
    private CustomDialog mLoadingView;
    Integer pageNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        removeTaskBar();
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        String url = getIntent().getStringExtra("url");
        if (!TextUtils.isEmpty(url)) {
            displayFromUrl(url);
        }
    }

    private void displayFromUrl(String assetFileName) {
        mLoadingView.show();
        new RetrivePDFStream().execute(assetFileName);


    }


    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
    }


    @Override
    public void loadComplete(int nbPages) {
        mLoadingView.hideDialog();
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        printBookmarksTree(pdfView.getTableOfContents(), "-");

    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {
            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                clearActivityStack();
                break;
        }

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_pdf_view;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        String name = getIntent().getStringExtra("name");
        tbTv.setText(name);

    }


    @SuppressLint("StaticFieldLeak")
    class RetrivePDFStream extends AsyncTask<String, Void, InputStream> {
        protected InputStream doInBackground(String... strings) {
            InputStream inputStream = null;
            try {
                URL uri = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) uri.openConnection();
                if (urlConnection.getResponseCode() == 200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                }
            } catch (IOException e) {
                mLoadingView.hideDialog();
                return null;
            }
            return inputStream;
        }

        protected void onPostExecute(InputStream inputStream) {
//            pdfView.fromStream(inputStream).password("Your Password").load();
            LoadPdfView(inputStream);
        }
    }

    private void LoadPdfView(InputStream inputStream) {
        pdfView.fromStream(inputStream)
//        pdfView.fromUri(Uri.parse(assetFileName))
                .defaultPage(pageNumber)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .onError(new OnErrorListener() {
                    @Override
                    public void onError(Throwable t) {
                        mLoadingView.hideDialog();
                        Toast.makeText(ActivityPdfView.this, "File not found", Toast.LENGTH_SHORT).show();
                        Log.i("error", Objects.requireNonNull(t.getMessage()));
                    }
                })
                .scrollHandle(new DefaultScrollHandle(this))
                .load();
    }
}
