package com.pongo.health.ui.vet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.kinda.alert.KAlertDialog;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.ui.ActivityAddPrimaryHospital;
import com.pongo.health.ui.AddNoteActivity;
import com.pongo.health.ui.ChatViewActivity;
import com.pongo.health.ui.FeedbackActivity;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.audioListener.AppRTCAudioManager;
import com.twilio.video.CameraCapturer;
import com.twilio.video.ConnectOptions;
import com.twilio.video.LocalAudioTrack;
import com.twilio.video.LocalDataTrack;
import com.twilio.video.LocalParticipant;
import com.twilio.video.LocalVideoTrack;
import com.twilio.video.RemoteAudioTrack;
import com.twilio.video.RemoteAudioTrackPublication;
import com.twilio.video.RemoteDataTrack;
import com.twilio.video.RemoteDataTrackPublication;
import com.twilio.video.RemoteParticipant;
import com.twilio.video.RemoteVideoTrack;
import com.twilio.video.RemoteVideoTrackPublication;
import com.twilio.video.Room;
import com.twilio.video.TwilioException;
import com.twilio.video.Video;
import com.twilio.video.VideoView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideocallActivity extends BaseActivity {
    boolean enable = true;
    @BindView(R.id.video_view)
    VideoView videoView;
    @BindView(R.id.video_container)
    FrameLayout videoContainer;
    @BindView(R.id.loader_icon)
    ImageView loaderImage;
    @BindView(R.id.waiting_container)
    LinearLayout waitingLayout;
    @BindView(R.id.pet_name_tv)
    TextView petName;
    @BindView(R.id.primary_video_view)
    VideoView mPrimaryvideoView;
    @BindView(R.id.connect_action_fab)
    FloatingActionButton endcall;
    private LocalAudioTrack localAudioTrack;
    private List<LocalAudioTrack> localAudioTrackList = new ArrayList<>();
    private LocalVideoTrack localVideoTrack;
    private List<LocalVideoTrack> localVideoTrackList = new ArrayList<>();
    private LocalDataTrack localDataTrack;
    private List<LocalDataTrack> localDataTrackList = new ArrayList<>();
    private CameraCapturer cameraCapturer;
    private Room room;
    private String token;
    private Session mSession;
    private CustomDialog mCustomDialog;
    private String chat_with;
    private String chatRoom;
    private String logo;
    private String petNameString;
    private String chatId = "";
    private String doctorId;
    boolean isConnected = false;
    private CountDownTimer mCountDownTimer;
    private CountDownTimer mCallTimer;
    private int previousAudioMode;
    private AudioManager audioManager;
    private static final String TAG = "VideocallActivity";
    //    private MusicIntentReceiver myReceiver;
    private boolean isHeadset = false;
    private boolean callConnected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mSession = new Session(this);
        mCustomDialog = new CustomDialog(this);
        setEndcallListener();
        setWaitingView();
        if (!checkPermission()) {
            initRequiredFields();
            getOtherUser();
            getToken();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
                    102);
        }
//        myReceiver = new MusicIntentReceiver();

        audioListener();
    }

    private void audioListener() {
        AppRTCAudioManager audioManager = AppRTCAudioManager.create(this);
        audioManager.start(new AppRTCAudioManager.AudioManagerEvents() {
            @Override
            public void onAudioDeviceChanged(AppRTCAudioManager.AudioDevice audioDevice, Set<AppRTCAudioManager.AudioDevice> availableAudioDevices) {
//                onAudioManagerDevicesChanged(audioDevice, availableAudioDevices);
//                Toast.makeText(VideocallActivity.this, "device", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void startTimer() {
        String appointment = getIntent().getStringExtra("appointment");
        int timer = 300000;
        if (!TextUtils.isEmpty(appointment) && appointment.equalsIgnoreCase("yes")) {
            timer = 900000;
        }
        mCountDownTimer = new CountDownTimer(timer, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                if (!isConnected) {
                    String type = mSession.getuser_type();
                    if (TextUtils.isEmpty(type)) {
                        if (!TextUtils.isEmpty(appointment) && appointment.equalsIgnoreCase("yes")) {

                        } else {
                            appointmentconfimrDialog();
                        }
                    }
                    callDisconnect();
                }
            }
        }.start();
    }

    private void callTimer() {

        mCallTimer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                callConnected = true;
            }
        }.start();
    }


    private void appointmentconfimrDialog() {
        new KAlertDialog(this, KAlertDialog.WARNING_TYPE)
                .setTitleText("Sorry")
                .setContentText("No Vets are available to speak immediately")
                .setCancelText("No")
                .setConfirmText("Book appointment")
                .showCancelButton(true)
                .setCancelClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        finish();
                    }
                })
                .setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog kAlertDialog) {
                        kAlertDialog.dismissWithAnimation();
                        startActivity(new Intent(VideocallActivity.this, ActivityAddPrimaryHospital.class)
                                .putExtra("screen", "video"));
                        finish();
                    }
                })
                .show();
    }

    private void setWaitingView() {
        String type = mSession.getuser_type();
        if (TextUtils.isEmpty(type)) {
            startTimer();
            videoContainer.setVisibility(View.GONE);
            waitingLayout.setVisibility(View.VISIBLE);
            String petname = getIntent().getStringExtra("pet_name");
            petName.setText("Doctor is reviewing " + petname + " history");
            Glide.with(this).load(R.drawable.loading).into(loaderImage);
            final String[] array = {"Doctor is reviewing " + petname + " history", "Doctor is putting their cape on", "Doctor will join momentarily"};
            petName.post(new Runnable() {
                int i = 0;

                @Override
                public void run() {
                    petName.setText(array[i]);
                    i++;
                    if (i == 3)
                        i = 0;
                    petName.postDelayed(this, 3000);
                }
            });
        }

    }

    private void setEndcallListener() {
        endcall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkConnection();

            }
        });
    }

    private void checkConnection() {
        if (isConnected) {
            String type = mSession.getuser_type();
            if (TextUtils.isEmpty(type)) {
                if (!TextUtils.isEmpty(doctorId)) {
                    Intent intent = new Intent(VideocallActivity.this, FeedbackActivity.class);
                    String user_id = mSession.getuser_id();
                    intent.putExtra("logo", logo);
                    intent.putExtra("user_id", user_id);
                    intent.putExtra("tech_id", doctorId);
                    intent.putExtra("pet_name", petNameString);
                    intent.putExtra("chatid", chatId + "_video");
                    startActivity(intent);
                }
            } else {
                String user_id = getIntent().getStringExtra("user_id");
                String chatid = getIntent().getStringExtra("chatid");
                if (!TextUtils.isEmpty(chatid)) {
                    callDuration(chatid);
                }
                if (!TextUtils.isEmpty(user_id) && !TextUtils.isEmpty(chatid)) {
                    Intent intent = new Intent(VideocallActivity.this, AddNoteActivity.class);
                    intent.putExtra("user_id", user_id);
                    intent.putExtra("chatid", chatid + "_video");
                    startActivity(intent);
                }
            }
        }
        if (!callConnected) {
            callDisconnect();
        }
        finish();
    }

    private void getOtherUser() {
        Intent intent = getIntent();
        if (null != intent) {
            chat_with = intent.getStringExtra("user_id");
            chatRoom = intent.getStringExtra("chatroom");
        }
    }

    private boolean checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
        ) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 102) {
            if (grantResults.length > 0) {

                boolean recordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (recordAccepted) {
                    initRequiredFields();
                    getOtherUser();
                    getToken();
                }
            }
        }
    }


    @Override
    protected void setText() {

    }

    @Override
    protected void setOnClick() {

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_videocall;
    }


    private void initRequiredFields() {
        // Get AudioManager
        setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (isHeadset) {
            // Route audio through headset
            audioManager.setSpeakerphoneOn(false);
        } else {
            // Route audio through speaker
            audioManager.setSpeakerphoneOn(true);
        }
// Create an audio track
        localAudioTrack = LocalAudioTrack.create(this, enable);
        localAudioTrackList.add(localAudioTrack);
// A video track requires an implementation of VideoCapturer
        cameraCapturer = new CameraCapturer(this,
                CameraCapturer.CameraSource.FRONT_CAMERA);
// Create a video track
        localVideoTrack = LocalVideoTrack.create(this, enable, cameraCapturer);
        localVideoTrackList.add(localVideoTrack);
// Create a Data track

        localDataTrack = LocalDataTrack.create(this);
        localDataTrackList.add((localDataTrack));
// Rendering a local video track requires an implementation of VideoRenderer
// Let's assume we have added a VideoView in our view hierarchy

// Render a local video track to preview your camera
        localVideoTrack.addRenderer(videoView);

    }

    private Room.Listener roomListener() {
        return new Room.Listener() {
            @Override
            public void onConnected(Room room) {
//                mCallLayout.setVisibility(View.VISIBLE);
//                mButtonLayout.setVisibility(View.GONE);
                Log.d("room connected", "Connected to " + room.getName());
                if (null != room && room.getRemoteParticipants().size() > 0) {
                    videoContainer.setVisibility(View.VISIBLE);
                    waitingLayout.setVisibility(View.GONE);
                    RemoteParticipant participant = room.getRemoteParticipants().get(0);
                    /*
                     * Add remote participant renderer
                     */
                    RemoteVideoTrackPublication remoteVideoTrackPublication =
                            participant.getRemoteVideoTracks().get(0);

                    /*
                     * Only render video tracks that are subscribed to
                     */
                    if (remoteVideoTrackPublication.isTrackSubscribed()) {
                        mPrimaryvideoView.setMirror(false);
                        Objects.requireNonNull(remoteVideoTrackPublication.getRemoteVideoTrack()).addRenderer(mPrimaryvideoView);
                    }
                    isConnected = true;
                    callTimer();
                    participant.setListener(remoteParticipantListener());
                    Log.i("HandleParticipants", participant.getIdentity() + " is in the room.");
                } else {
                    String type = mSession.getuser_type();
                    if (!TextUtils.isEmpty(type) && type.equalsIgnoreCase("vet")) {
                        failureDialog();
                    }
                }
            }

            @Override
            public void onConnectFailure(@NonNull Room room, @NonNull TwilioException twilioException) {
                Log.d("connection fail", "Connected to " + room.getName());

            }

            @Override
            public void onReconnecting(@NonNull Room room, @NonNull TwilioException twilioException) {
                Log.d("reconnecting", "Connected to " + room.getName());

            }

            @Override
            public void onReconnected(@NonNull Room room) {
                Log.d("reconnected", "Connected to " + room.getName());

            }

            @Override
            public void onDisconnected(@NonNull Room room, @Nullable TwilioException twilioException) {
                Log.d("disconnected", "Connected to " + room.getName());
                enableAudioFocus(false);
                enableVolumeControl(false);
                finish();
            }

            @Override
            public void onParticipantConnected(@NonNull Room room, @NonNull RemoteParticipant remoteParticipant) {
                Log.d("participant connected", "Connected to " + room.getName());
                String type = mSession.getuser_type();
                if (TextUtils.isEmpty(type)) {
                    getChatDetail();
                    isConnected = true;
                }
                callTimer();
                remoteParticipant.setListener(remoteParticipantListener());
            }

            @Override
            public void onParticipantDisconnected(@NonNull Room room, @NonNull RemoteParticipant remoteParticipant) {
                Log.d("participant disconnect", "Connected to " + room.getName());
                String type = mSession.getuser_type();
                if (TextUtils.isEmpty(type)) {
                    if (!TextUtils.isEmpty(doctorId)) {
                        Intent intent = new Intent(VideocallActivity.this, FeedbackActivity.class);
                        String user_id = mSession.getuser_id();
                        intent.putExtra("logo", logo);
                        intent.putExtra("user_id", user_id);
                        intent.putExtra("tech_id", doctorId);
                        intent.putExtra("pet_name", petNameString);
                        intent.putExtra("chatid", chatId + "_video");
                        startActivity(intent);
                    }
                } else {
                    String user_id = getIntent().getStringExtra("user_id");
                    String chatid = getIntent().getStringExtra("chatid");
                    if (!TextUtils.isEmpty(user_id) && !TextUtils.isEmpty(chatid)) {
                        Intent intent = new Intent(VideocallActivity.this, AddNoteActivity.class);
                        intent.putExtra("user_id", user_id);
                        intent.putExtra("chatid", chatid + "_video");
                        startActivity(intent);
                    }
                }

                finish();
            }

            @Override
            public void onRecordingStarted(@NonNull Room room) {
                Log.d("on recording", "Connected to " + room.getName());

            }

            @Override
            public void onRecordingStopped(@NonNull Room room) {
                Log.d("on recording stop", "Connected to " + room.getName());

            }
        };
    }

    private void enableAudioFocus(boolean focus) {
        if (focus) {
            previousAudioMode = audioManager.getMode();
            // Request audio focus before making any device switch.
            requestAudioFocus();
            /*
             * Use MODE_IN_COMMUNICATION as the default audio mode. It is required
             * to be in this mode when playout and/or recording starts for the best
             * possible VoIP performance. Some devices have difficulties with
             * speaker mode if this is not set.
             */
            audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        } else {
            audioManager.setMode(previousAudioMode);
            audioManager.abandonAudioFocus(null);
        }
    }

    private void requestAudioFocus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            AudioAttributes playbackAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_VOICE_COMMUNICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();
            AudioFocusRequest focusRequest =
                    new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                            .setAudioAttributes(playbackAttributes)
                            .setAcceptsDelayedFocusGain(true)
                            .setOnAudioFocusChangeListener(
                                    i -> {
                                    })
                            .build();
            audioManager.requestAudioFocus(focusRequest);
        } else {
            audioManager.requestAudioFocus(null, AudioManager.STREAM_VOICE_CALL,
                    AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
        }
    }

    private void enableVolumeControl(boolean volumeControl) {
        if (volumeControl) {
            /*
             * Enable changing the volume using the up/down keys during a conversation
             */
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
        } else {
            setVolumeControlStream(getVolumeControlStream());
        }
    }

    private void failureDialog() {
        KAlertDialog kAlertDialog = new KAlertDialog(this, KAlertDialog.ERROR_TYPE);
        kAlertDialog.setTitleText("Oops...");
        kAlertDialog.setContentText("No User available");
        kAlertDialog.setCancelable(true);
        kAlertDialog.setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(KAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
                callDisconnect();
                finish();
            }
        })
                .show();
    }

    public void connectToRoom(String roomName, String token) {
        enableAudioFocus(true);
        enableVolumeControl(true);

        ConnectOptions connectOptions = new ConnectOptions.Builder(token)
                .roomName(roomName)
                .audioTracks(localAudioTrackList)
                .videoTracks(localVideoTrackList)
                .dataTracks(localDataTrackList)
                .build();
        room = Video.connect(this, connectOptions, roomListener());

// ... Assume we have received the connected callback

// After receiving the connected callback the LocalParticipant becomes available
        LocalParticipant localParticipant = room.getLocalParticipant();
        if (localParticipant != null) {
            Log.i("LocalParticipant ", localParticipant.getIdentity());
        }

// Get the first participant from the room

    }

    /* In the Participant listener, we can respond when the Participant adds a Video
Track by rendering it on screen: */
    private RemoteParticipant.Listener remoteParticipantListener() {
        return new RemoteParticipant.Listener() {
            @Override
            public void onAudioTrackPublished(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteAudioTrackPublication remoteAudioTrackPublication) {

            }

            @Override
            public void onAudioTrackUnpublished(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteAudioTrackPublication remoteAudioTrackPublication) {

            }

            @Override
            public void onAudioTrackSubscribed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteAudioTrackPublication remoteAudioTrackPublication, @NonNull RemoteAudioTrack remoteAudioTrack) {

            }

            @Override
            public void onAudioTrackSubscriptionFailed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteAudioTrackPublication remoteAudioTrackPublication, @NonNull TwilioException twilioException) {

            }

            @Override
            public void onAudioTrackUnsubscribed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteAudioTrackPublication remoteAudioTrackPublication, @NonNull RemoteAudioTrack remoteAudioTrack) {

            }

            @Override
            public void onVideoTrackPublished(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteVideoTrackPublication remoteVideoTrackPublication) {

            }

            @Override
            public void onVideoTrackUnpublished(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteVideoTrackPublication remoteVideoTrackPublication) {

            }

            @Override
            public void onVideoTrackSubscribed(RemoteParticipant participant,
                                               RemoteVideoTrackPublication remoteVideoTrackPublication,
                                               RemoteVideoTrack remoteVideoTrack) {
                mPrimaryvideoView.setMirror(false);
                remoteVideoTrack.addRenderer(mPrimaryvideoView);
                videoContainer.setVisibility(View.VISIBLE);
                waitingLayout.setVisibility(View.GONE);
            }

            @Override
            public void onVideoTrackSubscriptionFailed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteVideoTrackPublication remoteVideoTrackPublication, @NonNull TwilioException twilioException) {

            }

            @Override
            public void onVideoTrackUnsubscribed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteVideoTrackPublication remoteVideoTrackPublication, @NonNull RemoteVideoTrack remoteVideoTrack) {

            }

            @Override
            public void onDataTrackPublished(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteDataTrackPublication remoteDataTrackPublication) {

            }

            @Override
            public void onDataTrackUnpublished(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteDataTrackPublication remoteDataTrackPublication) {

            }

            @Override
            public void onDataTrackSubscribed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteDataTrackPublication remoteDataTrackPublication, @NonNull RemoteDataTrack remoteDataTrack) {

            }

            @Override
            public void onDataTrackSubscriptionFailed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteDataTrackPublication remoteDataTrackPublication, @NonNull TwilioException twilioException) {

            }

            @Override
            public void onDataTrackUnsubscribed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteDataTrackPublication remoteDataTrackPublication, @NonNull RemoteDataTrack remoteDataTrack) {

            }

            @Override
            public void onAudioTrackEnabled(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteAudioTrackPublication remoteAudioTrackPublication) {

            }

            @Override
            public void onAudioTrackDisabled(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteAudioTrackPublication remoteAudioTrackPublication) {

            }

            @Override
            public void onVideoTrackEnabled(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteVideoTrackPublication remoteVideoTrackPublication) {

            }

            @Override
            public void onVideoTrackDisabled(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteVideoTrackPublication remoteVideoTrackPublication) {

            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release the audio track to free native memory resources
        localAudioTrack.release();

// Release the video track to free native memory resources
        localVideoTrack.release();
        // To disconnect from a Room, we call:
        if (room != null) {
            room.disconnect();
        }
        if (null != mCountDownTimer) {
            mCountDownTimer.cancel();
        }
        if (null != mCallTimer) {
            mCallTimer.cancel();
        }
    }

    private void getToken() {
        mCustomDialog.show();
        String id = mSession.getuser_id();
        String roomName = "";
        if (!TextUtils.isEmpty(chatRoom)) {
            roomName = chatRoom;
        } else {
            roomName = "chat_" + chat_with;
        }
        String finalRoomName = roomName;
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getToken(roomName, id).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mCustomDialog.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            token = jsonObject.getString("token");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    connectToRoom(finalRoomName, token);
                                } else {
                                    Toast.makeText(VideocallActivity.this, "Token failed", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(VideocallActivity.this, "Token failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mCustomDialog.hideDialog();
                Toast.makeText(VideocallActivity.this, "Failed to get Token", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        confimrDialog();
    }

    private void confimrDialog() {
        new KAlertDialog(this, KAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("You want to exit!")
                .setCancelText("No")
                .setConfirmText("Yes")
                .showCancelButton(true)
                .setCancelClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                .setConfirmClickListener(new KAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(KAlertDialog kAlertDialog) {
                        kAlertDialog.dismissWithAnimation();
                        checkConnection();
                    }
                })
                .show();
    }

    private void getChatDetail() {
        String userid = mSession.getuser_id();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getChatDetail(userid, chat_with).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    petNameString = jsonObject.getString("pet_name");
                                    logo = jsonObject.getString("pet_img");
                                    chatId = jsonObject.getString("chatid");
                                    doctorId = jsonObject.getString("doctor_id");
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                Log.e("cate", th.toString());
            }
        });
    }

    @Override
    public void onResume() {
        IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
//        registerReceiver(myReceiver, filter);
        super.onResume();
    }

    private class MusicIntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                int state = intent.getIntExtra("state", -1);
                switch (state) {
                    case 0:
                        Log.d(TAG, "Headset is unplugged");
                        isHeadset = false;
                        if (null != audioManager) {
                            audioManager.setSpeakerphoneOn(true);
                        }
                        break;
                    case 1:
                        Log.d(TAG, "Headset is plugged");
                        isHeadset = true;
                        if (null != audioManager) {
                            audioManager.setSpeakerphoneOn(false);
                        }
                        break;
                    default:
                        Log.d(TAG, "I have no idea what the headset state is");
                        isHeadset = true;
                        if (null != audioManager) {
                            audioManager.setSpeakerphoneOn(true);
                        }
                }
            }
        }
    }

    @Override
    public void onPause() {
//        unregisterReceiver(myReceiver);
        super.onPause();
    }

    private void callDisconnect() {
//        mCustomDialog.show();
     /*   String id = mSession.getuser_id();
        String roomName = "";
        if (!TextUtils.isEmpty(chatRoom)) {
            roomName = chatRoom;
        } else {
            roomName = "chat_" + chat_with;
        }
        String finalRoomName = roomName;*/
        String userid = mSession.getuser_id();
        String type = mSession.getuser_type();
        String chatWith = chat_with;
        String chatid = chatId;
        if (!TextUtils.isEmpty(type)) {
            chatWith = "";
            chatid = getIntent().getStringExtra("chatid");
        }
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).videoDisconnect(userid, chatid, chatWith).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                mCustomDialog.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            String detail = jsonObject.getString("detail");
                           /* if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
//                                    connectToRoom(finalRoomName, token);
                                } else {
                                    Toast.makeText(VideocallActivity.this, "Token failed", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(VideocallActivity.this, "Token failed", Toast.LENGTH_SHORT).show();
                            }*/
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
//                mCustomDialog.hideDialog();
//                Toast.makeText(VideocallActivity.this, "Failed to get Token", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

    private void callDuration(String chatId) {

        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).updateVideocallDuration(chatId).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                mCustomDialog.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
//                            String detail = jsonObject.getString("detail");
                           /* if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
//                                    connectToRoom(finalRoomName, token);
                                } else {
                                    Toast.makeText(VideocallActivity.this, "Token failed", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(VideocallActivity.this, "Token failed", Toast.LENGTH_SHORT).show();
                            }*/
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
//                mCustomDialog.hideDialog();
//                Toast.makeText(VideocallActivity.this, "Failed to get Token", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }

}
