package com.pongo.health.ui.vet;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityAvailability extends BaseActivity {

    @BindView(R.id.tab_second_tv) TextView repeatAvailabilityTv;
    @BindView(R.id.tab_one_tv) TextView weeklyAvailability;
    @BindView(R.id.tb_tv) TextView tbTv;
    @BindView(R.id.tb_iv) ImageView tbIv;
    @BindView(R.id.blue_btn_tv) TextView updateBtnTv;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setRepeatAvailability();

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tab_one_tv:
                repeatAvailabilityTv.setBackground(getResources().getDrawable(R.drawable.button_shaper));
                weeklyAvailability.setBackground(getResources().getDrawable(R.drawable.button_shaper_selected));
                repeatAvailabilityTv.setTextColor(getResources().getColor(R.color.text_color_blue));
                weeklyAvailability.setTextColor(getResources().getColor(R.color.white_color));

                break;
            case R.id.tab_second_tv:
                setRepeatAvailability();
                break;
            case R.id.v_tb_iv:
                onBackPressed();
                finish();
                break;
        }
    }

        private void setRepeatAvailability() {

            weeklyAvailability.setBackground(getResources().getDrawable(R.drawable.button_shaper));
            repeatAvailabilityTv.setBackground(getResources().getDrawable(R.drawable.button_shaper_selected));
            weeklyAvailability.setTextColor(getResources().getColor(R.color.text_color_blue));
            repeatAvailabilityTv.setTextColor(getResources().getColor(R.color.white_color));
          
        }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
    tbTv.setText("Availability");
    updateBtnTv.setText("Update");
    weeklyAvailability.setText("Set weekly Availability");
    repeatAvailabilityTv.setText("Repeat\n Availability");
        }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        weeklyAvailability.setOnClickListener(this);
        repeatAvailabilityTv.setOnClickListener(this);

        }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_availability;
    }
}

