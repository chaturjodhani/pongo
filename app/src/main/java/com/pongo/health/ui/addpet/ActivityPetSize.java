package com.pongo.health.ui.addpet;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pongo.health.R;
import com.pongo.health.adapter.AddPetSizeAdapter;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.PetSizeModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityPetSize extends BaseActivity {

    @BindView(R.id.blue_btn_tv)
    TextView nextTv;
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.pet_name)
    TextView petnameTv;
    @BindView(R.id.pet_img)
    CircleImageView petImage;
    @BindView(R.id.pet_size_description_tv)
    TextView petSizeDescriptionTv;
    @BindView(R.id.pet_size_rv)
    RecyclerView petSizeRv;
    private List<PetSizeModel> petSizeList = new ArrayList<>();
    private AddPetSizeAdapter mAdapter;
    private String petname;
    private String petimage;
    private String petType = "";
    private String petSize = "";
    private String breedSeleceted = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        if (null != intent) {
            petname = intent.getStringExtra("pet_name");
            petimage = intent.getStringExtra("pet_image");
            petType = intent.getStringExtra("pet_type");
            breedSeleceted = intent.getStringExtra("breed");
        }
        petnameTv.setText("What is " + petname + "'s size?");
        Glide.with(this).load(petimage).apply(RequestOptions.circleCropTransform()).into(petImage);

        setPetSize();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                goBack();
                break;
            case R.id.blue_btn_tv:
                verify_data();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        Intent intent = new Intent(ActivityPetSize.this, ActivityPetType.class);
        intent.putExtra("pet_name", petname);
        intent.putExtra("pet_image", petimage);
        startActivity(intent);
        finish();
    }


    private void setPetSize() {
        petSizeRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        petSizeList.add(new PetSizeModel(R.drawable.pet_small_size_img, "Small", "5 - 22lbs"));
        petSizeList.add(new PetSizeModel(R.drawable.pet_small, "Medium", "22 - 44lbs"));
        petSizeList.add(new PetSizeModel(R.drawable.pet_large_size_img, "Large", "45 - 88lbs"));
        petSizeList.add(new PetSizeModel(R.drawable.pet_e_large_img, "Extra Large", "89 - 132lbs"));
        mAdapter = new AddPetSizeAdapter(this, petSizeList);
        petSizeRv.setAdapter(mAdapter);
    }

    private void verify_data() {
        if (mAdapter.selectedPosition == -1) {
            Toast.makeText(this, "Please select pet size", Toast.LENGTH_SHORT).show();
        } else {
            petSize = petSizeList.get(mAdapter.selectedPosition).getSizeType();
            Intent intent = new Intent(ActivityPetSize.this, ActivityPetOld.class);
            intent.putExtra("pet_name", petname);
            intent.putExtra("pet_image", petimage);
            intent.putExtra("pet_type", petType);
            intent.putExtra("pet_size", petSize);
            intent.putExtra("breed", breedSeleceted);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void setText() {
        petSizeDescriptionTv.setText(Html.fromHtml("<p>We don't support pets under " +
                "5 lbs and<br/>over 132 lbs at this time"));
        tbTv.setText(R.string.add_pet);
        nextTv.setText(R.string.next);

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        nextTv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_add_pet_size;
    }
}
