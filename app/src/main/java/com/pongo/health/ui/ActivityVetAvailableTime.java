package com.pongo.health.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.MedicationQuantityAdapter;
import com.pongo.health.adapter.TimeSlotAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.PastVetSearchModel;
import com.pongo.health.model.TimeslotModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.InternetConnection;
import com.pongo.health.utils.ItemClickListener;
import com.github.jhonnyx2012.horizontalpicker.DatePickerListener;
import com.github.jhonnyx2012.horizontalpicker.HorizontalPicker;


import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityVetAvailableTime extends BaseActivity implements DatePickerListener {

    @BindView(R.id.vet_availability_des_tv)
    TextView vetAvailabilityDesTv;
    @BindView(R.id.tb_iv)
    ImageView tb_iv;
    @BindView(R.id.tb_tv)
    TextView tb_tv;
    @BindView(R.id.blue_btn_tv)
    TextView bookTv;
    @BindView(R.id.datePicker)
    HorizontalPicker datePicker;
    @BindView(R.id.time_rv)
    RecyclerView timeRv;
    String vetId;
    String price;
    String bookDate = "";
    private List<TimeslotModel.TimeslotsBean> timeList = new ArrayList<>();
    private CustomDialog mLoadingView;
    private Session mSession;
    private String timeSlot = "";
    TimeSlotAdapter timeSlotAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
        Intent intent = getIntent();
        if (null != intent) {
            vetId = getIntent().getStringExtra("vet_id");
            String screen = intent.getStringExtra("screen");
            if (!TextUtils.isEmpty(screen) && screen.equalsIgnoreCase("video")) {
                vetAvailabilityDesTv.setVisibility(View.VISIBLE);
            }
            else
            {
                vetAvailabilityDesTv.setVisibility(View.GONE);
            }
        }
        datePicker.showTodayButton(false);
//        datePicker.setDate(new DateTime().plusDays(1));
        datePicker.setDate(new DateTime());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.blue_btn_tv:
                if (TextUtils.isEmpty(timeSlot)) {
                    Toast.makeText(this, "Please select time for appointment", Toast.LENGTH_SHORT).show();
                } else {
                    startActivity(new Intent(this, BookAppointmentActivity.class)
                            .putExtra("vet_id", vetId)
                            .putExtra("time", timeSlot)
                            .putExtra("price", price)
                            .putExtra("booking_date", bookDate));
                }
//                Toast.makeText(this, "Booked", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        vetAvailabilityDesTv.setText(Html.fromHtml("We are Sorry!<br/> All our Vets are " +
                "currently seeing patients."));
        bookTv.setText("Continue");
        tb_tv.setText("Vet Availablity");

    }

    @Override
    protected void setOnClick() {
        tb_iv.setOnClickListener(this);
        bookTv.setOnClickListener(this);
        datePicker.setListener(this).init();
//        setTime();

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_vet_available_time;
    }

    @Override
    public void onDateSelected(DateTime dateSelected) {
//        Toast.makeText(this, "Selected date is " + dateSelected.toString(), Toast.LENGTH_SHORT).show();

        try {
            SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
            SimpleDateFormat dateFormatter = new SimpleDateFormat("YYYY-MM-dd");
            Date date = dateParser.parse(dateSelected.toString());
            String dt = dateFormatter.format(date);
            if (!TextUtils.isEmpty(vetId)) {
                if (InternetConnection.checkConnection(this)) {
                    bookDate = dt;
                    getTimeSlot(dt);
                } else {
                    Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    private void getTimeSlot(String date) {
        timeSlot = "";
        this.mLoadingView.show();
        if (null != timeSlotAdapter) {
            timeList.clear();
            timeSlotAdapter.notifyDataSetChanged();
        }
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getTimeSlot(vetId, date).enqueue(new Callback<TimeslotModel>() {
            public void onResponse(Call<TimeslotModel> call, Response<TimeslotModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((TimeslotModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        timeList = response.body().getTimeslots();
                        price = response.body().getPrice();
                        setTime();
                    }
                }

            }

            public void onFailure(Call<TimeslotModel> call, Throwable th) {
                timeList = new ArrayList<>();
                setTime();
                mLoadingView.hideDialog();
                Toast.makeText(ActivityVetAvailableTime.this, "No time available", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });
    }


    private void setTime() {
        timeRv.setLayoutManager(new GridLayoutManager(this, 5));

//        timeRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
      /*  timeList.add("05:00pm");
        timeList.add("06:00pm");
        timeList.add("07:00pm");
        timeList.add("08:00pm");*/


        // strengthTv.setText(strengthList.get(postion));
        ItemClickListener itemClickListener = new ItemClickListener() {

            @Override
            public void onClicked(View view, int postion) {
                // strengthTv.setText(strengthList.get(postion));
                timeSlot = timeList.get(postion).getStart();
            }
        };
        timeSlotAdapter = new TimeSlotAdapter(this, timeList, itemClickListener);
        timeRv.setAdapter(timeSlotAdapter);


    }


}
