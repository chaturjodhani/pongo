package com.pongo.health.ui.vet;

import androidx.appcompat.widget.SwitchCompat;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.OtpModel;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityAboutYourself extends BaseActivity {

    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.dog_sb)
    SwitchCompat dogSwitch;
    @BindView(R.id.bird_sb)
    SwitchCompat birdSwitch;
    @BindView(R.id.cat_sb)
    SwitchCompat catSwitch;
    @BindView(R.id.food_sb)
    SwitchCompat foodSwitch;
    @BindView(R.id.horses_sb)
    SwitchCompat horseSwitch;
    @BindView(R.id.exotic_sb)
    SwitchCompat exoticSwitch;
    @BindView(R.id.desc_edit)
    EditText descEt;
    String strDog = "";
    String strBird = "";
    String strCat = "";
    String strFood = "";
    String strHorse = "";
    String strExostic = "";
    private String vetType = "";
    private OtpModel otpModel;
    private String mLoginType;
    private String mMobile;
    private String str_firstname, str_lastname;
    private String mPassword;
    private String mEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setListener();
        Gson gson = new Gson();
        otpModel = gson.fromJson(getIntent().getStringExtra("otp"), OtpModel.class);
        mLoginType = getIntent().getStringExtra("login_type");
        mMobile = getIntent().getStringExtra("phone");
        mEmail = getIntent().getStringExtra("email");
        mPassword = getIntent().getStringExtra("password");
        str_firstname = getIntent().getStringExtra("first_name");
        str_lastname = getIntent().getStringExtra("last_name");
        vetType = getIntent().getStringExtra("vetType");
    }

    private void setListener() {
        dogSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strDog = "Dog (Cannies)";
                } else {
                    strDog = "";
                }
            }
        });
        birdSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strBird = "Birds (Avian)";
                } else {
                    strBird = "";
                }
            }
        });
        catSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strCat = "Cats (Felines)";
                } else {
                    strCat = "";
                }
            }
        });
        horseSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strHorse = "Horses (Equine)";
                } else {
                    strHorse = "";
                }
            }
        });
        exoticSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strExostic = "Exotic Companian Mammals";
                } else {
                    strExostic = "";
                }
            }
        });
        foodSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    strFood = "Food animal (Cattle and Pigs)";
                } else {
                    strFood = "";
                }
            }
        });
    }

    private void verifyData() {
        String expertise = "";
        if (!TextUtils.isEmpty(strDog)) {
            expertise = strDog;
        }
        if (!TextUtils.isEmpty(strBird)) {
            expertise = expertise + "," + strBird;
        }
        if (!TextUtils.isEmpty(strCat)) {
            expertise = expertise + "," + strCat;
        }
        if (!TextUtils.isEmpty(strFood)) {
            expertise = expertise + "," + strFood;
        }
        if (!TextUtils.isEmpty(strHorse)) {
            expertise = expertise + "," + strHorse;
        }
        if (!TextUtils.isEmpty(strExostic)) {
            expertise = expertise + "," + strExostic;
        }
        String desc = descEt.getText().toString();
        if (TextUtils.isEmpty(expertise))
        {
            Toast.makeText(this, "Please select expertise", Toast.LENGTH_SHORT).show();
        }
        else  if (TextUtils.isEmpty(desc))
        {
            Toast.makeText(this, "Please enter description", Toast.LENGTH_SHORT).show();
        }
        else {
            Intent intent = new Intent(this, ActivityIdentification.class);
            Gson gson = new Gson();
            String myJson = gson.toJson(otpModel);
            intent.putExtra("otp", myJson);
            intent.putExtra("login_type", mLoginType);
            intent.putExtra("phone", mMobile);
            intent.putExtra("email", mEmail);
            intent.putExtra("password", mPassword);
            intent.putExtra("first_name", str_firstname);
            intent.putExtra("last_name", str_lastname);
            intent.putExtra("vetType", vetType);
            intent.putExtra("expertise", expertise);
            intent.putExtra("desc", desc);
            startActivity(intent);
        }
           /* if ("technician".equals(vetType)) {
                startActivity(intent);
            } else {
                startActivity(new Intent(this, ActivityAboutYourself.class));
            }*/
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_tb_iv:
                clearActivityStack();
                break;
            case R.id.blue_btn_tv:
                verifyData();
//                startActivity(new Intent(this, ActivityIdentification.class));
                break;
        }
    }

    @Override
    protected void setText() {
        continueTv.setText(R.string.continue_);


    }

    @Override
    protected void setOnClick() {
        vTbIv.setOnClickListener(this);
        continueTv.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_about_yourself;
    }
}
