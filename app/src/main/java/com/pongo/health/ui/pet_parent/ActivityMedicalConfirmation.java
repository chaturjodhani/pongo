package com.pongo.health.ui.pet_parent;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.HomeModel;
import com.pongo.health.ui.ActivityHome;
import com.pongo.health.ui.AddNoteActivity;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.ImageUtil;

import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMedicalConfirmation extends BaseActivity {

    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.blue_btn_tv)
    TextView confirmTv;
    @BindView(R.id.phone)
    TextView phoneTv;
    @BindView(R.id.email)
    TextView emailTv;
    @BindView(R.id.zip)
    TextView zipTv;
    @BindView(R.id.pet_name)
    TextView pet_name;
    @BindView(R.id.clear)
    TextView clearSign;
    @BindView(R.id.note)
    EditText noteEt;
    @BindView(R.id.signature)
    SignaturePad signatureEt;
    @BindView(R.id.aggree_cb)
    CheckBox aggreeCb;
    private HomeModel.PetlistBean mPetBean;
    private String mHospitalname;
    private String mPhone;
    private String mEmail;
    private String mzip;
    private CustomDialog mLoadingView;
    private Session mSession;
    private boolean isSign = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
        Gson gson = new Gson();
        mPetBean = gson.fromJson(getIntent().getStringExtra("model"), HomeModel.PetlistBean.class);
        mHospitalname = getIntent().getStringExtra("hospital_name");
        mPhone = getIntent().getStringExtra("phone");
        mEmail = getIntent().getStringExtra("email");
        mzip = getIntent().getStringExtra("zip");
        setData();
        signListener();
    }

    private void signListener() {

        signatureEt.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                isSign = true;
            }

            @Override
            public void onClear() {
                isSign = false;
            }
        });
    }

    private void setData() {
        phoneTv.setText(mPhone);
        emailTv.setText(mEmail);
        zipTv.setText(mzip);
        pet_name.setText(mPetBean.getPet_name());
        aggreeCb.setText(Html.fromHtml("<p>I " + mSession.getfirst_name() + ", hereby certify that I am the owner " +
                "or authorized agent for the Pet owner of " + mPetBean.getPet_name() + ". Further more, I hereby request and authorize " +
                mHospitalname + " ('Veterinary Entity') to release the requested medical information" +
                "for the pet to Pongo Health. I release the Veterinary Entity and the staff form any legal " +
                "responsibility and liability for the release of information to the extent indicated " +
                "and authorized herein. </p>"));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.blue_btn_tv:
                verifydata();
                break;
            case R.id.tb_iv:
                clearActivityStack();
                break;
            case R.id.clear:
                signatureEt.clear();
                break;
        }
    }

    private void verifydata() {
        Bitmap bitmap = signatureEt.getSignatureBitmap();
        Bitmap emptyBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());

        if (!aggreeCb.isChecked()) {
            Toast.makeText(this, "Please select checkbox", Toast.LENGTH_SHORT).show();
        } else if (null == bitmap) {
            Toast.makeText(this, "Please add signature", Toast.LENGTH_SHORT).show();
        } else if (bitmap.sameAs(emptyBitmap)) {
            Toast.makeText(this, "Please add signature", Toast.LENGTH_SHORT).show();
        } else if (!isSign) {
            Toast.makeText(this, "Please add signature", Toast.LENGTH_SHORT).show();
        } else {
            SubmitRecordApi();
        }

    }

    private void SubmitRecordApi() {
        String base64String = ImageUtil.convert(signatureEt.getSignatureBitmap());
        this.mLoadingView.show();
        String petid = mPetBean.getId();
        String userId = mSession.getuser_id();
        String note = noteEt.getText().toString();
        if (TextUtils.isEmpty(note))
        {
            note = "";
        }
        String s = "";
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(mPhone); //editText.getText().toString()
        while (m.find()) {
            s = s + m.group(0);
        }
        s = s.substring(1);
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).addMedicalRecord(petid, userId, mHospitalname, s, mEmail, mzip, base64String,note).enqueue(new Callback<ResponseBody>() {
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {
                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    createConfirmDialog();

                                } else {
                                    Toast.makeText(ActivityMedicalConfirmation.this, "Opps .. Something went wrong", Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(ActivityMedicalConfirmation.this, "Opps .. Something went wrong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            public void onFailure(Call<ResponseBody> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityMedicalConfirmation.this, "Failed to send record", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });

    }

    private void createConfirmDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dailog_medical_record);
        dialog.setCancelable(false);
        TextView oktv = dialog.findViewById(R.id.ok_tv);
        TextView info_txt = dialog.findViewById(R.id.info_txt);
        info_txt.setText("We will request " + mPetBean.getPet_name() + "'s record from " + mHospitalname);
        oktv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent i = new Intent(ActivityMedicalConfirmation.this, ActivityHome.class);
                // set the new task and clear flags
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                finish();
            }
        });
        dialog.show();
    }

    @Override
    protected void setText() {
        tbTv.setText("Medical Records");
        confirmTv.setText("Confirm");


    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
        confirmTv.setOnClickListener(this);
        clearSign.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_medical_confirmation;
    }
}
