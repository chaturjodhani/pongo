package com.pongo.health.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.barteksc.pdfviewer.PDFView;
import com.pongo.health.R;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.TouchImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageViewActivity extends BaseActivity {
    @BindView(R.id.tb_tv)
    TextView tbTv;
    @BindView(R.id.time)
    TextView timeTv;
    @BindView(R.id.tb_iv)
    ImageView tbIv;
    @BindView(R.id.showAnimationButton)
    TouchImageView touchImageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        removeTaskBar();
        ButterKnife.bind(this);
        String image = getIntent().getStringExtra("image");
        Glide.with(this)
                .load(image)
                .into(touchImageView);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tb_iv:
                clearActivityStack();
                break;
        }

    }

    @Override
    protected void setOnClick() {
        tbIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_image_view;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        String name = getIntent().getStringExtra("name");
        String time = getIntent().getStringExtra("time");
        tbTv.setText(name);
        if (!TextUtils.isEmpty(time)) {
            timeTv.setVisibility(View.VISIBLE);
            timeTv.setText(time);
        }
        else
        {
            timeTv.setVisibility(View.GONE);
        }
    }

}
