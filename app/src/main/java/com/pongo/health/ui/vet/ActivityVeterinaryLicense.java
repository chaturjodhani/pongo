package com.pongo.health.ui.vet;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.adapter.CardListAdapter;
import com.pongo.health.adapter.StateListAdapter;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.BreedModel;
import com.pongo.health.model.CheckoutResponseModel;
import com.pongo.health.model.OtpModel;
import com.google.gson.Gson;
import com.pongo.health.model.StateListModel;
import com.pongo.health.ui.ActivityPayment;
import com.pongo.health.ui.addpet.ActivityPetType;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityVeterinaryLicense extends BaseActivity {

    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.vet_heading_one_tv)
    TextView vetHeadingOneTv;
    @BindView(R.id.vet_heading_second_tv)
    TextView vetHeadingSecondTv;
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.add_licence_iv)
    ImageView addLicenceIv;
    @BindView(R.id.add_license_tv)
    TextView addLicenseTv;
    @BindView(R.id.licences_added_tv)
    TextView licencesAddedTv;
    /* @BindView(R.id.stateEt)
     AppCompatEditText stateEd;
     @BindView(R.id.licenceEt)
     AppCompatEditText licenceEt;*/
    @BindView(R.id.containerLayout)
    LinearLayout container;
    static int rowIndex = 0;
    private ArrayList imageList = new ArrayList<>();
    private String hospital = "";
    private String vetType = "";
    private OtpModel otpModel;
    private String mLoginType;
    private String mMobile;
    private String str_firstname, str_lastname;
    private String mPassword;
    private String mEmail;
    private String mDesc;
    private String mExpertice;
    private List<StateListModel.StatelistBean> stateList = new ArrayList<>();
    BottomSheetDialog mBottomSheetDialog;
    private CustomDialog mLoadingView;
    RecyclerView stateRv;
    private int clicked = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Gson gson = new Gson();
        otpModel = gson.fromJson(getIntent().getStringExtra("otp"), OtpModel.class);
        mLoginType = getIntent().getStringExtra("login_type");
        mMobile = getIntent().getStringExtra("phone");
        mEmail = getIntent().getStringExtra("email");
        mPassword = getIntent().getStringExtra("password");
        str_firstname = getIntent().getStringExtra("first_name");
        str_lastname = getIntent().getStringExtra("last_name");
        vetType = getIntent().getStringExtra("vetType");
        mExpertice = getIntent().getStringExtra("desc");
        mDesc = getIntent().getStringExtra("expertise");
//        res = getIntent().getStringExtra("img_identification");
        imageList = (ArrayList<String>) getIntent().getSerializableExtra("img_identification");
        hospital = getIntent().getStringExtra("primary_hospital_name");
        addDynamicView();
        mLoadingView = new CustomDialog(this);

        createBottomList();
        getState();
    }

    private void createBottomList() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.card_list_bottomsheet, null);
        stateRv = sheetView.findViewById(R.id.card_rv);
        TextView done = sheetView.findViewById(R.id.done);
        TextView card = sheetView.findViewById(R.id.blue_btn_tv);
        card.setVisibility(View.GONE);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.setContentView(sheetView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_tb_iv:
                onBackPressed();
                finish();
                break;
            case R.id.blue_btn_tv:
                verify_data();
//                startActivity(new Intent(this, ActivityYourProfileSet.class));
                break;
            case R.id.add_licence_iv:
//                licencesAddedTv.setVisibility(View.VISIBLE);
                addLicenseTv.setText("Add More Licenses");
                addDynamicView();
                break;
        }
    }

    private void addDynamicView() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View newRowView = inflater.inflate(R.layout.licence_layout, null);
        EditText stateEt = newRowView.findViewById(R.id.stateEt);
        stateEt.setTag(rowIndex);
        stateEt.setInputType(InputType.TYPE_NULL);
        stateEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicked = (int) stateEt.getTag();
                mBottomSheetDialog.show();
            }
        });
        container.addView(newRowView, rowIndex);
        rowIndex++;
    }

    private void verify_data() {

        ArrayList<String> stateList = new ArrayList<>();
        ArrayList<String> licenceList = new ArrayList<>();

        for (int i = 0; i < container.getChildCount(); i++) {
            View child = container.getChildAt(i);
            EditText stateEd = child.findViewById(R.id.stateEt);
            EditText licenceEt = child.findViewById(R.id.licenceEt);
            String state = stateEd.getText().toString();
            String licence = licenceEt.getText().toString();
            if (!TextUtils.isEmpty(state) && !TextUtils.isEmpty(licence)) {
                if (TextUtils.isEmpty(state) || TextUtils.isEmpty(licence)) {
                    Toast.makeText(this, "Please enter state and licence", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    stateList.add(state);
                    licenceList.add(licence);
                }
            }
        }
        if (stateList.size() == 0) {
            Toast.makeText(this, "Please enter state", Toast.LENGTH_SHORT).show();

        } else if (licenceList.size() == 0) {
            Toast.makeText(this, "Please enter licence", Toast.LENGTH_SHORT).show();

        } else {
            Intent intent = new Intent(ActivityVeterinaryLicense.this, ActivityYourProfileSet.class);
            Gson gson = new Gson();
            String myJson = gson.toJson(otpModel);
            intent.putExtra("otp", myJson);
            intent.putExtra("login_type", mLoginType);
            intent.putExtra("phone", mMobile);
            intent.putExtra("email", mEmail);
            intent.putExtra("password", mPassword);
            intent.putExtra("first_name", str_firstname);
            intent.putExtra("last_name", str_lastname);
            intent.putExtra("vetType", vetType);
            intent.putExtra("expertise", mExpertice);
            intent.putExtra("desc", mDesc);
            intent.putExtra("img_identification", imageList);
            intent.putExtra("primary_hospital_name", hospital);
            intent.putExtra("state", stateList);
            intent.putExtra("licence", licenceList);
            startActivity(intent);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        vetHeadingOneTv.setText("Great, now we need your" +
                " Veterinary license number");
        vetHeadingSecondTv.setText("Please add the State" +
                " where your license was issued " +
                "along with your Veterinary license numbers");
        addLicenseTv.setText("Are your licensed in multiple states?\n" +
                "Add another state ");
        continueTv.setText(R.string.submit);

    }

    @Override
    protected void setOnClick() {
        continueTv.setOnClickListener(this);
        vTbIv.setOnClickListener(this);
        addLicenceIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_veterinary_license;
    }

    private void getState() {
        mLoadingView.show();
        ((ApiInterface) ApiClient.getClient().create(ApiInterface.class)).getStateList().enqueue(new Callback<StateListModel>() {
            public void onResponse(Call<StateListModel> call, Response<StateListModel> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String status = ((StateListModel) response.body()).getStatus();
                    if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("success")) {
                        stateList = response.body().getStatelist();
                        setStateList();
                    }
                }

            }

            public void onFailure(Call<StateListModel> call, Throwable th) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityVeterinaryLicense.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.e("cate", th.toString());
            }
        });

    }

    private void setStateList() {
        stateRv.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
      /*  for (int i = 0; i < 3; i++) {
            orderList.add("Lorem Ipsum");

        }*/
        ItemClickListener itemClickListener = new ItemClickListener() {
            @Override
            public void onClicked(View view, int position) {
                View child = container.getChildAt(clicked);
                EditText stateEd = child.findViewById(R.id.stateEt);
                stateEd.setText(stateList.get(position).getState());
                mBottomSheetDialog.dismiss();
            }
        };
        stateRv.setAdapter(new StateListAdapter(this, stateList, itemClickListener));
    }

}
