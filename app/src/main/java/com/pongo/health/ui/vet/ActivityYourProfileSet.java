package com.pongo.health.ui.vet;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.OtpModel;
import com.pongo.health.utils.CustomDialog;
import com.pongo.health.utils.GPSTracker;
import com.pongo.health.utils.InternetConnection;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityYourProfileSet extends BaseActivity {
    @BindView(R.id.v_tb_iv)
    ImageView vTbIv;
    @BindView(R.id.vet_heading_one_tv)
    TextView vetHeadingOneTv;
    @BindView(R.id.vet_heading_second_tv)
    TextView vetHeadingSecondTv;
    @BindView(R.id.blue_btn_tv)
    TextView continueTv;
    @BindView(R.id.username)
    TextView usernameTv;
    @BindView(R.id.useremail)
    EditText useremailTv;
    //    @BindView(R.id.state)
//    TextView stateTv;
    @BindView(R.id.hospital)
    TextView hospitalTv;
    @BindView(R.id.containerLayout)
    LinearLayout container;
    String type;
    private ArrayList<String> imageList = new ArrayList<>();
    private String hospital = "";
    private String vetType = "";
    private OtpModel otpModel;
    private String mLoginType;
    private String mMobile;
    private String str_firstname, str_lastname;
    private String mPassword;
    private String mEmail;
    private String mDesc;
    private String mExpertice;
    private ArrayList<String> mSateList = new ArrayList<>();
    private ArrayList<String> mLicenceList = new ArrayList<>();
    private CustomDialog mLoadingView;
    private Session mSession;
    private GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);
        type = ActivityVetJoin.checkType;
        Gson gson = new Gson();
        gps = new GPSTracker(this);
        mSession = new Session(this);
        otpModel = gson.fromJson(getIntent().getStringExtra("otp"), OtpModel.class);
        mLoginType = getIntent().getStringExtra("login_type");
        mMobile = getIntent().getStringExtra("phone");
        mEmail = getIntent().getStringExtra("email");
        mPassword = getIntent().getStringExtra("password");
        str_firstname = getIntent().getStringExtra("first_name");
        str_lastname = getIntent().getStringExtra("last_name");
        vetType = getIntent().getStringExtra("vetType");
        mExpertice = getIntent().getStringExtra("desc");
        mDesc = getIntent().getStringExtra("expertise");
        imageList = (ArrayList<String>) getIntent().getSerializableExtra("img_identification");
        hospital = getIntent().getStringExtra("primary_hospital_name");
        mSateList = (ArrayList<String>) getIntent().getSerializableExtra("state");
        mLicenceList = (ArrayList<String>) getIntent().getSerializableExtra("licence");
        usernameTv.setText(str_firstname + " " + str_lastname);
        hospitalTv.setText(hospital);
        mLoadingView = new CustomDialog(this);
        if (!TextUtils.isEmpty(mEmail)) {
            usernameTv.setFocusable(false);
            useremailTv.setText(mEmail);
        }
        addDynamicView();
    }

    private void addDynamicView() {
        for (int i = 0; i < mSateList.size(); i++) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View newRowView = inflater.inflate(R.layout.licence_view, null);
            TextView stateTv = newRowView.findViewById(R.id.state);
            stateTv.setText(mSateList.get(i) + "-" + mLicenceList.get(i));
            container.addView(newRowView, i);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_tb_iv:
                clearActivityStack();
                break;
            case R.id.blue_btn_tv:
                verify_signup();


                break;
        }
    }

    private void verify_signup() {
//        String team = spin_team.getSelectedItem().toString();
        mEmail = useremailTv.getText().toString().trim();

        if (mEmail.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
            useremailTv.requestFocus();
            useremailTv.setError("Please enter your valid email");

        } else {
            if (InternetConnection.checkConnection(this)) {
                signupUser();
            } else {
                Toast.makeText(this, "No Internet available", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void signupUser() {
        mLoadingView.show();
        double latitude = gps.getLatitude();
        double longitude = gps.getLongitude();
        String token = mSession.getToken();
        String state = "";
        String licence = "";
        if (mSateList != null && mSateList.size() > 0) {
            for (int i = 0; i < mSateList.size(); i++) {
                state = state + "," + mSateList.get(i);
                licence = licence + "," + mLicenceList.get(i);
            }
        }
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("login_type", mLoginType);
        builder.addFormDataPart("phone_no", mMobile);
        builder.addFormDataPart("email_id", mEmail);
        builder.addFormDataPart("password", mPassword);
        builder.addFormDataPart("first_name", str_firstname);
        builder.addFormDataPart("last_name", str_lastname);
        builder.addFormDataPart("location", "");
        builder.addFormDataPart("vet_type", vetType);
        builder.addFormDataPart("expertise", mExpertice);
        builder.addFormDataPart("area_description", mDesc);
        builder.addFormDataPart("primary_hospital_name", hospital);
        builder.addFormDataPart("state_issued", state);
        builder.addFormDataPart("license_number", licence);
        builder.addFormDataPart("email_verification_status", "");
        builder.addFormDataPart("notification_status", "");
        builder.addFormDataPart("devicetoken", token);
        builder.addFormDataPart("devicename", "android");
        builder.addFormDataPart("lat", "" + latitude);
        builder.addFormDataPart("long", "" + longitude);
//        File file = new File(res);
//        builder.addFormDataPart("img_identification", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
        if (imageList != null && imageList.size() > 0) {
            for (int i = 0; i < imageList.size(); i++) {
                if (imageList.get(i) != null) {
                    String model = imageList.get(i);
                    File file = new File(model);
                    builder.addFormDataPart("img_identification[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));

                }
            }
        } else {
            builder.addFormDataPart("img_identification[]", "");
        }
//        builder.addFormDataPart("flag", filePath.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), filePath));


        MultipartBody requestBody = builder.build();
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.usersignup(requestBody);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mLoadingView.hideDialog();
                if (response.body() != null) {
                    String responseData = "";
                    try {
                        responseData = response.body().string();
                        if (!TextUtils.isEmpty(responseData)) {

                            JSONObject jsonObject = new JSONObject(responseData);
                            String status = jsonObject.getString("status");
//                            String message = jsonObject.getString("message");
                            if (!TextUtils.isEmpty(status)) {
                                if (status.equalsIgnoreCase("success")) {
                                    JSONObject obj = jsonObject.getJSONObject("userinfo");
                                    String userId = obj.getString("userId");
                                    mSession.setuser_id(userId);
                                    mSession.setfirt_name(str_firstname);
                                    mSession.setlast_name(str_lastname);
                                    mSession.setuser_email(mEmail);
                                    mSession.setuser_phone(mMobile);
                                    mSession.setuser_login(true);
                                    mSession.setuser_type(vetType);
                                    mSession.setuserPaid("free");

                                    /*if ("vet".equals(vetType)) {
                                        startActivity(new Intent(ActivityYourProfileSet.this, ActivityTechnician.class));
                                    } else {
                                        startActivity(new Intent(ActivityYourProfileSet.this, ActivityVerifyEmail.class));
                                    }*/
//                                    Toast.makeText(ActivityJoinUs.this, message, Toast.LENGTH_SHORT).show();
//                                    startActivity(new Intent(Signup.this, Login.class));
//                                    finishAffinity();
                                    if ("vet".equals(vetType)) {
                                        Intent i = new Intent(ActivityYourProfileSet.this, ActivityVetDashboard.class);
                                        // set the new task and clear flags
                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(i);
                                        finish();
                                    } else {
                                        Intent i = new Intent(ActivityYourProfileSet.this, ActivityTechnician.class);
                                        // set the new task and clear flags
                                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(i);
                                        finish();
                                    }
                                 /*   Intent i = new Intent(ActivityYourProfileSet.this, ActivityEmailVerified.class);
                                    i.putExtra("type", vetType);
                                    // set the new task and clear flags
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                    finish();*/
//                                    Toast.makeText(Signup.this, message, Toast.LENGTH_SHORT).show();
//
//                                    startBillingSubscription();

                                } else {
                                    Toast.makeText(ActivityYourProfileSet.this, "Some thing went wrong", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(ActivityYourProfileSet.this, "Some thing went wrong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mLoadingView.hideDialog();
                Toast.makeText(ActivityYourProfileSet.this, "Failed to signup", Toast.LENGTH_SHORT).show();
                Log.e("cate", t.toString());
            }

        });

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {
        vetHeadingOneTv.setText("Your Profile is all set!");
        vetHeadingSecondTv.setText("Please review your information below to make sure it is all correct");
        continueTv.setText(R.string.submit);

    }

    @Override
    protected void setOnClick() {
        continueTv.setOnClickListener(this);
        vTbIv.setOnClickListener(this);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_your_profile_set;
    }
}
