package com.pongo.health.ui.pet_parent;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.pongo.health.BuildConfig;
import com.pongo.health.R;
import com.pongo.health.Session;
import com.pongo.health.api.ApiClient;
import com.pongo.health.api.ApiInterface;
import com.pongo.health.base.BaseActivity;
import com.pongo.health.model.HomeModel;

import com.pongo.health.utils.Constants;
import com.pongo.health.utils.CustomDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityYourPetSingleType extends BaseActivity {

    @BindView(R.id.s_tb_tv)
    TextView sTbTv;
    @BindView(R.id.s_tb_iv)
    ImageView sTbIv;
    @BindView(R.id.general_info_ll)
    LinearLayout generalInfoLl;
    @BindView(R.id.medical_records_cv)
    CardView medicalRecordsLl;
    @BindView(R.id.pet_layout)
    FrameLayout petLl;
    @BindView(R.id.pet_image)
    CircleImageView pet_image;
    @BindView(R.id.pet_icon)
    CircleImageView pet_icon;
    String mCurrentPhotoPath;
    String res;
    Bitmap bitmap;
    private HomeModel.PetlistBean mPetBean;
    private CustomDialog mLoadingView;
    private Session mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        mLoadingView = new CustomDialog(this);
        mSession = new Session(this);
        Gson gson = new Gson();
        mPetBean = gson.fromJson(getIntent().getStringExtra("model"), HomeModel.PetlistBean.class);
        setData();

    }

    private void setData() {
        if (null != mPetBean) {
            String image = mPetBean.getPetimage();
            String name = mPetBean.getPet_name();
            sTbTv.setText(name);
            if (!TextUtils.isEmpty(image)) {
//                Picasso.get().load(image).into(pet_icon);
//                Picasso.get().load(image).into(pet_image);
                Glide.with(this).load(image).into(pet_icon);
                Glide.with(this).load(image).into(pet_image);

            }
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        Gson gson = new Gson();
        String myJson = gson.toJson(mPetBean);
        switch (view.getId()) {
            case R.id.s_tb_iv:
                clearActivityStack();
                break;
            case R.id.pet_layout:
                if (!checkPermission()) {
                    selectImage();
                } else {
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            102);
                }
                break;
            case R.id.general_info_ll:
                intent = new Intent(this, ActivityGeneralInformationPet.class);
                intent.putExtra("model", myJson);
                startActivity(intent);
//                startActivity(new Intent(this, ActivityGeneralInformationPet.class));
                break;
            case R.id.medical_records_cv:
                intent = new Intent(this, ActivityMedicalRecords.class);
                intent.putExtra("model", myJson);
                startActivity(intent);
//                startActivity(new Intent(this, ActivityMedicalRecords.class));
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void setText() {


    }

    @Override
    protected void setOnClick() {
        sTbIv.setOnClickListener(this);
        generalInfoLl.setOnClickListener(this);
        medicalRecordsLl.setOnClickListener(this);
        petLl.setOnClickListener(this);

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_your_pet_single_type;
    }

    private boolean checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
        ) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 102) {
            if (grantResults.length > 0) {

                boolean recordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (recordAccepted) {
                    selectImage();
                }
            }
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (cameraIntent.resolveActivity(getPackageManager()) != null) {
// Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
// Error occurred while creating the File
                        }
// Continue only if the File was successfully created
                        if (photoFile != null) {
                            Uri outputFileUri = FileProvider.getUriForFile(ActivityYourPetSingleType.this, BuildConfig.APPLICATION_ID, photoFile);
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                            startActivityForResult(cameraIntent, 0);
                        }
                    }


                } else if (options[item].equals("Choose from Gallery")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 1);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:


                    if (resultCode == Activity.RESULT_OK) {

                        File file = new File(Objects.requireNonNull(mCurrentPhotoPath));
                        String fileName = Constants.compressImage(mCurrentPhotoPath, ActivityYourPetSingleType.this);
                        Bitmap bitmap = BitmapFactory.decodeFile(fileName);
                        bitmap = getResizedBitmap(bitmap, 150);
                        String path = mCurrentPhotoPath;
                        res = path;
                        pet_image.setImageBitmap(bitmap);
                        pet_icon.setImageBitmap(bitmap);
                        addPet();
                    }

                    break;

                case 1:

                    if (resultCode == RESULT_OK && data != null) {

                        Uri selectedImage = data.getData();

                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(ActivityYourPetSingleType.this.getContentResolver(), selectedImage);
                            bitmap = getPath(selectedImage);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    break;
            }
        }
    }

    private Bitmap getPath(Uri selectedImage) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Log.e("Before", "Creating cursor");
        Cursor cursor = ActivityYourPetSingleType.this.managedQuery(selectedImage, projection, null, null, null);
        Log.e("After", "Creating cursor");
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        res = cursor.getString(column_index);
        Log.e("Selected Image Path", res);
        Log.e("After", "Closing cursor");
        bitmap = BitmapFactory.decodeFile(res);
        pet_image.setImageBitmap(bitmap);
        Glide.with(this).load(res).apply(RequestOptions.circleCropTransform()).into(pet_image);
        Glide.with(this).load(res).apply(RequestOptions.circleCropTransform()).into(pet_icon);
        addPet();
        return bitmap;
    }

    private File createImageFile() throws IOException {
// Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName, // prefix
                ".jpg", // suffix
                storageDir // directory
        );

// Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();

        return image;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    private void addPet() {
        if (res != null) {

            mLoadingView.show();

            MultipartBody.Builder builder = new MultipartBody.Builder();
            builder.setType(MultipartBody.FORM);
            builder.addFormDataPart("pet_id", mPetBean.getId());
            File file = new File(res);
            builder.addFormDataPart("pet_image", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));


            MultipartBody requestBody = builder.build();
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ResponseBody> call = apiInterface.updatePetImage(requestBody);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    mLoadingView.hideDialog();
                    if (response.body() != null) {
                        String responseData = "";
                        try {
                            responseData = response.body().string();
                            if (!TextUtils.isEmpty(responseData)) {
                                JSONObject jsonObject = new JSONObject(responseData);
                                String status = jsonObject.getString("status");
//                            String message = jsonObject.getString("message");
                                if (!TextUtils.isEmpty(status)) {
                                    if (status.equalsIgnoreCase("success")) {
                                        Toast.makeText(ActivityYourPetSingleType.this, "Image Update succesfully", Toast.LENGTH_SHORT).show();

                                    }


                                } else {
                                    Toast.makeText(ActivityYourPetSingleType.this, "Failed to update image", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(ActivityYourPetSingleType.this, "Failed to update image", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    mLoadingView.hideDialog();
                    Toast.makeText(ActivityYourPetSingleType.this, "Failed to update image", Toast.LENGTH_SHORT).show();
                    Log.e("cate", t.toString());
                }

            });

        }
    }
}
